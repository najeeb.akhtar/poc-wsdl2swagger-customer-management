FROM docker-dev.global.dish.com/buildpacks/springboot/jre8u191-alpine:stable
ARG APP_VER
RUN echo $APP_VER

COPY build/libs/blueport-fiber-service.jar /tmp/blueport-fiber-service.jar

ENV BINARY_URL /tmp/blueport-fiber-service.jar
