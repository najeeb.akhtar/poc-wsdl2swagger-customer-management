package support;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Collectors;

@SuppressWarnings("Duplicates")
public class ResourceReader {
    public static String readFixture(String fileWhichExistsInResources) {
        // "/" exists because getResource calls ClassLoader which takes the path relative to classpath
        URL url = ResourceReader.class.getResource("/"+fileWhichExistsInResources);

        return getFile(fileWhichExistsInResources, url);
    }

    public static String readFixture(String fileWhichExistsInResources, Class clazz)
        throws IOException {
        URL url = ResourceReader.class.getResource("/"+fileWhichExistsInResources);
        String fileString = getFile(fileWhichExistsInResources, url);

        ObjectMapper mapper = new ObjectMapper();
        Object formattedObject = mapper.readValue(fileString, clazz);

        return mapper.writeValueAsString(formattedObject);
    }

    public static String readSchema(String fileWhichExistsInResources) {
        // "/" exists because getResource calls ClassLoader which takes the path relative to classpath
        URL url = ResourceReader.class.getResource("/schemas/"+fileWhichExistsInResources);

        return getFile(fileWhichExistsInResources, url);
    }

    private static String getFile(String fileWhichExistsInResources, URL url) {
        try {
            if (url == null) {
                throw new FileNotFoundException(fileWhichExistsInResources);
            }

            Path path = Paths.get(url.toURI());

            return Files.readAllLines(path).stream()
                .collect(Collectors.joining("\n"));
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
            return null;
        }
    }
}
