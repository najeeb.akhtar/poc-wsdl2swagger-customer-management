package support;


import org.mockserver.client.MockServerClient;
import org.mockserver.matchers.TimeToLive;
import org.mockserver.matchers.Times;
import org.mockserver.model.Header;
import org.mockserver.model.HttpError;
import org.mockserver.model.HttpRequest;
import org.mockserver.verify.VerificationTimes;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static com.dish.es.wsUtil.util.Constants.*;
import static com.dish.es.wsUtil.util.LoggingUtils.convertJsonObjectToString;
import static com.dish.es.wsUtil.util.LoggingUtils.convertJsonStringToObject;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;
import static org.mockserver.verify.VerificationTimes.exactly;
import static org.mockserver.verify.VerificationTimes.once;
import static support.ResourceReader.readFixture;

public class AcceptanceTestUtils {
    MockServerClient externalServices;

    public AcceptanceTestUtils(MockServerClient externalServices) {
        this.externalServices = externalServices;
    }

    public void verifyZeroInteraction(String httpMethod, String httpPath) {
        externalServices.verify(request()
                .withMethod(httpMethod)
                .withPath(httpPath),
            exactly(0));
    }

    public void mockExternalCall(String httpMethod, String httpPath,
        Integer returnHttpStatusCode, String responseFixturePath) {

        externalServices.when(
            request()
                .withMethod(httpMethod)
                .withPath(httpPath)
        ).respond(
            response()
                .withStatusCode(returnHttpStatusCode)
                .withHeaders(new Header("Content-Type", "application/json"))
                .withBody(readFixture(responseFixturePath))
        );
    }

    public void mockExternalCall(String httpMethod, String httpPath,
        Integer returnHttpStatusCode) {

        externalServices.when(
            request()
                .withMethod(httpMethod)
                .withPath(httpPath)
        ).respond(
            response()
                .withStatusCode(returnHttpStatusCode)
                .withHeaders(new Header("Content-Type", "application/json"))
        );
    }

    public void mockExternalTimeoutCall(String httpMethod, String httpPath,
        Integer returnHttpStatusCode, String responseFixturePath) {

        externalServices.when(
            request()
                .withMethod(httpMethod)
                .withPath(httpPath),
            Times.once(),
            TimeToLive.exactly(MILLISECONDS, Long.valueOf(1 + 10))
        ).respond(
            response()
                .withStatusCode(returnHttpStatusCode)
                .withHeaders(new Header("Content-Type", "application/json"))
                .withBody(readFixture(responseFixturePath))
        );
    }

    public void mockTimeout(String method, String url, long restTimeout) {
        externalServices.when(request()
            .withMethod(method)
            .withPath(url)
        ).error(
            HttpError.error().withDelay(
                TimeUnit.MILLISECONDS, (restTimeout + 5)).withResponseBytes("{}".getBytes()
            )
        );
    }


    public void mockDropConnection(String method, String url) {
        externalServices.when(request()
            .withMethod(method)
            .withPath(url)
        ).error(
            HttpError.error().withDropConnection(true).withResponseBytes("{}".getBytes()
            )
        );
    }

    public <T> void verifyExternalCall(
        String httpMethod,
        String httpPath,
        List<Header> headers,
        String requestFixturePath,
        Class<T> requestEntityClass) throws IOException {

        externalServices.verify(request()
                .withMethod(httpMethod)
                .withPath(httpPath)
                .withHeaders(headers),
            once()
        );

        HttpRequest[] httpRequestsRecorded = (HttpRequest[]) externalServices.retrieveRecordedRequests(request()
            .withMethod(httpMethod)
            .withPath(httpPath));

        assertThat(convertJsonStringToObject(httpRequestsRecorded[0].getBody().toString(), requestEntityClass),
            equalTo(convertJsonStringToObject(
                readFixture(requestFixturePath),
                requestEntityClass)));
    }

    public <T> void verifyExternalCallAtLeastTwice(
        String httpMethod,
        String httpPath,
        List<Header> headers,
        String requestFixturePath,
        Class<T> requestEntityClass) throws IOException {

        externalServices.verify(request()
                .withMethod(httpMethod)
                .withPath(httpPath)
                .withHeaders(headers),
            VerificationTimes.atLeast(2)
        );

        HttpRequest[] httpRequestsRecorded = (HttpRequest[]) externalServices.retrieveRecordedRequests(request()
            .withMethod(httpMethod)
            .withPath(httpPath));

        assertThat(convertJsonStringToObject(httpRequestsRecorded[0].getBody().toString(), requestEntityClass),
            equalTo(convertJsonStringToObject(
                readFixture(requestFixturePath),
                requestEntityClass)));

        assertThat(convertJsonStringToObject(httpRequestsRecorded[1].getBody().toString(), requestEntityClass),
            equalTo(convertJsonStringToObject(
                readFixture(requestFixturePath),
                requestEntityClass)));
    }

    public <T> void verifyWithRequestBody(
        String httpMethod,
        String httpPath,
        List<Header> headers,
        String requestFixturePath,
        Class<T> requestEntityClass
    ) throws IOException {

        externalServices.verify(request()
                .withMethod(httpMethod)
                .withPath(httpPath)
                .withHeaders(headers)
                .withBody(readFixture(requestFixturePath, requestEntityClass)),
            once());

        HttpRequest[] httpRequestsRecorded = (HttpRequest[]) externalServices.retrieveRecordedRequests(request()
            .withMethod(httpMethod)
            .withPath(httpPath)
            .withHeaders(headers)
            .withBody(readFixture(requestFixturePath, requestEntityClass))
        );

        assertThat(convertJsonStringToObject(httpRequestsRecorded[0].getBody().toString(), requestEntityClass),
            equalTo(convertJsonStringToObject(
                readFixture(requestFixturePath),
                requestEntityClass)));
    }

    public <T> void verifyWithRequestBodyAndZeroInteraction(
        String httpMethod,
        String httpPath,
        List<Header> headers,
        String responseFixturePath,
        Class<T> requestEntityClass) throws IOException {

        Object request = convertJsonStringToObject(readFixture(responseFixturePath), requestEntityClass);

        externalServices.verify(request()
                .withMethod(httpMethod)
                .withPath(httpPath)
                .withHeaders(headers)
                .withBody(convertJsonObjectToString(request)),
            exactly(0));


    }

    public <T> void verifyWithHeadersAndZeroInteraction(
        String httpMethod,
        String httpPath,
        List<Header> headers) {
        externalServices.verify(request()
                .withMethod(httpMethod)
                .withPath(httpPath)
                .withHeaders(headers),
            exactly(0));


    }

    public Map<String, String> createHttpHeaders(String interactionId, String requestId, String cft) {
        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        headers.put("Accept", "application/json");
        headers.put(INTERACTION_ID, interactionId);
        headers.put(REQUEST_ID, requestId);
        headers.put(CUSTOMER_FACING_TOOL, cft);
        return headers;
    }
}
