package com.dish.es.wsUtil.util;

public class Constants {
    public static final String APP_NAME = "wsdl2swagger";
    public static final String REQUEST_ID = "REQUEST_ID";
    public static final String CLIENT = "CLIENT";
    public static final String CUSTOMER_FACING_TOOL = "CUSTOMER_FACING_TOOL";
    public static final String INTERACTION_ID = "INTERACTION_ID";
    public static final String INTERACTION_STEP = "INTERACTION_STEP";
    public static final String interaction_Id = "Interaction-Id";
    public static final String request_Id = "Request-Id";
    public static final String interaction_Step = "Interaction-Step";
    public static final String customer_Facing_Tool = "Customer-Facing-Tool";
}
