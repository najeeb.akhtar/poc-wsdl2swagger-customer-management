package com.dish.es.wsUtil.util;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public class TimeService {
    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
    private final ZoneId zoneId;

    public TimeService() {
        this.zoneId = ZoneId.of("America/Denver");
    }

    public String getLocalDateTimeFormatted() {
        return LocalDateTime.now(this.zoneId).format(formatter);
    }
}
