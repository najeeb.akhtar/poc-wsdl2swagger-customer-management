package com.dish.es.wsUtil.util;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.google.common.base.Throwables;
import org.slf4j.Logger;
import org.springframework.util.StringUtils;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPMessage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;

import static java.util.Arrays.asList;
import static org.springframework.util.StringUtils.isEmpty;

public class LoggingUtils {

    private static ObjectReader objectReader = new ObjectMapper().setSerializationInclusion(JsonInclude.Include.NON_NULL).reader();
    private static ObjectWriter objectWriter = new ObjectMapper().setSerializationInclusion(JsonInclude.Include.ALWAYS).writer();

    private static final List<String> fieldsList = asList("Password:4");

    private static final String LT = "<";

    private static final String GT = ">";

    private static final String EQ = "=";

    private static final String FS = "/";

    private static final String CM = ",";

    private static final String SP = " ";

    private static final String VALUE = "value";

    private static final String NAME = "name";

    private static final String COLON = ":";

    private static final String SINGLE_QUOTE = "'";

    private static final String OPEN_BRACE = "{";

    private static final String DOUBLE_QUOTE = "\"";

    public LoggingUtils() {
    }

    public static String maskString(String fieldToMask, int unmaskedCharLength) {

        return maskString(fieldToMask, unmaskedCharLength, true, "*", 0);
    }

    public static Object maskString(Object inMessage) {
        String message;
        if (null == inMessage || null == String.valueOf(inMessage) || String.valueOf(inMessage).trim().length() == 0) {
            return inMessage;
        } else
            message = inMessage.toString();

        if (fieldsList.size() > 0) {
            for (int i = 0; i < fieldsList.size(); i++) {

                String[] maskFields = StringUtils.split(fieldsList.get(i), ":");

                String fieldName = maskFields[0];
                int fieldUnmaskedLength = Integer.valueOf(maskFields[1]);

                if (message.indexOf(fieldName) != -1) {
                    String searchParam1 = (new StringBuilder()).append(LT).append(fieldName)
                        .append(GT).toString();

                    String searchParam2 = (new StringBuilder()).append(EQ).append(fieldName)
                        .append(EQ).toString();

                    String searchParam3 = (new StringBuilder()).append(NAME).append(EQ)
                        .append(fieldName).append(CM).append(SP).append(VALUE).append(EQ)
                        .toString();

                    String searchParam4 = (new StringBuilder()).append(SP).append(fieldName)
                        .append(EQ).append(SINGLE_QUOTE).toString();

                    String searchParam5 = (new StringBuilder()).append(CM).append(SP).append(fieldName)
                        .append(EQ).toString();

                    String searchParam6 = (new StringBuilder()).append(fieldName)
                        .append(EQ).toString();

                    String searchParam7 = (new StringBuilder()).append(OPEN_BRACE).append(fieldName)
                        .append(EQ).append(SINGLE_QUOTE).toString();

                    String searchParam8 = (new StringBuilder()).append(OPEN_BRACE).append(fieldName)
                        .append(EQ).toString();

                    String searchParam9 = (new StringBuilder())
                        .append(DOUBLE_QUOTE)
                        .append(fieldName)
                        .append(DOUBLE_QUOTE)
                        .append(COLON)
                        .append(DOUBLE_QUOTE).toString();

                    String searchParam10 = (new StringBuilder())
                        .append(DOUBLE_QUOTE)
                        .append(fieldName)
                        .append(DOUBLE_QUOTE)
                        .append(COLON).toString();

                    // Independent scans and masking
                    if (message.indexOf(searchParam1) != -1) {
                        String EndString = (new StringBuilder()).append(LT).append(FS)
                            .append(fieldName).append(GT).toString();
                        message = scanAndMaskSensitiveData(message, searchParam1, EndString, fieldUnmaskedLength);
                    }
                    if (message.indexOf(searchParam2) != -1) {
                        message = scanAndMaskSensitiveData(message, searchParam2, CM, fieldUnmaskedLength);
                    }
                    if (message.indexOf(searchParam3) != -1) {
                        message = scanAndMaskSensitiveData(message, searchParam3, CM, fieldUnmaskedLength);
                    }

                    // hierarchical scan and masking
                    if (message.indexOf(searchParam7) != -1) {
                        message = scanAndMaskSensitiveData(message, searchParam7, SINGLE_QUOTE, fieldUnmaskedLength);
                    } else if (message.indexOf(searchParam8) != -1) {
                        message = scanAndMaskSensitiveData(message, searchParam8, CM, fieldUnmaskedLength);
                    } else if (message.indexOf(searchParam4) != -1) {
                        message = scanAndMaskSensitiveData(message, searchParam4, SINGLE_QUOTE, fieldUnmaskedLength);
                    } else if (message.indexOf(searchParam5) != -1) {
                        message = scanAndMaskSensitiveData(message, searchParam5, CM, fieldUnmaskedLength);
                    } else if (message.indexOf(searchParam6) != -1) {
                        message = scanAndMaskSensitiveData(message, searchParam6, CM, fieldUnmaskedLength);
                    } else if (message.indexOf(searchParam9) != -1) {
                        message = scanAndMaskSensitiveData(message, searchParam9, DOUBLE_QUOTE, fieldUnmaskedLength);
                    } else if (message.indexOf(searchParam10) != -1) {
                        message = scanAndMaskSensitiveData(message, searchParam10, CM, fieldUnmaskedLength);
                    }
                }
            }
        }
        return message;
    }

    private static String scanAndMaskSensitiveData(String message, String searchStr, String endStr, int fieldUnmaskedLength) {
        StringBuilder replyBuffer = new StringBuilder();
        while (message.indexOf(searchStr) > 0) {

            replyBuffer.append(message.substring(0,
                message.indexOf(searchStr) + searchStr.length()));

            int EndStringIndex = message.indexOf(endStr,
                message.indexOf(searchStr) + searchStr.length());

            String value;

            if (EndStringIndex > -1) {
                value = message.substring(message.indexOf(searchStr) + searchStr.length(),
                    EndStringIndex);

                if (!isEmpty(value) && value.indexOf("}") > 0) {
                    value = value.substring(0, value.indexOf("}"));

                    EndStringIndex = message.indexOf("}",
                        message.indexOf(searchStr) + searchStr.length());
                }

            } else {
                value = message.substring(message.indexOf(searchStr) + searchStr.length());
            }

            replyBuffer.append(
                maskString(value, fieldUnmaskedLength, true, "*", 0));

            if (EndStringIndex > -1) {
                message = message.substring(EndStringIndex);
            } else {
                message = "";
            }
        }
        return replyBuffer.append(message).toString();
    }

    public static String maskString(String value, int unmaskedCharLength, boolean leftPaded,
        String padChar, int defaultMaskLength) {
        StringBuilder maskedValue = new StringBuilder();
        if (value.length() >= unmaskedCharLength) {
            if (leftPaded) {
                for (int k = 0; k < value.length() - unmaskedCharLength; k++) {
                    maskedValue.append(padChar);
                }

                maskedValue.append(value.substring(value.length() - unmaskedCharLength));
            } else {
                maskedValue.append(value.substring(0, unmaskedCharLength));
                for (int l = 0; l < defaultMaskLength; l++) {
                    maskedValue.append(padChar);
                }

            }
        }
        return maskedValue.toString();
    }

    public static String convertObjectToJson(Object object, Logger logger, String appName) {
        try {
            return (object != null) ? objectWriter.writeValueAsString(object) : null;
        } catch (JsonProcessingException e) {
            logger.info("Error while converting " + appName + "'s Request/Response object to JSON", Throwables.getStackTraceAsString(e));
            return object.toString();
        }
    }

    public static <T> T convertJsonStringToObject(String json, Class<T> clazz)
        throws IOException {
        return objectReader.forType(clazz).readValue(json);
    }

    public static String convertJsonObjectToString(Object object) throws IOException {
        com.fasterxml.jackson.databind.ObjectMapper mapper = new com.fasterxml.jackson.databind.ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        return mapper.writeValueAsString(object);
    }

    public static String convertJsonObjectToStringWithNulls(Object object) throws IOException {
        com.fasterxml.jackson.databind.ObjectMapper mapper = new com.fasterxml.jackson.databind.ObjectMapper();
        return mapper.writeValueAsString(object);
    }

    public static Object convertSoapStringToObject(String soap, Class clazz) throws Exception {
        SOAPMessage message = MessageFactory.newInstance().createMessage(null,
            new ByteArrayInputStream(soap.getBytes()));
        Unmarshaller unmarshaller = JAXBContext.newInstance(clazz).createUnmarshaller();

        return unmarshaller.unmarshal(message.getSOAPBody().extractContentAsDocument());
    }
}


