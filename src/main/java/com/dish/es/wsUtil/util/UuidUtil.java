package com.dish.es.wsUtil.util;

import java.util.UUID;

public class UuidUtil {

    public String generate() {
        return UUID.randomUUID().toString();
    }

}
