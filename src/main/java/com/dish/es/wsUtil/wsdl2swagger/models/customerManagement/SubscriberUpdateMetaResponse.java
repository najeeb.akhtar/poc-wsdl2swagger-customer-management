package com.dish.es.wsUtil.wsdl2swagger.models.customerManagement;

import com.dish.es.wsUtil.wsdl2swagger.models.common.Status;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import dishnetwork.schemas.SubscriberUpdateMetaOutput;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(description = "response")
public class SubscriberUpdateMetaResponse {
    protected Status status;
    @JsonProperty("subscriberUpdateMetaOutput")
    protected SubscriberUpdateMetaOutput subscriberUpdateMetaOutput;
}
