package com.dish.es.wsUtil.wsdl2swagger.service;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class WsdlToSwaggerService {

    public ResponseEntity post() {
        return new ResponseEntity(HttpStatus.OK);
    }
}
