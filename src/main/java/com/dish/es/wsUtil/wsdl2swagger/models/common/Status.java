package com.dish.es.wsUtil.wsdl2swagger.models.common;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(description = "response context")
public class Status {
    protected String interactionId;
    protected String requestId;
    protected String requestReceivedTimestamp;
    protected String responseGeneratedTimestamp;
    protected String serverHostPort;
    protected String serviceDuration;
    protected String statusCode;
    protected String statusMessage;
    protected String displayMessage;
}
