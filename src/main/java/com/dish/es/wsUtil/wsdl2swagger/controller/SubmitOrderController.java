package com.dish.es.wsUtil.wsdl2swagger.controller;

import com.dish.es.wsUtil.wsdl2swagger.models.common.ErrorResponse;
import com.dish.es.wsUtil.wsdl2swagger.models.customerManagement.SubmitOrderResponse;
import dishnetwork.schemas.SubmitOrderInputType;
import io.swagger.annotations.*;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static com.dish.es.wsUtil.wsdl2swagger.util.Constants.*;
import static org.springframework.http.HttpStatus.OK;

@CrossOrigin
@RestController
@Api(tags = {"CustomerManagement"}, value = "/wsdl2Swagger", description = "CustomerManagement Controller - REST resources with respect to SOAP resources")
public class SubmitOrderController {

    @RequestMapping(method = RequestMethod.POST, path = "/v1/submitOrder",
        consumes = {"application/json"}, produces = {"application/json"})
    @ApiOperation(
        tags = {"CustomerManagement"},
        value = "Submit Order",
        notes = "TBD",
        httpMethod = "POST",
        produces = "application/json",
        consumes = "application/json",
        nickname = "submitOrder"
    )
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK", response = SubmitOrderResponse.class),
        @ApiResponse(code = 400, message = "Bad Request", response = ErrorResponse.class),
        @ApiResponse(code = 500, message = "Internal Server Error", response = ErrorResponse.class)
    })
    @ResponseStatus(OK)
    public ResponseEntity<SubmitOrderResponse> submitOrder(
        @RequestHeader HttpHeaders headers,
        @ApiParam(value = "unique id for each interaction e.g. da47dc-4eca-a953", required = true, example = "da47dc-4eca-a953")
        @RequestHeader(value = Interaction_Id) String interactionId,
        @ApiParam(value = "unique id for each request during interaction da47dc-4eca-a953 e.g. 1,2,3,4", required = true, example = "5423")
        @RequestHeader(value = Request_Id) String requestId,
        @ApiParam(value = "customer facing tool e.g. AXIOM", required = true, example = "AXIOM")
        @RequestHeader(value = Customer_Facing_Tool) String customerFacingTool,
        @ApiParam(value = "Text identifying the frontend flow ex: TECH_UPSALE", required = false, example = "")
        @RequestHeader(value = Interaction_Process_Name, required = false) String interactionProcessName,
        @ApiParam(value = "Text identifying step in the frontend flow ex: PAYMENT_FINANCING", required = false, example = "")
        @RequestHeader(value = Interaction_Step, required = false) String interactionStep,
        @ApiParam(value = "Sales channel ex: DIRECT, RETAIL, PARTNER", required = false, example = "")
        @RequestHeader(value = Acquisition_Channel, required = false) String acquisitionChannel,
        @ApiParam(value = "Billing system operator id of direct agents", required = false, example = "")
        @RequestHeader(value = Operator_Id, required = false) String operatorId,
        @ApiParam(value = "Agent login type. Use only one of these: RWEB_LOGIN, DP_LOGIN, CSAWEB_LOGIN, PARTNER_LOGIN, ALT_REP_ID etc", required = false, example = "")
        @RequestHeader(value = Agent_Id_Type, required = false) String agentIdType,
        @ApiParam(value = "Agent login id.", required = false, example = "")
        @RequestHeader(value = Agent_Id, required = false) String agentId,
        @ApiParam(value = "Id type of selling company. Use only one of these: OE_NUMBER, SELLER_ID, SALES_ID", required = false, example = "")
        @RequestHeader(value = Seller_Id_Type, required = false) String sellerIdType,
        @ApiParam(value = "Type of seller. Use only one of these: DIRECT, RETAIL, PARTNER", required = false, example = "")
        @RequestHeader(value = Seller_Type, required = false) String sellerType,
        @ApiParam(value = "Id of the selling company", required = false, example = "")
        @RequestHeader(value = Seller_Id, required = false) String sellerId,
        @ApiParam(value = "Name of the seller. ex: DISH", required = false, example = "")
        @RequestHeader(value = Seller_Name, required = false) String sellerName,
        @ApiParam(value = "host/IP address of client server", required = false, example = "")
        @RequestHeader(value = Client_App_Host, required = false) String clientAppHost,
        @ApiParam(value = "Date and time when request was sent", required = false, example = "")
        @RequestHeader(value = Client_App_Timestamp, required = false) String clientAppTimestamp,
        @ApiParam(value = "IP address of end user", required = false, example = "")
        @RequestHeader(value = End_User_Ip_Address, required = false) String endUserIpAddress,
        @ApiParam(value = "Submit order request", required = true)
        @RequestBody SubmitOrderInputType submitOrderInput
    ) {
        return null;
    }
}
