package com.dish.es.wsUtil.wsdl2swagger.util;

public class Constants {
    public static final String CUSTOMER_FACING_TOOL = "CUSTOMER_FACING_TOOL";
    public static final String INTERACTION_ID = "INTERACTION_ID";
    public static final String REQUEST_ID = "REQUEST_ID";
    public static final String Customer_Facing_Tool = "Customer-Facing-Tool";
    public static final String Interaction_Id = "Interaction-Id";
    public static final String Request_Id = "Request-Id";
    public static final String Interaction_Process_Name = "Interaction-Process-Name";
    public static final String Interaction_Step = "Interaction-Step";
    public static final String Acquisition_Channel = "Acquisition-Channel";
    public static final String Operator_Id = "Operator-Id";
    public static final String Agent_Id_Type = "Agent-Id-Type";
    public static final String Agent_Id = "Agent-Id";
    public static final String Seller_Id_Type = "Seller-Id-Type";
    public static final String Seller_Type = "Seller-Type";
    public static final String Seller_Id = "Seller-Id";
    public static final String Seller_Name = "Seller-Name";
    public static final String Client_App_Host = "Client-App-Host";
    public static final String Client_App_Timestamp = "Client-App-Timestamp";
    public static final String End_User_Ip_Address = "End-User-Ip-Address";
}
