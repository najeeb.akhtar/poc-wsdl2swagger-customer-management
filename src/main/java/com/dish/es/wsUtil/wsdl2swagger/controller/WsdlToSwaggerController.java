package com.dish.es.wsUtil.wsdl2swagger.controller;

import com.dish.es.wsUtil.wsdl2swagger.models.wsdlToSwagger.WsdlToSwaggerRequest;
import com.dish.es.wsUtil.wsdl2swagger.models.wsdlToSwagger.WsdlToSwaggerResponse;
import com.dish.es.wsUtil.wsdl2swagger.service.WsdlToSwaggerService;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static com.dish.es.wsUtil.wsdl2swagger.util.Constants.CUSTOMER_FACING_TOOL;
import static com.dish.es.wsUtil.wsdl2swagger.util.Constants.INTERACTION_ID;
import static org.springframework.http.HttpStatus.OK;

//@CrossOrigin
//@RestController
@Api(tags = {"1 WsdlToSwagger"}, value = "/wsdlToSwagger", description = "Wsdl To Swagger")
public class WsdlToSwaggerController {

    private static final Logger LOGGER = LoggerFactory.getLogger(WsdlToSwaggerController.class);
    private final WsdlToSwaggerService wsdlToSwaggerService;

    @Autowired
    public WsdlToSwaggerController(WsdlToSwaggerService wsdlToSwaggerService) {
        this.wsdlToSwaggerService = wsdlToSwaggerService;
    }

    @RequestMapping(method = RequestMethod.POST, path = "/wsdlToSwagger",
        consumes = {"application/json"}, produces = {"application/json"})
    @ApiOperation(
        tags = {"1 WsdlToSwagger"},
        value = "",
        httpMethod = "POST",
        produces = "application/json",
        consumes = "application/json"
    )
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK", response = WsdlToSwaggerResponse.class)})
    @ResponseStatus(OK)
    public ResponseEntity post(
        @RequestHeader HttpHeaders headers,
        @ApiParam(value = "unique id for each interaction e.g. xeb588729ed", required = true, example = "xeb588729ed")
        @RequestHeader(value = INTERACTION_ID) String interactionId,
        @ApiParam(value = "customer facing tool e.g. AXIOM", required = true, example = "AXIOM")
        @RequestHeader(value = CUSTOMER_FACING_TOOL) String customerFacingTool,
        @ApiParam(value = "consumer personally identifiable information request", required = true)
        @RequestBody WsdlToSwaggerRequest wsdlToSwaggerRequest
    ) {
        LOGGER.info("Received POST /wsdlToSwagger with Request= {}");

        ResponseEntity responseEntity = wsdlToSwaggerService.post();

        return responseEntity;
    }

}
