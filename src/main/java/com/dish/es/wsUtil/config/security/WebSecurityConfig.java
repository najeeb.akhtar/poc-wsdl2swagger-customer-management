package com.dish.es.wsUtil.config.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import java.util.List;

@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private final List<String> allowedHttpMethods;

    @Autowired
    public WebSecurityConfig(List<String> allowedHttpMethods) {
        this.allowedHttpMethods = allowedHttpMethods;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .csrf()
            .disable()
            .httpBasic()
            .disable()
            .formLogin()
            .disable();
    }

    @Override
    public final void configure(final WebSecurity web) throws Exception {
        super.configure(web);
        web.httpFirewall(new LoggingHttpFirewall(allowedHttpMethods)); // Set the custom firewall.
        return;
    }
}
