package com.dish.es.wsUtil.config;

import com.dish.es.wsUtil.util.TimeService;
import com.dish.es.wsUtil.util.UuidUtil;
import org.mockito.Mock;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.RestTemplate;

import static java.time.Duration.ofMillis;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

@Configuration
public class ApplicationConfig {

    private RestTemplate getRestTemplate(RestTemplateBuilder restTemplateBuilder, int restTimeout) {
        RestTemplate restTemplate = restTemplateBuilder
            .setConnectTimeout(ofMillis(restTimeout))
            .setReadTimeout(ofMillis(restTimeout))
            .build();

        restTemplate.setErrorHandler(new DefaultResponseErrorHandler() {
            protected boolean hasError(HttpStatus statusCode) {
                return false;
            }
        });
        return restTemplate;
    }

    @Bean
    public RestTemplateBuilder restTemplateBuilder() {
        return new RestTemplateBuilder();
    }

    @Bean
    @Profile({"!ci && !ci-timeout"})
    public TimeService timeService() {
        return new TimeService();
    }

    @Bean
    @Profile({"!ci && !ci-timeout"})
    public UuidUtil uuidUtil() {
        return new UuidUtil();
    }

    @Configuration
    @Profile({"ci", "ci-timeout"})
    class CiTestConfiguration {

        @Mock
        private TimeService timeService;

        @Mock
        private UuidUtil uuidUtil;

        public CiTestConfiguration() {
            initMocks(this);
            when(timeService.getLocalDateTimeFormatted()).thenReturn("2021-07-30T14:15:16.001Z");
            when(uuidUtil.generate()).thenReturn("9f0d0132-6037-4494-af7f-468e7cf5f7bb");
        }

        @Bean
        public TimeService timeService() {
            return timeService;
        }

        @Bean
        public UuidUtil uuidUtil() {
            return uuidUtil;
        }

    }
}
