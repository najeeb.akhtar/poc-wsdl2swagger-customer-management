package com.dish.es.wsUtil.config.security;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.CollectionUtils;

import java.util.List;


@Configuration
@ConfigurationProperties(prefix = "http.methods")
@Data
@NoArgsConstructor
public class AllowedHttpMethodsConfig {
    private List<String> allowedHttpMethods;

    @Bean
    public List<String> allowedHttpMethods() throws BeanCreationException {
        if (CollectionUtils.isEmpty(allowedHttpMethods)) {
            throw new BeanCreationException("allowedHttpMethods",
                "application config validation failed, check configuration http.methods.allowed-http-methods");
        }
        return allowedHttpMethods;
    }
}
