package com.dish.es.wsUtil.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * Home redirection to swagger api documentation
 */
@Controller
public class HomeController {
    @RequestMapping(value = "/", path = "/", method = GET)
    public String index(@Value("${application.homePageUrl}") String homePageUrl) {
        return String.format("%s:%s%s", "redirect", homePageUrl, "swagger-ui.html");
    }
}
