package com.dish.es.wsUtil.config.security;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.firewall.RequestRejectedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.stream.Collectors;

import static javax.servlet.http.HttpServletResponse.SC_METHOD_NOT_ALLOWED;

@Aspect
@ControllerAdvice
public class FilterChainProxyAdvice extends ResponseEntityExceptionHandler {

    private final String allowedHttpMethods;

    @Autowired
    public FilterChainProxyAdvice(List<String> allowedHttpMethods) {
        this.allowedHttpMethods = allowedHttpMethods.stream().collect(Collectors.joining(","));
    }

    @Around("execution(public void org.springframework.security.web.FilterChainProxy.doFilter(..))")
    public void handleRequestRejectedException(ProceedingJoinPoint pjp) throws Throwable {
        try {
            pjp.proceed();
        } catch (RequestRejectedException exception) {
            HttpServletResponse response = ((HttpServletResponse) pjp.getArgs()[1]);
            response.setHeader("Allow", allowedHttpMethods);
            response.setStatus(SC_METHOD_NOT_ALLOWED);
        }
    }
}

