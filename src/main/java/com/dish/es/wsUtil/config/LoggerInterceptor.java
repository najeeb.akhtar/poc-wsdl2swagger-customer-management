package com.dish.es.wsUtil.config;

import com.dish.es.wsUtil.util.Constants;
import org.slf4j.MDC;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LoggerInterceptor extends HandlerInterceptorAdapter {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws IllegalArgumentException {
        MDC.put(Constants.INTERACTION_ID, request.getHeader(Constants.INTERACTION_ID));
        MDC.put(Constants.REQUEST_ID, request.getHeader(Constants.REQUEST_ID));
        MDC.put(Constants.CUSTOMER_FACING_TOOL, request.getHeader(Constants.CUSTOMER_FACING_TOOL));
        MDC.put("URI", request.getRequestURI());
        if (handler instanceof HandlerMethod) {
            HandlerMethod handlerMethod = (HandlerMethod) handler;
            MDC.put("controllerFlowName", String.format("%s.%s", handlerMethod.getBeanType().getSimpleName(), handlerMethod.getMethod().getName()));
        }
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws IllegalArgumentException {
        MDC.clear();
    }
}
