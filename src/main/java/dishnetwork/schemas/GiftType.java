
package dishnetwork.schemas;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.jvnet.jaxb2_commons.lang.Equals2;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy2;
import org.jvnet.jaxb2_commons.lang.HashCode2;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy2;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString2;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy2;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for GiftType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="GiftType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="giftDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="type" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="fulfilmentVendor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="fulfilmentPhoneNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="giftOptionValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GiftType", propOrder = {
    "id",
    "giftDescription",
    "type",
    "fulfilmentVendor",
    "fulfilmentPhoneNumber",
    "giftOptionValue"
})
@Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
public class GiftType implements Equals2, HashCode2, ToString2
{

    //@XmlElement(required = true)
    @JsonProperty(required = true)
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String id;
    //@XmlElement(nillable = true)
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String giftDescription;
    //@XmlElement(nillable = true)
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String type;
    //@XmlElement(nillable = true)
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String fulfilmentVendor;
    //@XmlElement(nillable = true)
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String fulfilmentPhoneNumber;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String giftOptionValue;

    /**
     * Default no-arg constructor
     *
     */
    public GiftType() {
        super();
    }

    /**
     * Fully-initialising value constructor
     *
     */
    public GiftType(final String id, final String giftDescription, final String type, final String fulfilmentVendor, final String fulfilmentPhoneNumber, final String giftOptionValue) {
        this.id = id;
        this.giftDescription = giftDescription;
        this.type = type;
        this.fulfilmentVendor = fulfilmentVendor;
        this.fulfilmentPhoneNumber = fulfilmentPhoneNumber;
        this.giftOptionValue = giftOptionValue;
    }

    /**
     * Gets the value of the id property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the giftDescription property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getGiftDescription() {
        return giftDescription;
    }

    /**
     * Sets the value of the giftDescription property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setGiftDescription(String value) {
        this.giftDescription = value;
    }

    /**
     * Gets the value of the type property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setType(String value) {
        this.type = value;
    }

    /**
     * Gets the value of the fulfilmentVendor property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getFulfilmentVendor() {
        return fulfilmentVendor;
    }

    /**
     * Sets the value of the fulfilmentVendor property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setFulfilmentVendor(String value) {
        this.fulfilmentVendor = value;
    }

    /**
     * Gets the value of the fulfilmentPhoneNumber property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getFulfilmentPhoneNumber() {
        return fulfilmentPhoneNumber;
    }

    /**
     * Sets the value of the fulfilmentPhoneNumber property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setFulfilmentPhoneNumber(String value) {
        this.fulfilmentPhoneNumber = value;
    }

    /**
     * Gets the value of the giftOptionValue property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getGiftOptionValue() {
        return giftOptionValue;
    }

    /**
     * Sets the value of the giftOptionValue property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setGiftOptionValue(String value) {
        this.giftOptionValue = value;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy2 strategy) {
        if ((object == null)||(this.getClass()!= object.getClass())) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final GiftType that = ((GiftType) object);
        {
            String lhsId;
            lhsId = this.getId();
            String rhsId;
            rhsId = that.getId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "id", lhsId), LocatorUtils.property(thatLocator, "id", rhsId), lhsId, rhsId, (this.id!= null), (that.id!= null))) {
                return false;
            }
        }
        {
            String lhsGiftDescription;
            lhsGiftDescription = this.getGiftDescription();
            String rhsGiftDescription;
            rhsGiftDescription = that.getGiftDescription();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "giftDescription", lhsGiftDescription), LocatorUtils.property(thatLocator, "giftDescription", rhsGiftDescription), lhsGiftDescription, rhsGiftDescription, (this.giftDescription!= null), (that.giftDescription!= null))) {
                return false;
            }
        }
        {
            String lhsType;
            lhsType = this.getType();
            String rhsType;
            rhsType = that.getType();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "type", lhsType), LocatorUtils.property(thatLocator, "type", rhsType), lhsType, rhsType, (this.type!= null), (that.type!= null))) {
                return false;
            }
        }
        {
            String lhsFulfilmentVendor;
            lhsFulfilmentVendor = this.getFulfilmentVendor();
            String rhsFulfilmentVendor;
            rhsFulfilmentVendor = that.getFulfilmentVendor();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "fulfilmentVendor", lhsFulfilmentVendor), LocatorUtils.property(thatLocator, "fulfilmentVendor", rhsFulfilmentVendor), lhsFulfilmentVendor, rhsFulfilmentVendor, (this.fulfilmentVendor!= null), (that.fulfilmentVendor!= null))) {
                return false;
            }
        }
        {
            String lhsFulfilmentPhoneNumber;
            lhsFulfilmentPhoneNumber = this.getFulfilmentPhoneNumber();
            String rhsFulfilmentPhoneNumber;
            rhsFulfilmentPhoneNumber = that.getFulfilmentPhoneNumber();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "fulfilmentPhoneNumber", lhsFulfilmentPhoneNumber), LocatorUtils.property(thatLocator, "fulfilmentPhoneNumber", rhsFulfilmentPhoneNumber), lhsFulfilmentPhoneNumber, rhsFulfilmentPhoneNumber, (this.fulfilmentPhoneNumber!= null), (that.fulfilmentPhoneNumber!= null))) {
                return false;
            }
        }
        {
            String lhsGiftOptionValue;
            lhsGiftOptionValue = this.getGiftOptionValue();
            String rhsGiftOptionValue;
            rhsGiftOptionValue = that.getGiftOptionValue();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "giftOptionValue", lhsGiftOptionValue), LocatorUtils.property(thatLocator, "giftOptionValue", rhsGiftOptionValue), lhsGiftOptionValue, rhsGiftOptionValue, (this.giftOptionValue!= null), (that.giftOptionValue!= null))) {
                return false;
            }
        }
        return true;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(Object object) {
        final EqualsStrategy2 strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode(ObjectLocator locator, HashCodeStrategy2 strategy) {
        int currentHashCode = 1;
        {
            String theId;
            theId = this.getId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "id", theId), currentHashCode, theId, (this.id!= null));
        }
        {
            String theGiftDescription;
            theGiftDescription = this.getGiftDescription();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "giftDescription", theGiftDescription), currentHashCode, theGiftDescription, (this.giftDescription!= null));
        }
        {
            String theType;
            theType = this.getType();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "type", theType), currentHashCode, theType, (this.type!= null));
        }
        {
            String theFulfilmentVendor;
            theFulfilmentVendor = this.getFulfilmentVendor();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "fulfilmentVendor", theFulfilmentVendor), currentHashCode, theFulfilmentVendor, (this.fulfilmentVendor!= null));
        }
        {
            String theFulfilmentPhoneNumber;
            theFulfilmentPhoneNumber = this.getFulfilmentPhoneNumber();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "fulfilmentPhoneNumber", theFulfilmentPhoneNumber), currentHashCode, theFulfilmentPhoneNumber, (this.fulfilmentPhoneNumber!= null));
        }
        {
            String theGiftOptionValue;
            theGiftOptionValue = this.getGiftOptionValue();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "giftOptionValue", theGiftOptionValue), currentHashCode, theGiftOptionValue, (this.giftOptionValue!= null));
        }
        return currentHashCode;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode() {
        final HashCodeStrategy2 strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String toString() {
        final ToStringStrategy2 strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        {
            String theId;
            theId = this.getId();
            strategy.appendField(locator, this, "id", buffer, theId, (this.id!= null));
        }
        {
            String theGiftDescription;
            theGiftDescription = this.getGiftDescription();
            strategy.appendField(locator, this, "giftDescription", buffer, theGiftDescription, (this.giftDescription!= null));
        }
        {
            String theType;
            theType = this.getType();
            strategy.appendField(locator, this, "type", buffer, theType, (this.type!= null));
        }
        {
            String theFulfilmentVendor;
            theFulfilmentVendor = this.getFulfilmentVendor();
            strategy.appendField(locator, this, "fulfilmentVendor", buffer, theFulfilmentVendor, (this.fulfilmentVendor!= null));
        }
        {
            String theFulfilmentPhoneNumber;
            theFulfilmentPhoneNumber = this.getFulfilmentPhoneNumber();
            strategy.appendField(locator, this, "fulfilmentPhoneNumber", buffer, theFulfilmentPhoneNumber, (this.fulfilmentPhoneNumber!= null));
        }
        {
            String theGiftOptionValue;
            theGiftOptionValue = this.getGiftOptionValue();
            strategy.appendField(locator, this, "giftOptionValue", buffer, theGiftOptionValue, (this.giftOptionValue!= null));
        }
        return buffer;
    }

}
