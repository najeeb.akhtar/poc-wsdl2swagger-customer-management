
package dishnetwork.schemas;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.jvnet.jaxb2_commons.lang.Equals2;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy2;
import org.jvnet.jaxb2_commons.lang.HashCode2;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy2;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString2;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy2;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * The AuthorizedUser
 *
 * <p>Java class for AuthorizedUserType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="AuthorizedUserType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="name" type="{http://www.dishnetwork.com/schema/Party/IndividualName/2010_12_12}IndividualNameType"/&gt;
 *         &lt;element name="authLevel" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="authUserId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AuthorizedUserType", propOrder = {
    "name",
    "authLevel",
    "authUserId"
})
@Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
public class AuthorizedUserType implements Equals2, HashCode2, ToString2
{

    //@XmlElement(required = true)
    @JsonProperty(required = true)
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected IndividualNameType name;
    //@XmlElement(required = true)
    @JsonProperty(required = true)
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String authLevel;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String authUserId;

    /**
     * Default no-arg constructor
     *
     */
    public AuthorizedUserType() {
        super();
    }

    /**
     * Fully-initialising value constructor
     *
     */
    public AuthorizedUserType(final IndividualNameType name, final String authLevel, final String authUserId) {
        this.name = name;
        this.authLevel = authLevel;
        this.authUserId = authUserId;
    }

    /**
     * Gets the value of the name property.
     *
     * @return
     *     possible object is
     *     {@link IndividualNameType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public IndividualNameType getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     *
     * @param value
     *     allowed object is
     *     {@link IndividualNameType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setName(IndividualNameType value) {
        this.name = value;
    }

    /**
     * Gets the value of the authLevel property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getAuthLevel() {
        return authLevel;
    }

    /**
     * Sets the value of the authLevel property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setAuthLevel(String value) {
        this.authLevel = value;
    }

    /**
     * Gets the value of the authUserId property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getAuthUserId() {
        return authUserId;
    }

    /**
     * Sets the value of the authUserId property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setAuthUserId(String value) {
        this.authUserId = value;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy2 strategy) {
        if ((object == null)||(this.getClass()!= object.getClass())) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final AuthorizedUserType that = ((AuthorizedUserType) object);
        {
            IndividualNameType lhsName;
            lhsName = this.getName();
            IndividualNameType rhsName;
            rhsName = that.getName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "name", lhsName), LocatorUtils.property(thatLocator, "name", rhsName), lhsName, rhsName, (this.name!= null), (that.name!= null))) {
                return false;
            }
        }
        {
            String lhsAuthLevel;
            lhsAuthLevel = this.getAuthLevel();
            String rhsAuthLevel;
            rhsAuthLevel = that.getAuthLevel();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "authLevel", lhsAuthLevel), LocatorUtils.property(thatLocator, "authLevel", rhsAuthLevel), lhsAuthLevel, rhsAuthLevel, (this.authLevel!= null), (that.authLevel!= null))) {
                return false;
            }
        }
        {
            String lhsAuthUserId;
            lhsAuthUserId = this.getAuthUserId();
            String rhsAuthUserId;
            rhsAuthUserId = that.getAuthUserId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "authUserId", lhsAuthUserId), LocatorUtils.property(thatLocator, "authUserId", rhsAuthUserId), lhsAuthUserId, rhsAuthUserId, (this.authUserId!= null), (that.authUserId!= null))) {
                return false;
            }
        }
        return true;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(Object object) {
        final EqualsStrategy2 strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode(ObjectLocator locator, HashCodeStrategy2 strategy) {
        int currentHashCode = 1;
        {
            IndividualNameType theName;
            theName = this.getName();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "name", theName), currentHashCode, theName, (this.name!= null));
        }
        {
            String theAuthLevel;
            theAuthLevel = this.getAuthLevel();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "authLevel", theAuthLevel), currentHashCode, theAuthLevel, (this.authLevel!= null));
        }
        {
            String theAuthUserId;
            theAuthUserId = this.getAuthUserId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "authUserId", theAuthUserId), currentHashCode, theAuthUserId, (this.authUserId!= null));
        }
        return currentHashCode;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode() {
        final HashCodeStrategy2 strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String toString() {
        final ToStringStrategy2 strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        {
            IndividualNameType theName;
            theName = this.getName();
            strategy.appendField(locator, this, "name", buffer, theName, (this.name!= null));
        }
        {
            String theAuthLevel;
            theAuthLevel = this.getAuthLevel();
            strategy.appendField(locator, this, "authLevel", buffer, theAuthLevel, (this.authLevel!= null));
        }
        {
            String theAuthUserId;
            theAuthUserId = this.getAuthUserId();
            strategy.appendField(locator, this, "authUserId", buffer, theAuthUserId, (this.authUserId!= null));
        }
        return buffer;
    }

}
