
package dishnetwork.schemas;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.jvnet.jaxb2_commons.lang.Equals2;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy2;
import org.jvnet.jaxb2_commons.lang.HashCode2;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy2;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString2;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy2;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * Contains Order information
 *
 * <p>Java class for OrderType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="OrderType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Id" type="{http://www.dishnetwork.com/schema/AccountOrder/OrderId/2011_04_01}OrderIdType" minOccurs="0"/&gt;
 *         &lt;element name="status" type="{http://www.dishnetwork.com/schema/AccountOrder/OrderStatus/2011_04_01}OrderStatusType" minOccurs="0"/&gt;
 *         &lt;element name="type" type="{http://www.dishnetwork.com/schema/AccountOrder/OrderType/2011_04_01}OrderTypeType" minOccurs="0"/&gt;
 *         &lt;element name="class" type="{http://www.dishnetwork.com/schema/AccountOrder/OrderClass/2011_04_01}OrderClassType" minOccurs="0"/&gt;
 *         &lt;element name="billing" type="{http://www.dishnetwork.com/schema/Billing/Billing/2011_04_01}BillingType" minOccurs="0"/&gt;
 *         &lt;element name="campaign" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="commentCode" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="commentText" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="reasonCode" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="reasolutionCode" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="salesRepresentative" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="scheduleList" type="{http://www.dishnetwork.com/schema/Schedule/ScheduleList/2015_01_15}ScheduleListType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrderType", propOrder = {
    "id",
    "status",
    "type",
    "clazz",
    "billing",
    "campaign",
    "commentCode",
    "commentText",
    "reasonCode",
    "reasolutionCode",
    "salesRepresentative",
    "scheduleList"
})
@Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
public class OrderType implements Equals2, HashCode2, ToString2
{

    //@XmlElement(name = "Id")
    @JsonProperty(value = "Id")
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected OrderIdType id;
    @XmlSchemaType(name = "string")
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected OrderStatusType status;
    @XmlSchemaType(name = "string")
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected OrderTypeType type;
    //@XmlElement(name = "class")
    @JsonProperty(value = "class")
    @XmlSchemaType(name = "string")
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected OrderClassType clazz;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected BillingType billing;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String campaign;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected List<String> commentCode;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected List<String> commentText;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected List<String> reasonCode;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected List<String> reasolutionCode;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String salesRepresentative;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected ScheduleListType scheduleList;

    /**
     * Default no-arg constructor
     *
     */
    public OrderType() {
        super();
    }

    /**
     * Fully-initialising value constructor
     *
     */
    public OrderType(final OrderIdType id, final OrderStatusType status, final OrderTypeType type, final OrderClassType clazz, final BillingType billing, final String campaign, final List<String> commentCode, final List<String> commentText, final List<String> reasonCode, final List<String> reasolutionCode, final String salesRepresentative, final ScheduleListType scheduleList) {
        this.id = id;
        this.status = status;
        this.type = type;
        this.clazz = clazz;
        this.billing = billing;
        this.campaign = campaign;
        this.commentCode = commentCode;
        this.commentText = commentText;
        this.reasonCode = reasonCode;
        this.reasolutionCode = reasolutionCode;
        this.salesRepresentative = salesRepresentative;
        this.scheduleList = scheduleList;
    }

    /**
     * Gets the value of the id property.
     *
     * @return
     *     possible object is
     *     {@link OrderIdType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public OrderIdType getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     *
     * @param value
     *     allowed object is
     *     {@link OrderIdType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setId(OrderIdType value) {
        this.id = value;
    }

    /**
     * Gets the value of the status property.
     *
     * @return
     *     possible object is
     *     {@link OrderStatusType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public OrderStatusType getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     *
     * @param value
     *     allowed object is
     *     {@link OrderStatusType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setStatus(OrderStatusType value) {
        this.status = value;
    }

    /**
     * Gets the value of the type property.
     *
     * @return
     *     possible object is
     *     {@link OrderTypeType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public OrderTypeType getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     *
     * @param value
     *     allowed object is
     *     {@link OrderTypeType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setType(OrderTypeType value) {
        this.type = value;
    }

    /**
     * Gets the value of the clazz property.
     *
     * @return
     *     possible object is
     *     {@link OrderClassType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public OrderClassType getClazz() {
        return clazz;
    }

    /**
     * Sets the value of the clazz property.
     *
     * @param value
     *     allowed object is
     *     {@link OrderClassType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setClazz(OrderClassType value) {
        this.clazz = value;
    }

    /**
     * Gets the value of the billing property.
     *
     * @return
     *     possible object is
     *     {@link BillingType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public BillingType getBilling() {
        return billing;
    }

    /**
     * Sets the value of the billing property.
     *
     * @param value
     *     allowed object is
     *     {@link BillingType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setBilling(BillingType value) {
        this.billing = value;
    }

    /**
     * Gets the value of the campaign property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getCampaign() {
        return campaign;
    }

    /**
     * Sets the value of the campaign property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setCampaign(String value) {
        this.campaign = value;
    }

    /**
     * Gets the value of the commentCode property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the commentCode property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCommentCode().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     *
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public List<String> getCommentCode() {
        if (commentCode == null) {
            commentCode = new ArrayList<String>();
        }
        return this.commentCode;
    }

    /**
     * Gets the value of the commentText property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the commentText property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCommentText().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     *
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public List<String> getCommentText() {
        if (commentText == null) {
            commentText = new ArrayList<String>();
        }
        return this.commentText;
    }

    /**
     * Gets the value of the reasonCode property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the reasonCode property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReasonCode().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     *
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public List<String> getReasonCode() {
        if (reasonCode == null) {
            reasonCode = new ArrayList<String>();
        }
        return this.reasonCode;
    }

    /**
     * Gets the value of the reasolutionCode property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the reasolutionCode property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReasolutionCode().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     *
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public List<String> getReasolutionCode() {
        if (reasolutionCode == null) {
            reasolutionCode = new ArrayList<String>();
        }
        return this.reasolutionCode;
    }

    /**
     * Gets the value of the salesRepresentative property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getSalesRepresentative() {
        return salesRepresentative;
    }

    /**
     * Sets the value of the salesRepresentative property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setSalesRepresentative(String value) {
        this.salesRepresentative = value;
    }

    /**
     * Gets the value of the scheduleList property.
     *
     * @return
     *     possible object is
     *     {@link ScheduleListType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public ScheduleListType getScheduleList() {
        return scheduleList;
    }

    /**
     * Sets the value of the scheduleList property.
     *
     * @param value
     *     allowed object is
     *     {@link ScheduleListType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setScheduleList(ScheduleListType value) {
        this.scheduleList = value;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy2 strategy) {
        if ((object == null)||(this.getClass()!= object.getClass())) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final OrderType that = ((OrderType) object);
        {
            OrderIdType lhsId;
            lhsId = this.getId();
            OrderIdType rhsId;
            rhsId = that.getId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "id", lhsId), LocatorUtils.property(thatLocator, "id", rhsId), lhsId, rhsId, (this.id!= null), (that.id!= null))) {
                return false;
            }
        }
        {
            OrderStatusType lhsStatus;
            lhsStatus = this.getStatus();
            OrderStatusType rhsStatus;
            rhsStatus = that.getStatus();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "status", lhsStatus), LocatorUtils.property(thatLocator, "status", rhsStatus), lhsStatus, rhsStatus, (this.status!= null), (that.status!= null))) {
                return false;
            }
        }
        {
            OrderTypeType lhsType;
            lhsType = this.getType();
            OrderTypeType rhsType;
            rhsType = that.getType();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "type", lhsType), LocatorUtils.property(thatLocator, "type", rhsType), lhsType, rhsType, (this.type!= null), (that.type!= null))) {
                return false;
            }
        }
        {
            OrderClassType lhsClazz;
            lhsClazz = this.getClazz();
            OrderClassType rhsClazz;
            rhsClazz = that.getClazz();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "clazz", lhsClazz), LocatorUtils.property(thatLocator, "clazz", rhsClazz), lhsClazz, rhsClazz, (this.clazz!= null), (that.clazz!= null))) {
                return false;
            }
        }
        {
            BillingType lhsBilling;
            lhsBilling = this.getBilling();
            BillingType rhsBilling;
            rhsBilling = that.getBilling();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "billing", lhsBilling), LocatorUtils.property(thatLocator, "billing", rhsBilling), lhsBilling, rhsBilling, (this.billing!= null), (that.billing!= null))) {
                return false;
            }
        }
        {
            String lhsCampaign;
            lhsCampaign = this.getCampaign();
            String rhsCampaign;
            rhsCampaign = that.getCampaign();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "campaign", lhsCampaign), LocatorUtils.property(thatLocator, "campaign", rhsCampaign), lhsCampaign, rhsCampaign, (this.campaign!= null), (that.campaign!= null))) {
                return false;
            }
        }
        {
            List<String> lhsCommentCode;
            lhsCommentCode = (((this.commentCode!= null)&&(!this.commentCode.isEmpty()))?this.getCommentCode():null);
            List<String> rhsCommentCode;
            rhsCommentCode = (((that.commentCode!= null)&&(!that.commentCode.isEmpty()))?that.getCommentCode():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "commentCode", lhsCommentCode), LocatorUtils.property(thatLocator, "commentCode", rhsCommentCode), lhsCommentCode, rhsCommentCode, ((this.commentCode!= null)&&(!this.commentCode.isEmpty())), ((that.commentCode!= null)&&(!that.commentCode.isEmpty())))) {
                return false;
            }
        }
        {
            List<String> lhsCommentText;
            lhsCommentText = (((this.commentText!= null)&&(!this.commentText.isEmpty()))?this.getCommentText():null);
            List<String> rhsCommentText;
            rhsCommentText = (((that.commentText!= null)&&(!that.commentText.isEmpty()))?that.getCommentText():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "commentText", lhsCommentText), LocatorUtils.property(thatLocator, "commentText", rhsCommentText), lhsCommentText, rhsCommentText, ((this.commentText!= null)&&(!this.commentText.isEmpty())), ((that.commentText!= null)&&(!that.commentText.isEmpty())))) {
                return false;
            }
        }
        {
            List<String> lhsReasonCode;
            lhsReasonCode = (((this.reasonCode!= null)&&(!this.reasonCode.isEmpty()))?this.getReasonCode():null);
            List<String> rhsReasonCode;
            rhsReasonCode = (((that.reasonCode!= null)&&(!that.reasonCode.isEmpty()))?that.getReasonCode():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "reasonCode", lhsReasonCode), LocatorUtils.property(thatLocator, "reasonCode", rhsReasonCode), lhsReasonCode, rhsReasonCode, ((this.reasonCode!= null)&&(!this.reasonCode.isEmpty())), ((that.reasonCode!= null)&&(!that.reasonCode.isEmpty())))) {
                return false;
            }
        }
        {
            List<String> lhsReasolutionCode;
            lhsReasolutionCode = (((this.reasolutionCode!= null)&&(!this.reasolutionCode.isEmpty()))?this.getReasolutionCode():null);
            List<String> rhsReasolutionCode;
            rhsReasolutionCode = (((that.reasolutionCode!= null)&&(!that.reasolutionCode.isEmpty()))?that.getReasolutionCode():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "reasolutionCode", lhsReasolutionCode), LocatorUtils.property(thatLocator, "reasolutionCode", rhsReasolutionCode), lhsReasolutionCode, rhsReasolutionCode, ((this.reasolutionCode!= null)&&(!this.reasolutionCode.isEmpty())), ((that.reasolutionCode!= null)&&(!that.reasolutionCode.isEmpty())))) {
                return false;
            }
        }
        {
            String lhsSalesRepresentative;
            lhsSalesRepresentative = this.getSalesRepresentative();
            String rhsSalesRepresentative;
            rhsSalesRepresentative = that.getSalesRepresentative();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "salesRepresentative", lhsSalesRepresentative), LocatorUtils.property(thatLocator, "salesRepresentative", rhsSalesRepresentative), lhsSalesRepresentative, rhsSalesRepresentative, (this.salesRepresentative!= null), (that.salesRepresentative!= null))) {
                return false;
            }
        }
        {
            ScheduleListType lhsScheduleList;
            lhsScheduleList = this.getScheduleList();
            ScheduleListType rhsScheduleList;
            rhsScheduleList = that.getScheduleList();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "scheduleList", lhsScheduleList), LocatorUtils.property(thatLocator, "scheduleList", rhsScheduleList), lhsScheduleList, rhsScheduleList, (this.scheduleList!= null), (that.scheduleList!= null))) {
                return false;
            }
        }
        return true;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(Object object) {
        final EqualsStrategy2 strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode(ObjectLocator locator, HashCodeStrategy2 strategy) {
        int currentHashCode = 1;
        {
            OrderIdType theId;
            theId = this.getId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "id", theId), currentHashCode, theId, (this.id!= null));
        }
        {
            OrderStatusType theStatus;
            theStatus = this.getStatus();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "status", theStatus), currentHashCode, theStatus, (this.status!= null));
        }
        {
            OrderTypeType theType;
            theType = this.getType();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "type", theType), currentHashCode, theType, (this.type!= null));
        }
        {
            OrderClassType theClazz;
            theClazz = this.getClazz();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "clazz", theClazz), currentHashCode, theClazz, (this.clazz!= null));
        }
        {
            BillingType theBilling;
            theBilling = this.getBilling();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "billing", theBilling), currentHashCode, theBilling, (this.billing!= null));
        }
        {
            String theCampaign;
            theCampaign = this.getCampaign();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "campaign", theCampaign), currentHashCode, theCampaign, (this.campaign!= null));
        }
        {
            List<String> theCommentCode;
            theCommentCode = (((this.commentCode!= null)&&(!this.commentCode.isEmpty()))?this.getCommentCode():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "commentCode", theCommentCode), currentHashCode, theCommentCode, ((this.commentCode!= null)&&(!this.commentCode.isEmpty())));
        }
        {
            List<String> theCommentText;
            theCommentText = (((this.commentText!= null)&&(!this.commentText.isEmpty()))?this.getCommentText():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "commentText", theCommentText), currentHashCode, theCommentText, ((this.commentText!= null)&&(!this.commentText.isEmpty())));
        }
        {
            List<String> theReasonCode;
            theReasonCode = (((this.reasonCode!= null)&&(!this.reasonCode.isEmpty()))?this.getReasonCode():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "reasonCode", theReasonCode), currentHashCode, theReasonCode, ((this.reasonCode!= null)&&(!this.reasonCode.isEmpty())));
        }
        {
            List<String> theReasolutionCode;
            theReasolutionCode = (((this.reasolutionCode!= null)&&(!this.reasolutionCode.isEmpty()))?this.getReasolutionCode():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "reasolutionCode", theReasolutionCode), currentHashCode, theReasolutionCode, ((this.reasolutionCode!= null)&&(!this.reasolutionCode.isEmpty())));
        }
        {
            String theSalesRepresentative;
            theSalesRepresentative = this.getSalesRepresentative();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "salesRepresentative", theSalesRepresentative), currentHashCode, theSalesRepresentative, (this.salesRepresentative!= null));
        }
        {
            ScheduleListType theScheduleList;
            theScheduleList = this.getScheduleList();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "scheduleList", theScheduleList), currentHashCode, theScheduleList, (this.scheduleList!= null));
        }
        return currentHashCode;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode() {
        final HashCodeStrategy2 strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String toString() {
        final ToStringStrategy2 strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        {
            OrderIdType theId;
            theId = this.getId();
            strategy.appendField(locator, this, "id", buffer, theId, (this.id!= null));
        }
        {
            OrderStatusType theStatus;
            theStatus = this.getStatus();
            strategy.appendField(locator, this, "status", buffer, theStatus, (this.status!= null));
        }
        {
            OrderTypeType theType;
            theType = this.getType();
            strategy.appendField(locator, this, "type", buffer, theType, (this.type!= null));
        }
        {
            OrderClassType theClazz;
            theClazz = this.getClazz();
            strategy.appendField(locator, this, "clazz", buffer, theClazz, (this.clazz!= null));
        }
        {
            BillingType theBilling;
            theBilling = this.getBilling();
            strategy.appendField(locator, this, "billing", buffer, theBilling, (this.billing!= null));
        }
        {
            String theCampaign;
            theCampaign = this.getCampaign();
            strategy.appendField(locator, this, "campaign", buffer, theCampaign, (this.campaign!= null));
        }
        {
            List<String> theCommentCode;
            theCommentCode = (((this.commentCode!= null)&&(!this.commentCode.isEmpty()))?this.getCommentCode():null);
            strategy.appendField(locator, this, "commentCode", buffer, theCommentCode, ((this.commentCode!= null)&&(!this.commentCode.isEmpty())));
        }
        {
            List<String> theCommentText;
            theCommentText = (((this.commentText!= null)&&(!this.commentText.isEmpty()))?this.getCommentText():null);
            strategy.appendField(locator, this, "commentText", buffer, theCommentText, ((this.commentText!= null)&&(!this.commentText.isEmpty())));
        }
        {
            List<String> theReasonCode;
            theReasonCode = (((this.reasonCode!= null)&&(!this.reasonCode.isEmpty()))?this.getReasonCode():null);
            strategy.appendField(locator, this, "reasonCode", buffer, theReasonCode, ((this.reasonCode!= null)&&(!this.reasonCode.isEmpty())));
        }
        {
            List<String> theReasolutionCode;
            theReasolutionCode = (((this.reasolutionCode!= null)&&(!this.reasolutionCode.isEmpty()))?this.getReasolutionCode():null);
            strategy.appendField(locator, this, "reasolutionCode", buffer, theReasolutionCode, ((this.reasolutionCode!= null)&&(!this.reasolutionCode.isEmpty())));
        }
        {
            String theSalesRepresentative;
            theSalesRepresentative = this.getSalesRepresentative();
            strategy.appendField(locator, this, "salesRepresentative", buffer, theSalesRepresentative, (this.salesRepresentative!= null));
        }
        {
            ScheduleListType theScheduleList;
            theScheduleList = this.getScheduleList();
            strategy.appendField(locator, this, "scheduleList", buffer, theScheduleList, (this.scheduleList!= null));
        }
        return buffer;
    }

}
