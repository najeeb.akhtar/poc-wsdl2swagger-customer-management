package dishnetwork.schemas;

import org.jvnet.jaxb2_commons.lang.*;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="receiverInputInfo" maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="accessNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="model" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ownership" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="attributeList" type="{http://www.dishnetwork.com/schema/common/AttributeList/2015_10_08}AttributeListTypeType" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "receiverInputInfo"
})
@Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
public class ReceiverList implements Equals2, HashCode2, ToString2 {

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected List<ReceiverInputInfo> receiverInputInfo;

    /**
     * Default no-arg constructor
     */
    public ReceiverList() {
        super();
    }

    /**
     * Fully-initialising value constructor
     */
    public ReceiverList(final List<ReceiverInputInfo> receiverInputInfo) {
        this.receiverInputInfo = receiverInputInfo;
    }

    /**
     * Gets the value of the receiverInputInfo property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the receiverInputInfo property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReceiverInputInfo().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReceiverInputInfo }
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public List<ReceiverInputInfo> getReceiverInputInfo() {
        if (receiverInputInfo == null) {
            receiverInputInfo = new ArrayList<ReceiverInputInfo>();
        }
        return this.receiverInputInfo;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy2 strategy) {
        if ((object == null) || (this.getClass() != object.getClass())) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final ReceiverList that = ((ReceiverList) object);
        {
            List<ReceiverInputInfo> lhsReceiverInputInfo;
            lhsReceiverInputInfo = (((this.receiverInputInfo != null) && (!this.receiverInputInfo.isEmpty())) ? this.getReceiverInputInfo() : null);
            List<ReceiverInputInfo> rhsReceiverInputInfo;
            rhsReceiverInputInfo = (((that.receiverInputInfo != null) && (!that.receiverInputInfo.isEmpty())) ? that.getReceiverInputInfo() : null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "receiverInputInfo", lhsReceiverInputInfo), LocatorUtils.property(thatLocator, "receiverInputInfo", rhsReceiverInputInfo), lhsReceiverInputInfo, rhsReceiverInputInfo, ((this.receiverInputInfo != null) && (!this.receiverInputInfo.isEmpty())), ((that.receiverInputInfo != null) && (!that.receiverInputInfo.isEmpty())))) {
                return false;
            }
        }
        return true;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(Object object) {
        final EqualsStrategy2 strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode(ObjectLocator locator, HashCodeStrategy2 strategy) {
        int currentHashCode = 1;
        {
            List<ReceiverInputInfo> theReceiverInputInfo;
            theReceiverInputInfo = (((this.receiverInputInfo != null) && (!this.receiverInputInfo.isEmpty())) ? this.getReceiverInputInfo() : null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "receiverInputInfo", theReceiverInputInfo), currentHashCode, theReceiverInputInfo, ((this.receiverInputInfo != null) && (!this.receiverInputInfo.isEmpty())));
        }
        return currentHashCode;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode() {
        final HashCodeStrategy2 strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String toString() {
        final ToStringStrategy2 strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        {
            List<ReceiverInputInfo> theReceiverInputInfo;
            theReceiverInputInfo = (((this.receiverInputInfo != null) && (!this.receiverInputInfo.isEmpty())) ? this.getReceiverInputInfo() : null);
            strategy.appendField(locator, this, "receiverInputInfo", buffer, theReceiverInputInfo, ((this.receiverInputInfo != null) && (!this.receiverInputInfo.isEmpty())));
        }
        return buffer;
    }


}
