package dishnetwork.schemas;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class AccountCreated {
    @JsonProperty(value = "AccountCreated")
    protected AccountCreatedInfo accountCreated;
    protected String distributorCompanyOe;
    protected String companyOe;
    protected String agentLoginId;
    protected String opId;
    protected String serviceHouseNumber;
    protected String serviceStreetName;
    protected String billToHouseNumber;
    protected String billToStreetName;
    protected List<String> authorizedUserNameList;
    protected List<String> equipmentConditionalAccessNumberList;
}
