package dishnetwork.schemas;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.annotation.Generated;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class CustomerQualification {
    protected CustomerQualificationIdentifier customerQualificationIdentifier;
    protected String creditCardAuthTransactionId;
    @JsonProperty(defaultValue = "false")
    protected String premiumFlag;
    @JsonProperty(defaultValue = "false")
    protected String exCustomerFlag;
    protected String formerQualCode;
    protected OverrideFlagType qualificationOverrideFlag;
}
