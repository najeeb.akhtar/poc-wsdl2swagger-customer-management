package dishnetwork.schemas;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.jvnet.jaxb2_commons.lang.*;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="planInfo" maxOccurs="unbounded"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;extension base="{http://www.dishnetwork.com/schema/CustomerManagement/PlanInfo/2015_01_15}PlanInfoType"&gt;
 *               &lt;/extension&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="planAttributes" type="{http://www.dishnetwork.com/schema/common/AttributeList/2013_09_19}AttributeListType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "planInfo",
    "planAttributes"
})
@Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T06:08:51-06:00")
public class PlanList implements Equals2, HashCode2, ToString2 {

    //@XmlElement(required = true)
    @JsonProperty(required = true)
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T06:08:51-06:00")
    protected List<PlanInfo> planInfo;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T06:08:51-06:00")
    protected AttributeListType planAttributes;

    /**
     * Default no-arg constructor
     */
    public PlanList() {
        super();
    }

    /**
     * Fully-initialising value constructor
     */
    public PlanList(final List<PlanInfo> planInfo, final AttributeListType planAttributes) {
        this.planInfo = planInfo;
        this.planAttributes = planAttributes;
    }

    /**
     * Gets the value of the planInfo property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the planInfo property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPlanInfo().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PlanInfo }
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T06:08:51-06:00")
    public List<PlanInfo> getPlanInfo() {
        if (planInfo == null) {
            planInfo = new ArrayList<PlanInfo>();
        }
        return this.planInfo;
    }

    /**
     * Gets the value of the planAttributes property.
     *
     * @return possible object is
     * {@link AttributeListType }
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T06:08:51-06:00")
    public AttributeListType getPlanAttributes() {
        return planAttributes;
    }

    /**
     * Sets the value of the planAttributes property.
     *
     * @param value allowed object is
     *              {@link AttributeListType }
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T06:08:51-06:00")
    public void setPlanAttributes(AttributeListType value) {
        this.planAttributes = value;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T06:08:51-06:00")
    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy2 strategy) {
        if ((object == null) || (this.getClass() != object.getClass())) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final PlanList that = ((PlanList) object);
        {
            List<PlanInfo> lhsPlanInfo;
            lhsPlanInfo = (((this.planInfo != null) && (!this.planInfo.isEmpty())) ? this.getPlanInfo() : null);
            List<PlanInfo> rhsPlanInfo;
            rhsPlanInfo = (((that.planInfo != null) && (!that.planInfo.isEmpty())) ? that.getPlanInfo() : null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "planInfo", lhsPlanInfo), LocatorUtils.property(thatLocator, "planInfo", rhsPlanInfo), lhsPlanInfo, rhsPlanInfo, ((this.planInfo != null) && (!this.planInfo.isEmpty())), ((that.planInfo != null) && (!that.planInfo.isEmpty())))) {
                return false;
            }
        }
        {
            AttributeListType lhsPlanAttributes;
            lhsPlanAttributes = this.getPlanAttributes();
            AttributeListType rhsPlanAttributes;
            rhsPlanAttributes = that.getPlanAttributes();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "planAttributes", lhsPlanAttributes), LocatorUtils.property(thatLocator, "planAttributes", rhsPlanAttributes), lhsPlanAttributes, rhsPlanAttributes, (this.planAttributes != null), (that.planAttributes != null))) {
                return false;
            }
        }
        return true;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T06:08:51-06:00")
    public boolean equals(Object object) {
        final EqualsStrategy2 strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T06:08:51-06:00")
    public int hashCode(ObjectLocator locator, HashCodeStrategy2 strategy) {
        int currentHashCode = 1;
        {
            List<PlanInfo> thePlanInfo;
            thePlanInfo = (((this.planInfo != null) && (!this.planInfo.isEmpty())) ? this.getPlanInfo() : null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "planInfo", thePlanInfo), currentHashCode, thePlanInfo, ((this.planInfo != null) && (!this.planInfo.isEmpty())));
        }
        {
            AttributeListType thePlanAttributes;
            thePlanAttributes = this.getPlanAttributes();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "planAttributes", thePlanAttributes), currentHashCode, thePlanAttributes, (this.planAttributes != null));
        }
        return currentHashCode;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T06:08:51-06:00")
    public int hashCode() {
        final HashCodeStrategy2 strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T06:08:51-06:00")
    public String toString() {
        final ToStringStrategy2 strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T06:08:51-06:00")
    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T06:08:51-06:00")
    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        {
            List<PlanInfo> thePlanInfo;
            thePlanInfo = (((this.planInfo != null) && (!this.planInfo.isEmpty())) ? this.getPlanInfo() : null);
            strategy.appendField(locator, this, "planInfo", buffer, thePlanInfo, ((this.planInfo != null) && (!this.planInfo.isEmpty())));
        }
        {
            AttributeListType thePlanAttributes;
            thePlanAttributes = this.getPlanAttributes();
            strategy.appendField(locator, this, "planAttributes", buffer, thePlanAttributes, (this.planAttributes != null));
        }
        return buffer;
    }


}
