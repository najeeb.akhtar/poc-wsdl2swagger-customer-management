
package dishnetwork.schemas;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DwellingTypeType.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <pre>
 * &lt;simpleType name="DwellingTypeType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="SINGLE_FAMILY"/&gt;
 *     &lt;enumeration value="MULTI_FAMILY"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 *
 */
@XmlType(name = "DwellingTypeType", namespace = "http://www.dishnetwork.com/schema/Location/DwellingType/2009_08_25")
@XmlEnum
@Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
public enum DwellingTypeType {

    SINGLE_FAMILY,
    MULTI_FAMILY;

    public String value() {
        return name();
    }

    public static DwellingTypeType fromValue(String v) {
        return valueOf(v);
    }

}
