
package dishnetwork.schemas;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.jvnet.jaxb2_commons.lang.Equals2;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy2;
import org.jvnet.jaxb2_commons.lang.HashCode2;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy2;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString2;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy2;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * Describe the Location details
 *
 * <p>Java class for LocationType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="LocationType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="locationId" type="{http://www.dishnetwork.com/schema/Customer/LocationId/2011_04_01}LocationIdType" minOccurs="0"/&gt;
 *         &lt;element name="dwellingType" type="{http://www.dishnetwork.com/schema/Location/DwellingType/2009_08_25}DwellingTypeType"/&gt;
 *         &lt;element name="address" type="{http://www.dishnetwork.com/schema/AccountOrder/Address/2016_03_24}AddressType"/&gt;
 *         &lt;element name="geoCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="localsDma" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="miscellaneous" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LocationTypeSubmitOrder", propOrder = {
    "locationId",
    "dwellingType",
    "address",
    "geoCode",
    "localsDma",
    "miscellaneous"
})
@Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
public class LocationTypeSubmitOrder implements Equals2, HashCode2, ToString2
{

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected LocationIdType locationId;
    //@XmlElement(required = true)
    @JsonProperty(required = true)
    @XmlSchemaType(name = "string")
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected DwellingTypeType dwellingType;
    //@XmlElement(required = true)
    @JsonProperty(required = true)
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected AddressTypeScrubAddressDetail address;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String geoCode;
    //@XmlElement(required = true)
    @JsonProperty(required = true)
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String localsDma;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String miscellaneous;

    /**
     * Default no-arg constructor
     *
     */
    public LocationTypeSubmitOrder() {
        super();
    }

    /**
     * Fully-initialising value constructor
     *
     */
    public LocationTypeSubmitOrder(final LocationIdType locationId, final DwellingTypeType dwellingType, final AddressTypeScrubAddressDetail address, final String geoCode, final String localsDma, final String miscellaneous) {
        this.locationId = locationId;
        this.dwellingType = dwellingType;
        this.address = address;
        this.geoCode = geoCode;
        this.localsDma = localsDma;
        this.miscellaneous = miscellaneous;
    }

    /**
     * Gets the value of the locationId property.
     *
     * @return
     *     possible object is
     *     {@link LocationIdType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public LocationIdType getLocationId() {
        return locationId;
    }

    /**
     * Sets the value of the locationId property.
     *
     * @param value
     *     allowed object is
     *     {@link LocationIdType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setLocationId(LocationIdType value) {
        this.locationId = value;
    }

    /**
     * Gets the value of the dwellingType property.
     *
     * @return
     *     possible object is
     *     {@link DwellingTypeType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public DwellingTypeType getDwellingType() {
        return dwellingType;
    }

    /**
     * Sets the value of the dwellingType property.
     *
     * @param value
     *     allowed object is
     *     {@link DwellingTypeType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setDwellingType(DwellingTypeType value) {
        this.dwellingType = value;
    }

    /**
     * Gets the value of the address property.
     *
     * @return
     *     possible object is
     *     {@link AddressTypeScrubAddressDetail }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public AddressTypeScrubAddressDetail getAddress() {
        return address;
    }

    /**
     * Sets the value of the address property.
     *
     * @param value
     *     allowed object is
     *     {@link AddressTypeScrubAddressDetail }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setAddress(AddressTypeScrubAddressDetail value) {
        this.address = value;
    }

    /**
     * Gets the value of the geoCode property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getGeoCode() {
        return geoCode;
    }

    /**
     * Sets the value of the geoCode property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setGeoCode(String value) {
        this.geoCode = value;
    }

    /**
     * Gets the value of the localsDma property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getLocalsDma() {
        return localsDma;
    }

    /**
     * Sets the value of the localsDma property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setLocalsDma(String value) {
        this.localsDma = value;
    }

    /**
     * Gets the value of the miscellaneous property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getMiscellaneous() {
        return miscellaneous;
    }

    /**
     * Sets the value of the miscellaneous property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setMiscellaneous(String value) {
        this.miscellaneous = value;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy2 strategy) {
        if ((object == null)||(this.getClass()!= object.getClass())) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final LocationTypeSubmitOrder that = ((LocationTypeSubmitOrder) object);
        {
            LocationIdType lhsLocationId;
            lhsLocationId = this.getLocationId();
            LocationIdType rhsLocationId;
            rhsLocationId = that.getLocationId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "locationId", lhsLocationId), LocatorUtils.property(thatLocator, "locationId", rhsLocationId), lhsLocationId, rhsLocationId, (this.locationId!= null), (that.locationId!= null))) {
                return false;
            }
        }
        {
            DwellingTypeType lhsDwellingType;
            lhsDwellingType = this.getDwellingType();
            DwellingTypeType rhsDwellingType;
            rhsDwellingType = that.getDwellingType();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "dwellingType", lhsDwellingType), LocatorUtils.property(thatLocator, "dwellingType", rhsDwellingType), lhsDwellingType, rhsDwellingType, (this.dwellingType!= null), (that.dwellingType!= null))) {
                return false;
            }
        }
        {
            AddressTypeScrubAddressDetail lhsAddress;
            lhsAddress = this.getAddress();
            AddressTypeScrubAddressDetail rhsAddress;
            rhsAddress = that.getAddress();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "address", lhsAddress), LocatorUtils.property(thatLocator, "address", rhsAddress), lhsAddress, rhsAddress, (this.address!= null), (that.address!= null))) {
                return false;
            }
        }
        {
            String lhsGeoCode;
            lhsGeoCode = this.getGeoCode();
            String rhsGeoCode;
            rhsGeoCode = that.getGeoCode();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "geoCode", lhsGeoCode), LocatorUtils.property(thatLocator, "geoCode", rhsGeoCode), lhsGeoCode, rhsGeoCode, (this.geoCode!= null), (that.geoCode!= null))) {
                return false;
            }
        }
        {
            String lhsLocalsDma;
            lhsLocalsDma = this.getLocalsDma();
            String rhsLocalsDma;
            rhsLocalsDma = that.getLocalsDma();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "localsDma", lhsLocalsDma), LocatorUtils.property(thatLocator, "localsDma", rhsLocalsDma), lhsLocalsDma, rhsLocalsDma, (this.localsDma!= null), (that.localsDma!= null))) {
                return false;
            }
        }
        {
            String lhsMiscellaneous;
            lhsMiscellaneous = this.getMiscellaneous();
            String rhsMiscellaneous;
            rhsMiscellaneous = that.getMiscellaneous();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "miscellaneous", lhsMiscellaneous), LocatorUtils.property(thatLocator, "miscellaneous", rhsMiscellaneous), lhsMiscellaneous, rhsMiscellaneous, (this.miscellaneous!= null), (that.miscellaneous!= null))) {
                return false;
            }
        }
        return true;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(Object object) {
        final EqualsStrategy2 strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode(ObjectLocator locator, HashCodeStrategy2 strategy) {
        int currentHashCode = 1;
        {
            LocationIdType theLocationId;
            theLocationId = this.getLocationId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "locationId", theLocationId), currentHashCode, theLocationId, (this.locationId!= null));
        }
        {
            DwellingTypeType theDwellingType;
            theDwellingType = this.getDwellingType();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "dwellingType", theDwellingType), currentHashCode, theDwellingType, (this.dwellingType!= null));
        }
        {
            AddressTypeScrubAddressDetail theAddress;
            theAddress = this.getAddress();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "address", theAddress), currentHashCode, theAddress, (this.address!= null));
        }
        {
            String theGeoCode;
            theGeoCode = this.getGeoCode();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "geoCode", theGeoCode), currentHashCode, theGeoCode, (this.geoCode!= null));
        }
        {
            String theLocalsDma;
            theLocalsDma = this.getLocalsDma();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "localsDma", theLocalsDma), currentHashCode, theLocalsDma, (this.localsDma!= null));
        }
        {
            String theMiscellaneous;
            theMiscellaneous = this.getMiscellaneous();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "miscellaneous", theMiscellaneous), currentHashCode, theMiscellaneous, (this.miscellaneous!= null));
        }
        return currentHashCode;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode() {
        final HashCodeStrategy2 strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String toString() {
        final ToStringStrategy2 strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        {
            LocationIdType theLocationId;
            theLocationId = this.getLocationId();
            strategy.appendField(locator, this, "locationId", buffer, theLocationId, (this.locationId!= null));
        }
        {
            DwellingTypeType theDwellingType;
            theDwellingType = this.getDwellingType();
            strategy.appendField(locator, this, "dwellingType", buffer, theDwellingType, (this.dwellingType!= null));
        }
        {
            AddressTypeScrubAddressDetail theAddress;
            theAddress = this.getAddress();
            strategy.appendField(locator, this, "address", buffer, theAddress, (this.address!= null));
        }
        {
            String theGeoCode;
            theGeoCode = this.getGeoCode();
            strategy.appendField(locator, this, "geoCode", buffer, theGeoCode, (this.geoCode!= null));
        }
        {
            String theLocalsDma;
            theLocalsDma = this.getLocalsDma();
            strategy.appendField(locator, this, "localsDma", buffer, theLocalsDma, (this.localsDma!= null));
        }
        {
            String theMiscellaneous;
            theMiscellaneous = this.getMiscellaneous();
            strategy.appendField(locator, this, "miscellaneous", buffer, theMiscellaneous, (this.miscellaneous!= null));
        }
        return buffer;
    }

}
