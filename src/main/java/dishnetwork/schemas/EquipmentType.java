
package dishnetwork.schemas;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.jvnet.jaxb2_commons.lang.Equals2;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy2;
import org.jvnet.jaxb2_commons.lang.HashCode2;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy2;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString2;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy2;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for EquipmentType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="EquipmentType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="outlet" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="caNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="type" type="{http://www.dishnetwork.com/schema/common/EquipmentType/2014_06_13}EquipmentTypeType"/&gt;
 *         &lt;element name="ownership" type="{http://www.dishnetwork.com/schema/Equipment/EquipmentOwnershipType/2013_01_17}EquipmentOwnershipTypeType"/&gt;
 *         &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="modelFamily" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="modelFamilyDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="modelFamilyIdentifier" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="address" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="saleType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="reasonCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="raNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="activationType" type="{http://www.dishnetwork.com/schema/AccountEquipment/common/ActivationType/2013_01_17}ActivationTypeType" minOccurs="0"/&gt;
 *         &lt;element name="swapOutEquipmentModel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EquipmentType", propOrder = {
    "outlet",
    "caNumber",
    "type",
    "ownership",
    "status",
    "modelFamily",
    "modelFamilyDescription",
    "modelFamilyIdentifier",
    "address",
    "saleType",
    "reasonCode",
    "raNumber",
    "activationType",
    "swapOutEquipmentModel"
})
@Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-12T08:55:33-06:00")
public class EquipmentType implements Equals2, HashCode2, ToString2
{

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-12T08:55:33-06:00")
    protected String outlet;
    //@XmlElement(nillable = true)
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-12T08:55:33-06:00")
    protected String caNumber;
    //@XmlElement(required = true)
    @JsonProperty(required = true)
    @XmlSchemaType(name = "string")
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-12T08:55:33-06:00")
    protected EquipmentTypeType type;
    //@XmlElement(required = true)
    @JsonProperty(required = true)
    @XmlSchemaType(name = "string")
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-12T08:55:33-06:00")
    protected EquipmentOwnershipTypeType ownership;
    //@XmlElement(nillable = true)
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-12T08:55:33-06:00")
    protected String status;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-12T08:55:33-06:00")
    protected String modelFamily;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-12T08:55:33-06:00")
    protected String modelFamilyDescription;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-12T08:55:33-06:00")
    protected String modelFamilyIdentifier;
    //@XmlElement(nillable = true)
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-12T08:55:33-06:00")
    protected String address;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-12T08:55:33-06:00")
    protected String saleType;
    //@XmlElement(nillable = true)
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-12T08:55:33-06:00")
    protected String reasonCode;
    //@XmlElement(nillable = true)
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-12T08:55:33-06:00")
    protected String raNumber;
    //@XmlElement(nillable = true)
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-12T08:55:33-06:00")
    protected ActivationTypeType activationType;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-12T08:55:33-06:00")
    protected String swapOutEquipmentModel;

    /**
     * Default no-arg constructor
     *
     */
    public EquipmentType() {
        super();
    }

    /**
     * Fully-initialising value constructor
     *
     */
    public EquipmentType(final String outlet, final String caNumber, final EquipmentTypeType type, final EquipmentOwnershipTypeType ownership, final String status, final String modelFamily, final String modelFamilyDescription, final String modelFamilyIdentifier, final String address, final String saleType, final String reasonCode, final String raNumber, final ActivationTypeType activationType, final String swapOutEquipmentModel) {
        this.outlet = outlet;
        this.caNumber = caNumber;
        this.type = type;
        this.ownership = ownership;
        this.status = status;
        this.modelFamily = modelFamily;
        this.modelFamilyDescription = modelFamilyDescription;
        this.modelFamilyIdentifier = modelFamilyIdentifier;
        this.address = address;
        this.saleType = saleType;
        this.reasonCode = reasonCode;
        this.raNumber = raNumber;
        this.activationType = activationType;
        this.swapOutEquipmentModel = swapOutEquipmentModel;
    }

    /**
     * Gets the value of the outlet property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-12T08:55:33-06:00")
    public String getOutlet() {
        return outlet;
    }

    /**
     * Sets the value of the outlet property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-12T08:55:33-06:00")
    public void setOutlet(String value) {
        this.outlet = value;
    }

    /**
     * Gets the value of the caNumber property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-12T08:55:33-06:00")
    public String getCaNumber() {
        return caNumber;
    }

    /**
     * Sets the value of the caNumber property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-12T08:55:33-06:00")
    public void setCaNumber(String value) {
        this.caNumber = value;
    }

    /**
     * Gets the value of the type property.
     *
     * @return
     *     possible object is
     *     {@link EquipmentTypeType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-12T08:55:33-06:00")
    public EquipmentTypeType getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     *
     * @param value
     *     allowed object is
     *     {@link EquipmentTypeType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-12T08:55:33-06:00")
    public void setType(EquipmentTypeType value) {
        this.type = value;
    }

    /**
     * Gets the value of the ownership property.
     *
     * @return
     *     possible object is
     *     {@link EquipmentOwnershipTypeType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-12T08:55:33-06:00")
    public EquipmentOwnershipTypeType getOwnership() {
        return ownership;
    }

    /**
     * Sets the value of the ownership property.
     *
     * @param value
     *     allowed object is
     *     {@link EquipmentOwnershipTypeType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-12T08:55:33-06:00")
    public void setOwnership(EquipmentOwnershipTypeType value) {
        this.ownership = value;
    }

    /**
     * Gets the value of the status property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-12T08:55:33-06:00")
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-12T08:55:33-06:00")
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the modelFamily property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-12T08:55:33-06:00")
    public String getModelFamily() {
        return modelFamily;
    }

    /**
     * Sets the value of the modelFamily property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-12T08:55:33-06:00")
    public void setModelFamily(String value) {
        this.modelFamily = value;
    }

    /**
     * Gets the value of the modelFamilyDescription property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-12T08:55:33-06:00")
    public String getModelFamilyDescription() {
        return modelFamilyDescription;
    }

    /**
     * Sets the value of the modelFamilyDescription property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-12T08:55:33-06:00")
    public void setModelFamilyDescription(String value) {
        this.modelFamilyDescription = value;
    }

    /**
     * Gets the value of the modelFamilyIdentifier property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-12T08:55:33-06:00")
    public String getModelFamilyIdentifier() {
        return modelFamilyIdentifier;
    }

    /**
     * Sets the value of the modelFamilyIdentifier property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-12T08:55:33-06:00")
    public void setModelFamilyIdentifier(String value) {
        this.modelFamilyIdentifier = value;
    }

    /**
     * Gets the value of the address property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-12T08:55:33-06:00")
    public String getAddress() {
        return address;
    }

    /**
     * Sets the value of the address property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-12T08:55:33-06:00")
    public void setAddress(String value) {
        this.address = value;
    }

    /**
     * Gets the value of the saleType property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-12T08:55:33-06:00")
    public String getSaleType() {
        return saleType;
    }

    /**
     * Sets the value of the saleType property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-12T08:55:33-06:00")
    public void setSaleType(String value) {
        this.saleType = value;
    }

    /**
     * Gets the value of the reasonCode property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-12T08:55:33-06:00")
    public String getReasonCode() {
        return reasonCode;
    }

    /**
     * Sets the value of the reasonCode property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-12T08:55:33-06:00")
    public void setReasonCode(String value) {
        this.reasonCode = value;
    }

    /**
     * Gets the value of the raNumber property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-12T08:55:33-06:00")
    public String getRaNumber() {
        return raNumber;
    }

    /**
     * Sets the value of the raNumber property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-12T08:55:33-06:00")
    public void setRaNumber(String value) {
        this.raNumber = value;
    }

    /**
     * Gets the value of the activationType property.
     *
     * @return
     *     possible object is
     *     {@link ActivationTypeType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-12T08:55:33-06:00")
    public ActivationTypeType getActivationType() {
        return activationType;
    }

    /**
     * Sets the value of the activationType property.
     *
     * @param value
     *     allowed object is
     *     {@link ActivationTypeType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-12T08:55:33-06:00")
    public void setActivationType(ActivationTypeType value) {
        this.activationType = value;
    }

    /**
     * Gets the value of the swapOutEquipmentModel property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-12T08:55:33-06:00")
    public String getSwapOutEquipmentModel() {
        return swapOutEquipmentModel;
    }

    /**
     * Sets the value of the swapOutEquipmentModel property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-12T08:55:33-06:00")
    public void setSwapOutEquipmentModel(String value) {
        this.swapOutEquipmentModel = value;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-12T08:55:33-06:00")
    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy2 strategy) {
        if ((object == null)||(this.getClass()!= object.getClass())) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final EquipmentType that = ((EquipmentType) object);
        {
            String lhsOutlet;
            lhsOutlet = this.getOutlet();
            String rhsOutlet;
            rhsOutlet = that.getOutlet();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "outlet", lhsOutlet), LocatorUtils.property(thatLocator, "outlet", rhsOutlet), lhsOutlet, rhsOutlet, (this.outlet!= null), (that.outlet!= null))) {
                return false;
            }
        }
        {
            String lhsCaNumber;
            lhsCaNumber = this.getCaNumber();
            String rhsCaNumber;
            rhsCaNumber = that.getCaNumber();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "caNumber", lhsCaNumber), LocatorUtils.property(thatLocator, "caNumber", rhsCaNumber), lhsCaNumber, rhsCaNumber, (this.caNumber!= null), (that.caNumber!= null))) {
                return false;
            }
        }
        {
            EquipmentTypeType lhsType;
            lhsType = this.getType();
            EquipmentTypeType rhsType;
            rhsType = that.getType();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "type", lhsType), LocatorUtils.property(thatLocator, "type", rhsType), lhsType, rhsType, (this.type!= null), (that.type!= null))) {
                return false;
            }
        }
        {
            EquipmentOwnershipTypeType lhsOwnership;
            lhsOwnership = this.getOwnership();
            EquipmentOwnershipTypeType rhsOwnership;
            rhsOwnership = that.getOwnership();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "ownership", lhsOwnership), LocatorUtils.property(thatLocator, "ownership", rhsOwnership), lhsOwnership, rhsOwnership, (this.ownership!= null), (that.ownership!= null))) {
                return false;
            }
        }
        {
            String lhsStatus;
            lhsStatus = this.getStatus();
            String rhsStatus;
            rhsStatus = that.getStatus();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "status", lhsStatus), LocatorUtils.property(thatLocator, "status", rhsStatus), lhsStatus, rhsStatus, (this.status!= null), (that.status!= null))) {
                return false;
            }
        }
        {
            String lhsModelFamily;
            lhsModelFamily = this.getModelFamily();
            String rhsModelFamily;
            rhsModelFamily = that.getModelFamily();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "modelFamily", lhsModelFamily), LocatorUtils.property(thatLocator, "modelFamily", rhsModelFamily), lhsModelFamily, rhsModelFamily, (this.modelFamily!= null), (that.modelFamily!= null))) {
                return false;
            }
        }
        {
            String lhsModelFamilyDescription;
            lhsModelFamilyDescription = this.getModelFamilyDescription();
            String rhsModelFamilyDescription;
            rhsModelFamilyDescription = that.getModelFamilyDescription();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "modelFamilyDescription", lhsModelFamilyDescription), LocatorUtils.property(thatLocator, "modelFamilyDescription", rhsModelFamilyDescription), lhsModelFamilyDescription, rhsModelFamilyDescription, (this.modelFamilyDescription!= null), (that.modelFamilyDescription!= null))) {
                return false;
            }
        }
        {
            String lhsModelFamilyIdentifier;
            lhsModelFamilyIdentifier = this.getModelFamilyIdentifier();
            String rhsModelFamilyIdentifier;
            rhsModelFamilyIdentifier = that.getModelFamilyIdentifier();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "modelFamilyIdentifier", lhsModelFamilyIdentifier), LocatorUtils.property(thatLocator, "modelFamilyIdentifier", rhsModelFamilyIdentifier), lhsModelFamilyIdentifier, rhsModelFamilyIdentifier, (this.modelFamilyIdentifier!= null), (that.modelFamilyIdentifier!= null))) {
                return false;
            }
        }
        {
            String lhsAddress;
            lhsAddress = this.getAddress();
            String rhsAddress;
            rhsAddress = that.getAddress();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "address", lhsAddress), LocatorUtils.property(thatLocator, "address", rhsAddress), lhsAddress, rhsAddress, (this.address!= null), (that.address!= null))) {
                return false;
            }
        }
        {
            String lhsSaleType;
            lhsSaleType = this.getSaleType();
            String rhsSaleType;
            rhsSaleType = that.getSaleType();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "saleType", lhsSaleType), LocatorUtils.property(thatLocator, "saleType", rhsSaleType), lhsSaleType, rhsSaleType, (this.saleType!= null), (that.saleType!= null))) {
                return false;
            }
        }
        {
            String lhsReasonCode;
            lhsReasonCode = this.getReasonCode();
            String rhsReasonCode;
            rhsReasonCode = that.getReasonCode();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "reasonCode", lhsReasonCode), LocatorUtils.property(thatLocator, "reasonCode", rhsReasonCode), lhsReasonCode, rhsReasonCode, (this.reasonCode!= null), (that.reasonCode!= null))) {
                return false;
            }
        }
        {
            String lhsRaNumber;
            lhsRaNumber = this.getRaNumber();
            String rhsRaNumber;
            rhsRaNumber = that.getRaNumber();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "raNumber", lhsRaNumber), LocatorUtils.property(thatLocator, "raNumber", rhsRaNumber), lhsRaNumber, rhsRaNumber, (this.raNumber!= null), (that.raNumber!= null))) {
                return false;
            }
        }
        {
            ActivationTypeType lhsActivationType;
            lhsActivationType = this.getActivationType();
            ActivationTypeType rhsActivationType;
            rhsActivationType = that.getActivationType();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "activationType", lhsActivationType), LocatorUtils.property(thatLocator, "activationType", rhsActivationType), lhsActivationType, rhsActivationType, (this.activationType!= null), (that.activationType!= null))) {
                return false;
            }
        }
        {
            String lhsSwapOutEquipmentModel;
            lhsSwapOutEquipmentModel = this.getSwapOutEquipmentModel();
            String rhsSwapOutEquipmentModel;
            rhsSwapOutEquipmentModel = that.getSwapOutEquipmentModel();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "swapOutEquipmentModel", lhsSwapOutEquipmentModel), LocatorUtils.property(thatLocator, "swapOutEquipmentModel", rhsSwapOutEquipmentModel), lhsSwapOutEquipmentModel, rhsSwapOutEquipmentModel, (this.swapOutEquipmentModel!= null), (that.swapOutEquipmentModel!= null))) {
                return false;
            }
        }
        return true;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-12T08:55:33-06:00")
    public boolean equals(Object object) {
        final EqualsStrategy2 strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-12T08:55:33-06:00")
    public int hashCode(ObjectLocator locator, HashCodeStrategy2 strategy) {
        int currentHashCode = 1;
        {
            String theOutlet;
            theOutlet = this.getOutlet();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "outlet", theOutlet), currentHashCode, theOutlet, (this.outlet!= null));
        }
        {
            String theCaNumber;
            theCaNumber = this.getCaNumber();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "caNumber", theCaNumber), currentHashCode, theCaNumber, (this.caNumber!= null));
        }
        {
            EquipmentTypeType theType;
            theType = this.getType();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "type", theType), currentHashCode, theType, (this.type!= null));
        }
        {
            EquipmentOwnershipTypeType theOwnership;
            theOwnership = this.getOwnership();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "ownership", theOwnership), currentHashCode, theOwnership, (this.ownership!= null));
        }
        {
            String theStatus;
            theStatus = this.getStatus();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "status", theStatus), currentHashCode, theStatus, (this.status!= null));
        }
        {
            String theModelFamily;
            theModelFamily = this.getModelFamily();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "modelFamily", theModelFamily), currentHashCode, theModelFamily, (this.modelFamily!= null));
        }
        {
            String theModelFamilyDescription;
            theModelFamilyDescription = this.getModelFamilyDescription();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "modelFamilyDescription", theModelFamilyDescription), currentHashCode, theModelFamilyDescription, (this.modelFamilyDescription!= null));
        }
        {
            String theModelFamilyIdentifier;
            theModelFamilyIdentifier = this.getModelFamilyIdentifier();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "modelFamilyIdentifier", theModelFamilyIdentifier), currentHashCode, theModelFamilyIdentifier, (this.modelFamilyIdentifier!= null));
        }
        {
            String theAddress;
            theAddress = this.getAddress();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "address", theAddress), currentHashCode, theAddress, (this.address!= null));
        }
        {
            String theSaleType;
            theSaleType = this.getSaleType();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "saleType", theSaleType), currentHashCode, theSaleType, (this.saleType!= null));
        }
        {
            String theReasonCode;
            theReasonCode = this.getReasonCode();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "reasonCode", theReasonCode), currentHashCode, theReasonCode, (this.reasonCode!= null));
        }
        {
            String theRaNumber;
            theRaNumber = this.getRaNumber();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "raNumber", theRaNumber), currentHashCode, theRaNumber, (this.raNumber!= null));
        }
        {
            ActivationTypeType theActivationType;
            theActivationType = this.getActivationType();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "activationType", theActivationType), currentHashCode, theActivationType, (this.activationType!= null));
        }
        {
            String theSwapOutEquipmentModel;
            theSwapOutEquipmentModel = this.getSwapOutEquipmentModel();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "swapOutEquipmentModel", theSwapOutEquipmentModel), currentHashCode, theSwapOutEquipmentModel, (this.swapOutEquipmentModel!= null));
        }
        return currentHashCode;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-12T08:55:33-06:00")
    public int hashCode() {
        final HashCodeStrategy2 strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-12T08:55:33-06:00")
    public String toString() {
        final ToStringStrategy2 strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-12T08:55:33-06:00")
    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-12T08:55:33-06:00")
    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        {
            String theOutlet;
            theOutlet = this.getOutlet();
            strategy.appendField(locator, this, "outlet", buffer, theOutlet, (this.outlet!= null));
        }
        {
            String theCaNumber;
            theCaNumber = this.getCaNumber();
            strategy.appendField(locator, this, "caNumber", buffer, theCaNumber, (this.caNumber!= null));
        }
        {
            EquipmentTypeType theType;
            theType = this.getType();
            strategy.appendField(locator, this, "type", buffer, theType, (this.type!= null));
        }
        {
            EquipmentOwnershipTypeType theOwnership;
            theOwnership = this.getOwnership();
            strategy.appendField(locator, this, "ownership", buffer, theOwnership, (this.ownership!= null));
        }
        {
            String theStatus;
            theStatus = this.getStatus();
            strategy.appendField(locator, this, "status", buffer, theStatus, (this.status!= null));
        }
        {
            String theModelFamily;
            theModelFamily = this.getModelFamily();
            strategy.appendField(locator, this, "modelFamily", buffer, theModelFamily, (this.modelFamily!= null));
        }
        {
            String theModelFamilyDescription;
            theModelFamilyDescription = this.getModelFamilyDescription();
            strategy.appendField(locator, this, "modelFamilyDescription", buffer, theModelFamilyDescription, (this.modelFamilyDescription!= null));
        }
        {
            String theModelFamilyIdentifier;
            theModelFamilyIdentifier = this.getModelFamilyIdentifier();
            strategy.appendField(locator, this, "modelFamilyIdentifier", buffer, theModelFamilyIdentifier, (this.modelFamilyIdentifier!= null));
        }
        {
            String theAddress;
            theAddress = this.getAddress();
            strategy.appendField(locator, this, "address", buffer, theAddress, (this.address!= null));
        }
        {
            String theSaleType;
            theSaleType = this.getSaleType();
            strategy.appendField(locator, this, "saleType", buffer, theSaleType, (this.saleType!= null));
        }
        {
            String theReasonCode;
            theReasonCode = this.getReasonCode();
            strategy.appendField(locator, this, "reasonCode", buffer, theReasonCode, (this.reasonCode!= null));
        }
        {
            String theRaNumber;
            theRaNumber = this.getRaNumber();
            strategy.appendField(locator, this, "raNumber", buffer, theRaNumber, (this.raNumber!= null));
        }
        {
            ActivationTypeType theActivationType;
            theActivationType = this.getActivationType();
            strategy.appendField(locator, this, "activationType", buffer, theActivationType, (this.activationType!= null));
        }
        {
            String theSwapOutEquipmentModel;
            theSwapOutEquipmentModel = this.getSwapOutEquipmentModel();
            strategy.appendField(locator, this, "swapOutEquipmentModel", buffer, theSwapOutEquipmentModel, (this.swapOutEquipmentModel!= null));
        }
        return buffer;
    }

}
