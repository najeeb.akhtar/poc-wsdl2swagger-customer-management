
package dishnetwork.schemas;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.jvnet.jaxb2_commons.lang.Equals2;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy2;
import org.jvnet.jaxb2_commons.lang.HashCode2;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy2;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString2;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy2;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for PaymentMethodType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="PaymentMethodType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="creditCardDetail" type="{http://www.dishnetwork.com/schema/Payment/PaymentCardDetail/2022_05_12}PaymentCardType" minOccurs="0"/&gt;
 *         &lt;element name="eftDetail" type="{http://www.dishnetwork.com/schema/Payment/EFTDetails/2011_04_01}EFTDetailsType" minOccurs="0"/&gt;
 *         &lt;element name="name" type="{http://www.dishnetwork.com/schema/Party/IndividualName/2010_12_12}IndividualNameType" minOccurs="0"/&gt;
 *         &lt;element name="cashPaymentFlag" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="reuseFlag" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaymentMethodTypeSubmitOrderOneTime", propOrder = {
    "creditCardDetail",
    "eftDetail",
    "name",
    "cashPaymentFlag",
    "reuseFlag"
})
@Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
public class PaymentMethodTypeSubmitOrderOneTime implements Equals2, HashCode2, ToString2
{

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected PaymentCardTypeSubmitOrder creditCardDetail;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected EFTDetailsType eftDetail;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected IndividualNameType name;
    //@XmlElement(defaultValue = "false")
    @JsonProperty(defaultValue = "false")
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String cashPaymentFlag;
    //@XmlElement(required = true)
    @JsonProperty(required = true)
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String reuseFlag;

    /**
     * Default no-arg constructor
     *
     */
    public PaymentMethodTypeSubmitOrderOneTime() {
        super();
    }

    /**
     * Fully-initialising value constructor
     *
     */
    public PaymentMethodTypeSubmitOrderOneTime(final PaymentCardTypeSubmitOrder creditCardDetail, final EFTDetailsType eftDetail, final IndividualNameType name, final String cashPaymentFlag, final String reuseFlag) {
        this.creditCardDetail = creditCardDetail;
        this.eftDetail = eftDetail;
        this.name = name;
        this.cashPaymentFlag = cashPaymentFlag;
        this.reuseFlag = reuseFlag;
    }

    /**
     * Gets the value of the creditCardDetail property.
     *
     * @return
     *     possible object is
     *     {@link PaymentCardTypeSubmitOrder }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public PaymentCardTypeSubmitOrder getCreditCardDetail() {
        return creditCardDetail;
    }

    /**
     * Sets the value of the creditCardDetail property.
     *
     * @param value
     *     allowed object is
     *     {@link PaymentCardTypeSubmitOrder }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setCreditCardDetail(PaymentCardTypeSubmitOrder value) {
        this.creditCardDetail = value;
    }

    /**
     * Gets the value of the eftDetail property.
     *
     * @return
     *     possible object is
     *     {@link EFTDetailsType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public EFTDetailsType getEftDetail() {
        return eftDetail;
    }

    /**
     * Sets the value of the eftDetail property.
     *
     * @param value
     *     allowed object is
     *     {@link EFTDetailsType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setEftDetail(EFTDetailsType value) {
        this.eftDetail = value;
    }

    /**
     * Gets the value of the name property.
     *
     * @return
     *     possible object is
     *     {@link IndividualNameType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public IndividualNameType getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     *
     * @param value
     *     allowed object is
     *     {@link IndividualNameType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setName(IndividualNameType value) {
        this.name = value;
    }

    /**
     * Gets the value of the cashPaymentFlag property.
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getCashPaymentFlag() {
        return cashPaymentFlag;
    }

    /**
     * Sets the value of the cashPaymentFlag property.
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setCashPaymentFlag(String value) {
        this.cashPaymentFlag = value;
    }

    /**
     * Gets the value of the reuseFlag property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getReuseFlag() {
        return reuseFlag;
    }

    /**
     * Sets the value of the reuseFlag property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setReuseFlag(String value) {
        this.reuseFlag = value;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy2 strategy) {
        if ((object == null)||(this.getClass()!= object.getClass())) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final PaymentMethodTypeSubmitOrderOneTime that = ((PaymentMethodTypeSubmitOrderOneTime) object);
        {
            PaymentCardTypeSubmitOrder lhsCreditCardDetail;
            lhsCreditCardDetail = this.getCreditCardDetail();
            PaymentCardTypeSubmitOrder rhsCreditCardDetail;
            rhsCreditCardDetail = that.getCreditCardDetail();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "creditCardDetail", lhsCreditCardDetail), LocatorUtils.property(thatLocator, "creditCardDetail", rhsCreditCardDetail), lhsCreditCardDetail, rhsCreditCardDetail, (this.creditCardDetail!= null), (that.creditCardDetail!= null))) {
                return false;
            }
        }
        {
            EFTDetailsType lhsEftDetail;
            lhsEftDetail = this.getEftDetail();
            EFTDetailsType rhsEftDetail;
            rhsEftDetail = that.getEftDetail();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "eftDetail", lhsEftDetail), LocatorUtils.property(thatLocator, "eftDetail", rhsEftDetail), lhsEftDetail, rhsEftDetail, (this.eftDetail!= null), (that.eftDetail!= null))) {
                return false;
            }
        }
        {
            IndividualNameType lhsName;
            lhsName = this.getName();
            IndividualNameType rhsName;
            rhsName = that.getName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "name", lhsName), LocatorUtils.property(thatLocator, "name", rhsName), lhsName, rhsName, (this.name!= null), (that.name!= null))) {
                return false;
            }
        }
        {
            String lhsCashPaymentFlag;
            lhsCashPaymentFlag = this.getCashPaymentFlag();
            String rhsCashPaymentFlag;
            rhsCashPaymentFlag = that.getCashPaymentFlag();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "cashPaymentFlag", lhsCashPaymentFlag), LocatorUtils.property(thatLocator, "cashPaymentFlag", rhsCashPaymentFlag), lhsCashPaymentFlag, rhsCashPaymentFlag, true, true)) {
                return false;
            }
        }
        {
            String lhsReuseFlag;
            lhsReuseFlag = this.getReuseFlag();
            String rhsReuseFlag;
            rhsReuseFlag = that.getReuseFlag();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "reuseFlag", lhsReuseFlag), LocatorUtils.property(thatLocator, "reuseFlag", rhsReuseFlag), lhsReuseFlag, rhsReuseFlag, (this.reuseFlag!= null), (that.reuseFlag!= null))) {
                return false;
            }
        }
        return true;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(Object object) {
        final EqualsStrategy2 strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode(ObjectLocator locator, HashCodeStrategy2 strategy) {
        int currentHashCode = 1;
        {
            PaymentCardTypeSubmitOrder theCreditCardDetail;
            theCreditCardDetail = this.getCreditCardDetail();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "creditCardDetail", theCreditCardDetail), currentHashCode, theCreditCardDetail, (this.creditCardDetail!= null));
        }
        {
            EFTDetailsType theEftDetail;
            theEftDetail = this.getEftDetail();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "eftDetail", theEftDetail), currentHashCode, theEftDetail, (this.eftDetail!= null));
        }
        {
            IndividualNameType theName;
            theName = this.getName();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "name", theName), currentHashCode, theName, (this.name!= null));
        }
        {
            String theCashPaymentFlag;
            theCashPaymentFlag = this.getCashPaymentFlag();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "cashPaymentFlag", theCashPaymentFlag), currentHashCode, theCashPaymentFlag, true);
        }
        {
            String theReuseFlag;
            theReuseFlag = this.getReuseFlag();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "reuseFlag", theReuseFlag), currentHashCode, theReuseFlag, (this.reuseFlag!= null));
        }
        return currentHashCode;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode() {
        final HashCodeStrategy2 strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String toString() {
        final ToStringStrategy2 strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        {
            PaymentCardTypeSubmitOrder theCreditCardDetail;
            theCreditCardDetail = this.getCreditCardDetail();
            strategy.appendField(locator, this, "creditCardDetail", buffer, theCreditCardDetail, (this.creditCardDetail!= null));
        }
        {
            EFTDetailsType theEftDetail;
            theEftDetail = this.getEftDetail();
            strategy.appendField(locator, this, "eftDetail", buffer, theEftDetail, (this.eftDetail!= null));
        }
        {
            IndividualNameType theName;
            theName = this.getName();
            strategy.appendField(locator, this, "name", buffer, theName, (this.name!= null));
        }
        {
            String theCashPaymentFlag;
            theCashPaymentFlag = this.getCashPaymentFlag();
            strategy.appendField(locator, this, "cashPaymentFlag", buffer, theCashPaymentFlag, true);
        }
        {
            String theReuseFlag;
            theReuseFlag = this.getReuseFlag();
            strategy.appendField(locator, this, "reuseFlag", buffer, theReuseFlag, (this.reuseFlag!= null));
        }
        return buffer;
    }

}
