
package dishnetwork.schemas;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EquipmentTypeType.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <pre>
 * &lt;simpleType name="EquipmentTypeType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="RECEIVER"/&gt;
 *     &lt;enumeration value="SMARTCARD"/&gt;
 *     &lt;enumeration value="TRANSCEIVER"/&gt;
 *     &lt;enumeration value="MODEM"/&gt;
 *     &lt;enumeration value="DTV_CONVERTER"/&gt;
 *     &lt;enumeration value="DTV_SMARTCARD"/&gt;
 *     &lt;enumeration value="STARBAND_RECEIVER"/&gt;
 *     &lt;enumeration value="WIRELESS_DEVICE"/&gt;
 *     &lt;enumeration value="WIRELESS_SMARTCARD"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 *
 */
@XmlType(name = "EquipmentTypeType", namespace = "http://www.dishnetwork.com/schema/common/EquipmentType/2014_06_13")
@XmlEnum
@Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-12T08:55:33-06:00")
public enum EquipmentTypeType {

    RECEIVER,
    SMARTCARD,
    TRANSCEIVER,
    MODEM,
    DTV_CONVERTER,
    DTV_SMARTCARD,
    STARBAND_RECEIVER,
    WIRELESS_DEVICE,
    WIRELESS_SMARTCARD;

    public String value() {
        return name();
    }

    public static EquipmentTypeType fromValue(String v) {
        return valueOf(v);
    }

}
