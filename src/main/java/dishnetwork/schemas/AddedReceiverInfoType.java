
package dishnetwork.schemas;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.jvnet.jaxb2_commons.lang.Equals2;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy2;
import org.jvnet.jaxb2_commons.lang.HashCode2;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy2;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString2;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy2;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for AddedReceiverInfoType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="AddedReceiverInfoType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="receiverModelGroup" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="receiverOwnership" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AddedReceiverInfoType", propOrder = {
    "receiverModelGroup",
    "receiverOwnership"
})
@Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
public class AddedReceiverInfoType implements Equals2, HashCode2, ToString2
{

    //@XmlElement(required = true)
    @JsonProperty(required = true)
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String receiverModelGroup;
    //@XmlElement(required = true)
    @JsonProperty(required = true)
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String receiverOwnership;

    /**
     * Default no-arg constructor
     *
     */
    public AddedReceiverInfoType() {
        super();
    }

    /**
     * Fully-initialising value constructor
     *
     */
    public AddedReceiverInfoType(final String receiverModelGroup, final String receiverOwnership) {
        this.receiverModelGroup = receiverModelGroup;
        this.receiverOwnership = receiverOwnership;
    }

    /**
     * Gets the value of the receiverModelGroup property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getReceiverModelGroup() {
        return receiverModelGroup;
    }

    /**
     * Sets the value of the receiverModelGroup property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setReceiverModelGroup(String value) {
        this.receiverModelGroup = value;
    }

    /**
     * Gets the value of the receiverOwnership property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getReceiverOwnership() {
        return receiverOwnership;
    }

    /**
     * Sets the value of the receiverOwnership property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setReceiverOwnership(String value) {
        this.receiverOwnership = value;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy2 strategy) {
        if ((object == null)||(this.getClass()!= object.getClass())) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final AddedReceiverInfoType that = ((AddedReceiverInfoType) object);
        {
            String lhsReceiverModelGroup;
            lhsReceiverModelGroup = this.getReceiverModelGroup();
            String rhsReceiverModelGroup;
            rhsReceiverModelGroup = that.getReceiverModelGroup();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "receiverModelGroup", lhsReceiverModelGroup), LocatorUtils.property(thatLocator, "receiverModelGroup", rhsReceiverModelGroup), lhsReceiverModelGroup, rhsReceiverModelGroup, (this.receiverModelGroup!= null), (that.receiverModelGroup!= null))) {
                return false;
            }
        }
        {
            String lhsReceiverOwnership;
            lhsReceiverOwnership = this.getReceiverOwnership();
            String rhsReceiverOwnership;
            rhsReceiverOwnership = that.getReceiverOwnership();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "receiverOwnership", lhsReceiverOwnership), LocatorUtils.property(thatLocator, "receiverOwnership", rhsReceiverOwnership), lhsReceiverOwnership, rhsReceiverOwnership, (this.receiverOwnership!= null), (that.receiverOwnership!= null))) {
                return false;
            }
        }
        return true;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(Object object) {
        final EqualsStrategy2 strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode(ObjectLocator locator, HashCodeStrategy2 strategy) {
        int currentHashCode = 1;
        {
            String theReceiverModelGroup;
            theReceiverModelGroup = this.getReceiverModelGroup();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "receiverModelGroup", theReceiverModelGroup), currentHashCode, theReceiverModelGroup, (this.receiverModelGroup!= null));
        }
        {
            String theReceiverOwnership;
            theReceiverOwnership = this.getReceiverOwnership();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "receiverOwnership", theReceiverOwnership), currentHashCode, theReceiverOwnership, (this.receiverOwnership!= null));
        }
        return currentHashCode;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode() {
        final HashCodeStrategy2 strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String toString() {
        final ToStringStrategy2 strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        {
            String theReceiverModelGroup;
            theReceiverModelGroup = this.getReceiverModelGroup();
            strategy.appendField(locator, this, "receiverModelGroup", buffer, theReceiverModelGroup, (this.receiverModelGroup!= null));
        }
        {
            String theReceiverOwnership;
            theReceiverOwnership = this.getReceiverOwnership();
            strategy.appendField(locator, this, "receiverOwnership", buffer, theReceiverOwnership, (this.receiverOwnership!= null));
        }
        return buffer;
    }

}
