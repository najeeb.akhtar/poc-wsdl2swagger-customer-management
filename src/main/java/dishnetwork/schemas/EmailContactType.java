
package dishnetwork.schemas;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.jvnet.jaxb2_commons.lang.Equals2;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy2;
import org.jvnet.jaxb2_commons.lang.HashCode2;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy2;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString2;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy2;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for EmailContactType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="EmailContactType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="emailAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="declineDateFlag" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="preferenceList" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="preference" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EmailContactType", propOrder = {
    "emailAddress",
    "declineDateFlag",
    "preferenceList"
})
@Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
public class EmailContactType implements Equals2, HashCode2, ToString2
{

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String emailAddress;
    //@XmlElement(defaultValue = "false")
    @JsonProperty(defaultValue = "false")
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String declineDateFlag;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected PreferenceList preferenceList;

    /**
     * Default no-arg constructor
     *
     */
    public EmailContactType() {
        super();
    }

    /**
     * Fully-initialising value constructor
     *
     */
    public EmailContactType(final String emailAddress, final String declineDateFlag, final PreferenceList preferenceList) {
        this.emailAddress = emailAddress;
        this.declineDateFlag = declineDateFlag;
        this.preferenceList = preferenceList;
    }

    /**
     * Gets the value of the emailAddress property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getEmailAddress() {
        return emailAddress;
    }

    /**
     * Sets the value of the emailAddress property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setEmailAddress(String value) {
        this.emailAddress = value;
    }

    /**
     * Gets the value of the declineDateFlag property.
     *
     * @return
     *     possible object is
     *     {@link Boolean }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String isDeclineDateFlag() {
        return declineDateFlag;
    }

    /**
     * Sets the value of the declineDateFlag property.
     *
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setDeclineDateFlag(String value) {
        this.declineDateFlag = value;
    }

    /**
     * Gets the value of the preferenceList property.
     *
     * @return
     *     possible object is
     *     {@link PreferenceList }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public PreferenceList getPreferenceList() {
        return preferenceList;
    }

    /**
     * Sets the value of the preferenceList property.
     *
     * @param value
     *     allowed object is
     *     {@link PreferenceList }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setPreferenceList(PreferenceList value) {
        this.preferenceList = value;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy2 strategy) {
        if ((object == null)||(this.getClass()!= object.getClass())) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final EmailContactType that = ((EmailContactType) object);
        {
            String lhsEmailAddress;
            lhsEmailAddress = this.getEmailAddress();
            String rhsEmailAddress;
            rhsEmailAddress = that.getEmailAddress();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "emailAddress", lhsEmailAddress), LocatorUtils.property(thatLocator, "emailAddress", rhsEmailAddress), lhsEmailAddress, rhsEmailAddress, (this.emailAddress!= null), (that.emailAddress!= null))) {
                return false;
            }
        }
        {
            String lhsDeclineDateFlag;
            lhsDeclineDateFlag = this.isDeclineDateFlag();
            String rhsDeclineDateFlag;
            rhsDeclineDateFlag = that.isDeclineDateFlag();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "declineDateFlag", lhsDeclineDateFlag), LocatorUtils.property(thatLocator, "declineDateFlag", rhsDeclineDateFlag), lhsDeclineDateFlag, rhsDeclineDateFlag, (this.declineDateFlag!= null), (that.declineDateFlag!= null))) {
                return false;
            }
        }
        {
            PreferenceList lhsPreferenceList;
            lhsPreferenceList = this.getPreferenceList();
            PreferenceList rhsPreferenceList;
            rhsPreferenceList = that.getPreferenceList();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "preferenceList", lhsPreferenceList), LocatorUtils.property(thatLocator, "preferenceList", rhsPreferenceList), lhsPreferenceList, rhsPreferenceList, (this.preferenceList!= null), (that.preferenceList!= null))) {
                return false;
            }
        }
        return true;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(Object object) {
        final EqualsStrategy2 strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode(ObjectLocator locator, HashCodeStrategy2 strategy) {
        int currentHashCode = 1;
        {
            String theEmailAddress;
            theEmailAddress = this.getEmailAddress();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "emailAddress", theEmailAddress), currentHashCode, theEmailAddress, (this.emailAddress!= null));
        }
        {
            String theDeclineDateFlag;
            theDeclineDateFlag = this.isDeclineDateFlag();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "declineDateFlag", theDeclineDateFlag), currentHashCode, theDeclineDateFlag, (this.declineDateFlag!= null));
        }
        {
            PreferenceList thePreferenceList;
            thePreferenceList = this.getPreferenceList();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "preferenceList", thePreferenceList), currentHashCode, thePreferenceList, (this.preferenceList!= null));
        }
        return currentHashCode;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode() {
        final HashCodeStrategy2 strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String toString() {
        final ToStringStrategy2 strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        {
            String theEmailAddress;
            theEmailAddress = this.getEmailAddress();
            strategy.appendField(locator, this, "emailAddress", buffer, theEmailAddress, (this.emailAddress!= null));
        }
        {
            String theDeclineDateFlag;
            theDeclineDateFlag = this.isDeclineDateFlag();
            strategy.appendField(locator, this, "declineDateFlag", buffer, theDeclineDateFlag, (this.declineDateFlag!= null));
        }
        {
            PreferenceList thePreferenceList;
            thePreferenceList = this.getPreferenceList();
            strategy.appendField(locator, this, "preferenceList", buffer, thePreferenceList, (this.preferenceList!= null));
        }
        return buffer;
    }


}
