package dishnetwork.schemas;

import org.jvnet.jaxb2_commons.lang.*;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="previousAccountName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="previousProviderAccountId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="previousPersonalIdentificationNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="previousBillingZipCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="attributeList" type="{http://www.dishnetwork.com/schema/common/AttributeList/2015_10_08}AttributeListTypeType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "previousAccountName",
    "previousProviderAccountId",
    "previousPersonalIdentificationNumber",
    "previousBillingZipCode",
    "attributeList"
})
@Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
public class PortInfo implements Equals2, HashCode2, ToString2 {

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String previousAccountName;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String previousProviderAccountId;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String previousPersonalIdentificationNumber;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String previousBillingZipCode;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected AttributeListTypeType attributeList;

    /**
     * Default no-arg constructor
     */
    public PortInfo() {
        super();
    }

    /**
     * Fully-initialising value constructor
     */
    public PortInfo(final String previousAccountName, final String previousProviderAccountId, final String previousPersonalIdentificationNumber, final String previousBillingZipCode, final AttributeListTypeType attributeList) {
        this.previousAccountName = previousAccountName;
        this.previousProviderAccountId = previousProviderAccountId;
        this.previousPersonalIdentificationNumber = previousPersonalIdentificationNumber;
        this.previousBillingZipCode = previousBillingZipCode;
        this.attributeList = attributeList;
    }

    /**
     * Gets the value of the previousAccountName property.
     *
     * @return possible object is
     * {@link String }
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getPreviousAccountName() {
        return previousAccountName;
    }

    /**
     * Sets the value of the previousAccountName property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setPreviousAccountName(String value) {
        this.previousAccountName = value;
    }

    /**
     * Gets the value of the previousProviderAccountId property.
     *
     * @return possible object is
     * {@link String }
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getPreviousProviderAccountId() {
        return previousProviderAccountId;
    }

    /**
     * Sets the value of the previousProviderAccountId property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setPreviousProviderAccountId(String value) {
        this.previousProviderAccountId = value;
    }

    /**
     * Gets the value of the previousPersonalIdentificationNumber property.
     *
     * @return possible object is
     * {@link String }
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getPreviousPersonalIdentificationNumber() {
        return previousPersonalIdentificationNumber;
    }

    /**
     * Sets the value of the previousPersonalIdentificationNumber property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setPreviousPersonalIdentificationNumber(String value) {
        this.previousPersonalIdentificationNumber = value;
    }

    /**
     * Gets the value of the previousBillingZipCode property.
     *
     * @return possible object is
     * {@link String }
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getPreviousBillingZipCode() {
        return previousBillingZipCode;
    }

    /**
     * Sets the value of the previousBillingZipCode property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setPreviousBillingZipCode(String value) {
        this.previousBillingZipCode = value;
    }

    /**
     * Gets the value of the attributeList property.
     *
     * @return possible object is
     * {@link AttributeListTypeType }
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public AttributeListTypeType getAttributeList() {
        return attributeList;
    }

    /**
     * Sets the value of the attributeList property.
     *
     * @param value allowed object is
     *              {@link AttributeListTypeType }
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setAttributeList(AttributeListTypeType value) {
        this.attributeList = value;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy2 strategy) {
        if ((object == null) || (this.getClass() != object.getClass())) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final PortInfo that = ((PortInfo) object);
        {
            String lhsPreviousAccountName;
            lhsPreviousAccountName = this.getPreviousAccountName();
            String rhsPreviousAccountName;
            rhsPreviousAccountName = that.getPreviousAccountName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "previousAccountName", lhsPreviousAccountName), LocatorUtils.property(thatLocator, "previousAccountName", rhsPreviousAccountName), lhsPreviousAccountName, rhsPreviousAccountName, (this.previousAccountName != null), (that.previousAccountName != null))) {
                return false;
            }
        }
        {
            String lhsPreviousProviderAccountId;
            lhsPreviousProviderAccountId = this.getPreviousProviderAccountId();
            String rhsPreviousProviderAccountId;
            rhsPreviousProviderAccountId = that.getPreviousProviderAccountId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "previousProviderAccountId", lhsPreviousProviderAccountId), LocatorUtils.property(thatLocator, "previousProviderAccountId", rhsPreviousProviderAccountId), lhsPreviousProviderAccountId, rhsPreviousProviderAccountId, (this.previousProviderAccountId != null), (that.previousProviderAccountId != null))) {
                return false;
            }
        }
        {
            String lhsPreviousPersonalIdentificationNumber;
            lhsPreviousPersonalIdentificationNumber = this.getPreviousPersonalIdentificationNumber();
            String rhsPreviousPersonalIdentificationNumber;
            rhsPreviousPersonalIdentificationNumber = that.getPreviousPersonalIdentificationNumber();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "previousPersonalIdentificationNumber", lhsPreviousPersonalIdentificationNumber), LocatorUtils.property(thatLocator, "previousPersonalIdentificationNumber", rhsPreviousPersonalIdentificationNumber), lhsPreviousPersonalIdentificationNumber, rhsPreviousPersonalIdentificationNumber, (this.previousPersonalIdentificationNumber != null), (that.previousPersonalIdentificationNumber != null))) {
                return false;
            }
        }
        {
            String lhsPreviousBillingZipCode;
            lhsPreviousBillingZipCode = this.getPreviousBillingZipCode();
            String rhsPreviousBillingZipCode;
            rhsPreviousBillingZipCode = that.getPreviousBillingZipCode();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "previousBillingZipCode", lhsPreviousBillingZipCode), LocatorUtils.property(thatLocator, "previousBillingZipCode", rhsPreviousBillingZipCode), lhsPreviousBillingZipCode, rhsPreviousBillingZipCode, (this.previousBillingZipCode != null), (that.previousBillingZipCode != null))) {
                return false;
            }
        }
        {
            AttributeListTypeType lhsAttributeList;
            lhsAttributeList = this.getAttributeList();
            AttributeListTypeType rhsAttributeList;
            rhsAttributeList = that.getAttributeList();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "attributeList", lhsAttributeList), LocatorUtils.property(thatLocator, "attributeList", rhsAttributeList), lhsAttributeList, rhsAttributeList, (this.attributeList != null), (that.attributeList != null))) {
                return false;
            }
        }
        return true;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(Object object) {
        final EqualsStrategy2 strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode(ObjectLocator locator, HashCodeStrategy2 strategy) {
        int currentHashCode = 1;
        {
            String thePreviousAccountName;
            thePreviousAccountName = this.getPreviousAccountName();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "previousAccountName", thePreviousAccountName), currentHashCode, thePreviousAccountName, (this.previousAccountName != null));
        }
        {
            String thePreviousProviderAccountId;
            thePreviousProviderAccountId = this.getPreviousProviderAccountId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "previousProviderAccountId", thePreviousProviderAccountId), currentHashCode, thePreviousProviderAccountId, (this.previousProviderAccountId != null));
        }
        {
            String thePreviousPersonalIdentificationNumber;
            thePreviousPersonalIdentificationNumber = this.getPreviousPersonalIdentificationNumber();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "previousPersonalIdentificationNumber", thePreviousPersonalIdentificationNumber), currentHashCode, thePreviousPersonalIdentificationNumber, (this.previousPersonalIdentificationNumber != null));
        }
        {
            String thePreviousBillingZipCode;
            thePreviousBillingZipCode = this.getPreviousBillingZipCode();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "previousBillingZipCode", thePreviousBillingZipCode), currentHashCode, thePreviousBillingZipCode, (this.previousBillingZipCode != null));
        }
        {
            AttributeListTypeType theAttributeList;
            theAttributeList = this.getAttributeList();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "attributeList", theAttributeList), currentHashCode, theAttributeList, (this.attributeList != null));
        }
        return currentHashCode;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode() {
        final HashCodeStrategy2 strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String toString() {
        final ToStringStrategy2 strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        {
            String thePreviousAccountName;
            thePreviousAccountName = this.getPreviousAccountName();
            strategy.appendField(locator, this, "previousAccountName", buffer, thePreviousAccountName, (this.previousAccountName != null));
        }
        {
            String thePreviousProviderAccountId;
            thePreviousProviderAccountId = this.getPreviousProviderAccountId();
            strategy.appendField(locator, this, "previousProviderAccountId", buffer, thePreviousProviderAccountId, (this.previousProviderAccountId != null));
        }
        {
            String thePreviousPersonalIdentificationNumber;
            thePreviousPersonalIdentificationNumber = this.getPreviousPersonalIdentificationNumber();
            strategy.appendField(locator, this, "previousPersonalIdentificationNumber", buffer, thePreviousPersonalIdentificationNumber, (this.previousPersonalIdentificationNumber != null));
        }
        {
            String thePreviousBillingZipCode;
            thePreviousBillingZipCode = this.getPreviousBillingZipCode();
            strategy.appendField(locator, this, "previousBillingZipCode", buffer, thePreviousBillingZipCode, (this.previousBillingZipCode != null));
        }
        {
            AttributeListTypeType theAttributeList;
            theAttributeList = this.getAttributeList();
            strategy.appendField(locator, this, "attributeList", buffer, theAttributeList, (this.attributeList != null));
        }
        return buffer;
    }

}
