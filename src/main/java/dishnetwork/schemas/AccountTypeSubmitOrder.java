
package dishnetwork.schemas;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.jvnet.jaxb2_commons.lang.Equals2;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy2;
import org.jvnet.jaxb2_commons.lang.HashCode2;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy2;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString2;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy2;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * Account Details
 *
 * <p>Java class for AccountType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="AccountType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="accountId" type="{http://www.dishnetwork.com/schema/Customer/AccountId/2011_04_01}AccountIdType" minOccurs="0"/&gt;
 *         &lt;element name="billingAccountNumber" type="{http://www.dishnetwork.com/schema/Customer/BillingAccountNumber/2013_05_22}BillingAccountNumberType" minOccurs="0"/&gt;
 *         &lt;element name="tailgaterAccountFlag" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="hardCopyStatementFlag" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="statementLanguage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="estViewOccupancy" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *         &lt;element name="fireOccupancy" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *         &lt;element name="accountType" type="{http://www.dishnetwork.com/schema/common/AccountTypeType/2013_05_22}AccountTypeType"/&gt;
 *         &lt;element name="billTo" type="{http://www.dishnetwork.com/schema/CustomerManagement/BillTo/2016_03_24}BillToType" minOccurs="0"/&gt;
 *         &lt;element name="partnerAccountInfo" type="{http://www.dishnetwork.com/schema/CustomerManagement/PartnerAccountInfo/2011_04_01}PartnerAccountInfoType" minOccurs="0"/&gt;
 *         &lt;element name="dunningGroup" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="dunningGroupPriority" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="commercialAccountInfo" type="{http://www.dishnetwork.com/schema/common/CommAcctInfo/2014_06_13}CommAcctInfoType" minOccurs="0"/&gt;
 *         &lt;element name="taxGroup" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="vipCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="notes" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="accountAttributes" type="{http://www.dishnetwork.com/schema/common/AttributeList/2013_09_19}AttributeListType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AccountTypeSubmitOrder", propOrder = {
    "accountId",
    "billingAccountNumber",
    "tailgaterAccountFlag",
    "hardCopyStatementFlag",
    "statementLanguage",
    "estViewOccupancy",
    "fireOccupancy",
    "accountType",
    "billTo",
    "partnerAccountInfo",
    "dunningGroup",
    "dunningGroupPriority",
    "commercialAccountInfo",
    "taxGroup",
    "vipCode",
    "notes",
    "accountAttributes"
})
@Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
public class AccountTypeSubmitOrder implements Equals2, HashCode2, ToString2
{

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected AccountIdType accountId;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected BillingAccountNumberType billingAccountNumber;
    //@XmlElement(defaultValue = "false")
    @JsonProperty(defaultValue = "false")
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String tailgaterAccountFlag;
    //@XmlElement(defaultValue = "false")
    @JsonProperty(defaultValue = "false")
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String hardCopyStatementFlag;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String statementLanguage;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String estViewOccupancy;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String fireOccupancy;
    //@XmlElement(required = true)
    @JsonProperty(required = true)
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected AccountTypeType accountType;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected BillToType billTo;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected PartnerAccountInfoType partnerAccountInfo;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String dunningGroup;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String dunningGroupPriority;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected CommAcctInfoTypeSubmitOrder commercialAccountInfo;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String taxGroup;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String vipCode;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected List<String> notes;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected AttributeListType accountAttributes;

    /**
     * Default no-arg constructor
     *
     */
    public AccountTypeSubmitOrder() {
        super();
    }

    /**
     * Fully-initialising value constructor
     *
     */
    public AccountTypeSubmitOrder(final AccountIdType accountId, final BillingAccountNumberType billingAccountNumber, final String tailgaterAccountFlag, final String hardCopyStatementFlag, final String statementLanguage, final String estViewOccupancy, final String fireOccupancy, final AccountTypeType accountType, final BillToType billTo, final PartnerAccountInfoType partnerAccountInfo, final String dunningGroup, final String dunningGroupPriority, final CommAcctInfoTypeSubmitOrder commercialAccountInfo, final String taxGroup, final String vipCode, final List<String> notes, final AttributeListType accountAttributes) {
        this.accountId = accountId;
        this.billingAccountNumber = billingAccountNumber;
        this.tailgaterAccountFlag = tailgaterAccountFlag;
        this.hardCopyStatementFlag = hardCopyStatementFlag;
        this.statementLanguage = statementLanguage;
        this.estViewOccupancy = estViewOccupancy;
        this.fireOccupancy = fireOccupancy;
        this.accountType = accountType;
        this.billTo = billTo;
        this.partnerAccountInfo = partnerAccountInfo;
        this.dunningGroup = dunningGroup;
        this.dunningGroupPriority = dunningGroupPriority;
        this.commercialAccountInfo = commercialAccountInfo;
        this.taxGroup = taxGroup;
        this.vipCode = vipCode;
        this.notes = notes;
        this.accountAttributes = accountAttributes;
    }

    /**
     * Gets the value of the accountId property.
     *
     * @return
     *     possible object is
     *     {@link AccountIdType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public AccountIdType getAccountId() {
        return accountId;
    }

    /**
     * Sets the value of the accountId property.
     *
     * @param value
     *     allowed object is
     *     {@link AccountIdType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setAccountId(AccountIdType value) {
        this.accountId = value;
    }

    /**
     * Gets the value of the billingAccountNumber property.
     *
     * @return
     *     possible object is
     *     {@link BillingAccountNumberType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public BillingAccountNumberType getBillingAccountNumber() {
        return billingAccountNumber;
    }

    /**
     * Sets the value of the billingAccountNumber property.
     *
     * @param value
     *     allowed object is
     *     {@link BillingAccountNumberType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setBillingAccountNumber(BillingAccountNumberType value) {
        this.billingAccountNumber = value;
    }

    /**
     * Gets the value of the tailgaterAccountFlag property.
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getTailgaterAccountFlag() {
        return tailgaterAccountFlag;
    }

    /**
     * Sets the value of the tailgaterAccountFlag property.
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setTailgaterAccountFlag(String value) {
        this.tailgaterAccountFlag = value;
    }

    /**
     * Gets the value of the hardCopyStatementFlag property.
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getHardCopyStatementFlag() {
        return hardCopyStatementFlag;
    }

    /**
     * Sets the value of the hardCopyStatementFlag property.
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setHardCopyStatementFlag(String value) {
        this.hardCopyStatementFlag = value;
    }

    /**
     * Gets the value of the statementLanguage property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getStatementLanguage() {
        return statementLanguage;
    }

    /**
     * Sets the value of the statementLanguage property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setStatementLanguage(String value) {
        this.statementLanguage = value;
    }

    /**
     * Gets the value of the estViewOccupancy property.
     *
     * @return
     *     possible object is
     *     {@link BigInteger }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getEstViewOccupancy() {
        return estViewOccupancy;
    }

    /**
     * Sets the value of the estViewOccupancy property.
     *
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setEstViewOccupancy(String value) {
        this.estViewOccupancy = value;
    }

    /**
     * Gets the value of the fireOccupancy property.
     *
     * @return
     *     possible object is
     *     {@link BigInteger }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getFireOccupancy() {
        return fireOccupancy;
    }

    /**
     * Sets the value of the fireOccupancy property.
     *
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setFireOccupancy(String value) {
        this.fireOccupancy = value;
    }

    /**
     * Gets the value of the accountType property.
     *
     * @return
     *     possible object is
     *     {@link AccountTypeType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public AccountTypeType getAccountType() {
        return accountType;
    }

    /**
     * Sets the value of the accountType property.
     *
     * @param value
     *     allowed object is
     *     {@link AccountTypeType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setAccountType(AccountTypeType value) {
        this.accountType = value;
    }

    /**
     * Gets the value of the billTo property.
     *
     * @return
     *     possible object is
     *     {@link BillToType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public BillToType getBillTo() {
        return billTo;
    }

    /**
     * Sets the value of the billTo property.
     *
     * @param value
     *     allowed object is
     *     {@link BillToType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setBillTo(BillToType value) {
        this.billTo = value;
    }

    /**
     * Gets the value of the partnerAccountInfo property.
     *
     * @return
     *     possible object is
     *     {@link PartnerAccountInfoType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public PartnerAccountInfoType getPartnerAccountInfo() {
        return partnerAccountInfo;
    }

    /**
     * Sets the value of the partnerAccountInfo property.
     *
     * @param value
     *     allowed object is
     *     {@link PartnerAccountInfoType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setPartnerAccountInfo(PartnerAccountInfoType value) {
        this.partnerAccountInfo = value;
    }

    /**
     * Gets the value of the dunningGroup property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getDunningGroup() {
        return dunningGroup;
    }

    /**
     * Sets the value of the dunningGroup property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setDunningGroup(String value) {
        this.dunningGroup = value;
    }

    /**
     * Gets the value of the dunningGroupPriority property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getDunningGroupPriority() {
        return dunningGroupPriority;
    }

    /**
     * Sets the value of the dunningGroupPriority property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setDunningGroupPriority(String value) {
        this.dunningGroupPriority = value;
    }

    /**
     * Gets the value of the commercialAccountInfo property.
     *
     * @return
     *     possible object is
     *     {@link CommAcctInfoTypeSubmitOrder }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public CommAcctInfoTypeSubmitOrder getCommercialAccountInfo() {
        return commercialAccountInfo;
    }

    /**
     * Sets the value of the commercialAccountInfo property.
     *
     * @param value
     *     allowed object is
     *     {@link CommAcctInfoTypeSubmitOrder }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setCommercialAccountInfo(CommAcctInfoTypeSubmitOrder value) {
        this.commercialAccountInfo = value;
    }

    /**
     * Gets the value of the taxGroup property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getTaxGroup() {
        return taxGroup;
    }

    /**
     * Sets the value of the taxGroup property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setTaxGroup(String value) {
        this.taxGroup = value;
    }

    /**
     * Gets the value of the vipCode property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getVipCode() {
        return vipCode;
    }

    /**
     * Sets the value of the vipCode property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setVipCode(String value) {
        this.vipCode = value;
    }

    /**
     * Gets the value of the notes property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the notes property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getNotes().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     *
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public List<String> getNotes() {
        if (notes == null) {
            notes = new ArrayList<String>();
        }
        return this.notes;
    }

    /**
     * Gets the value of the accountAttributes property.
     *
     * @return
     *     possible object is
     *     {@link AttributeListType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public AttributeListType getAccountAttributes() {
        return accountAttributes;
    }

    /**
     * Sets the value of the accountAttributes property.
     *
     * @param value
     *     allowed object is
     *     {@link AttributeListType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setAccountAttributes(AttributeListType value) {
        this.accountAttributes = value;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy2 strategy) {
        if ((object == null)||(this.getClass()!= object.getClass())) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final AccountTypeSubmitOrder that = ((AccountTypeSubmitOrder) object);
        {
            AccountIdType lhsAccountId;
            lhsAccountId = this.getAccountId();
            AccountIdType rhsAccountId;
            rhsAccountId = that.getAccountId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "accountId", lhsAccountId), LocatorUtils.property(thatLocator, "accountId", rhsAccountId), lhsAccountId, rhsAccountId, (this.accountId!= null), (that.accountId!= null))) {
                return false;
            }
        }
        {
            BillingAccountNumberType lhsBillingAccountNumber;
            lhsBillingAccountNumber = this.getBillingAccountNumber();
            BillingAccountNumberType rhsBillingAccountNumber;
            rhsBillingAccountNumber = that.getBillingAccountNumber();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "billingAccountNumber", lhsBillingAccountNumber), LocatorUtils.property(thatLocator, "billingAccountNumber", rhsBillingAccountNumber), lhsBillingAccountNumber, rhsBillingAccountNumber, (this.billingAccountNumber!= null), (that.billingAccountNumber!= null))) {
                return false;
            }
        }
        {
            String lhsTailgaterAccountFlag;
            lhsTailgaterAccountFlag = this.getTailgaterAccountFlag();
            String rhsTailgaterAccountFlag;
            rhsTailgaterAccountFlag = that.getTailgaterAccountFlag();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "tailgaterAccountFlag", lhsTailgaterAccountFlag), LocatorUtils.property(thatLocator, "tailgaterAccountFlag", rhsTailgaterAccountFlag), lhsTailgaterAccountFlag, rhsTailgaterAccountFlag, true, true)) {
                return false;
            }
        }
        {
            String lhsHardCopyStatementFlag;
            lhsHardCopyStatementFlag = this.getHardCopyStatementFlag();
            String rhsHardCopyStatementFlag;
            rhsHardCopyStatementFlag = that.getHardCopyStatementFlag();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "hardCopyStatementFlag", lhsHardCopyStatementFlag), LocatorUtils.property(thatLocator, "hardCopyStatementFlag", rhsHardCopyStatementFlag), lhsHardCopyStatementFlag, rhsHardCopyStatementFlag, true, true)) {
                return false;
            }
        }
        {
            String lhsStatementLanguage;
            lhsStatementLanguage = this.getStatementLanguage();
            String rhsStatementLanguage;
            rhsStatementLanguage = that.getStatementLanguage();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "statementLanguage", lhsStatementLanguage), LocatorUtils.property(thatLocator, "statementLanguage", rhsStatementLanguage), lhsStatementLanguage, rhsStatementLanguage, (this.statementLanguage!= null), (that.statementLanguage!= null))) {
                return false;
            }
        }
        {
            String lhsEstViewOccupancy;
            lhsEstViewOccupancy = this.getEstViewOccupancy();
            String rhsEstViewOccupancy;
            rhsEstViewOccupancy = that.getEstViewOccupancy();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "estViewOccupancy", lhsEstViewOccupancy), LocatorUtils.property(thatLocator, "estViewOccupancy", rhsEstViewOccupancy), lhsEstViewOccupancy, rhsEstViewOccupancy, (this.estViewOccupancy!= null), (that.estViewOccupancy!= null))) {
                return false;
            }
        }
        {
            String lhsFireOccupancy;
            lhsFireOccupancy = this.getFireOccupancy();
            String rhsFireOccupancy;
            rhsFireOccupancy = that.getFireOccupancy();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "fireOccupancy", lhsFireOccupancy), LocatorUtils.property(thatLocator, "fireOccupancy", rhsFireOccupancy), lhsFireOccupancy, rhsFireOccupancy, (this.fireOccupancy!= null), (that.fireOccupancy!= null))) {
                return false;
            }
        }
        {
            AccountTypeType lhsAccountType;
            lhsAccountType = this.getAccountType();
            AccountTypeType rhsAccountType;
            rhsAccountType = that.getAccountType();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "accountType", lhsAccountType), LocatorUtils.property(thatLocator, "accountType", rhsAccountType), lhsAccountType, rhsAccountType, (this.accountType!= null), (that.accountType!= null))) {
                return false;
            }
        }
        {
            BillToType lhsBillTo;
            lhsBillTo = this.getBillTo();
            BillToType rhsBillTo;
            rhsBillTo = that.getBillTo();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "billTo", lhsBillTo), LocatorUtils.property(thatLocator, "billTo", rhsBillTo), lhsBillTo, rhsBillTo, (this.billTo!= null), (that.billTo!= null))) {
                return false;
            }
        }
        {
            PartnerAccountInfoType lhsPartnerAccountInfo;
            lhsPartnerAccountInfo = this.getPartnerAccountInfo();
            PartnerAccountInfoType rhsPartnerAccountInfo;
            rhsPartnerAccountInfo = that.getPartnerAccountInfo();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "partnerAccountInfo", lhsPartnerAccountInfo), LocatorUtils.property(thatLocator, "partnerAccountInfo", rhsPartnerAccountInfo), lhsPartnerAccountInfo, rhsPartnerAccountInfo, (this.partnerAccountInfo!= null), (that.partnerAccountInfo!= null))) {
                return false;
            }
        }
        {
            String lhsDunningGroup;
            lhsDunningGroup = this.getDunningGroup();
            String rhsDunningGroup;
            rhsDunningGroup = that.getDunningGroup();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "dunningGroup", lhsDunningGroup), LocatorUtils.property(thatLocator, "dunningGroup", rhsDunningGroup), lhsDunningGroup, rhsDunningGroup, (this.dunningGroup!= null), (that.dunningGroup!= null))) {
                return false;
            }
        }
        {
            String lhsDunningGroupPriority;
            lhsDunningGroupPriority = this.getDunningGroupPriority();
            String rhsDunningGroupPriority;
            rhsDunningGroupPriority = that.getDunningGroupPriority();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "dunningGroupPriority", lhsDunningGroupPriority), LocatorUtils.property(thatLocator, "dunningGroupPriority", rhsDunningGroupPriority), lhsDunningGroupPriority, rhsDunningGroupPriority, (this.dunningGroupPriority!= null), (that.dunningGroupPriority!= null))) {
                return false;
            }
        }
        {
            CommAcctInfoTypeSubmitOrder lhsCommercialAccountInfo;
            lhsCommercialAccountInfo = this.getCommercialAccountInfo();
            CommAcctInfoTypeSubmitOrder rhsCommercialAccountInfo;
            rhsCommercialAccountInfo = that.getCommercialAccountInfo();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "commercialAccountInfo", lhsCommercialAccountInfo), LocatorUtils.property(thatLocator, "commercialAccountInfo", rhsCommercialAccountInfo), lhsCommercialAccountInfo, rhsCommercialAccountInfo, (this.commercialAccountInfo!= null), (that.commercialAccountInfo!= null))) {
                return false;
            }
        }
        {
            String lhsTaxGroup;
            lhsTaxGroup = this.getTaxGroup();
            String rhsTaxGroup;
            rhsTaxGroup = that.getTaxGroup();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "taxGroup", lhsTaxGroup), LocatorUtils.property(thatLocator, "taxGroup", rhsTaxGroup), lhsTaxGroup, rhsTaxGroup, (this.taxGroup!= null), (that.taxGroup!= null))) {
                return false;
            }
        }
        {
            String lhsVipCode;
            lhsVipCode = this.getVipCode();
            String rhsVipCode;
            rhsVipCode = that.getVipCode();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "vipCode", lhsVipCode), LocatorUtils.property(thatLocator, "vipCode", rhsVipCode), lhsVipCode, rhsVipCode, (this.vipCode!= null), (that.vipCode!= null))) {
                return false;
            }
        }
        {
            List<String> lhsNotes;
            lhsNotes = (((this.notes!= null)&&(!this.notes.isEmpty()))?this.getNotes():null);
            List<String> rhsNotes;
            rhsNotes = (((that.notes!= null)&&(!that.notes.isEmpty()))?that.getNotes():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "notes", lhsNotes), LocatorUtils.property(thatLocator, "notes", rhsNotes), lhsNotes, rhsNotes, ((this.notes!= null)&&(!this.notes.isEmpty())), ((that.notes!= null)&&(!that.notes.isEmpty())))) {
                return false;
            }
        }
        {
            AttributeListType lhsAccountAttributes;
            lhsAccountAttributes = this.getAccountAttributes();
            AttributeListType rhsAccountAttributes;
            rhsAccountAttributes = that.getAccountAttributes();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "accountAttributes", lhsAccountAttributes), LocatorUtils.property(thatLocator, "accountAttributes", rhsAccountAttributes), lhsAccountAttributes, rhsAccountAttributes, (this.accountAttributes!= null), (that.accountAttributes!= null))) {
                return false;
            }
        }
        return true;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(Object object) {
        final EqualsStrategy2 strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode(ObjectLocator locator, HashCodeStrategy2 strategy) {
        int currentHashCode = 1;
        {
            AccountIdType theAccountId;
            theAccountId = this.getAccountId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "accountId", theAccountId), currentHashCode, theAccountId, (this.accountId!= null));
        }
        {
            BillingAccountNumberType theBillingAccountNumber;
            theBillingAccountNumber = this.getBillingAccountNumber();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "billingAccountNumber", theBillingAccountNumber), currentHashCode, theBillingAccountNumber, (this.billingAccountNumber!= null));
        }
        {
            String theTailgaterAccountFlag;
            theTailgaterAccountFlag = this.getTailgaterAccountFlag();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "tailgaterAccountFlag", theTailgaterAccountFlag), currentHashCode, theTailgaterAccountFlag, true);
        }
        {
            String theHardCopyStatementFlag;
            theHardCopyStatementFlag = this.getHardCopyStatementFlag();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "hardCopyStatementFlag", theHardCopyStatementFlag), currentHashCode, theHardCopyStatementFlag, true);
        }
        {
            String theStatementLanguage;
            theStatementLanguage = this.getStatementLanguage();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "statementLanguage", theStatementLanguage), currentHashCode, theStatementLanguage, (this.statementLanguage!= null));
        }
        {
            String theEstViewOccupancy;
            theEstViewOccupancy = this.getEstViewOccupancy();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "estViewOccupancy", theEstViewOccupancy), currentHashCode, theEstViewOccupancy, (this.estViewOccupancy!= null));
        }
        {
            String theFireOccupancy;
            theFireOccupancy = this.getFireOccupancy();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "fireOccupancy", theFireOccupancy), currentHashCode, theFireOccupancy, (this.fireOccupancy!= null));
        }
        {
            AccountTypeType theAccountType;
            theAccountType = this.getAccountType();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "accountType", theAccountType), currentHashCode, theAccountType, (this.accountType!= null));
        }
        {
            BillToType theBillTo;
            theBillTo = this.getBillTo();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "billTo", theBillTo), currentHashCode, theBillTo, (this.billTo!= null));
        }
        {
            PartnerAccountInfoType thePartnerAccountInfo;
            thePartnerAccountInfo = this.getPartnerAccountInfo();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "partnerAccountInfo", thePartnerAccountInfo), currentHashCode, thePartnerAccountInfo, (this.partnerAccountInfo!= null));
        }
        {
            String theDunningGroup;
            theDunningGroup = this.getDunningGroup();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "dunningGroup", theDunningGroup), currentHashCode, theDunningGroup, (this.dunningGroup!= null));
        }
        {
            String theDunningGroupPriority;
            theDunningGroupPriority = this.getDunningGroupPriority();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "dunningGroupPriority", theDunningGroupPriority), currentHashCode, theDunningGroupPriority, (this.dunningGroupPriority!= null));
        }
        {
            CommAcctInfoTypeSubmitOrder theCommercialAccountInfo;
            theCommercialAccountInfo = this.getCommercialAccountInfo();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "commercialAccountInfo", theCommercialAccountInfo), currentHashCode, theCommercialAccountInfo, (this.commercialAccountInfo!= null));
        }
        {
            String theTaxGroup;
            theTaxGroup = this.getTaxGroup();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "taxGroup", theTaxGroup), currentHashCode, theTaxGroup, (this.taxGroup!= null));
        }
        {
            String theVipCode;
            theVipCode = this.getVipCode();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "vipCode", theVipCode), currentHashCode, theVipCode, (this.vipCode!= null));
        }
        {
            List<String> theNotes;
            theNotes = (((this.notes!= null)&&(!this.notes.isEmpty()))?this.getNotes():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "notes", theNotes), currentHashCode, theNotes, ((this.notes!= null)&&(!this.notes.isEmpty())));
        }
        {
            AttributeListType theAccountAttributes;
            theAccountAttributes = this.getAccountAttributes();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "accountAttributes", theAccountAttributes), currentHashCode, theAccountAttributes, (this.accountAttributes!= null));
        }
        return currentHashCode;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode() {
        final HashCodeStrategy2 strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String toString() {
        final ToStringStrategy2 strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        {
            AccountIdType theAccountId;
            theAccountId = this.getAccountId();
            strategy.appendField(locator, this, "accountId", buffer, theAccountId, (this.accountId!= null));
        }
        {
            BillingAccountNumberType theBillingAccountNumber;
            theBillingAccountNumber = this.getBillingAccountNumber();
            strategy.appendField(locator, this, "billingAccountNumber", buffer, theBillingAccountNumber, (this.billingAccountNumber!= null));
        }
        {
            String theTailgaterAccountFlag;
            theTailgaterAccountFlag = this.getTailgaterAccountFlag();
            strategy.appendField(locator, this, "tailgaterAccountFlag", buffer, theTailgaterAccountFlag, true);
        }
        {
            String theHardCopyStatementFlag;
            theHardCopyStatementFlag = this.getHardCopyStatementFlag();
            strategy.appendField(locator, this, "hardCopyStatementFlag", buffer, theHardCopyStatementFlag, true);
        }
        {
            String theStatementLanguage;
            theStatementLanguage = this.getStatementLanguage();
            strategy.appendField(locator, this, "statementLanguage", buffer, theStatementLanguage, (this.statementLanguage!= null));
        }
        {
            String theEstViewOccupancy;
            theEstViewOccupancy = this.getEstViewOccupancy();
            strategy.appendField(locator, this, "estViewOccupancy", buffer, theEstViewOccupancy, (this.estViewOccupancy!= null));
        }
        {
            String theFireOccupancy;
            theFireOccupancy = this.getFireOccupancy();
            strategy.appendField(locator, this, "fireOccupancy", buffer, theFireOccupancy, (this.fireOccupancy!= null));
        }
        {
            AccountTypeType theAccountType;
            theAccountType = this.getAccountType();
            strategy.appendField(locator, this, "accountType", buffer, theAccountType, (this.accountType!= null));
        }
        {
            BillToType theBillTo;
            theBillTo = this.getBillTo();
            strategy.appendField(locator, this, "billTo", buffer, theBillTo, (this.billTo!= null));
        }
        {
            PartnerAccountInfoType thePartnerAccountInfo;
            thePartnerAccountInfo = this.getPartnerAccountInfo();
            strategy.appendField(locator, this, "partnerAccountInfo", buffer, thePartnerAccountInfo, (this.partnerAccountInfo!= null));
        }
        {
            String theDunningGroup;
            theDunningGroup = this.getDunningGroup();
            strategy.appendField(locator, this, "dunningGroup", buffer, theDunningGroup, (this.dunningGroup!= null));
        }
        {
            String theDunningGroupPriority;
            theDunningGroupPriority = this.getDunningGroupPriority();
            strategy.appendField(locator, this, "dunningGroupPriority", buffer, theDunningGroupPriority, (this.dunningGroupPriority!= null));
        }
        {
            CommAcctInfoTypeSubmitOrder theCommercialAccountInfo;
            theCommercialAccountInfo = this.getCommercialAccountInfo();
            strategy.appendField(locator, this, "commercialAccountInfo", buffer, theCommercialAccountInfo, (this.commercialAccountInfo!= null));
        }
        {
            String theTaxGroup;
            theTaxGroup = this.getTaxGroup();
            strategy.appendField(locator, this, "taxGroup", buffer, theTaxGroup, (this.taxGroup!= null));
        }
        {
            String theVipCode;
            theVipCode = this.getVipCode();
            strategy.appendField(locator, this, "vipCode", buffer, theVipCode, (this.vipCode!= null));
        }
        {
            List<String> theNotes;
            theNotes = (((this.notes!= null)&&(!this.notes.isEmpty()))?this.getNotes():null);
            strategy.appendField(locator, this, "notes", buffer, theNotes, ((this.notes!= null)&&(!this.notes.isEmpty())));
        }
        {
            AttributeListType theAccountAttributes;
            theAccountAttributes = this.getAccountAttributes();
            strategy.appendField(locator, this, "accountAttributes", buffer, theAccountAttributes, (this.accountAttributes!= null));
        }
        return buffer;
    }

}
