
package dishnetwork.schemas;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;

import io.swagger.annotations.ApiModelProperty;
import org.jvnet.jaxb2_commons.lang.Equals2;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy2;
import org.jvnet.jaxb2_commons.lang.HashCode2;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy2;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString2;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy2;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for ExternalOrderInput complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="ExternalOrderInput"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="shippingAddress" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="addressLineOne" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="addressLineTwo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="city" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="state" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="zip" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="zipPlus4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="attributeList" type="{http://www.dishnetwork.com/schema/common/AttributeList/2015_10_08}AttributeListTypeType" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="previousExternalProviderServiceAddress" type="{http://www.dishnetwork.com/schema/common/ExternalOrder/2015_10_08}providerServiceAddressType" minOccurs="0"/&gt;
 *         &lt;element name="externalProviderServiceAddress" type="{http://www.dishnetwork.com/schema/common/ExternalOrder/2015_10_08}providerServiceAddressType" minOccurs="0"/&gt;
 *         &lt;element name="receiverList" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="receiverInputInfo" maxOccurs="unbounded" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="accessNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="model" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="ownership" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="attributeList" type="{http://www.dishnetwork.com/schema/common/AttributeList/2015_10_08}AttributeListTypeType" minOccurs="0"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="portInfo" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="previousAccountName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="previousProviderAccountId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="previousPersonalIdentificationNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="previousBillingZipCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="attributeList" type="{http://www.dishnetwork.com/schema/common/AttributeList/2015_10_08}AttributeListTypeType" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="expectedCompletionDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="expectedCompletionTimeRange" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="appointmentConfirmationId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="questionAnswerList" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="questionAnswer" maxOccurs="unbounded" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="questionId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="answerId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="attributeList" type="{http://www.dishnetwork.com/schema/common/AttributeList/2015_10_08}AttributeListTypeType" minOccurs="0"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="attributeList" type="{http://www.dishnetwork.com/schema/common/AttributeList/2013_09_19}AttributeListType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ExternalOrderInput", propOrder = {
    "shippingAddress",
    "previousExternalProviderServiceAddress",
    "externalProviderServiceAddress",
    "receiverList",
    "portInfo",
    "expectedCompletionDate",
    "expectedCompletionTimeRange",
    "appointmentConfirmationId",
    "questionAnswerList",
    "attributeList"
})
@Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
public class ExternalOrderInput implements Equals2, HashCode2, ToString2
{

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected ShippingAddress shippingAddress;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected ProviderServiceAddressType previousExternalProviderServiceAddress;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected ProviderServiceAddressType externalProviderServiceAddress;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected ReceiverList receiverList;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected PortInfo portInfo;
    @XmlSchemaType(name = "date")
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    @ApiModelProperty(example = "2022-07-12T21:32:55.842Z")
    protected String expectedCompletionDate;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String expectedCompletionTimeRange;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String appointmentConfirmationId;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected QuestionAnswerList questionAnswerList;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected AttributeListType attributeList;

    /**
     * Default no-arg constructor
     *
     */
    public ExternalOrderInput() {
        super();
    }

    /**
     * Fully-initialising value constructor
     *
     */
    public ExternalOrderInput(final ShippingAddress shippingAddress, final ProviderServiceAddressType previousExternalProviderServiceAddress, final ProviderServiceAddressType externalProviderServiceAddress, final ReceiverList receiverList, final PortInfo portInfo, final String expectedCompletionDate, final String expectedCompletionTimeRange, final String appointmentConfirmationId, final QuestionAnswerList questionAnswerList, final AttributeListType attributeList) {
        this.shippingAddress = shippingAddress;
        this.previousExternalProviderServiceAddress = previousExternalProviderServiceAddress;
        this.externalProviderServiceAddress = externalProviderServiceAddress;
        this.receiverList = receiverList;
        this.portInfo = portInfo;
        this.expectedCompletionDate = expectedCompletionDate;
        this.expectedCompletionTimeRange = expectedCompletionTimeRange;
        this.appointmentConfirmationId = appointmentConfirmationId;
        this.questionAnswerList = questionAnswerList;
        this.attributeList = attributeList;
    }

    /**
     * Gets the value of the shippingAddress property.
     *
     * @return
     *     possible object is
     *     {@link ShippingAddress }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public ShippingAddress getShippingAddress() {
        return shippingAddress;
    }

    /**
     * Sets the value of the shippingAddress property.
     *
     * @param value
     *     allowed object is
     *     {@link ShippingAddress }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setShippingAddress(ShippingAddress value) {
        this.shippingAddress = value;
    }

    /**
     * Gets the value of the previousExternalProviderServiceAddress property.
     *
     * @return
     *     possible object is
     *     {@link ProviderServiceAddressType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public ProviderServiceAddressType getPreviousExternalProviderServiceAddress() {
        return previousExternalProviderServiceAddress;
    }

    /**
     * Sets the value of the previousExternalProviderServiceAddress property.
     *
     * @param value
     *     allowed object is
     *     {@link ProviderServiceAddressType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setPreviousExternalProviderServiceAddress(ProviderServiceAddressType value) {
        this.previousExternalProviderServiceAddress = value;
    }

    /**
     * Gets the value of the externalProviderServiceAddress property.
     *
     * @return
     *     possible object is
     *     {@link ProviderServiceAddressType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public ProviderServiceAddressType getExternalProviderServiceAddress() {
        return externalProviderServiceAddress;
    }

    /**
     * Sets the value of the externalProviderServiceAddress property.
     *
     * @param value
     *     allowed object is
     *     {@link ProviderServiceAddressType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setExternalProviderServiceAddress(ProviderServiceAddressType value) {
        this.externalProviderServiceAddress = value;
    }

    /**
     * Gets the value of the receiverList property.
     *
     * @return
     *     possible object is
     *     {@link ReceiverList }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public ReceiverList getReceiverList() {
        return receiverList;
    }

    /**
     * Sets the value of the receiverList property.
     *
     * @param value
     *     allowed object is
     *     {@link ReceiverList }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setReceiverList(ReceiverList value) {
        this.receiverList = value;
    }

    /**
     * Gets the value of the portInfo property.
     *
     * @return
     *     possible object is
     *     {@link PortInfo }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public PortInfo getPortInfo() {
        return portInfo;
    }

    /**
     * Sets the value of the portInfo property.
     *
     * @param value
     *     allowed object is
     *     {@link PortInfo }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setPortInfo(PortInfo value) {
        this.portInfo = value;
    }

    /**
     * Gets the value of the expectedCompletionDate property.
     *
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getExpectedCompletionDate() {
        return expectedCompletionDate;
    }

    /**
     * Sets the value of the expectedCompletionDate property.
     *
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setExpectedCompletionDate(String value) {
        this.expectedCompletionDate = value;
    }

    /**
     * Gets the value of the expectedCompletionTimeRange property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getExpectedCompletionTimeRange() {
        return expectedCompletionTimeRange;
    }

    /**
     * Sets the value of the expectedCompletionTimeRange property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setExpectedCompletionTimeRange(String value) {
        this.expectedCompletionTimeRange = value;
    }

    /**
     * Gets the value of the appointmentConfirmationId property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getAppointmentConfirmationId() {
        return appointmentConfirmationId;
    }

    /**
     * Sets the value of the appointmentConfirmationId property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setAppointmentConfirmationId(String value) {
        this.appointmentConfirmationId = value;
    }

    /**
     * Gets the value of the questionAnswerList property.
     *
     * @return
     *     possible object is
     *     {@link QuestionAnswerList }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public QuestionAnswerList getQuestionAnswerList() {
        return questionAnswerList;
    }

    /**
     * Sets the value of the questionAnswerList property.
     *
     * @param value
     *     allowed object is
     *     {@link QuestionAnswerList }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setQuestionAnswerList(QuestionAnswerList value) {
        this.questionAnswerList = value;
    }

    /**
     * Gets the value of the attributeList property.
     *
     * @return
     *     possible object is
     *     {@link AttributeListType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public AttributeListType getAttributeList() {
        return attributeList;
    }

    /**
     * Sets the value of the attributeList property.
     *
     * @param value
     *     allowed object is
     *     {@link AttributeListType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setAttributeList(AttributeListType value) {
        this.attributeList = value;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy2 strategy) {
        if ((object == null)||(this.getClass()!= object.getClass())) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final ExternalOrderInput that = ((ExternalOrderInput) object);
        {
            ShippingAddress lhsShippingAddress;
            lhsShippingAddress = this.getShippingAddress();
            ShippingAddress rhsShippingAddress;
            rhsShippingAddress = that.getShippingAddress();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "shippingAddress", lhsShippingAddress), LocatorUtils.property(thatLocator, "shippingAddress", rhsShippingAddress), lhsShippingAddress, rhsShippingAddress, (this.shippingAddress!= null), (that.shippingAddress!= null))) {
                return false;
            }
        }
        {
            ProviderServiceAddressType lhsPreviousExternalProviderServiceAddress;
            lhsPreviousExternalProviderServiceAddress = this.getPreviousExternalProviderServiceAddress();
            ProviderServiceAddressType rhsPreviousExternalProviderServiceAddress;
            rhsPreviousExternalProviderServiceAddress = that.getPreviousExternalProviderServiceAddress();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "previousExternalProviderServiceAddress", lhsPreviousExternalProviderServiceAddress), LocatorUtils.property(thatLocator, "previousExternalProviderServiceAddress", rhsPreviousExternalProviderServiceAddress), lhsPreviousExternalProviderServiceAddress, rhsPreviousExternalProviderServiceAddress, (this.previousExternalProviderServiceAddress!= null), (that.previousExternalProviderServiceAddress!= null))) {
                return false;
            }
        }
        {
            ProviderServiceAddressType lhsExternalProviderServiceAddress;
            lhsExternalProviderServiceAddress = this.getExternalProviderServiceAddress();
            ProviderServiceAddressType rhsExternalProviderServiceAddress;
            rhsExternalProviderServiceAddress = that.getExternalProviderServiceAddress();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "externalProviderServiceAddress", lhsExternalProviderServiceAddress), LocatorUtils.property(thatLocator, "externalProviderServiceAddress", rhsExternalProviderServiceAddress), lhsExternalProviderServiceAddress, rhsExternalProviderServiceAddress, (this.externalProviderServiceAddress!= null), (that.externalProviderServiceAddress!= null))) {
                return false;
            }
        }
        {
            ReceiverList lhsReceiverList;
            lhsReceiverList = this.getReceiverList();
            ReceiverList rhsReceiverList;
            rhsReceiverList = that.getReceiverList();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "receiverList", lhsReceiverList), LocatorUtils.property(thatLocator, "receiverList", rhsReceiverList), lhsReceiverList, rhsReceiverList, (this.receiverList!= null), (that.receiverList!= null))) {
                return false;
            }
        }
        {
            PortInfo lhsPortInfo;
            lhsPortInfo = this.getPortInfo();
            PortInfo rhsPortInfo;
            rhsPortInfo = that.getPortInfo();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "portInfo", lhsPortInfo), LocatorUtils.property(thatLocator, "portInfo", rhsPortInfo), lhsPortInfo, rhsPortInfo, (this.portInfo!= null), (that.portInfo!= null))) {
                return false;
            }
        }
        {
            String lhsExpectedCompletionDate;
            lhsExpectedCompletionDate = this.getExpectedCompletionDate();
            String rhsExpectedCompletionDate;
            rhsExpectedCompletionDate = that.getExpectedCompletionDate();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "expectedCompletionDate", lhsExpectedCompletionDate), LocatorUtils.property(thatLocator, "expectedCompletionDate", rhsExpectedCompletionDate), lhsExpectedCompletionDate, rhsExpectedCompletionDate, (this.expectedCompletionDate!= null), (that.expectedCompletionDate!= null))) {
                return false;
            }
        }
        {
            String lhsExpectedCompletionTimeRange;
            lhsExpectedCompletionTimeRange = this.getExpectedCompletionTimeRange();
            String rhsExpectedCompletionTimeRange;
            rhsExpectedCompletionTimeRange = that.getExpectedCompletionTimeRange();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "expectedCompletionTimeRange", lhsExpectedCompletionTimeRange), LocatorUtils.property(thatLocator, "expectedCompletionTimeRange", rhsExpectedCompletionTimeRange), lhsExpectedCompletionTimeRange, rhsExpectedCompletionTimeRange, (this.expectedCompletionTimeRange!= null), (that.expectedCompletionTimeRange!= null))) {
                return false;
            }
        }
        {
            String lhsAppointmentConfirmationId;
            lhsAppointmentConfirmationId = this.getAppointmentConfirmationId();
            String rhsAppointmentConfirmationId;
            rhsAppointmentConfirmationId = that.getAppointmentConfirmationId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "appointmentConfirmationId", lhsAppointmentConfirmationId), LocatorUtils.property(thatLocator, "appointmentConfirmationId", rhsAppointmentConfirmationId), lhsAppointmentConfirmationId, rhsAppointmentConfirmationId, (this.appointmentConfirmationId!= null), (that.appointmentConfirmationId!= null))) {
                return false;
            }
        }
        {
            QuestionAnswerList lhsQuestionAnswerList;
            lhsQuestionAnswerList = this.getQuestionAnswerList();
            QuestionAnswerList rhsQuestionAnswerList;
            rhsQuestionAnswerList = that.getQuestionAnswerList();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "questionAnswerList", lhsQuestionAnswerList), LocatorUtils.property(thatLocator, "questionAnswerList", rhsQuestionAnswerList), lhsQuestionAnswerList, rhsQuestionAnswerList, (this.questionAnswerList!= null), (that.questionAnswerList!= null))) {
                return false;
            }
        }
        {
            AttributeListType lhsAttributeList;
            lhsAttributeList = this.getAttributeList();
            AttributeListType rhsAttributeList;
            rhsAttributeList = that.getAttributeList();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "attributeList", lhsAttributeList), LocatorUtils.property(thatLocator, "attributeList", rhsAttributeList), lhsAttributeList, rhsAttributeList, (this.attributeList!= null), (that.attributeList!= null))) {
                return false;
            }
        }
        return true;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(Object object) {
        final EqualsStrategy2 strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode(ObjectLocator locator, HashCodeStrategy2 strategy) {
        int currentHashCode = 1;
        {
            ShippingAddress theShippingAddress;
            theShippingAddress = this.getShippingAddress();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "shippingAddress", theShippingAddress), currentHashCode, theShippingAddress, (this.shippingAddress!= null));
        }
        {
            ProviderServiceAddressType thePreviousExternalProviderServiceAddress;
            thePreviousExternalProviderServiceAddress = this.getPreviousExternalProviderServiceAddress();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "previousExternalProviderServiceAddress", thePreviousExternalProviderServiceAddress), currentHashCode, thePreviousExternalProviderServiceAddress, (this.previousExternalProviderServiceAddress!= null));
        }
        {
            ProviderServiceAddressType theExternalProviderServiceAddress;
            theExternalProviderServiceAddress = this.getExternalProviderServiceAddress();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "externalProviderServiceAddress", theExternalProviderServiceAddress), currentHashCode, theExternalProviderServiceAddress, (this.externalProviderServiceAddress!= null));
        }
        {
            ReceiverList theReceiverList;
            theReceiverList = this.getReceiverList();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "receiverList", theReceiverList), currentHashCode, theReceiverList, (this.receiverList!= null));
        }
        {
            PortInfo thePortInfo;
            thePortInfo = this.getPortInfo();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "portInfo", thePortInfo), currentHashCode, thePortInfo, (this.portInfo!= null));
        }
        {
            String theExpectedCompletionDate;
            theExpectedCompletionDate = this.getExpectedCompletionDate();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "expectedCompletionDate", theExpectedCompletionDate), currentHashCode, theExpectedCompletionDate, (this.expectedCompletionDate!= null));
        }
        {
            String theExpectedCompletionTimeRange;
            theExpectedCompletionTimeRange = this.getExpectedCompletionTimeRange();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "expectedCompletionTimeRange", theExpectedCompletionTimeRange), currentHashCode, theExpectedCompletionTimeRange, (this.expectedCompletionTimeRange!= null));
        }
        {
            String theAppointmentConfirmationId;
            theAppointmentConfirmationId = this.getAppointmentConfirmationId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "appointmentConfirmationId", theAppointmentConfirmationId), currentHashCode, theAppointmentConfirmationId, (this.appointmentConfirmationId!= null));
        }
        {
            QuestionAnswerList theQuestionAnswerList;
            theQuestionAnswerList = this.getQuestionAnswerList();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "questionAnswerList", theQuestionAnswerList), currentHashCode, theQuestionAnswerList, (this.questionAnswerList!= null));
        }
        {
            AttributeListType theAttributeList;
            theAttributeList = this.getAttributeList();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "attributeList", theAttributeList), currentHashCode, theAttributeList, (this.attributeList!= null));
        }
        return currentHashCode;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode() {
        final HashCodeStrategy2 strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String toString() {
        final ToStringStrategy2 strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        {
            ShippingAddress theShippingAddress;
            theShippingAddress = this.getShippingAddress();
            strategy.appendField(locator, this, "shippingAddress", buffer, theShippingAddress, (this.shippingAddress!= null));
        }
        {
            ProviderServiceAddressType thePreviousExternalProviderServiceAddress;
            thePreviousExternalProviderServiceAddress = this.getPreviousExternalProviderServiceAddress();
            strategy.appendField(locator, this, "previousExternalProviderServiceAddress", buffer, thePreviousExternalProviderServiceAddress, (this.previousExternalProviderServiceAddress!= null));
        }
        {
            ProviderServiceAddressType theExternalProviderServiceAddress;
            theExternalProviderServiceAddress = this.getExternalProviderServiceAddress();
            strategy.appendField(locator, this, "externalProviderServiceAddress", buffer, theExternalProviderServiceAddress, (this.externalProviderServiceAddress!= null));
        }
        {
            ReceiverList theReceiverList;
            theReceiverList = this.getReceiverList();
            strategy.appendField(locator, this, "receiverList", buffer, theReceiverList, (this.receiverList!= null));
        }
        {
            PortInfo thePortInfo;
            thePortInfo = this.getPortInfo();
            strategy.appendField(locator, this, "portInfo", buffer, thePortInfo, (this.portInfo!= null));
        }
        {
            String theExpectedCompletionDate;
            theExpectedCompletionDate = this.getExpectedCompletionDate();
            strategy.appendField(locator, this, "expectedCompletionDate", buffer, theExpectedCompletionDate, (this.expectedCompletionDate!= null));
        }
        {
            String theExpectedCompletionTimeRange;
            theExpectedCompletionTimeRange = this.getExpectedCompletionTimeRange();
            strategy.appendField(locator, this, "expectedCompletionTimeRange", buffer, theExpectedCompletionTimeRange, (this.expectedCompletionTimeRange!= null));
        }
        {
            String theAppointmentConfirmationId;
            theAppointmentConfirmationId = this.getAppointmentConfirmationId();
            strategy.appendField(locator, this, "appointmentConfirmationId", buffer, theAppointmentConfirmationId, (this.appointmentConfirmationId!= null));
        }
        {
            QuestionAnswerList theQuestionAnswerList;
            theQuestionAnswerList = this.getQuestionAnswerList();
            strategy.appendField(locator, this, "questionAnswerList", buffer, theQuestionAnswerList, (this.questionAnswerList!= null));
        }
        {
            AttributeListType theAttributeList;
            theAttributeList = this.getAttributeList();
            strategy.appendField(locator, this, "attributeList", buffer, theAttributeList, (this.attributeList!= null));
        }
        return buffer;
    }


}
