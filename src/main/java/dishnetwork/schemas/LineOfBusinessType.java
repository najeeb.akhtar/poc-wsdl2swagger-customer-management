
package dishnetwork.schemas;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for LineOfBusinessType.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <pre>
 * &lt;simpleType name="LineOfBusinessType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="VIDEO"/&gt;
 *     &lt;enumeration value="DATA"/&gt;
 *     &lt;enumeration value="PHONE"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 *
 */
@XmlType(name = "LineOfBusinessType", namespace = "http://www.dishnetwork.com/schema/Service/LineOfBusinessType/2015_01_15")
@XmlEnum
@Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
public enum LineOfBusinessType {

    VIDEO,
    DATA,
    PHONE;

    public String value() {
        return name();
    }

    public static LineOfBusinessType fromValue(String v) {
        return valueOf(v);
    }

}
