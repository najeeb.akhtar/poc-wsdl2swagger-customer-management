
package dishnetwork.schemas;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CustomerQualificationIdentifierType.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <pre>
 * &lt;simpleType name="CustomerQualificationIdentifierType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="creditScoreId"/&gt;
 *     &lt;enumeration value="confidenceLevelID"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 *
 */
@XmlType(name = "CustomerQualificationIdentifierType")
@XmlEnum
@Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
public enum CustomerQualificationIdentifierType {

    @XmlEnumValue("creditScoreId")
    CREDIT_SCORE_ID("creditScoreId"),
    @XmlEnumValue("confidenceLevelID")
    CONFIDENCE_LEVEL_ID("confidenceLevelID");
    private final String value;

    CustomerQualificationIdentifierType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CustomerQualificationIdentifierType fromValue(String v) {
        for (CustomerQualificationIdentifierType c: CustomerQualificationIdentifierType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
