
package dishnetwork.schemas;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.jvnet.jaxb2_commons.lang.Equals2;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy2;
import org.jvnet.jaxb2_commons.lang.HashCode2;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy2;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString2;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy2;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for AccountPaymentInfoType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="AccountPaymentInfoType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="totalAmountDue" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/&gt;
 *         &lt;element name="paymentAmount" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/&gt;
 *         &lt;element name="paymentOnHoldFlag" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="autopayInfo" type="{http://www.dishnetwork.com/schema/CustomerManagement/AutoPayPaymentMethodSubmitOrder/2022_05_12}PaymentMethodType" minOccurs="0"/&gt;
 *         &lt;element name="oneTimePaymentInfo" type="{http://www.dishnetwork.com/schema/CustomerManagement/OneTimePaymentMethodSubmitOrder/2022_05_12}PaymentMethodType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AccountPaymentInfoTypeSubmitOrder", propOrder = {
    "totalAmountDue",
    "paymentAmount",
    "paymentOnHoldFlag",
    "autopayInfo",
    "oneTimePaymentInfo"
})
@Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
public class AccountPaymentInfoTypeSubmitOrder implements Equals2, HashCode2, ToString2
{

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String totalAmountDue;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String paymentAmount;
    //@XmlElement(defaultValue = "false")
    @JsonProperty(defaultValue = "false")
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String paymentOnHoldFlag;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected PaymentMethodTypeSubmitOrder autopayInfo;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected PaymentMethodTypeSubmitOrderOneTime oneTimePaymentInfo;

    /**
     * Default no-arg constructor
     *
     */
    public AccountPaymentInfoTypeSubmitOrder() {
        super();
    }

    /**
     * Fully-initialising value constructor
     *
     */
    public AccountPaymentInfoTypeSubmitOrder(final String totalAmountDue, final String paymentAmount, final String paymentOnHoldFlag, final PaymentMethodTypeSubmitOrder autopayInfo, final PaymentMethodTypeSubmitOrderOneTime oneTimePaymentInfo) {
        this.totalAmountDue = totalAmountDue;
        this.paymentAmount = paymentAmount;
        this.paymentOnHoldFlag = paymentOnHoldFlag;
        this.autopayInfo = autopayInfo;
        this.oneTimePaymentInfo = oneTimePaymentInfo;
    }

    /**
     * Gets the value of the totalAmountDue property.
     *
     * @return
     *     possible object is
     *     {@link Double }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getTotalAmountDue() {
        return totalAmountDue;
    }

    /**
     * Sets the value of the totalAmountDue property.
     *
     * @param value
     *     allowed object is
     *     {@link Double }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setTotalAmountDue(String value) {
        this.totalAmountDue = value;
    }

    /**
     * Gets the value of the paymentAmount property.
     *
     * @return
     *     possible object is
     *     {@link Double }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getPaymentAmount() {
        return paymentAmount;
    }

    /**
     * Sets the value of the paymentAmount property.
     *
     * @param value
     *     allowed object is
     *     {@link Double }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setPaymentAmount(String value) {
        this.paymentAmount = value;
    }

    /**
     * Gets the value of the paymentOnHoldFlag property.
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getPaymentOnHoldFlag() {
        return paymentOnHoldFlag;
    }

    /**
     * Sets the value of the paymentOnHoldFlag property.
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setPaymentOnHoldFlag(String value) {
        this.paymentOnHoldFlag = value;
    }

    /**
     * Gets the value of the autopayInfo property.
     *
     * @return
     *     possible object is
     *     {@link PaymentMethodTypeSubmitOrder }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public PaymentMethodTypeSubmitOrder getAutopayInfo() {
        return autopayInfo;
    }

    /**
     * Sets the value of the autopayInfo property.
     *
     * @param value
     *     allowed object is
     *     {@link PaymentMethodTypeSubmitOrder }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setAutopayInfo(PaymentMethodTypeSubmitOrder value) {
        this.autopayInfo = value;
    }

    /**
     * Gets the value of the oneTimePaymentInfo property.
     *
     * @return
     *     possible object is
     *     {@link PaymentMethodTypeSubmitOrderOneTime }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public PaymentMethodTypeSubmitOrderOneTime getOneTimePaymentInfo() {
        return oneTimePaymentInfo;
    }

    /**
     * Sets the value of the oneTimePaymentInfo property.
     *
     * @param value
     *     allowed object is
     *     {@link PaymentMethodTypeSubmitOrderOneTime }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setOneTimePaymentInfo(PaymentMethodTypeSubmitOrderOneTime value) {
        this.oneTimePaymentInfo = value;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy2 strategy) {
        if ((object == null)||(this.getClass()!= object.getClass())) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final AccountPaymentInfoTypeSubmitOrder that = ((AccountPaymentInfoTypeSubmitOrder) object);
        {
            String lhsTotalAmountDue;
            lhsTotalAmountDue = this.getTotalAmountDue();
            String rhsTotalAmountDue;
            rhsTotalAmountDue = that.getTotalAmountDue();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "totalAmountDue", lhsTotalAmountDue), LocatorUtils.property(thatLocator, "totalAmountDue", rhsTotalAmountDue), lhsTotalAmountDue, rhsTotalAmountDue, (this.totalAmountDue!= null), (that.totalAmountDue!= null))) {
                return false;
            }
        }
        {
            String lhsPaymentAmount;
            lhsPaymentAmount = this.getPaymentAmount();
            String rhsPaymentAmount;
            rhsPaymentAmount = that.getPaymentAmount();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "paymentAmount", lhsPaymentAmount), LocatorUtils.property(thatLocator, "paymentAmount", rhsPaymentAmount), lhsPaymentAmount, rhsPaymentAmount, (this.paymentAmount!= null), (that.paymentAmount!= null))) {
                return false;
            }
        }
        {
            String lhsPaymentOnHoldFlag;
            lhsPaymentOnHoldFlag = this.getPaymentOnHoldFlag();
            String rhsPaymentOnHoldFlag;
            rhsPaymentOnHoldFlag = that.getPaymentOnHoldFlag();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "paymentOnHoldFlag", lhsPaymentOnHoldFlag), LocatorUtils.property(thatLocator, "paymentOnHoldFlag", rhsPaymentOnHoldFlag), lhsPaymentOnHoldFlag, rhsPaymentOnHoldFlag, true, true)) {
                return false;
            }
        }
        {
            PaymentMethodTypeSubmitOrder lhsAutopayInfo;
            lhsAutopayInfo = this.getAutopayInfo();
            PaymentMethodTypeSubmitOrder rhsAutopayInfo;
            rhsAutopayInfo = that.getAutopayInfo();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "autopayInfo", lhsAutopayInfo), LocatorUtils.property(thatLocator, "autopayInfo", rhsAutopayInfo), lhsAutopayInfo, rhsAutopayInfo, (this.autopayInfo!= null), (that.autopayInfo!= null))) {
                return false;
            }
        }
        {
            PaymentMethodTypeSubmitOrderOneTime lhsOneTimePaymentInfo;
            lhsOneTimePaymentInfo = this.getOneTimePaymentInfo();
            PaymentMethodTypeSubmitOrderOneTime rhsOneTimePaymentInfo;
            rhsOneTimePaymentInfo = that.getOneTimePaymentInfo();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "oneTimePaymentInfo", lhsOneTimePaymentInfo), LocatorUtils.property(thatLocator, "oneTimePaymentInfo", rhsOneTimePaymentInfo), lhsOneTimePaymentInfo, rhsOneTimePaymentInfo, (this.oneTimePaymentInfo!= null), (that.oneTimePaymentInfo!= null))) {
                return false;
            }
        }
        return true;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(Object object) {
        final EqualsStrategy2 strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode(ObjectLocator locator, HashCodeStrategy2 strategy) {
        int currentHashCode = 1;
        {
            String theTotalAmountDue;
            theTotalAmountDue = this.getTotalAmountDue();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "totalAmountDue", theTotalAmountDue), currentHashCode, theTotalAmountDue, (this.totalAmountDue!= null));
        }
        {
            String thePaymentAmount;
            thePaymentAmount = this.getPaymentAmount();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "paymentAmount", thePaymentAmount), currentHashCode, thePaymentAmount, (this.paymentAmount!= null));
        }
        {
            String thePaymentOnHoldFlag;
            thePaymentOnHoldFlag = this.getPaymentOnHoldFlag();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "paymentOnHoldFlag", thePaymentOnHoldFlag), currentHashCode, thePaymentOnHoldFlag, true);
        }
        {
            PaymentMethodTypeSubmitOrder theAutopayInfo;
            theAutopayInfo = this.getAutopayInfo();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "autopayInfo", theAutopayInfo), currentHashCode, theAutopayInfo, (this.autopayInfo!= null));
        }
        {
            PaymentMethodTypeSubmitOrderOneTime theOneTimePaymentInfo;
            theOneTimePaymentInfo = this.getOneTimePaymentInfo();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "oneTimePaymentInfo", theOneTimePaymentInfo), currentHashCode, theOneTimePaymentInfo, (this.oneTimePaymentInfo!= null));
        }
        return currentHashCode;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode() {
        final HashCodeStrategy2 strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String toString() {
        final ToStringStrategy2 strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        {
            String theTotalAmountDue;
            theTotalAmountDue = this.getTotalAmountDue();
            strategy.appendField(locator, this, "totalAmountDue", buffer, theTotalAmountDue, (this.totalAmountDue!= null));
        }
        {
            String thePaymentAmount;
            thePaymentAmount = this.getPaymentAmount();
            strategy.appendField(locator, this, "paymentAmount", buffer, thePaymentAmount, (this.paymentAmount!= null));
        }
        {
            String thePaymentOnHoldFlag;
            thePaymentOnHoldFlag = this.getPaymentOnHoldFlag();
            strategy.appendField(locator, this, "paymentOnHoldFlag", buffer, thePaymentOnHoldFlag, true);
        }
        {
            PaymentMethodTypeSubmitOrder theAutopayInfo;
            theAutopayInfo = this.getAutopayInfo();
            strategy.appendField(locator, this, "autopayInfo", buffer, theAutopayInfo, (this.autopayInfo!= null));
        }
        {
            PaymentMethodTypeSubmitOrderOneTime theOneTimePaymentInfo;
            theOneTimePaymentInfo = this.getOneTimePaymentInfo();
            strategy.appendField(locator, this, "oneTimePaymentInfo", buffer, theOneTimePaymentInfo, (this.oneTimePaymentInfo!= null));
        }
        return buffer;
    }

}
