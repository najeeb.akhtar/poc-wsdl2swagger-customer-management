
package dishnetwork.schemas;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PaymentTypeType.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <pre>
 * &lt;simpleType name="PaymentTypeType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="CREDITCARD"/&gt;
 *     &lt;enumeration value="EFT"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 *
 */
@XmlType(name = "PaymentTypeType", namespace = "http://www.dishnetwork.com/schema/CustomerManagement/PaymentType/2010_02_22")
@XmlEnum
@Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
public enum PaymentTypeType {

    CREDITCARD,
    EFT;

    public String value() {
        return name();
    }

    public static PaymentTypeType fromValue(String v) {
        return valueOf(v);
    }

}
