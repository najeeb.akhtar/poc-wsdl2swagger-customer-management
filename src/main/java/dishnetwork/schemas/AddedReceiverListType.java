
package dishnetwork.schemas;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.jvnet.jaxb2_commons.lang.Equals2;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy2;
import org.jvnet.jaxb2_commons.lang.HashCode2;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy2;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString2;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy2;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for AddedReceiverListType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="AddedReceiverListType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="addedReceiver" type="{http://www.dishnetwork.com/schema/WorkOrderMod/AddedReceiverInfo/2013_09_19}AddedReceiverInfoType" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AddedReceiverListType", propOrder = {
    "addedReceiver"
})
@Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
public class AddedReceiverListType implements Equals2, HashCode2, ToString2
{

    //@XmlElement(required = true)
    @JsonProperty(required = true)
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected List<AddedReceiverInfoType> addedReceiver;

    /**
     * Default no-arg constructor
     *
     */
    public AddedReceiverListType() {
        super();
    }

    /**
     * Fully-initialising value constructor
     *
     */
    public AddedReceiverListType(final List<AddedReceiverInfoType> addedReceiver) {
        this.addedReceiver = addedReceiver;
    }

    /**
     * Gets the value of the addedReceiver property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the addedReceiver property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAddedReceiver().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AddedReceiverInfoType }
     *
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public List<AddedReceiverInfoType> getAddedReceiver() {
        if (addedReceiver == null) {
            addedReceiver = new ArrayList<AddedReceiverInfoType>();
        }
        return this.addedReceiver;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy2 strategy) {
        if ((object == null)||(this.getClass()!= object.getClass())) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final AddedReceiverListType that = ((AddedReceiverListType) object);
        {
            List<AddedReceiverInfoType> lhsAddedReceiver;
            lhsAddedReceiver = (((this.addedReceiver!= null)&&(!this.addedReceiver.isEmpty()))?this.getAddedReceiver():null);
            List<AddedReceiverInfoType> rhsAddedReceiver;
            rhsAddedReceiver = (((that.addedReceiver!= null)&&(!that.addedReceiver.isEmpty()))?that.getAddedReceiver():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "addedReceiver", lhsAddedReceiver), LocatorUtils.property(thatLocator, "addedReceiver", rhsAddedReceiver), lhsAddedReceiver, rhsAddedReceiver, ((this.addedReceiver!= null)&&(!this.addedReceiver.isEmpty())), ((that.addedReceiver!= null)&&(!that.addedReceiver.isEmpty())))) {
                return false;
            }
        }
        return true;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(Object object) {
        final EqualsStrategy2 strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode(ObjectLocator locator, HashCodeStrategy2 strategy) {
        int currentHashCode = 1;
        {
            List<AddedReceiverInfoType> theAddedReceiver;
            theAddedReceiver = (((this.addedReceiver!= null)&&(!this.addedReceiver.isEmpty()))?this.getAddedReceiver():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "addedReceiver", theAddedReceiver), currentHashCode, theAddedReceiver, ((this.addedReceiver!= null)&&(!this.addedReceiver.isEmpty())));
        }
        return currentHashCode;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode() {
        final HashCodeStrategy2 strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String toString() {
        final ToStringStrategy2 strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        {
            List<AddedReceiverInfoType> theAddedReceiver;
            theAddedReceiver = (((this.addedReceiver!= null)&&(!this.addedReceiver.isEmpty()))?this.getAddedReceiver():null);
            strategy.appendField(locator, this, "addedReceiver", buffer, theAddedReceiver, ((this.addedReceiver!= null)&&(!this.addedReceiver.isEmpty())));
        }
        return buffer;
    }

}
