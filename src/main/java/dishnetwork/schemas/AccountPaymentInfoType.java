
package dishnetwork.schemas;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.jvnet.jaxb2_commons.lang.Equals2;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy2;
import org.jvnet.jaxb2_commons.lang.HashCode2;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy2;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString2;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy2;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for AccountPaymentInfoType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="AccountPaymentInfoType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="amountDueNow" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/&gt;
 *         &lt;element name="overrideAmountDueFlag" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="useAutopayAsPaymentFlag" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="autopayInfo" type="{http://www.dishnetwork.com/schema/CustomerManagement/AutoPayPaymentMethod/2022_05_12}PaymentMethodType" minOccurs="0"/&gt;
 *         &lt;element name="oneTimePaymentInfo" type="{http://www.dishnetwork.com/schema/CustomerManagement/OneTimePaymentMethod/2022_05_12}PaymentMethodType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AccountPaymentInfoType", propOrder = {
    "amountDueNow",
    "overrideAmountDueFlag",
    "useAutopayAsPaymentFlag",
    "autopayInfo",
    "oneTimePaymentInfo"
})
@Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
public class AccountPaymentInfoType implements Equals2, HashCode2, ToString2
{

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String amountDueNow;
    //@XmlElement(defaultValue = "false")
    @JsonProperty(defaultValue = "false")
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String overrideAmountDueFlag;
    //@XmlElement(defaultValue = "false")
    @JsonProperty(defaultValue = "false")
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String useAutopayAsPaymentFlag;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected PaymentMethodType autopayInfo;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected PaymentMethodTypeOneTime oneTimePaymentInfo;
    protected String paymentOnHoldFlag;

    public String getPaymentOnHoldFlag() {
        return paymentOnHoldFlag;
    }

    public void setPaymentOnHoldFlag(String paymentOnHoldFlag) {
        this.paymentOnHoldFlag = paymentOnHoldFlag;
    }

    /**
     * Default no-arg constructor
     *
     */
    public AccountPaymentInfoType() {
        super();
    }

    /**
     * Fully-initialising value constructor
     *
     */
    public AccountPaymentInfoType(final String amountDueNow, final String overrideAmountDueFlag, final String useAutopayAsPaymentFlag, final PaymentMethodType autopayInfo, final PaymentMethodTypeOneTime oneTimePaymentInfo) {
        this.amountDueNow = amountDueNow;
        this.overrideAmountDueFlag = overrideAmountDueFlag;
        this.useAutopayAsPaymentFlag = useAutopayAsPaymentFlag;
        this.autopayInfo = autopayInfo;
        this.oneTimePaymentInfo = oneTimePaymentInfo;
    }

    /**
     * Gets the value of the amountDueNow property.
     *
     * @return
     *     possible object is
     *     {@link Double }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getAmountDueNow() {
        return amountDueNow;
    }

    /**
     * Sets the value of the amountDueNow property.
     *
     * @param value
     *     allowed object is
     *     {@link Double }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setAmountDueNow(String value) {
        this.amountDueNow = value;
    }

    /**
     * Gets the value of the overrideAmountDueFlag property.
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getOverrideAmountDueFlag() {
        return overrideAmountDueFlag;
    }

    /**
     * Sets the value of the overrideAmountDueFlag property.
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setOverrideAmountDueFlag(String value) {
        this.overrideAmountDueFlag = value;
    }

    /**
     * Gets the value of the useAutopayAsPaymentFlag property.
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getUseAutopayAsPaymentFlag() {
        return useAutopayAsPaymentFlag;
    }

    /**
     * Sets the value of the useAutopayAsPaymentFlag property.
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setUseAutopayAsPaymentFlag(String value) {
        this.useAutopayAsPaymentFlag = value;
    }

    /**
     * Gets the value of the autopayInfo property.
     *
     * @return
     *     possible object is
     *     {@link PaymentMethodType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public PaymentMethodType getAutopayInfo() {
        return autopayInfo;
    }

    /**
     * Sets the value of the autopayInfo property.
     *
     * @param value
     *     allowed object is
     *     {@link PaymentMethodType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setAutopayInfo(PaymentMethodType value) {
        this.autopayInfo = value;
    }

    /**
     * Gets the value of the oneTimePaymentInfo property.
     *
     * @return
     *     possible object is
     *     {@link PaymentMethodTypeOneTime }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public PaymentMethodTypeOneTime getOneTimePaymentInfo() {
        return oneTimePaymentInfo;
    }

    /**
     * Sets the value of the oneTimePaymentInfo property.
     *
     * @param value
     *     allowed object is
     *     {@link PaymentMethodTypeOneTime }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setOneTimePaymentInfo(PaymentMethodTypeOneTime value) {
        this.oneTimePaymentInfo = value;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy2 strategy) {
        if ((object == null)||(this.getClass()!= object.getClass())) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final AccountPaymentInfoType that = ((AccountPaymentInfoType) object);
        {
            String lhsAmountDueNow;
            lhsAmountDueNow = this.getAmountDueNow();
            String rhsAmountDueNow;
            rhsAmountDueNow = that.getAmountDueNow();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "amountDueNow", lhsAmountDueNow), LocatorUtils.property(thatLocator, "amountDueNow", rhsAmountDueNow), lhsAmountDueNow, rhsAmountDueNow, (this.amountDueNow!= null), (that.amountDueNow!= null))) {
                return false;
            }
        }
        {
            String lhsOverrideAmountDueFlag;
            lhsOverrideAmountDueFlag = this.getOverrideAmountDueFlag();
            String rhsOverrideAmountDueFlag;
            rhsOverrideAmountDueFlag = that.getOverrideAmountDueFlag();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "overrideAmountDueFlag", lhsOverrideAmountDueFlag), LocatorUtils.property(thatLocator, "overrideAmountDueFlag", rhsOverrideAmountDueFlag), lhsOverrideAmountDueFlag, rhsOverrideAmountDueFlag, true, true)) {
                return false;
            }
        }
        {
            String lhsUseAutopayAsPaymentFlag;
            lhsUseAutopayAsPaymentFlag = this.getUseAutopayAsPaymentFlag();
            String rhsUseAutopayAsPaymentFlag;
            rhsUseAutopayAsPaymentFlag = that.getUseAutopayAsPaymentFlag();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "useAutopayAsPaymentFlag", lhsUseAutopayAsPaymentFlag), LocatorUtils.property(thatLocator, "useAutopayAsPaymentFlag", rhsUseAutopayAsPaymentFlag), lhsUseAutopayAsPaymentFlag, rhsUseAutopayAsPaymentFlag, true, true)) {
                return false;
            }
        }
        {
            PaymentMethodType lhsAutopayInfo;
            lhsAutopayInfo = this.getAutopayInfo();
            PaymentMethodType rhsAutopayInfo;
            rhsAutopayInfo = that.getAutopayInfo();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "autopayInfo", lhsAutopayInfo), LocatorUtils.property(thatLocator, "autopayInfo", rhsAutopayInfo), lhsAutopayInfo, rhsAutopayInfo, (this.autopayInfo!= null), (that.autopayInfo!= null))) {
                return false;
            }
        }
        {
            PaymentMethodTypeOneTime lhsOneTimePaymentInfo;
            lhsOneTimePaymentInfo = this.getOneTimePaymentInfo();
            PaymentMethodTypeOneTime rhsOneTimePaymentInfo;
            rhsOneTimePaymentInfo = that.getOneTimePaymentInfo();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "oneTimePaymentInfo", lhsOneTimePaymentInfo), LocatorUtils.property(thatLocator, "oneTimePaymentInfo", rhsOneTimePaymentInfo), lhsOneTimePaymentInfo, rhsOneTimePaymentInfo, (this.oneTimePaymentInfo!= null), (that.oneTimePaymentInfo!= null))) {
                return false;
            }
        }
        return true;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(Object object) {
        final EqualsStrategy2 strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode(ObjectLocator locator, HashCodeStrategy2 strategy) {
        int currentHashCode = 1;
        {
            String theAmountDueNow;
            theAmountDueNow = this.getAmountDueNow();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "amountDueNow", theAmountDueNow), currentHashCode, theAmountDueNow, (this.amountDueNow!= null));
        }
        {
            String theOverrideAmountDueFlag;
            theOverrideAmountDueFlag = this.getOverrideAmountDueFlag();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "overrideAmountDueFlag", theOverrideAmountDueFlag), currentHashCode, theOverrideAmountDueFlag, true);
        }
        {
            String theUseAutopayAsPaymentFlag;
            theUseAutopayAsPaymentFlag = this.getUseAutopayAsPaymentFlag();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "useAutopayAsPaymentFlag", theUseAutopayAsPaymentFlag), currentHashCode, theUseAutopayAsPaymentFlag, true);
        }
        {
            PaymentMethodType theAutopayInfo;
            theAutopayInfo = this.getAutopayInfo();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "autopayInfo", theAutopayInfo), currentHashCode, theAutopayInfo, (this.autopayInfo!= null));
        }
        {
            PaymentMethodTypeOneTime theOneTimePaymentInfo;
            theOneTimePaymentInfo = this.getOneTimePaymentInfo();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "oneTimePaymentInfo", theOneTimePaymentInfo), currentHashCode, theOneTimePaymentInfo, (this.oneTimePaymentInfo!= null));
        }
        return currentHashCode;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode() {
        final HashCodeStrategy2 strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String toString() {
        final ToStringStrategy2 strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        {
            String theAmountDueNow;
            theAmountDueNow = this.getAmountDueNow();
            strategy.appendField(locator, this, "amountDueNow", buffer, theAmountDueNow, (this.amountDueNow!= null));
        }
        {
            String theOverrideAmountDueFlag;
            theOverrideAmountDueFlag = this.getOverrideAmountDueFlag();
            strategy.appendField(locator, this, "overrideAmountDueFlag", buffer, theOverrideAmountDueFlag, true);
        }
        {
            String theUseAutopayAsPaymentFlag;
            theUseAutopayAsPaymentFlag = this.getUseAutopayAsPaymentFlag();
            strategy.appendField(locator, this, "useAutopayAsPaymentFlag", buffer, theUseAutopayAsPaymentFlag, true);
        }
        {
            PaymentMethodType theAutopayInfo;
            theAutopayInfo = this.getAutopayInfo();
            strategy.appendField(locator, this, "autopayInfo", buffer, theAutopayInfo, (this.autopayInfo!= null));
        }
        {
            PaymentMethodTypeOneTime theOneTimePaymentInfo;
            theOneTimePaymentInfo = this.getOneTimePaymentInfo();
            strategy.appendField(locator, this, "oneTimePaymentInfo", buffer, theOneTimePaymentInfo, (this.oneTimePaymentInfo!= null));
        }
        return buffer;
    }

}
