
package dishnetwork.schemas;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.Equals2;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy2;
import org.jvnet.jaxb2_commons.lang.HashCode2;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy2;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString2;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy2;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * Contains the OrbitalInfo.
 *
 * <p>Java class for OrbitalInfoType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="OrbitalInfoType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="orbital" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="signalGrade" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="preference" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrbitalInfoType", propOrder = {
    "orbital",
    "signalGrade",
    "preference"
})
@Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
public class OrbitalInfoType implements Equals2, HashCode2, ToString2
{

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String orbital;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String signalGrade;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String preference;

    /**
     * Default no-arg constructor
     *
     */
    public OrbitalInfoType() {
        super();
    }

    /**
     * Fully-initialising value constructor
     *
     */
    public OrbitalInfoType(final String orbital, final String signalGrade, final String preference) {
        this.orbital = orbital;
        this.signalGrade = signalGrade;
        this.preference = preference;
    }

    /**
     * Gets the value of the orbital property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getOrbital() {
        return orbital;
    }

    /**
     * Sets the value of the orbital property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setOrbital(String value) {
        this.orbital = value;
    }

    /**
     * Gets the value of the signalGrade property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getSignalGrade() {
        return signalGrade;
    }

    /**
     * Sets the value of the signalGrade property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setSignalGrade(String value) {
        this.signalGrade = value;
    }

    /**
     * Gets the value of the preference property.
     *
     * @return
     *     possible object is
     *     {@link Integer }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getPreference() {
        return preference;
    }

    /**
     * Sets the value of the preference property.
     *
     * @param value
     *     allowed object is
     *     {@link Integer }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setPreference(String value) {
        this.preference = value;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy2 strategy) {
        if ((object == null)||(this.getClass()!= object.getClass())) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final OrbitalInfoType that = ((OrbitalInfoType) object);
        {
            String lhsOrbital;
            lhsOrbital = this.getOrbital();
            String rhsOrbital;
            rhsOrbital = that.getOrbital();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "orbital", lhsOrbital), LocatorUtils.property(thatLocator, "orbital", rhsOrbital), lhsOrbital, rhsOrbital, (this.orbital!= null), (that.orbital!= null))) {
                return false;
            }
        }
        {
            String lhsSignalGrade;
            lhsSignalGrade = this.getSignalGrade();
            String rhsSignalGrade;
            rhsSignalGrade = that.getSignalGrade();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "signalGrade", lhsSignalGrade), LocatorUtils.property(thatLocator, "signalGrade", rhsSignalGrade), lhsSignalGrade, rhsSignalGrade, (this.signalGrade!= null), (that.signalGrade!= null))) {
                return false;
            }
        }
        {
            String lhsPreference;
            lhsPreference = this.getPreference();
            String rhsPreference;
            rhsPreference = that.getPreference();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "preference", lhsPreference), LocatorUtils.property(thatLocator, "preference", rhsPreference), lhsPreference, rhsPreference, (this.preference!= null), (that.preference!= null))) {
                return false;
            }
        }
        return true;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(Object object) {
        final EqualsStrategy2 strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode(ObjectLocator locator, HashCodeStrategy2 strategy) {
        int currentHashCode = 1;
        {
            String theOrbital;
            theOrbital = this.getOrbital();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "orbital", theOrbital), currentHashCode, theOrbital, (this.orbital!= null));
        }
        {
            String theSignalGrade;
            theSignalGrade = this.getSignalGrade();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "signalGrade", theSignalGrade), currentHashCode, theSignalGrade, (this.signalGrade!= null));
        }
        {
            String thePreference;
            thePreference = this.getPreference();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "preference", thePreference), currentHashCode, thePreference, (this.preference!= null));
        }
        return currentHashCode;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode() {
        final HashCodeStrategy2 strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String toString() {
        final ToStringStrategy2 strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        {
            String theOrbital;
            theOrbital = this.getOrbital();
            strategy.appendField(locator, this, "orbital", buffer, theOrbital, (this.orbital!= null));
        }
        {
            String theSignalGrade;
            theSignalGrade = this.getSignalGrade();
            strategy.appendField(locator, this, "signalGrade", buffer, theSignalGrade, (this.signalGrade!= null));
        }
        {
            String thePreference;
            thePreference = this.getPreference();
            strategy.appendField(locator, this, "preference", buffer, thePreference, (this.preference!= null));
        }
        return buffer;
    }

}
