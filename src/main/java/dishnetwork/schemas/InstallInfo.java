package dishnetwork.schemas;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.jvnet.jaxb2_commons.lang.*;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="componentIdList" type="{http://www.dishnetwork.com/schema/common/ComponentId/2015_01_15}ComponentIdListType"/&gt;
 *         &lt;element name="easternArcFlag" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="spotBeamInformation" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="programmingOrbitalSlot" type="{http://www.dishnetwork.com/schema/CustomerManagement/ProgrammingOrbitalSlot/2013_01_17}ProgrammingOrbitalSlotType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="receiverItemList" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="receiverItem" type="{http://www.dishnetwork.com/schema/CustomerManagement/Item/2013_01_17}ItemType" maxOccurs="unbounded"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="antennaItemList" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="antennaItem" type="{http://www.dishnetwork.com/schema/CustomerManagement/Item/2013_01_17}ItemType" maxOccurs="unbounded"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="antennaTagCodeList" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="antennaTagCode" type="{http://www.dishnetwork.com/schema/CustomerManagement/Item/2013_01_17}ItemType" maxOccurs="unbounded"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="receiverConfigInfo" type="{http://www.dishnetwork.com/schema/common/ReceiverConfiguration/2016_01_14}ReceiverConfigurationType" minOccurs="0"/&gt;
 *         &lt;element name="expectedEquipmentList"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="expectedEquipment" type="{http://www.dishnetwork.com/schema/Equipment/ExpectedEquipmentInfoList/2014_06_13}EquipmentType" maxOccurs="unbounded"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="broadbandItemList" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="broadbandItem" type="{http://www.dishnetwork.com/schema/CustomerManagement/Item/2013_01_17}ItemType" maxOccurs="unbounded"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="installData" type="{http://www.dishnetwork.com/schema/common/CustomFieldList/2011_04_01}CustomFieldListType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "componentIdList",
    "easternArcFlag",
    "spotBeamInformation",
    "programmingOrbitalSlot",
    "receiverItemList",
    "antennaItemList",
    "antennaTagCodeList",
    "receiverConfigInfo",
    "expectedEquipmentList",
    "broadbandItemList",
    "installData"
})
@Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
public class InstallInfo implements Equals2, HashCode2, ToString2 {

    //@XmlElement(required = true)
    @JsonProperty(required = true)
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected ComponentIdListType componentIdList;
    //@XmlElement(defaultValue = "false")
    @JsonProperty(defaultValue = "false")
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String easternArcFlag;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected List<String> spotBeamInformation;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected List<ProgrammingOrbitalSlotType> programmingOrbitalSlot;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected ReceiverItemList receiverItemList;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected AntennaItemList antennaItemList;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected AntennaTagCodeList antennaTagCodeList;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected ReceiverConfigurationType receiverConfigInfo;
    //@XmlElement(required = true)
    @JsonProperty(required = true)
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected ExpectedEquipmentList expectedEquipmentList;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected BroadbandItemList broadbandItemList;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected CustomFieldListType installData;

    /**
     * Default no-arg constructor
     */
    public InstallInfo() {
        super();
    }

    /**
     * Fully-initialising value constructor
     */
    public InstallInfo(final ComponentIdListType componentIdList, final String easternArcFlag, final List<String> spotBeamInformation, final List<ProgrammingOrbitalSlotType> programmingOrbitalSlot, final ReceiverItemList receiverItemList, final AntennaItemList antennaItemList, final AntennaTagCodeList antennaTagCodeList, final ReceiverConfigurationType receiverConfigInfo, final ExpectedEquipmentList expectedEquipmentList, final BroadbandItemList broadbandItemList, final CustomFieldListType installData) {
        this.componentIdList = componentIdList;
        this.easternArcFlag = easternArcFlag;
        this.spotBeamInformation = spotBeamInformation;
        this.programmingOrbitalSlot = programmingOrbitalSlot;
        this.receiverItemList = receiverItemList;
        this.antennaItemList = antennaItemList;
        this.antennaTagCodeList = antennaTagCodeList;
        this.receiverConfigInfo = receiverConfigInfo;
        this.expectedEquipmentList = expectedEquipmentList;
        this.broadbandItemList = broadbandItemList;
        this.installData = installData;
    }

    /**
     * Gets the value of the componentIdList property.
     *
     * @return possible object is
     * {@link ComponentIdListType }
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public ComponentIdListType getComponentIdList() {
        return componentIdList;
    }

    /**
     * Sets the value of the componentIdList property.
     *
     * @param value allowed object is
     *              {@link ComponentIdListType }
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setComponentIdList(ComponentIdListType value) {
        this.componentIdList = value;
    }

    /**
     * Gets the value of the easternArcFlag property.
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getEasternArcFlag() {
        return easternArcFlag;
    }

    /**
     * Sets the value of the easternArcFlag property.
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setEasternArcFlag(String value) {
        this.easternArcFlag = value;
    }

    /**
     * Gets the value of the spotBeamInformation property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the spotBeamInformation property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSpotBeamInformation().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public List<String> getSpotBeamInformation() {
        if (spotBeamInformation == null) {
            spotBeamInformation = new ArrayList<String>();
        }
        return this.spotBeamInformation;
    }

    /**
     * Gets the value of the programmingOrbitalSlot property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the programmingOrbitalSlot property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProgrammingOrbitalSlot().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProgrammingOrbitalSlotType }
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public List<ProgrammingOrbitalSlotType> getProgrammingOrbitalSlot() {
        if (programmingOrbitalSlot == null) {
            programmingOrbitalSlot = new ArrayList<ProgrammingOrbitalSlotType>();
        }
        return this.programmingOrbitalSlot;
    }

    /**
     * Gets the value of the receiverItemList property.
     *
     * @return possible object is
     * {@link ReceiverItemList }
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public ReceiverItemList getReceiverItemList() {
        return receiverItemList;
    }

    /**
     * Sets the value of the receiverItemList property.
     *
     * @param value allowed object is
     *              {@link ReceiverItemList }
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setReceiverItemList(ReceiverItemList value) {
        this.receiverItemList = value;
    }

    /**
     * Gets the value of the antennaItemList property.
     *
     * @return possible object is
     * {@link AntennaItemList }
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public AntennaItemList getAntennaItemList() {
        return antennaItemList;
    }

    /**
     * Sets the value of the antennaItemList property.
     *
     * @param value allowed object is
     *              {@link AntennaItemList }
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setAntennaItemList(AntennaItemList value) {
        this.antennaItemList = value;
    }

    /**
     * Gets the value of the antennaTagCodeList property.
     *
     * @return possible object is
     * {@link AntennaTagCodeList }
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public AntennaTagCodeList getAntennaTagCodeList() {
        return antennaTagCodeList;
    }

    /**
     * Sets the value of the antennaTagCodeList property.
     *
     * @param value allowed object is
     *              {@link AntennaTagCodeList }
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setAntennaTagCodeList(AntennaTagCodeList value) {
        this.antennaTagCodeList = value;
    }

    /**
     * Gets the value of the receiverConfigInfo property.
     *
     * @return possible object is
     * {@link ReceiverConfigurationType }
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public ReceiverConfigurationType getReceiverConfigInfo() {
        return receiverConfigInfo;
    }

    /**
     * Sets the value of the receiverConfigInfo property.
     *
     * @param value allowed object is
     *              {@link ReceiverConfigurationType }
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setReceiverConfigInfo(ReceiverConfigurationType value) {
        this.receiverConfigInfo = value;
    }

    /**
     * Gets the value of the expectedEquipmentList property.
     *
     * @return possible object is
     * {@link ExpectedEquipmentList }
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public ExpectedEquipmentList getExpectedEquipmentList() {
        return expectedEquipmentList;
    }

    /**
     * Sets the value of the expectedEquipmentList property.
     *
     * @param value allowed object is
     *              {@link ExpectedEquipmentList }
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setExpectedEquipmentList(ExpectedEquipmentList value) {
        this.expectedEquipmentList = value;
    }

    /**
     * Gets the value of the broadbandItemList property.
     *
     * @return possible object is
     * {@link BroadbandItemList }
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public BroadbandItemList getBroadbandItemList() {
        return broadbandItemList;
    }

    /**
     * Sets the value of the broadbandItemList property.
     *
     * @param value allowed object is
     *              {@link BroadbandItemList }
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setBroadbandItemList(BroadbandItemList value) {
        this.broadbandItemList = value;
    }

    /**
     * Gets the value of the installData property.
     *
     * @return possible object is
     * {@link CustomFieldListType }
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public CustomFieldListType getInstallData() {
        return installData;
    }

    /**
     * Sets the value of the installData property.
     *
     * @param value allowed object is
     *              {@link CustomFieldListType }
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setInstallData(CustomFieldListType value) {
        this.installData = value;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy2 strategy) {
        if ((object == null) || (this.getClass() != object.getClass())) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final InstallInfo that = ((InstallInfo) object);
        {
            ComponentIdListType lhsComponentIdList;
            lhsComponentIdList = this.getComponentIdList();
            ComponentIdListType rhsComponentIdList;
            rhsComponentIdList = that.getComponentIdList();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "componentIdList", lhsComponentIdList), LocatorUtils.property(thatLocator, "componentIdList", rhsComponentIdList), lhsComponentIdList, rhsComponentIdList, (this.componentIdList != null), (that.componentIdList != null))) {
                return false;
            }
        }
        {
            String lhsEasternArcFlag;
            lhsEasternArcFlag = this.getEasternArcFlag();
            String rhsEasternArcFlag;
            rhsEasternArcFlag = that.getEasternArcFlag();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "easternArcFlag", lhsEasternArcFlag), LocatorUtils.property(thatLocator, "easternArcFlag", rhsEasternArcFlag), lhsEasternArcFlag, rhsEasternArcFlag, true, true)) {
                return false;
            }
        }
        {
            List<String> lhsSpotBeamInformation;
            lhsSpotBeamInformation = (((this.spotBeamInformation != null) && (!this.spotBeamInformation.isEmpty())) ? this.getSpotBeamInformation() : null);
            List<String> rhsSpotBeamInformation;
            rhsSpotBeamInformation = (((that.spotBeamInformation != null) && (!that.spotBeamInformation.isEmpty())) ? that.getSpotBeamInformation() : null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "spotBeamInformation", lhsSpotBeamInformation), LocatorUtils.property(thatLocator, "spotBeamInformation", rhsSpotBeamInformation), lhsSpotBeamInformation, rhsSpotBeamInformation, ((this.spotBeamInformation != null) && (!this.spotBeamInformation.isEmpty())), ((that.spotBeamInformation != null) && (!that.spotBeamInformation.isEmpty())))) {
                return false;
            }
        }
        {
            List<ProgrammingOrbitalSlotType> lhsProgrammingOrbitalSlot;
            lhsProgrammingOrbitalSlot = (((this.programmingOrbitalSlot != null) && (!this.programmingOrbitalSlot.isEmpty())) ? this.getProgrammingOrbitalSlot() : null);
            List<ProgrammingOrbitalSlotType> rhsProgrammingOrbitalSlot;
            rhsProgrammingOrbitalSlot = (((that.programmingOrbitalSlot != null) && (!that.programmingOrbitalSlot.isEmpty())) ? that.getProgrammingOrbitalSlot() : null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "programmingOrbitalSlot", lhsProgrammingOrbitalSlot), LocatorUtils.property(thatLocator, "programmingOrbitalSlot", rhsProgrammingOrbitalSlot), lhsProgrammingOrbitalSlot, rhsProgrammingOrbitalSlot, ((this.programmingOrbitalSlot != null) && (!this.programmingOrbitalSlot.isEmpty())), ((that.programmingOrbitalSlot != null) && (!that.programmingOrbitalSlot.isEmpty())))) {
                return false;
            }
        }
        {
            ReceiverItemList lhsReceiverItemList;
            lhsReceiverItemList = this.getReceiverItemList();
            ReceiverItemList rhsReceiverItemList;
            rhsReceiverItemList = that.getReceiverItemList();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "receiverItemList", lhsReceiverItemList), LocatorUtils.property(thatLocator, "receiverItemList", rhsReceiverItemList), lhsReceiverItemList, rhsReceiverItemList, (this.receiverItemList != null), (that.receiverItemList != null))) {
                return false;
            }
        }
        {
            AntennaItemList lhsAntennaItemList;
            lhsAntennaItemList = this.getAntennaItemList();
            AntennaItemList rhsAntennaItemList;
            rhsAntennaItemList = that.getAntennaItemList();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "antennaItemList", lhsAntennaItemList), LocatorUtils.property(thatLocator, "antennaItemList", rhsAntennaItemList), lhsAntennaItemList, rhsAntennaItemList, (this.antennaItemList != null), (that.antennaItemList != null))) {
                return false;
            }
        }
        {
            AntennaTagCodeList lhsAntennaTagCodeList;
            lhsAntennaTagCodeList = this.getAntennaTagCodeList();
            AntennaTagCodeList rhsAntennaTagCodeList;
            rhsAntennaTagCodeList = that.getAntennaTagCodeList();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "antennaTagCodeList", lhsAntennaTagCodeList), LocatorUtils.property(thatLocator, "antennaTagCodeList", rhsAntennaTagCodeList), lhsAntennaTagCodeList, rhsAntennaTagCodeList, (this.antennaTagCodeList != null), (that.antennaTagCodeList != null))) {
                return false;
            }
        }
        {
            ReceiverConfigurationType lhsReceiverConfigInfo;
            lhsReceiverConfigInfo = this.getReceiverConfigInfo();
            ReceiverConfigurationType rhsReceiverConfigInfo;
            rhsReceiverConfigInfo = that.getReceiverConfigInfo();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "receiverConfigInfo", lhsReceiverConfigInfo), LocatorUtils.property(thatLocator, "receiverConfigInfo", rhsReceiverConfigInfo), lhsReceiverConfigInfo, rhsReceiverConfigInfo, (this.receiverConfigInfo != null), (that.receiverConfigInfo != null))) {
                return false;
            }
        }
        {
            ExpectedEquipmentList lhsExpectedEquipmentList;
            lhsExpectedEquipmentList = this.getExpectedEquipmentList();
            ExpectedEquipmentList rhsExpectedEquipmentList;
            rhsExpectedEquipmentList = that.getExpectedEquipmentList();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "expectedEquipmentList", lhsExpectedEquipmentList), LocatorUtils.property(thatLocator, "expectedEquipmentList", rhsExpectedEquipmentList), lhsExpectedEquipmentList, rhsExpectedEquipmentList, (this.expectedEquipmentList != null), (that.expectedEquipmentList != null))) {
                return false;
            }
        }
        {
            BroadbandItemList lhsBroadbandItemList;
            lhsBroadbandItemList = this.getBroadbandItemList();
            BroadbandItemList rhsBroadbandItemList;
            rhsBroadbandItemList = that.getBroadbandItemList();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "broadbandItemList", lhsBroadbandItemList), LocatorUtils.property(thatLocator, "broadbandItemList", rhsBroadbandItemList), lhsBroadbandItemList, rhsBroadbandItemList, (this.broadbandItemList != null), (that.broadbandItemList != null))) {
                return false;
            }
        }
        {
            CustomFieldListType lhsInstallData;
            lhsInstallData = this.getInstallData();
            CustomFieldListType rhsInstallData;
            rhsInstallData = that.getInstallData();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "installData", lhsInstallData), LocatorUtils.property(thatLocator, "installData", rhsInstallData), lhsInstallData, rhsInstallData, (this.installData != null), (that.installData != null))) {
                return false;
            }
        }
        return true;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(Object object) {
        final EqualsStrategy2 strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode(ObjectLocator locator, HashCodeStrategy2 strategy) {
        int currentHashCode = 1;
        {
            ComponentIdListType theComponentIdList;
            theComponentIdList = this.getComponentIdList();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "componentIdList", theComponentIdList), currentHashCode, theComponentIdList, (this.componentIdList != null));
        }
        {
            String theEasternArcFlag;
            theEasternArcFlag = this.getEasternArcFlag();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "easternArcFlag", theEasternArcFlag), currentHashCode, theEasternArcFlag, true);
        }
        {
            List<String> theSpotBeamInformation;
            theSpotBeamInformation = (((this.spotBeamInformation != null) && (!this.spotBeamInformation.isEmpty())) ? this.getSpotBeamInformation() : null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "spotBeamInformation", theSpotBeamInformation), currentHashCode, theSpotBeamInformation, ((this.spotBeamInformation != null) && (!this.spotBeamInformation.isEmpty())));
        }
        {
            List<ProgrammingOrbitalSlotType> theProgrammingOrbitalSlot;
            theProgrammingOrbitalSlot = (((this.programmingOrbitalSlot != null) && (!this.programmingOrbitalSlot.isEmpty())) ? this.getProgrammingOrbitalSlot() : null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "programmingOrbitalSlot", theProgrammingOrbitalSlot), currentHashCode, theProgrammingOrbitalSlot, ((this.programmingOrbitalSlot != null) && (!this.programmingOrbitalSlot.isEmpty())));
        }
        {
            ReceiverItemList theReceiverItemList;
            theReceiverItemList = this.getReceiverItemList();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "receiverItemList", theReceiverItemList), currentHashCode, theReceiverItemList, (this.receiverItemList != null));
        }
        {
            AntennaItemList theAntennaItemList;
            theAntennaItemList = this.getAntennaItemList();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "antennaItemList", theAntennaItemList), currentHashCode, theAntennaItemList, (this.antennaItemList != null));
        }
        {
            AntennaTagCodeList theAntennaTagCodeList;
            theAntennaTagCodeList = this.getAntennaTagCodeList();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "antennaTagCodeList", theAntennaTagCodeList), currentHashCode, theAntennaTagCodeList, (this.antennaTagCodeList != null));
        }
        {
            ReceiverConfigurationType theReceiverConfigInfo;
            theReceiverConfigInfo = this.getReceiverConfigInfo();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "receiverConfigInfo", theReceiverConfigInfo), currentHashCode, theReceiverConfigInfo, (this.receiverConfigInfo != null));
        }
        {
            ExpectedEquipmentList theExpectedEquipmentList;
            theExpectedEquipmentList = this.getExpectedEquipmentList();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "expectedEquipmentList", theExpectedEquipmentList), currentHashCode, theExpectedEquipmentList, (this.expectedEquipmentList != null));
        }
        {
            BroadbandItemList theBroadbandItemList;
            theBroadbandItemList = this.getBroadbandItemList();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "broadbandItemList", theBroadbandItemList), currentHashCode, theBroadbandItemList, (this.broadbandItemList != null));
        }
        {
            CustomFieldListType theInstallData;
            theInstallData = this.getInstallData();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "installData", theInstallData), currentHashCode, theInstallData, (this.installData != null));
        }
        return currentHashCode;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode() {
        final HashCodeStrategy2 strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String toString() {
        final ToStringStrategy2 strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        {
            ComponentIdListType theComponentIdList;
            theComponentIdList = this.getComponentIdList();
            strategy.appendField(locator, this, "componentIdList", buffer, theComponentIdList, (this.componentIdList != null));
        }
        {
            String theEasternArcFlag;
            theEasternArcFlag = this.getEasternArcFlag();
            strategy.appendField(locator, this, "easternArcFlag", buffer, theEasternArcFlag, true);
        }
        {
            List<String> theSpotBeamInformation;
            theSpotBeamInformation = (((this.spotBeamInformation != null) && (!this.spotBeamInformation.isEmpty())) ? this.getSpotBeamInformation() : null);
            strategy.appendField(locator, this, "spotBeamInformation", buffer, theSpotBeamInformation, ((this.spotBeamInformation != null) && (!this.spotBeamInformation.isEmpty())));
        }
        {
            List<ProgrammingOrbitalSlotType> theProgrammingOrbitalSlot;
            theProgrammingOrbitalSlot = (((this.programmingOrbitalSlot != null) && (!this.programmingOrbitalSlot.isEmpty())) ? this.getProgrammingOrbitalSlot() : null);
            strategy.appendField(locator, this, "programmingOrbitalSlot", buffer, theProgrammingOrbitalSlot, ((this.programmingOrbitalSlot != null) && (!this.programmingOrbitalSlot.isEmpty())));
        }
        {
            ReceiverItemList theReceiverItemList;
            theReceiverItemList = this.getReceiverItemList();
            strategy.appendField(locator, this, "receiverItemList", buffer, theReceiverItemList, (this.receiverItemList != null));
        }
        {
            AntennaItemList theAntennaItemList;
            theAntennaItemList = this.getAntennaItemList();
            strategy.appendField(locator, this, "antennaItemList", buffer, theAntennaItemList, (this.antennaItemList != null));
        }
        {
            AntennaTagCodeList theAntennaTagCodeList;
            theAntennaTagCodeList = this.getAntennaTagCodeList();
            strategy.appendField(locator, this, "antennaTagCodeList", buffer, theAntennaTagCodeList, (this.antennaTagCodeList != null));
        }
        {
            ReceiverConfigurationType theReceiverConfigInfo;
            theReceiverConfigInfo = this.getReceiverConfigInfo();
            strategy.appendField(locator, this, "receiverConfigInfo", buffer, theReceiverConfigInfo, (this.receiverConfigInfo != null));
        }
        {
            ExpectedEquipmentList theExpectedEquipmentList;
            theExpectedEquipmentList = this.getExpectedEquipmentList();
            strategy.appendField(locator, this, "expectedEquipmentList", buffer, theExpectedEquipmentList, (this.expectedEquipmentList != null));
        }
        {
            BroadbandItemList theBroadbandItemList;
            theBroadbandItemList = this.getBroadbandItemList();
            strategy.appendField(locator, this, "broadbandItemList", buffer, theBroadbandItemList, (this.broadbandItemList != null));
        }
        {
            CustomFieldListType theInstallData;
            theInstallData = this.getInstallData();
            strategy.appendField(locator, this, "installData", buffer, theInstallData, (this.installData != null));
        }
        return buffer;
    }


}
