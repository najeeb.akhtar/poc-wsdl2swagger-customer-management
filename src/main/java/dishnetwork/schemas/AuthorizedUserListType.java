
package dishnetwork.schemas;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.jvnet.jaxb2_commons.lang.Equals2;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy2;
import org.jvnet.jaxb2_commons.lang.HashCode2;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy2;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString2;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy2;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * The AuthorizedUser
 *
 * <p>Java class for AuthorizedUserListType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="AuthorizedUserListType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="authorizedUser" type="{http://www.dishnetwork.com/schema/CustomerManagement/AuthorizedUser/2011_04_01}AuthorizedUserType" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AuthorizedUserListType", propOrder = {
    "authorizedUser"
})
@Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
public class AuthorizedUserListType implements Equals2, HashCode2, ToString2
{

    //@XmlElement(required = true)
    @JsonProperty(required = true)
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected List<AuthorizedUserType> authorizedUser;

    /**
     * Default no-arg constructor
     *
     */
    public AuthorizedUserListType() {
        super();
    }

    /**
     * Fully-initialising value constructor
     *
     */
    public AuthorizedUserListType(final List<AuthorizedUserType> authorizedUser) {
        this.authorizedUser = authorizedUser;
    }

    /**
     * Gets the value of the authorizedUser property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the authorizedUser property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAuthorizedUser().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AuthorizedUserType }
     *
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public List<AuthorizedUserType> getAuthorizedUser() {
        if (authorizedUser == null) {
            authorizedUser = new ArrayList<AuthorizedUserType>();
        }
        return this.authorizedUser;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy2 strategy) {
        if ((object == null)||(this.getClass()!= object.getClass())) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final AuthorizedUserListType that = ((AuthorizedUserListType) object);
        {
            List<AuthorizedUserType> lhsAuthorizedUser;
            lhsAuthorizedUser = (((this.authorizedUser!= null)&&(!this.authorizedUser.isEmpty()))?this.getAuthorizedUser():null);
            List<AuthorizedUserType> rhsAuthorizedUser;
            rhsAuthorizedUser = (((that.authorizedUser!= null)&&(!that.authorizedUser.isEmpty()))?that.getAuthorizedUser():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "authorizedUser", lhsAuthorizedUser), LocatorUtils.property(thatLocator, "authorizedUser", rhsAuthorizedUser), lhsAuthorizedUser, rhsAuthorizedUser, ((this.authorizedUser!= null)&&(!this.authorizedUser.isEmpty())), ((that.authorizedUser!= null)&&(!that.authorizedUser.isEmpty())))) {
                return false;
            }
        }
        return true;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(Object object) {
        final EqualsStrategy2 strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode(ObjectLocator locator, HashCodeStrategy2 strategy) {
        int currentHashCode = 1;
        {
            List<AuthorizedUserType> theAuthorizedUser;
            theAuthorizedUser = (((this.authorizedUser!= null)&&(!this.authorizedUser.isEmpty()))?this.getAuthorizedUser():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "authorizedUser", theAuthorizedUser), currentHashCode, theAuthorizedUser, ((this.authorizedUser!= null)&&(!this.authorizedUser.isEmpty())));
        }
        return currentHashCode;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode() {
        final HashCodeStrategy2 strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String toString() {
        final ToStringStrategy2 strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        {
            List<AuthorizedUserType> theAuthorizedUser;
            theAuthorizedUser = (((this.authorizedUser!= null)&&(!this.authorizedUser.isEmpty()))?this.getAuthorizedUser():null);
            strategy.appendField(locator, this, "authorizedUser", buffer, theAuthorizedUser, ((this.authorizedUser!= null)&&(!this.authorizedUser.isEmpty())));
        }
        return buffer;
    }

}
