
package dishnetwork.schemas;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.jvnet.jaxb2_commons.lang.Equals2;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy2;
import org.jvnet.jaxb2_commons.lang.HashCode2;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy2;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString2;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy2;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * contains the receiver information.
 *
 * <p>Java class for ReceiverLocationListType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="ReceiverLocationListType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="receiverLocation" type="{http://www.dishnetwork.com/xsd/Equipment/ReceiverLocation/2016_01_14}ReceiverLocationType" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReceiverLocationListType", propOrder = {
    "receiverLocation"
})
@Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
public class ReceiverLocationListType implements Equals2, HashCode2, ToString2
{

    //@XmlElement(required = true)
    @JsonProperty(required = true)
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected List<ReceiverLocationType> receiverLocation;

    /**
     * Default no-arg constructor
     *
     */
    public ReceiverLocationListType() {
        super();
    }

    /**
     * Fully-initialising value constructor
     *
     */
    public ReceiverLocationListType(final List<ReceiverLocationType> receiverLocation) {
        this.receiverLocation = receiverLocation;
    }

    /**
     * Gets the value of the receiverLocation property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the receiverLocation property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReceiverLocation().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReceiverLocationType }
     *
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public List<ReceiverLocationType> getReceiverLocation() {
        if (receiverLocation == null) {
            receiverLocation = new ArrayList<ReceiverLocationType>();
        }
        return this.receiverLocation;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy2 strategy) {
        if ((object == null)||(this.getClass()!= object.getClass())) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final ReceiverLocationListType that = ((ReceiverLocationListType) object);
        {
            List<ReceiverLocationType> lhsReceiverLocation;
            lhsReceiverLocation = (((this.receiverLocation!= null)&&(!this.receiverLocation.isEmpty()))?this.getReceiverLocation():null);
            List<ReceiverLocationType> rhsReceiverLocation;
            rhsReceiverLocation = (((that.receiverLocation!= null)&&(!that.receiverLocation.isEmpty()))?that.getReceiverLocation():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "receiverLocation", lhsReceiverLocation), LocatorUtils.property(thatLocator, "receiverLocation", rhsReceiverLocation), lhsReceiverLocation, rhsReceiverLocation, ((this.receiverLocation!= null)&&(!this.receiverLocation.isEmpty())), ((that.receiverLocation!= null)&&(!that.receiverLocation.isEmpty())))) {
                return false;
            }
        }
        return true;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(Object object) {
        final EqualsStrategy2 strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode(ObjectLocator locator, HashCodeStrategy2 strategy) {
        int currentHashCode = 1;
        {
            List<ReceiverLocationType> theReceiverLocation;
            theReceiverLocation = (((this.receiverLocation!= null)&&(!this.receiverLocation.isEmpty()))?this.getReceiverLocation():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "receiverLocation", theReceiverLocation), currentHashCode, theReceiverLocation, ((this.receiverLocation!= null)&&(!this.receiverLocation.isEmpty())));
        }
        return currentHashCode;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode() {
        final HashCodeStrategy2 strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String toString() {
        final ToStringStrategy2 strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        {
            List<ReceiverLocationType> theReceiverLocation;
            theReceiverLocation = (((this.receiverLocation!= null)&&(!this.receiverLocation.isEmpty()))?this.getReceiverLocation():null);
            strategy.appendField(locator, this, "receiverLocation", buffer, theReceiverLocation, ((this.receiverLocation!= null)&&(!this.receiverLocation.isEmpty())));
        }
        return buffer;
    }

}
