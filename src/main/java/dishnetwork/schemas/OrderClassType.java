
package dishnetwork.schemas;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OrderClassType.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <pre>
 * &lt;simpleType name="OrderClassType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="SERVICE_ORDER"/&gt;
 *     &lt;enumeration value="SPECIAL_REQUEST"/&gt;
 *     &lt;enumeration value="TROUBLE_CALL"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 *
 */
@XmlType(name = "OrderClassType", namespace = "http://www.dishnetwork.com/schema/AccountOrder/OrderClass/2011_04_01")
@XmlEnum
@Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
public enum OrderClassType {

    SERVICE_ORDER,
    SPECIAL_REQUEST,
    TROUBLE_CALL;

    public String value() {
        return name();
    }

    public static OrderClassType fromValue(String v) {
        return valueOf(v);
    }

}
