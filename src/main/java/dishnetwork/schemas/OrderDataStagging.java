package dishnetwork.schemas;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class OrderDataStagging {
    protected String invalidateStaggingFlag;
    protected String isDataStage;
    protected OrderDetailsInfo orderDetailsInfo;
    protected OrderedServiceDataList orderedServiceData;
    protected QuestionAnswerList questionAnswerList;
}
