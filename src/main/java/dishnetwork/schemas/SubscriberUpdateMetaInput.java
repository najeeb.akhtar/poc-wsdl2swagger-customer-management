package dishnetwork.schemas;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class SubscriberUpdateMetaInput {
    protected AccountInfo accountInfo;
    protected AccountTypeSpaDetails accountTypeSpaDetails;
    protected LocationIdType locationId;
    protected Location location;
    protected OrderInfo orderInfo;
    protected PlanListType planList;
    protected ReceiverConfigurationType receiverConfigInfo;
    protected ExpectedEquipmentInfoList expectedEquipmentList;
    protected AccountPrivacyInfoType securityInfo;
    protected CustomerType customer;
    protected OfferCodeInfoType offerCode;
    protected CustomerQualification customerQualification;
    protected CertificateType certificate;
    protected AgreementList agreementList;
    protected ServiceProviderInfo serviceProviderInfo;
    protected SellerInfo sellerInfo;
    protected OrderDataStagging orderDataStagging;
    protected AccountCreated accountCreated;
    protected AuditData auditData;
    protected AttributeListType attributeList;
    protected String serializedOfferCode;
    protected String callingService;
    protected String doNotMapForMoverFlag;
    protected String nullifyPOCId;
}
