
package dishnetwork.schemas;

import java.math.BigInteger;
import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.jvnet.jaxb2_commons.lang.Equals2;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy2;
import org.jvnet.jaxb2_commons.lang.HashCode2;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy2;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString2;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy2;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 *
 * 			Contains the customer qualifation details eg. creditScoreId,CreditCardAuthtorizationTraxId etc.
 *
 *
 * <p>Java class for CustomerQualificationType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="CustomerQualificationType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="customerQualificationIdentifier" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="customerQualificationType" type="{http://www.dishnetwork.com/schema/CustomerManagement/CustomerQualification/2015_01_15}CustomerQualificationIdentifierType" minOccurs="0"/&gt;
 *                   &lt;element name="customerQualificationID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="creditCardAuthTransactionId" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *         &lt;element name="premiumFlag" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="exCustomerFlag" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="formerQualCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="qualificationOverrideFlag" type="{http://www.dishnetwork.com/schema/CustomerManagement/OverrideFlag/2011_04_01}OverrideFlagType" minOccurs="0"/&gt;
 *         &lt;element name="customerQualityTier" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CustomerQualificationType", propOrder = {
    "customerQualificationIdentifier",
    "creditCardAuthTransactionId",
    "premiumFlag",
    "exCustomerFlag",
    "formerQualCode",
    "qualificationOverrideFlag",
    "customerQualityTier"
})
@Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
public class CustomerQualificationType implements Equals2, HashCode2, ToString2
{

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected CustomerQualificationIdentifier customerQualificationIdentifier;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String creditCardAuthTransactionId;
    //@XmlElement(defaultValue = "false")
    @JsonProperty(defaultValue = "false")
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String premiumFlag;
    //@XmlElement(defaultValue = "false")
    @JsonProperty(defaultValue = "false")
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String exCustomerFlag;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String formerQualCode;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected OverrideFlagType qualificationOverrideFlag;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String customerQualityTier;

    /**
     * Default no-arg constructor
     *
     */
    public CustomerQualificationType() {
        super();
    }

    /**
     * Fully-initialising value constructor
     *
     */
    public CustomerQualificationType(final CustomerQualificationIdentifier customerQualificationIdentifier, final String creditCardAuthTransactionId, final String premiumFlag, final String exCustomerFlag, final String formerQualCode, final OverrideFlagType qualificationOverrideFlag, final String customerQualityTier) {
        this.customerQualificationIdentifier = customerQualificationIdentifier;
        this.creditCardAuthTransactionId = creditCardAuthTransactionId;
        this.premiumFlag = premiumFlag;
        this.exCustomerFlag = exCustomerFlag;
        this.formerQualCode = formerQualCode;
        this.qualificationOverrideFlag = qualificationOverrideFlag;
        this.customerQualityTier = customerQualityTier;
    }

    /**
     * Gets the value of the customerQualificationIdentifier property.
     *
     * @return
     *     possible object is
     *     {@link CustomerQualificationIdentifier }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public CustomerQualificationIdentifier getCustomerQualificationIdentifier() {
        return customerQualificationIdentifier;
    }

    /**
     * Sets the value of the customerQualificationIdentifier property.
     *
     * @param value
     *     allowed object is
     *     {@link CustomerQualificationIdentifier }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setCustomerQualificationIdentifier(CustomerQualificationIdentifier value) {
        this.customerQualificationIdentifier = value;
    }

    /**
     * Gets the value of the creditCardAuthTransactionId property.
     *
     * @return
     *     possible object is
     *     {@link BigInteger }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getCreditCardAuthTransactionId() {
        return creditCardAuthTransactionId;
    }

    /**
     * Sets the value of the creditCardAuthTransactionId property.
     *
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setCreditCardAuthTransactionId(String value) {
        this.creditCardAuthTransactionId = value;
    }

    /**
     * Gets the value of the premiumFlag property.
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getPremiumFlag() {
        return premiumFlag;
    }

    /**
     * Sets the value of the premiumFlag property.
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setPremiumFlag(String value) {
        this.premiumFlag = value;
    }

    /**
     * Gets the value of the exCustomerFlag property.
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getExCustomerFlag() {
        return exCustomerFlag;
    }

    /**
     * Sets the value of the exCustomerFlag property.
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setExCustomerFlag(String value) {
        this.exCustomerFlag = value;
    }

    /**
     * Gets the value of the formerQualCode property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getFormerQualCode() {
        return formerQualCode;
    }

    /**
     * Sets the value of the formerQualCode property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setFormerQualCode(String value) {
        this.formerQualCode = value;
    }

    /**
     * Gets the value of the qualificationOverrideFlag property.
     *
     * @return
     *     possible object is
     *     {@link OverrideFlagType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public OverrideFlagType getQualificationOverrideFlag() {
        return qualificationOverrideFlag;
    }

    /**
     * Sets the value of the qualificationOverrideFlag property.
     *
     * @param value
     *     allowed object is
     *     {@link OverrideFlagType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setQualificationOverrideFlag(OverrideFlagType value) {
        this.qualificationOverrideFlag = value;
    }

    /**
     * Gets the value of the customerQualityTier property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getCustomerQualityTier() {
        return customerQualityTier;
    }

    /**
     * Sets the value of the customerQualityTier property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setCustomerQualityTier(String value) {
        this.customerQualityTier = value;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy2 strategy) {
        if ((object == null)||(this.getClass()!= object.getClass())) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final CustomerQualificationType that = ((CustomerQualificationType) object);
        {
            CustomerQualificationIdentifier lhsCustomerQualificationIdentifier;
            lhsCustomerQualificationIdentifier = this.getCustomerQualificationIdentifier();
            CustomerQualificationIdentifier rhsCustomerQualificationIdentifier;
            rhsCustomerQualificationIdentifier = that.getCustomerQualificationIdentifier();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "customerQualificationIdentifier", lhsCustomerQualificationIdentifier), LocatorUtils.property(thatLocator, "customerQualificationIdentifier", rhsCustomerQualificationIdentifier), lhsCustomerQualificationIdentifier, rhsCustomerQualificationIdentifier, (this.customerQualificationIdentifier!= null), (that.customerQualificationIdentifier!= null))) {
                return false;
            }
        }
        {
            String lhsCreditCardAuthTransactionId;
            lhsCreditCardAuthTransactionId = this.getCreditCardAuthTransactionId();
            String rhsCreditCardAuthTransactionId;
            rhsCreditCardAuthTransactionId = that.getCreditCardAuthTransactionId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "creditCardAuthTransactionId", lhsCreditCardAuthTransactionId), LocatorUtils.property(thatLocator, "creditCardAuthTransactionId", rhsCreditCardAuthTransactionId), lhsCreditCardAuthTransactionId, rhsCreditCardAuthTransactionId, (this.creditCardAuthTransactionId!= null), (that.creditCardAuthTransactionId!= null))) {
                return false;
            }
        }
        {
            String lhsPremiumFlag;
            lhsPremiumFlag = this.getPremiumFlag();
            String rhsPremiumFlag;
            rhsPremiumFlag = that.getPremiumFlag();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "premiumFlag", lhsPremiumFlag), LocatorUtils.property(thatLocator, "premiumFlag", rhsPremiumFlag), lhsPremiumFlag, rhsPremiumFlag, true, true)) {
                return false;
            }
        }
        {
            String lhsExCustomerFlag;
            lhsExCustomerFlag = this.getExCustomerFlag();
            String rhsExCustomerFlag;
            rhsExCustomerFlag = that.getExCustomerFlag();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "exCustomerFlag", lhsExCustomerFlag), LocatorUtils.property(thatLocator, "exCustomerFlag", rhsExCustomerFlag), lhsExCustomerFlag, rhsExCustomerFlag, true, true)) {
                return false;
            }
        }
        {
            String lhsFormerQualCode;
            lhsFormerQualCode = this.getFormerQualCode();
            String rhsFormerQualCode;
            rhsFormerQualCode = that.getFormerQualCode();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "formerQualCode", lhsFormerQualCode), LocatorUtils.property(thatLocator, "formerQualCode", rhsFormerQualCode), lhsFormerQualCode, rhsFormerQualCode, (this.formerQualCode!= null), (that.formerQualCode!= null))) {
                return false;
            }
        }
        {
            OverrideFlagType lhsQualificationOverrideFlag;
            lhsQualificationOverrideFlag = this.getQualificationOverrideFlag();
            OverrideFlagType rhsQualificationOverrideFlag;
            rhsQualificationOverrideFlag = that.getQualificationOverrideFlag();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "qualificationOverrideFlag", lhsQualificationOverrideFlag), LocatorUtils.property(thatLocator, "qualificationOverrideFlag", rhsQualificationOverrideFlag), lhsQualificationOverrideFlag, rhsQualificationOverrideFlag, (this.qualificationOverrideFlag!= null), (that.qualificationOverrideFlag!= null))) {
                return false;
            }
        }
        {
            String lhsCustomerQualityTier;
            lhsCustomerQualityTier = this.getCustomerQualityTier();
            String rhsCustomerQualityTier;
            rhsCustomerQualityTier = that.getCustomerQualityTier();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "customerQualityTier", lhsCustomerQualityTier), LocatorUtils.property(thatLocator, "customerQualityTier", rhsCustomerQualityTier), lhsCustomerQualityTier, rhsCustomerQualityTier, (this.customerQualityTier!= null), (that.customerQualityTier!= null))) {
                return false;
            }
        }
        return true;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(Object object) {
        final EqualsStrategy2 strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode(ObjectLocator locator, HashCodeStrategy2 strategy) {
        int currentHashCode = 1;
        {
            CustomerQualificationIdentifier theCustomerQualificationIdentifier;
            theCustomerQualificationIdentifier = this.getCustomerQualificationIdentifier();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "customerQualificationIdentifier", theCustomerQualificationIdentifier), currentHashCode, theCustomerQualificationIdentifier, (this.customerQualificationIdentifier!= null));
        }
        {
            String theCreditCardAuthTransactionId;
            theCreditCardAuthTransactionId = this.getCreditCardAuthTransactionId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "creditCardAuthTransactionId", theCreditCardAuthTransactionId), currentHashCode, theCreditCardAuthTransactionId, (this.creditCardAuthTransactionId!= null));
        }
        {
            String thePremiumFlag;
            thePremiumFlag = this.getPremiumFlag();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "premiumFlag", thePremiumFlag), currentHashCode, thePremiumFlag, true);
        }
        {
            String theExCustomerFlag;
            theExCustomerFlag = this.getExCustomerFlag();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "exCustomerFlag", theExCustomerFlag), currentHashCode, theExCustomerFlag, true);
        }
        {
            String theFormerQualCode;
            theFormerQualCode = this.getFormerQualCode();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "formerQualCode", theFormerQualCode), currentHashCode, theFormerQualCode, (this.formerQualCode!= null));
        }
        {
            OverrideFlagType theQualificationOverrideFlag;
            theQualificationOverrideFlag = this.getQualificationOverrideFlag();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "qualificationOverrideFlag", theQualificationOverrideFlag), currentHashCode, theQualificationOverrideFlag, (this.qualificationOverrideFlag!= null));
        }
        {
            String theCustomerQualityTier;
            theCustomerQualityTier = this.getCustomerQualityTier();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "customerQualityTier", theCustomerQualityTier), currentHashCode, theCustomerQualityTier, (this.customerQualityTier!= null));
        }
        return currentHashCode;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode() {
        final HashCodeStrategy2 strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String toString() {
        final ToStringStrategy2 strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        {
            CustomerQualificationIdentifier theCustomerQualificationIdentifier;
            theCustomerQualificationIdentifier = this.getCustomerQualificationIdentifier();
            strategy.appendField(locator, this, "customerQualificationIdentifier", buffer, theCustomerQualificationIdentifier, (this.customerQualificationIdentifier!= null));
        }
        {
            String theCreditCardAuthTransactionId;
            theCreditCardAuthTransactionId = this.getCreditCardAuthTransactionId();
            strategy.appendField(locator, this, "creditCardAuthTransactionId", buffer, theCreditCardAuthTransactionId, (this.creditCardAuthTransactionId!= null));
        }
        {
            String thePremiumFlag;
            thePremiumFlag = this.getPremiumFlag();
            strategy.appendField(locator, this, "premiumFlag", buffer, thePremiumFlag, true);
        }
        {
            String theExCustomerFlag;
            theExCustomerFlag = this.getExCustomerFlag();
            strategy.appendField(locator, this, "exCustomerFlag", buffer, theExCustomerFlag, true);
        }
        {
            String theFormerQualCode;
            theFormerQualCode = this.getFormerQualCode();
            strategy.appendField(locator, this, "formerQualCode", buffer, theFormerQualCode, (this.formerQualCode!= null));
        }
        {
            OverrideFlagType theQualificationOverrideFlag;
            theQualificationOverrideFlag = this.getQualificationOverrideFlag();
            strategy.appendField(locator, this, "qualificationOverrideFlag", buffer, theQualificationOverrideFlag, (this.qualificationOverrideFlag!= null));
        }
        {
            String theCustomerQualityTier;
            theCustomerQualityTier = this.getCustomerQualityTier();
            strategy.appendField(locator, this, "customerQualityTier", buffer, theCustomerQualityTier, (this.customerQualityTier!= null));
        }
        return buffer;
    }


}
