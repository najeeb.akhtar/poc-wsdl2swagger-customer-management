package dishnetwork.schemas;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class AccountCreatedInfo {
    protected AccountIdType accountId;
    protected String accountType;
    protected String accountSubType;
    protected NameList nameList;
    protected AddressList addressList;
    protected EmailList emailList;
    protected PhoneList phoneList;
    protected SecurityPin securityPin;
    protected String ssn;
    protected String hardCopyStatementFlag;
    protected String autoPayFlag;
    protected String autoPayCardHolderName;
    protected String accountCreatedDate;
    protected PlanListType planList;
    protected CommercialAccountInfo commercialAccountInfo;
    protected OfferCodeInfoType offerCode;
    protected ItemListType itemList;
    protected ScheduleListType scheduleList;
    protected CustomerQualificationIdentifier customerQualificationIdentifier;
    protected OrderDataStaggingAccountCreated orderDataStagging;
}
