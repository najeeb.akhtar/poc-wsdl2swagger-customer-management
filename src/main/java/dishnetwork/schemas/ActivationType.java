
package dishnetwork.schemas;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ActivationType.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <pre>
 * &lt;simpleType name="ActivationType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="NEW"/&gt;
 *     &lt;enumeration value="UPGRADE"/&gt;
 *     &lt;enumeration value="EXCHANGE"/&gt;
 *     &lt;enumeration value="TROUBLE_EXCHANGE"/&gt;
 *     &lt;enumeration value="ALL"/&gt;
 *     &lt;enumeration value="OTHER"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 *
 */
@XmlType(name = "ActivationType")
@XmlEnum
@Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-12T08:55:33-06:00")
public enum ActivationType {

    NEW,
    UPGRADE,
    EXCHANGE,
    TROUBLE_EXCHANGE,
    ALL,
    OTHER;

    public String value() {
        return name();
    }

    public static ActivationType fromValue(String v) {
        return valueOf(v);
    }

}
