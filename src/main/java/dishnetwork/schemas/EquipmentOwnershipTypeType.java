
package dishnetwork.schemas;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EquipmentOwnershipTypeType.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <pre>
 * &lt;simpleType name="EquipmentOwnershipTypeType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="LEASED_NEW"/&gt;
 *     &lt;enumeration value="LEASED_EXISTING"/&gt;
 *     &lt;enumeration value="PURCHASED"/&gt;
 *     &lt;enumeration value="UNKNOWN"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 *
 */
@XmlType(name = "EquipmentOwnershipTypeType", namespace = "http://www.dishnetwork.com/schema/Equipment/EquipmentOwnershipType/2013_01_17")
@XmlEnum
@Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-12T08:55:33-06:00")
public enum EquipmentOwnershipTypeType {

    LEASED_NEW,
    LEASED_EXISTING,
    PURCHASED,
    UNKNOWN;

    public String value() {
        return name();
    }

    public static EquipmentOwnershipTypeType fromValue(String v) {
        return valueOf(v);
    }

}
