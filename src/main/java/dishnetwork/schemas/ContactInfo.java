package dishnetwork.schemas;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.jvnet.jaxb2_commons.lang.*;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="contactType"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;enumeration value="CALL"/&gt;
 *               &lt;enumeration value="TEXT"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="textNotification" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="contactCategoryList" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="contactCategory" maxOccurs="unbounded"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="categoryName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="contactPreferenceList"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;complexContent&gt;
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                     &lt;sequence&gt;
 *                                       &lt;element name="contactPreference" maxOccurs="unbounded"&gt;
 *                                         &lt;complexType&gt;
 *                                           &lt;complexContent&gt;
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                               &lt;sequence&gt;
 *                                                 &lt;element name="preference" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                                 &lt;element name="status"&gt;
 *                                                   &lt;simpleType&gt;
 *                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *                                                       &lt;enumeration value="ACTIVE"/&gt;
 *                                                       &lt;enumeration value="INACTIVE"/&gt;
 *                                                     &lt;/restriction&gt;
 *                                                   &lt;/simpleType&gt;
 *                                                 &lt;/element&gt;
 *                                               &lt;/sequence&gt;
 *                                             &lt;/restriction&gt;
 *                                           &lt;/complexContent&gt;
 *                                         &lt;/complexType&gt;
 *                                       &lt;/element&gt;
 *                                     &lt;/sequence&gt;
 *                                   &lt;/restriction&gt;
 *                                 &lt;/complexContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "contactType",
    "textNotification",
    "contactCategoryList"
})
@Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
public class ContactInfo implements Equals2, HashCode2, ToString2 {

    //@XmlElement(required = true)
    @JsonProperty(required = true)
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String contactType;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String textNotification;
    //@XmlElement(nillable = true)
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected ContactCategoryList contactCategoryList;

    /**
     * Default no-arg constructor
     */
    public ContactInfo() {
        super();
    }

    /**
     * Fully-initialising value constructor
     */
    public ContactInfo(final String contactType, final String textNotification, final ContactCategoryList contactCategoryList) {
        this.contactType = contactType;
        this.textNotification = textNotification;
        this.contactCategoryList = contactCategoryList;
    }

    /**
     * Gets the value of the contactType property.
     *
     * @return possible object is
     * {@link String }
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getContactType() {
        return contactType;
    }

    /**
     * Sets the value of the contactType property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setContactType(String value) {
        this.contactType = value;
    }

    /**
     * Gets the value of the textNotification property.
     *
     * @return possible object is
     * {@link String }
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getTextNotification() {
        return textNotification;
    }

    /**
     * Sets the value of the textNotification property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setTextNotification(String value) {
        this.textNotification = value;
    }

    /**
     * Gets the value of the contactCategoryList property.
     *
     * @return possible object is
     * {@link ContactCategoryList }
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public ContactCategoryList getContactCategoryList() {
        return contactCategoryList;
    }

    /**
     * Sets the value of the contactCategoryList property.
     *
     * @param value allowed object is
     *              {@link ContactCategoryList }
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setContactCategoryList(ContactCategoryList value) {
        this.contactCategoryList = value;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy2 strategy) {
        if ((object == null) || (this.getClass() != object.getClass())) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final ContactInfo that = ((ContactInfo) object);
        {
            String lhsContactType;
            lhsContactType = this.getContactType();
            String rhsContactType;
            rhsContactType = that.getContactType();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "contactType", lhsContactType), LocatorUtils.property(thatLocator, "contactType", rhsContactType), lhsContactType, rhsContactType, (this.contactType != null), (that.contactType != null))) {
                return false;
            }
        }
        {
            String lhsTextNotification;
            lhsTextNotification = this.getTextNotification();
            String rhsTextNotification;
            rhsTextNotification = that.getTextNotification();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "textNotification", lhsTextNotification), LocatorUtils.property(thatLocator, "textNotification", rhsTextNotification), lhsTextNotification, rhsTextNotification, (this.textNotification != null), (that.textNotification != null))) {
                return false;
            }
        }
        {
            ContactCategoryList lhsContactCategoryList;
            lhsContactCategoryList = this.getContactCategoryList();
            ContactCategoryList rhsContactCategoryList;
            rhsContactCategoryList = that.getContactCategoryList();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "contactCategoryList", lhsContactCategoryList), LocatorUtils.property(thatLocator, "contactCategoryList", rhsContactCategoryList), lhsContactCategoryList, rhsContactCategoryList, (this.contactCategoryList != null), (that.contactCategoryList != null))) {
                return false;
            }
        }
        return true;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(Object object) {
        final EqualsStrategy2 strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode(ObjectLocator locator, HashCodeStrategy2 strategy) {
        int currentHashCode = 1;
        {
            String theContactType;
            theContactType = this.getContactType();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "contactType", theContactType), currentHashCode, theContactType, (this.contactType != null));
        }
        {
            String theTextNotification;
            theTextNotification = this.getTextNotification();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "textNotification", theTextNotification), currentHashCode, theTextNotification, (this.textNotification != null));
        }
        {
            ContactCategoryList theContactCategoryList;
            theContactCategoryList = this.getContactCategoryList();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "contactCategoryList", theContactCategoryList), currentHashCode, theContactCategoryList, (this.contactCategoryList != null));
        }
        return currentHashCode;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode() {
        final HashCodeStrategy2 strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String toString() {
        final ToStringStrategy2 strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        {
            String theContactType;
            theContactType = this.getContactType();
            strategy.appendField(locator, this, "contactType", buffer, theContactType, (this.contactType != null));
        }
        {
            String theTextNotification;
            theTextNotification = this.getTextNotification();
            strategy.appendField(locator, this, "textNotification", buffer, theTextNotification, (this.textNotification != null));
        }
        {
            ContactCategoryList theContactCategoryList;
            theContactCategoryList = this.getContactCategoryList();
            strategy.appendField(locator, this, "contactCategoryList", buffer, theContactCategoryList, (this.contactCategoryList != null));
        }
        return buffer;
    }


}
