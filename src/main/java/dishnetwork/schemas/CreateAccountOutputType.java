
package dishnetwork.schemas;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

import org.jvnet.jaxb2_commons.lang.Equals2;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy2;
import org.jvnet.jaxb2_commons.lang.HashCode2;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy2;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString2;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy2;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;

import java.util.List;


/**
 * Create Account Object
 *
 * <p>Java class for CreateAccountOutputType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="CreateAccountOutputType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="accountId" type="{http://www.dishnetwork.com/schema/Customer/AccountId/2011_04_01}AccountIdType" minOccurs="0"/&gt;
 *         &lt;element name="orderId" type="{http://www.dishnetwork.com/schema/AccountOrder/OrderId/2011_04_01}OrderIdType" minOccurs="0"/&gt;
 *         &lt;element name="customerId" type="{http://www.dishnetwork.com/schema/Customer/CustomerId/2011_04_01}CustomerIdType" minOccurs="0"/&gt;
 *         &lt;element name="locationId" type="{http://www.dishnetwork.com/schema/Customer/LocationId/2011_04_01}LocationIdType" minOccurs="0"/&gt;
 *         &lt;element name="claimInfo" type="{http://www.dishnetwork.com/schema/CustomerManagement/WebClaimList/2011_04_01}WebClaimListType" minOccurs="0"/&gt;
 *         &lt;element name="paymentStatus" type="{http://www.dishnetwork.com/schema/CustomerManagement/PaymentStatus/2011_04_01}PaymentStatusType" minOccurs="0"/&gt;
 *         &lt;element name="autopayStatus" type="{http://www.dishnetwork.com/schema/CustomerManagement/AutoPayStatus/2011_04_01}AutoPayStatusType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreateAccountOutputType", propOrder = {
    "accountId",
    "orderId",
    "customerId",
    "locationId",
    "claimInfo",
    "paymentStatus",
    "autopayStatus"
})
@Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
public class CreateAccountOutputType implements Equals2, HashCode2, ToString2
{

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected AccountIdType accountId;
    //@Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    //protected OrderIdType orderId;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected CustomerIdType customerId;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected LocationIdType locationId;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected WebClaimListType claimInfo;
    //@Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    //protected PaymentStatusType paymentStatus;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected AutoPayStatusType autopayStatus;
    protected ProviderInfoList providerInfoList;
    protected RaHeaderOutput raHeader;
    protected List<OrderOutput> orderList;
    protected List<PaymentOutput> paymentList;

    public ProviderInfoList getProviderInfoList() {
        return providerInfoList;
    }

    public void setProviderInfoList(ProviderInfoList providerInfoList) {
        this.providerInfoList = providerInfoList;
    }

    public RaHeaderOutput getRaHeader() {
        return raHeader;
    }

    public void setRaHeader(RaHeaderOutput raHeader) {
        this.raHeader = raHeader;
    }

    public List<OrderOutput> getOrderList() {
        return orderList;
    }

    public void setOrderList(List<OrderOutput> orderList) {
        this.orderList = orderList;
    }

    public List<PaymentOutput> getPaymentList() {
        return paymentList;
    }

    public void setPaymentList(List<PaymentOutput> paymentList) {
        this.paymentList = paymentList;
    }

    /**
     * Default no-arg constructor
     *
     */
    public CreateAccountOutputType() {
        super();
    }

    /**
     * Fully-initialising value constructor
     *
     */
    public CreateAccountOutputType(final AccountIdType accountId, final CustomerIdType customerId, final LocationIdType locationId, final WebClaimListType claimInfo, final AutoPayStatusType autopayStatus) {
        this.accountId = accountId;
        this.customerId = customerId;
        this.locationId = locationId;
        this.claimInfo = claimInfo;
        this.autopayStatus = autopayStatus;
    }

    /**
     * Gets the value of the accountId property.
     *
     * @return
     *     possible object is
     *     {@link AccountIdType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public AccountIdType getAccountId() {
        return accountId;
    }

    /**
     * Sets the value of the accountId property.
     *
     * @param value
     *     allowed object is
     *     {@link AccountIdType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setAccountId(AccountIdType value) {
        this.accountId = value;
    }

    /**
     * Gets the value of the orderId property.
     *
     * @return
     *     possible object is
     *     {@link OrderIdType }
     *
     */
    /*@Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public OrderIdType getOrderId() {
        return orderId;
    }*/

    /**
     * Sets the value of the orderId property.
     *
     * @param value
     *     allowed object is
     *     {@link OrderIdType }
     *
     */
    /*@Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setOrderId(OrderIdType value) {
        this.orderId = value;
    }*/

    /**
     * Gets the value of the customerId property.
     *
     * @return
     *     possible object is
     *     {@link CustomerIdType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public CustomerIdType getCustomerId() {
        return customerId;
    }

    /**
     * Sets the value of the customerId property.
     *
     * @param value
     *     allowed object is
     *     {@link CustomerIdType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setCustomerId(CustomerIdType value) {
        this.customerId = value;
    }

    /**
     * Gets the value of the locationId property.
     *
     * @return
     *     possible object is
     *     {@link LocationIdType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public LocationIdType getLocationId() {
        return locationId;
    }

    /**
     * Sets the value of the locationId property.
     *
     * @param value
     *     allowed object is
     *     {@link LocationIdType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setLocationId(LocationIdType value) {
        this.locationId = value;
    }

    /**
     * Gets the value of the claimInfo property.
     *
     * @return
     *     possible object is
     *     {@link WebClaimListType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public WebClaimListType getClaimInfo() {
        return claimInfo;
    }

    /**
     * Sets the value of the claimInfo property.
     *
     * @param value
     *     allowed object is
     *     {@link WebClaimListType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setClaimInfo(WebClaimListType value) {
        this.claimInfo = value;
    }

    /**
     * Gets the value of the paymentStatus property.
     *
     * @return
     *     possible object is
     *     {@link PaymentStatusType }
     *
     */
    /*@Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public PaymentStatusType getPaymentStatus() {
        return paymentStatus;
    }*/

    /**
     * Sets the value of the paymentStatus property.
     *
     * @param value
     *     allowed object is
     *     {@link PaymentStatusType }
     *
     */
    /*@Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setPaymentStatus(PaymentStatusType value) {
        this.paymentStatus = value;
    }*/

    /**
     * Gets the value of the autopayStatus property.
     *
     * @return
     *     possible object is
     *     {@link AutoPayStatusType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public AutoPayStatusType getAutopayStatus() {
        return autopayStatus;
    }

    /**
     * Sets the value of the autopayStatus property.
     *
     * @param value
     *     allowed object is
     *     {@link AutoPayStatusType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setAutopayStatus(AutoPayStatusType value) {
        this.autopayStatus = value;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy2 strategy) {
        if ((object == null)||(this.getClass()!= object.getClass())) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final CreateAccountOutputType that = ((CreateAccountOutputType) object);
        {
            AccountIdType lhsAccountId;
            lhsAccountId = this.getAccountId();
            AccountIdType rhsAccountId;
            rhsAccountId = that.getAccountId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "accountId", lhsAccountId), LocatorUtils.property(thatLocator, "accountId", rhsAccountId), lhsAccountId, rhsAccountId, (this.accountId!= null), (that.accountId!= null))) {
                return false;
            }
        }
        /*{
            OrderIdType lhsOrderId;
            lhsOrderId = this.getOrderId();
            OrderIdType rhsOrderId;
            rhsOrderId = that.getOrderId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "orderId", lhsOrderId), LocatorUtils.property(thatLocator, "orderId", rhsOrderId), lhsOrderId, rhsOrderId, (this.orderId!= null), (that.orderId!= null))) {
                return false;
            }
        }*/
        {
            CustomerIdType lhsCustomerId;
            lhsCustomerId = this.getCustomerId();
            CustomerIdType rhsCustomerId;
            rhsCustomerId = that.getCustomerId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "customerId", lhsCustomerId), LocatorUtils.property(thatLocator, "customerId", rhsCustomerId), lhsCustomerId, rhsCustomerId, (this.customerId!= null), (that.customerId!= null))) {
                return false;
            }
        }
        {
            LocationIdType lhsLocationId;
            lhsLocationId = this.getLocationId();
            LocationIdType rhsLocationId;
            rhsLocationId = that.getLocationId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "locationId", lhsLocationId), LocatorUtils.property(thatLocator, "locationId", rhsLocationId), lhsLocationId, rhsLocationId, (this.locationId!= null), (that.locationId!= null))) {
                return false;
            }
        }
        {
            WebClaimListType lhsClaimInfo;
            lhsClaimInfo = this.getClaimInfo();
            WebClaimListType rhsClaimInfo;
            rhsClaimInfo = that.getClaimInfo();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "claimInfo", lhsClaimInfo), LocatorUtils.property(thatLocator, "claimInfo", rhsClaimInfo), lhsClaimInfo, rhsClaimInfo, (this.claimInfo!= null), (that.claimInfo!= null))) {
                return false;
            }
        }
        /*{
            PaymentStatusType lhsPaymentStatus;
            lhsPaymentStatus = this.getPaymentStatus();
            PaymentStatusType rhsPaymentStatus;
            rhsPaymentStatus = that.getPaymentStatus();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "paymentStatus", lhsPaymentStatus), LocatorUtils.property(thatLocator, "paymentStatus", rhsPaymentStatus), lhsPaymentStatus, rhsPaymentStatus, (this.paymentStatus!= null), (that.paymentStatus!= null))) {
                return false;
            }
        }*/
        {
            AutoPayStatusType lhsAutopayStatus;
            lhsAutopayStatus = this.getAutopayStatus();
            AutoPayStatusType rhsAutopayStatus;
            rhsAutopayStatus = that.getAutopayStatus();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "autopayStatus", lhsAutopayStatus), LocatorUtils.property(thatLocator, "autopayStatus", rhsAutopayStatus), lhsAutopayStatus, rhsAutopayStatus, (this.autopayStatus!= null), (that.autopayStatus!= null))) {
                return false;
            }
        }
        return true;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(Object object) {
        final EqualsStrategy2 strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode(ObjectLocator locator, HashCodeStrategy2 strategy) {
        int currentHashCode = 1;
        {
            AccountIdType theAccountId;
            theAccountId = this.getAccountId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "accountId", theAccountId), currentHashCode, theAccountId, (this.accountId!= null));
        }
        /*{
            OrderIdType theOrderId;
            theOrderId = this.getOrderId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "orderId", theOrderId), currentHashCode, theOrderId, (this.orderId!= null));
        }*/
        {
            CustomerIdType theCustomerId;
            theCustomerId = this.getCustomerId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "customerId", theCustomerId), currentHashCode, theCustomerId, (this.customerId!= null));
        }
        {
            LocationIdType theLocationId;
            theLocationId = this.getLocationId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "locationId", theLocationId), currentHashCode, theLocationId, (this.locationId!= null));
        }
        {
            WebClaimListType theClaimInfo;
            theClaimInfo = this.getClaimInfo();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "claimInfo", theClaimInfo), currentHashCode, theClaimInfo, (this.claimInfo!= null));
        }
        /*{
            PaymentStatusType thePaymentStatus;
            thePaymentStatus = this.getPaymentStatus();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "paymentStatus", thePaymentStatus), currentHashCode, thePaymentStatus, (this.paymentStatus!= null));
        }*/
        {
            AutoPayStatusType theAutopayStatus;
            theAutopayStatus = this.getAutopayStatus();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "autopayStatus", theAutopayStatus), currentHashCode, theAutopayStatus, (this.autopayStatus!= null));
        }
        return currentHashCode;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode() {
        final HashCodeStrategy2 strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String toString() {
        final ToStringStrategy2 strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        {
            AccountIdType theAccountId;
            theAccountId = this.getAccountId();
            strategy.appendField(locator, this, "accountId", buffer, theAccountId, (this.accountId!= null));
        }
        /*{
            OrderIdType theOrderId;
            theOrderId = this.getOrderId();
            strategy.appendField(locator, this, "orderId", buffer, theOrderId, (this.orderId!= null));
        }*/
        {
            CustomerIdType theCustomerId;
            theCustomerId = this.getCustomerId();
            strategy.appendField(locator, this, "customerId", buffer, theCustomerId, (this.customerId!= null));
        }
        {
            LocationIdType theLocationId;
            theLocationId = this.getLocationId();
            strategy.appendField(locator, this, "locationId", buffer, theLocationId, (this.locationId!= null));
        }
        {
            WebClaimListType theClaimInfo;
            theClaimInfo = this.getClaimInfo();
            strategy.appendField(locator, this, "claimInfo", buffer, theClaimInfo, (this.claimInfo!= null));
        }
        /*{
            PaymentStatusType thePaymentStatus;
            thePaymentStatus = this.getPaymentStatus();
            strategy.appendField(locator, this, "paymentStatus", buffer, thePaymentStatus, (this.paymentStatus!= null));
        }*/
        {
            AutoPayStatusType theAutopayStatus;
            theAutopayStatus = this.getAutopayStatus();
            strategy.appendField(locator, this, "autopayStatus", buffer, theAutopayStatus, (this.autopayStatus!= null));
        }
        return buffer;
    }

}
