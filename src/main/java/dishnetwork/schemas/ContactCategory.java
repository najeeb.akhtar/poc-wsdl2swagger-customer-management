package dishnetwork.schemas;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.jvnet.jaxb2_commons.lang.*;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="categoryName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="contactPreferenceList"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="contactPreference" maxOccurs="unbounded"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="preference" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="status"&gt;
 *                               &lt;simpleType&gt;
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *                                   &lt;enumeration value="ACTIVE"/&gt;
 *                                   &lt;enumeration value="INACTIVE"/&gt;
 *                                 &lt;/restriction&gt;
 *                               &lt;/simpleType&gt;
 *                             &lt;/element&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "categoryName",
    "contactPreferenceList"
})
@Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
public class ContactCategory implements Equals2, HashCode2, ToString2 {

    //@XmlElement(required = true)
    @JsonProperty(required = true)
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String categoryName;
    //@XmlElement(required = true)
    @JsonProperty(required = true)
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected ContactPreferenceList contactPreferenceList;

    /**
     * Default no-arg constructor
     */
    public ContactCategory() {
        super();
    }

    /**
     * Fully-initialising value constructor
     */
    public ContactCategory(final String categoryName, final ContactPreferenceList contactPreferenceList) {
        this.categoryName = categoryName;
        this.contactPreferenceList = contactPreferenceList;
    }

    /**
     * Gets the value of the categoryName property.
     *
     * @return possible object is
     * {@link String }
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getCategoryName() {
        return categoryName;
    }

    /**
     * Sets the value of the categoryName property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setCategoryName(String value) {
        this.categoryName = value;
    }

    /**
     * Gets the value of the contactPreferenceList property.
     *
     * @return possible object is
     * {@link ContactPreferenceList }
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public ContactPreferenceList getContactPreferenceList() {
        return contactPreferenceList;
    }

    /**
     * Sets the value of the contactPreferenceList property.
     *
     * @param value allowed object is
     *              {@link ContactPreferenceList }
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setContactPreferenceList(ContactPreferenceList value) {
        this.contactPreferenceList = value;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy2 strategy) {
        if ((object == null) || (this.getClass() != object.getClass())) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final ContactCategory that = ((ContactCategory) object);
        {
            String lhsCategoryName;
            lhsCategoryName = this.getCategoryName();
            String rhsCategoryName;
            rhsCategoryName = that.getCategoryName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "categoryName", lhsCategoryName), LocatorUtils.property(thatLocator, "categoryName", rhsCategoryName), lhsCategoryName, rhsCategoryName, (this.categoryName != null), (that.categoryName != null))) {
                return false;
            }
        }
        {
            ContactPreferenceList lhsContactPreferenceList;
            lhsContactPreferenceList = this.getContactPreferenceList();
            ContactPreferenceList rhsContactPreferenceList;
            rhsContactPreferenceList = that.getContactPreferenceList();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "contactPreferenceList", lhsContactPreferenceList), LocatorUtils.property(thatLocator, "contactPreferenceList", rhsContactPreferenceList), lhsContactPreferenceList, rhsContactPreferenceList, (this.contactPreferenceList != null), (that.contactPreferenceList != null))) {
                return false;
            }
        }
        return true;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(Object object) {
        final EqualsStrategy2 strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode(ObjectLocator locator, HashCodeStrategy2 strategy) {
        int currentHashCode = 1;
        {
            String theCategoryName;
            theCategoryName = this.getCategoryName();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "categoryName", theCategoryName), currentHashCode, theCategoryName, (this.categoryName != null));
        }
        {
            ContactPreferenceList theContactPreferenceList;
            theContactPreferenceList = this.getContactPreferenceList();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "contactPreferenceList", theContactPreferenceList), currentHashCode, theContactPreferenceList, (this.contactPreferenceList != null));
        }
        return currentHashCode;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode() {
        final HashCodeStrategy2 strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String toString() {
        final ToStringStrategy2 strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        {
            String theCategoryName;
            theCategoryName = this.getCategoryName();
            strategy.appendField(locator, this, "categoryName", buffer, theCategoryName, (this.categoryName != null));
        }
        {
            ContactPreferenceList theContactPreferenceList;
            theContactPreferenceList = this.getContactPreferenceList();
            strategy.appendField(locator, this, "contactPreferenceList", buffer, theContactPreferenceList, (this.contactPreferenceList != null));
        }
        return buffer;
    }


}
