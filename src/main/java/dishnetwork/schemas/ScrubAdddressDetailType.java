
package dishnetwork.schemas;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.jvnet.jaxb2_commons.lang.Equals2;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy2;
import org.jvnet.jaxb2_commons.lang.HashCode2;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy2;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString2;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy2;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * Address Details info.(Scrubbed Address)
 *
 * <p>Java class for ScrubAdddressDetailType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="ScrubAdddressDetailType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="pushAddressPossibleFlag" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="addressStatusData" type="{http://www.dishnetwork.com/schema/ScrubAdddressDetail/2016_03_24}AddressStatusDataType" minOccurs="0"/&gt;
 *         &lt;element name="addressGeocodeData" type="{http://www.dishnetwork.com/schema/ScrubAdddressDetail/2016_03_24}AddressGeocodeDataType" minOccurs="0"/&gt;
 *         &lt;element name="addressDetailType" type="{http://www.dishnetwork.com/schema/AddressDetail/2016_03_24}AddressDetailType" minOccurs="0"/&gt;
 *         &lt;element name="additionalAttributes" type="{http://www.dishnetwork.com/schema/common/AttributeList/2013_09_19}AttributeListType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ScrubAdddressDetailType", propOrder = {
    "pushAddressPossibleFlag",
    "addressStatusData",
    "addressGeocodeData",
    "addressDetailType",
    "additionalAttributes"
})
@Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
public class ScrubAdddressDetailType implements Equals2, HashCode2, ToString2
{

    //@XmlElement(defaultValue = "false")
    @JsonProperty(defaultValue = "false")
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String pushAddressPossibleFlag;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected AddressStatusDataType addressStatusData;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected AddressGeocodeDataType addressGeocodeData;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected AddressDetailType addressDetailType;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected AttributeListType additionalAttributes;

    /**
     * Default no-arg constructor
     *
     */
    public ScrubAdddressDetailType() {
        super();
    }

    /**
     * Fully-initialising value constructor
     *
     */
    public ScrubAdddressDetailType(final String pushAddressPossibleFlag, final AddressStatusDataType addressStatusData, final AddressGeocodeDataType addressGeocodeData, final AddressDetailType addressDetailType, final AttributeListType additionalAttributes) {
        this.pushAddressPossibleFlag = pushAddressPossibleFlag;
        this.addressStatusData = addressStatusData;
        this.addressGeocodeData = addressGeocodeData;
        this.addressDetailType = addressDetailType;
        this.additionalAttributes = additionalAttributes;
    }

    /**
     * Gets the value of the pushAddressPossibleFlag property.
     *
     * @return
     *     possible object is
     *     {@link Boolean }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String isPushAddressPossibleFlag() {
        return pushAddressPossibleFlag;
    }

    /**
     * Sets the value of the pushAddressPossibleFlag property.
     *
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setPushAddressPossibleFlag(String value) {
        this.pushAddressPossibleFlag = value;
    }

    /**
     * Gets the value of the addressStatusData property.
     *
     * @return
     *     possible object is
     *     {@link AddressStatusDataType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public AddressStatusDataType getAddressStatusData() {
        return addressStatusData;
    }

    /**
     * Sets the value of the addressStatusData property.
     *
     * @param value
     *     allowed object is
     *     {@link AddressStatusDataType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setAddressStatusData(AddressStatusDataType value) {
        this.addressStatusData = value;
    }

    /**
     * Gets the value of the addressGeocodeData property.
     *
     * @return
     *     possible object is
     *     {@link AddressGeocodeDataType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public AddressGeocodeDataType getAddressGeocodeData() {
        return addressGeocodeData;
    }

    /**
     * Sets the value of the addressGeocodeData property.
     *
     * @param value
     *     allowed object is
     *     {@link AddressGeocodeDataType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setAddressGeocodeData(AddressGeocodeDataType value) {
        this.addressGeocodeData = value;
    }

    /**
     * Gets the value of the addressDetailType property.
     *
     * @return
     *     possible object is
     *     {@link AddressDetailType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public AddressDetailType getAddressDetailType() {
        return addressDetailType;
    }

    /**
     * Sets the value of the addressDetailType property.
     *
     * @param value
     *     allowed object is
     *     {@link AddressDetailType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setAddressDetailType(AddressDetailType value) {
        this.addressDetailType = value;
    }

    /**
     * Gets the value of the additionalAttributes property.
     *
     * @return
     *     possible object is
     *     {@link AttributeListType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public AttributeListType getAdditionalAttributes() {
        return additionalAttributes;
    }

    /**
     * Sets the value of the additionalAttributes property.
     *
     * @param value
     *     allowed object is
     *     {@link AttributeListType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setAdditionalAttributes(AttributeListType value) {
        this.additionalAttributes = value;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy2 strategy) {
        if ((object == null)||(this.getClass()!= object.getClass())) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final ScrubAdddressDetailType that = ((ScrubAdddressDetailType) object);
        {
            String lhsPushAddressPossibleFlag;
            lhsPushAddressPossibleFlag = this.isPushAddressPossibleFlag();
            String rhsPushAddressPossibleFlag;
            rhsPushAddressPossibleFlag = that.isPushAddressPossibleFlag();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "pushAddressPossibleFlag", lhsPushAddressPossibleFlag), LocatorUtils.property(thatLocator, "pushAddressPossibleFlag", rhsPushAddressPossibleFlag), lhsPushAddressPossibleFlag, rhsPushAddressPossibleFlag, (this.pushAddressPossibleFlag!= null), (that.pushAddressPossibleFlag!= null))) {
                return false;
            }
        }
        {
            AddressStatusDataType lhsAddressStatusData;
            lhsAddressStatusData = this.getAddressStatusData();
            AddressStatusDataType rhsAddressStatusData;
            rhsAddressStatusData = that.getAddressStatusData();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "addressStatusData", lhsAddressStatusData), LocatorUtils.property(thatLocator, "addressStatusData", rhsAddressStatusData), lhsAddressStatusData, rhsAddressStatusData, (this.addressStatusData!= null), (that.addressStatusData!= null))) {
                return false;
            }
        }
        {
            AddressGeocodeDataType lhsAddressGeocodeData;
            lhsAddressGeocodeData = this.getAddressGeocodeData();
            AddressGeocodeDataType rhsAddressGeocodeData;
            rhsAddressGeocodeData = that.getAddressGeocodeData();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "addressGeocodeData", lhsAddressGeocodeData), LocatorUtils.property(thatLocator, "addressGeocodeData", rhsAddressGeocodeData), lhsAddressGeocodeData, rhsAddressGeocodeData, (this.addressGeocodeData!= null), (that.addressGeocodeData!= null))) {
                return false;
            }
        }
        {
            AddressDetailType lhsAddressDetailType;
            lhsAddressDetailType = this.getAddressDetailType();
            AddressDetailType rhsAddressDetailType;
            rhsAddressDetailType = that.getAddressDetailType();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "addressDetailType", lhsAddressDetailType), LocatorUtils.property(thatLocator, "addressDetailType", rhsAddressDetailType), lhsAddressDetailType, rhsAddressDetailType, (this.addressDetailType!= null), (that.addressDetailType!= null))) {
                return false;
            }
        }
        {
            AttributeListType lhsAdditionalAttributes;
            lhsAdditionalAttributes = this.getAdditionalAttributes();
            AttributeListType rhsAdditionalAttributes;
            rhsAdditionalAttributes = that.getAdditionalAttributes();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "additionalAttributes", lhsAdditionalAttributes), LocatorUtils.property(thatLocator, "additionalAttributes", rhsAdditionalAttributes), lhsAdditionalAttributes, rhsAdditionalAttributes, (this.additionalAttributes!= null), (that.additionalAttributes!= null))) {
                return false;
            }
        }
        return true;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(Object object) {
        final EqualsStrategy2 strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode(ObjectLocator locator, HashCodeStrategy2 strategy) {
        int currentHashCode = 1;
        {
            String thePushAddressPossibleFlag;
            thePushAddressPossibleFlag = this.isPushAddressPossibleFlag();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "pushAddressPossibleFlag", thePushAddressPossibleFlag), currentHashCode, thePushAddressPossibleFlag, (this.pushAddressPossibleFlag!= null));
        }
        {
            AddressStatusDataType theAddressStatusData;
            theAddressStatusData = this.getAddressStatusData();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "addressStatusData", theAddressStatusData), currentHashCode, theAddressStatusData, (this.addressStatusData!= null));
        }
        {
            AddressGeocodeDataType theAddressGeocodeData;
            theAddressGeocodeData = this.getAddressGeocodeData();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "addressGeocodeData", theAddressGeocodeData), currentHashCode, theAddressGeocodeData, (this.addressGeocodeData!= null));
        }
        {
            AddressDetailType theAddressDetailType;
            theAddressDetailType = this.getAddressDetailType();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "addressDetailType", theAddressDetailType), currentHashCode, theAddressDetailType, (this.addressDetailType!= null));
        }
        {
            AttributeListType theAdditionalAttributes;
            theAdditionalAttributes = this.getAdditionalAttributes();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "additionalAttributes", theAdditionalAttributes), currentHashCode, theAdditionalAttributes, (this.additionalAttributes!= null));
        }
        return currentHashCode;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode() {
        final HashCodeStrategy2 strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String toString() {
        final ToStringStrategy2 strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        {
            String thePushAddressPossibleFlag;
            thePushAddressPossibleFlag = this.isPushAddressPossibleFlag();
            strategy.appendField(locator, this, "pushAddressPossibleFlag", buffer, thePushAddressPossibleFlag, (this.pushAddressPossibleFlag!= null));
        }
        {
            AddressStatusDataType theAddressStatusData;
            theAddressStatusData = this.getAddressStatusData();
            strategy.appendField(locator, this, "addressStatusData", buffer, theAddressStatusData, (this.addressStatusData!= null));
        }
        {
            AddressGeocodeDataType theAddressGeocodeData;
            theAddressGeocodeData = this.getAddressGeocodeData();
            strategy.appendField(locator, this, "addressGeocodeData", buffer, theAddressGeocodeData, (this.addressGeocodeData!= null));
        }
        {
            AddressDetailType theAddressDetailType;
            theAddressDetailType = this.getAddressDetailType();
            strategy.appendField(locator, this, "addressDetailType", buffer, theAddressDetailType, (this.addressDetailType!= null));
        }
        {
            AttributeListType theAdditionalAttributes;
            theAdditionalAttributes = this.getAdditionalAttributes();
            strategy.appendField(locator, this, "additionalAttributes", buffer, theAdditionalAttributes, (this.additionalAttributes!= null));
        }
        return buffer;
    }

}
