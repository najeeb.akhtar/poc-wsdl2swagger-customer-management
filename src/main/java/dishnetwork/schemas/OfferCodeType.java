
package dishnetwork.schemas;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.jvnet.jaxb2_commons.lang.Equals2;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy2;
import org.jvnet.jaxb2_commons.lang.HashCode2;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy2;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString2;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy2;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for OfferCodeType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="OfferCodeType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="offerCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="fullfillmentChannel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="billingChannel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="version" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OfferCodeType", propOrder = {
    "offerCode",
    "fullfillmentChannel",
    "billingChannel",
    "version"
})
@Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-13T07:11:01-06:00")
public class OfferCodeType implements Equals2, HashCode2, ToString2
{

    //@XmlElement(required = true)
    @JsonProperty(required = true)
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-13T07:11:01-06:00")
    protected String offerCode;
    //@XmlElement(nillable = true)
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-13T07:11:01-06:00")
    protected String fullfillmentChannel;
    //@XmlElement(nillable = true)
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-13T07:11:01-06:00")
    protected String billingChannel;
    //@XmlElement(nillable = true)
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-13T07:11:01-06:00")
    protected String version;

    /**
     * Default no-arg constructor
     *
     */
    public OfferCodeType() {
        super();
    }

    /**
     * Fully-initialising value constructor
     *
     */
    public OfferCodeType(final String offerCode, final String fullfillmentChannel, final String billingChannel, final String version) {
        this.offerCode = offerCode;
        this.fullfillmentChannel = fullfillmentChannel;
        this.billingChannel = billingChannel;
        this.version = version;
    }

    /**
     * Gets the value of the offerCode property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-13T07:11:01-06:00")
    public String getOfferCode() {
        return offerCode;
    }

    /**
     * Sets the value of the offerCode property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-13T07:11:01-06:00")
    public void setOfferCode(String value) {
        this.offerCode = value;
    }

    /**
     * Gets the value of the fullfillmentChannel property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-13T07:11:01-06:00")
    public String getFullfillmentChannel() {
        return fullfillmentChannel;
    }

    /**
     * Sets the value of the fullfillmentChannel property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-13T07:11:01-06:00")
    public void setFullfillmentChannel(String value) {
        this.fullfillmentChannel = value;
    }

    /**
     * Gets the value of the billingChannel property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-13T07:11:01-06:00")
    public String getBillingChannel() {
        return billingChannel;
    }

    /**
     * Sets the value of the billingChannel property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-13T07:11:01-06:00")
    public void setBillingChannel(String value) {
        this.billingChannel = value;
    }

    /**
     * Gets the value of the version property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-13T07:11:01-06:00")
    public String getVersion() {
        return version;
    }

    /**
     * Sets the value of the version property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-13T07:11:01-06:00")
    public void setVersion(String value) {
        this.version = value;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-13T07:11:01-06:00")
    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy2 strategy) {
        if ((object == null)||(this.getClass()!= object.getClass())) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final OfferCodeType that = ((OfferCodeType) object);
        {
            String lhsOfferCode;
            lhsOfferCode = this.getOfferCode();
            String rhsOfferCode;
            rhsOfferCode = that.getOfferCode();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "offerCode", lhsOfferCode), LocatorUtils.property(thatLocator, "offerCode", rhsOfferCode), lhsOfferCode, rhsOfferCode, (this.offerCode!= null), (that.offerCode!= null))) {
                return false;
            }
        }
        {
            String lhsFullfillmentChannel;
            lhsFullfillmentChannel = this.getFullfillmentChannel();
            String rhsFullfillmentChannel;
            rhsFullfillmentChannel = that.getFullfillmentChannel();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "fullfillmentChannel", lhsFullfillmentChannel), LocatorUtils.property(thatLocator, "fullfillmentChannel", rhsFullfillmentChannel), lhsFullfillmentChannel, rhsFullfillmentChannel, (this.fullfillmentChannel!= null), (that.fullfillmentChannel!= null))) {
                return false;
            }
        }
        {
            String lhsBillingChannel;
            lhsBillingChannel = this.getBillingChannel();
            String rhsBillingChannel;
            rhsBillingChannel = that.getBillingChannel();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "billingChannel", lhsBillingChannel), LocatorUtils.property(thatLocator, "billingChannel", rhsBillingChannel), lhsBillingChannel, rhsBillingChannel, (this.billingChannel!= null), (that.billingChannel!= null))) {
                return false;
            }
        }
        {
            String lhsVersion;
            lhsVersion = this.getVersion();
            String rhsVersion;
            rhsVersion = that.getVersion();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "version", lhsVersion), LocatorUtils.property(thatLocator, "version", rhsVersion), lhsVersion, rhsVersion, (this.version!= null), (that.version!= null))) {
                return false;
            }
        }
        return true;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-13T07:11:01-06:00")
    public boolean equals(Object object) {
        final EqualsStrategy2 strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-13T07:11:01-06:00")
    public int hashCode(ObjectLocator locator, HashCodeStrategy2 strategy) {
        int currentHashCode = 1;
        {
            String theOfferCode;
            theOfferCode = this.getOfferCode();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "offerCode", theOfferCode), currentHashCode, theOfferCode, (this.offerCode!= null));
        }
        {
            String theFullfillmentChannel;
            theFullfillmentChannel = this.getFullfillmentChannel();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "fullfillmentChannel", theFullfillmentChannel), currentHashCode, theFullfillmentChannel, (this.fullfillmentChannel!= null));
        }
        {
            String theBillingChannel;
            theBillingChannel = this.getBillingChannel();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "billingChannel", theBillingChannel), currentHashCode, theBillingChannel, (this.billingChannel!= null));
        }
        {
            String theVersion;
            theVersion = this.getVersion();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "version", theVersion), currentHashCode, theVersion, (this.version!= null));
        }
        return currentHashCode;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-13T07:11:01-06:00")
    public int hashCode() {
        final HashCodeStrategy2 strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-13T07:11:01-06:00")
    public String toString() {
        final ToStringStrategy2 strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-13T07:11:01-06:00")
    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-13T07:11:01-06:00")
    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        {
            String theOfferCode;
            theOfferCode = this.getOfferCode();
            strategy.appendField(locator, this, "offerCode", buffer, theOfferCode, (this.offerCode!= null));
        }
        {
            String theFullfillmentChannel;
            theFullfillmentChannel = this.getFullfillmentChannel();
            strategy.appendField(locator, this, "fullfillmentChannel", buffer, theFullfillmentChannel, (this.fullfillmentChannel!= null));
        }
        {
            String theBillingChannel;
            theBillingChannel = this.getBillingChannel();
            strategy.appendField(locator, this, "billingChannel", buffer, theBillingChannel, (this.billingChannel!= null));
        }
        {
            String theVersion;
            theVersion = this.getVersion();
            strategy.appendField(locator, this, "version", buffer, theVersion, (this.version!= null));
        }
        return buffer;
    }

}
