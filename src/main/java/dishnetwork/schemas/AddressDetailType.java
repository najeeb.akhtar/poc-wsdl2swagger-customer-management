
package dishnetwork.schemas;

import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.Equals2;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy2;
import org.jvnet.jaxb2_commons.lang.HashCode2;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy2;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString2;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy2;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * Address Details info.(Scrubbed Address)
 *
 * <p>Java class for AddressDetailType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="AddressDetailType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="houseDma" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="houseNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="unitNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="unitType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="prefixDirection" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="postfixDirection" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="streetName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="streetType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="mduIndicator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="latitude" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal"&gt;
 *               &lt;fractionDigits value="6"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="longitude" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal"&gt;
 *               &lt;fractionDigits value="6"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="county" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AddressDetailType", propOrder = {
    "houseDma",
    "houseNumber",
    "unitNumber",
    "unitType",
    "prefixDirection",
    "postfixDirection",
    "streetName",
    "streetType",
    "mduIndicator",
    "latitude",
    "longitude",
    "county"
})
@Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
public class AddressDetailType implements Equals2, HashCode2, ToString2
{

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String houseDma;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String houseNumber;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String unitNumber;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String unitType;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String prefixDirection;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String postfixDirection;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String streetName;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String streetType;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String mduIndicator;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String latitude;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String longitude;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String county;

    /**
     * Default no-arg constructor
     *
     */
    public AddressDetailType() {
        super();
    }

    /**
     * Fully-initialising value constructor
     *
     */
    public AddressDetailType(final String houseDma, final String houseNumber, final String unitNumber, final String unitType, final String prefixDirection, final String postfixDirection, final String streetName, final String streetType, final String mduIndicator, final String latitude, final String longitude, final String county) {
        this.houseDma = houseDma;
        this.houseNumber = houseNumber;
        this.unitNumber = unitNumber;
        this.unitType = unitType;
        this.prefixDirection = prefixDirection;
        this.postfixDirection = postfixDirection;
        this.streetName = streetName;
        this.streetType = streetType;
        this.mduIndicator = mduIndicator;
        this.latitude = latitude;
        this.longitude = longitude;
        this.county = county;
    }

    /**
     * Gets the value of the houseDma property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getHouseDma() {
        return houseDma;
    }

    /**
     * Sets the value of the houseDma property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setHouseDma(String value) {
        this.houseDma = value;
    }

    /**
     * Gets the value of the houseNumber property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getHouseNumber() {
        return houseNumber;
    }

    /**
     * Sets the value of the houseNumber property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setHouseNumber(String value) {
        this.houseNumber = value;
    }

    /**
     * Gets the value of the unitNumber property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getUnitNumber() {
        return unitNumber;
    }

    /**
     * Sets the value of the unitNumber property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setUnitNumber(String value) {
        this.unitNumber = value;
    }

    /**
     * Gets the value of the unitType property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getUnitType() {
        return unitType;
    }

    /**
     * Sets the value of the unitType property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setUnitType(String value) {
        this.unitType = value;
    }

    /**
     * Gets the value of the prefixDirection property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getPrefixDirection() {
        return prefixDirection;
    }

    /**
     * Sets the value of the prefixDirection property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setPrefixDirection(String value) {
        this.prefixDirection = value;
    }

    /**
     * Gets the value of the postfixDirection property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getPostfixDirection() {
        return postfixDirection;
    }

    /**
     * Sets the value of the postfixDirection property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setPostfixDirection(String value) {
        this.postfixDirection = value;
    }

    /**
     * Gets the value of the streetName property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getStreetName() {
        return streetName;
    }

    /**
     * Sets the value of the streetName property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setStreetName(String value) {
        this.streetName = value;
    }

    /**
     * Gets the value of the streetType property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getStreetType() {
        return streetType;
    }

    /**
     * Sets the value of the streetType property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setStreetType(String value) {
        this.streetType = value;
    }

    /**
     * Gets the value of the mduIndicator property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getMduIndicator() {
        return mduIndicator;
    }

    /**
     * Sets the value of the mduIndicator property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setMduIndicator(String value) {
        this.mduIndicator = value;
    }

    /**
     * Gets the value of the latitude property.
     *
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getLatitude() {
        return latitude;
    }

    /**
     * Sets the value of the latitude property.
     *
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setLatitude(String value) {
        this.latitude = value;
    }

    /**
     * Gets the value of the longitude property.
     *
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getLongitude() {
        return longitude;
    }

    /**
     * Sets the value of the longitude property.
     *
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setLongitude(String value) {
        this.longitude = value;
    }

    /**
     * Gets the value of the county property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getCounty() {
        return county;
    }

    /**
     * Sets the value of the county property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setCounty(String value) {
        this.county = value;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy2 strategy) {
        if ((object == null)||(this.getClass()!= object.getClass())) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final AddressDetailType that = ((AddressDetailType) object);
        {
            String lhsHouseDma;
            lhsHouseDma = this.getHouseDma();
            String rhsHouseDma;
            rhsHouseDma = that.getHouseDma();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "houseDma", lhsHouseDma), LocatorUtils.property(thatLocator, "houseDma", rhsHouseDma), lhsHouseDma, rhsHouseDma, (this.houseDma!= null), (that.houseDma!= null))) {
                return false;
            }
        }
        {
            String lhsHouseNumber;
            lhsHouseNumber = this.getHouseNumber();
            String rhsHouseNumber;
            rhsHouseNumber = that.getHouseNumber();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "houseNumber", lhsHouseNumber), LocatorUtils.property(thatLocator, "houseNumber", rhsHouseNumber), lhsHouseNumber, rhsHouseNumber, (this.houseNumber!= null), (that.houseNumber!= null))) {
                return false;
            }
        }
        {
            String lhsUnitNumber;
            lhsUnitNumber = this.getUnitNumber();
            String rhsUnitNumber;
            rhsUnitNumber = that.getUnitNumber();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "unitNumber", lhsUnitNumber), LocatorUtils.property(thatLocator, "unitNumber", rhsUnitNumber), lhsUnitNumber, rhsUnitNumber, (this.unitNumber!= null), (that.unitNumber!= null))) {
                return false;
            }
        }
        {
            String lhsUnitType;
            lhsUnitType = this.getUnitType();
            String rhsUnitType;
            rhsUnitType = that.getUnitType();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "unitType", lhsUnitType), LocatorUtils.property(thatLocator, "unitType", rhsUnitType), lhsUnitType, rhsUnitType, (this.unitType!= null), (that.unitType!= null))) {
                return false;
            }
        }
        {
            String lhsPrefixDirection;
            lhsPrefixDirection = this.getPrefixDirection();
            String rhsPrefixDirection;
            rhsPrefixDirection = that.getPrefixDirection();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "prefixDirection", lhsPrefixDirection), LocatorUtils.property(thatLocator, "prefixDirection", rhsPrefixDirection), lhsPrefixDirection, rhsPrefixDirection, (this.prefixDirection!= null), (that.prefixDirection!= null))) {
                return false;
            }
        }
        {
            String lhsPostfixDirection;
            lhsPostfixDirection = this.getPostfixDirection();
            String rhsPostfixDirection;
            rhsPostfixDirection = that.getPostfixDirection();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "postfixDirection", lhsPostfixDirection), LocatorUtils.property(thatLocator, "postfixDirection", rhsPostfixDirection), lhsPostfixDirection, rhsPostfixDirection, (this.postfixDirection!= null), (that.postfixDirection!= null))) {
                return false;
            }
        }
        {
            String lhsStreetName;
            lhsStreetName = this.getStreetName();
            String rhsStreetName;
            rhsStreetName = that.getStreetName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "streetName", lhsStreetName), LocatorUtils.property(thatLocator, "streetName", rhsStreetName), lhsStreetName, rhsStreetName, (this.streetName!= null), (that.streetName!= null))) {
                return false;
            }
        }
        {
            String lhsStreetType;
            lhsStreetType = this.getStreetType();
            String rhsStreetType;
            rhsStreetType = that.getStreetType();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "streetType", lhsStreetType), LocatorUtils.property(thatLocator, "streetType", rhsStreetType), lhsStreetType, rhsStreetType, (this.streetType!= null), (that.streetType!= null))) {
                return false;
            }
        }
        {
            String lhsMduIndicator;
            lhsMduIndicator = this.getMduIndicator();
            String rhsMduIndicator;
            rhsMduIndicator = that.getMduIndicator();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "mduIndicator", lhsMduIndicator), LocatorUtils.property(thatLocator, "mduIndicator", rhsMduIndicator), lhsMduIndicator, rhsMduIndicator, (this.mduIndicator!= null), (that.mduIndicator!= null))) {
                return false;
            }
        }
        {
            String lhsLatitude;
            lhsLatitude = this.getLatitude();
            String rhsLatitude;
            rhsLatitude = that.getLatitude();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "latitude", lhsLatitude), LocatorUtils.property(thatLocator, "latitude", rhsLatitude), lhsLatitude, rhsLatitude, (this.latitude!= null), (that.latitude!= null))) {
                return false;
            }
        }
        {
            String lhsLongitude;
            lhsLongitude = this.getLongitude();
            String rhsLongitude;
            rhsLongitude = that.getLongitude();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "longitude", lhsLongitude), LocatorUtils.property(thatLocator, "longitude", rhsLongitude), lhsLongitude, rhsLongitude, (this.longitude!= null), (that.longitude!= null))) {
                return false;
            }
        }
        {
            String lhsCounty;
            lhsCounty = this.getCounty();
            String rhsCounty;
            rhsCounty = that.getCounty();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "county", lhsCounty), LocatorUtils.property(thatLocator, "county", rhsCounty), lhsCounty, rhsCounty, (this.county!= null), (that.county!= null))) {
                return false;
            }
        }
        return true;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(Object object) {
        final EqualsStrategy2 strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode(ObjectLocator locator, HashCodeStrategy2 strategy) {
        int currentHashCode = 1;
        {
            String theHouseDma;
            theHouseDma = this.getHouseDma();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "houseDma", theHouseDma), currentHashCode, theHouseDma, (this.houseDma!= null));
        }
        {
            String theHouseNumber;
            theHouseNumber = this.getHouseNumber();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "houseNumber", theHouseNumber), currentHashCode, theHouseNumber, (this.houseNumber!= null));
        }
        {
            String theUnitNumber;
            theUnitNumber = this.getUnitNumber();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "unitNumber", theUnitNumber), currentHashCode, theUnitNumber, (this.unitNumber!= null));
        }
        {
            String theUnitType;
            theUnitType = this.getUnitType();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "unitType", theUnitType), currentHashCode, theUnitType, (this.unitType!= null));
        }
        {
            String thePrefixDirection;
            thePrefixDirection = this.getPrefixDirection();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "prefixDirection", thePrefixDirection), currentHashCode, thePrefixDirection, (this.prefixDirection!= null));
        }
        {
            String thePostfixDirection;
            thePostfixDirection = this.getPostfixDirection();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "postfixDirection", thePostfixDirection), currentHashCode, thePostfixDirection, (this.postfixDirection!= null));
        }
        {
            String theStreetName;
            theStreetName = this.getStreetName();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "streetName", theStreetName), currentHashCode, theStreetName, (this.streetName!= null));
        }
        {
            String theStreetType;
            theStreetType = this.getStreetType();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "streetType", theStreetType), currentHashCode, theStreetType, (this.streetType!= null));
        }
        {
            String theMduIndicator;
            theMduIndicator = this.getMduIndicator();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "mduIndicator", theMduIndicator), currentHashCode, theMduIndicator, (this.mduIndicator!= null));
        }
        {
            String theLatitude;
            theLatitude = this.getLatitude();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "latitude", theLatitude), currentHashCode, theLatitude, (this.latitude!= null));
        }
        {
            String theLongitude;
            theLongitude = this.getLongitude();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "longitude", theLongitude), currentHashCode, theLongitude, (this.longitude!= null));
        }
        {
            String theCounty;
            theCounty = this.getCounty();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "county", theCounty), currentHashCode, theCounty, (this.county!= null));
        }
        return currentHashCode;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode() {
        final HashCodeStrategy2 strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String toString() {
        final ToStringStrategy2 strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        {
            String theHouseDma;
            theHouseDma = this.getHouseDma();
            strategy.appendField(locator, this, "houseDma", buffer, theHouseDma, (this.houseDma!= null));
        }
        {
            String theHouseNumber;
            theHouseNumber = this.getHouseNumber();
            strategy.appendField(locator, this, "houseNumber", buffer, theHouseNumber, (this.houseNumber!= null));
        }
        {
            String theUnitNumber;
            theUnitNumber = this.getUnitNumber();
            strategy.appendField(locator, this, "unitNumber", buffer, theUnitNumber, (this.unitNumber!= null));
        }
        {
            String theUnitType;
            theUnitType = this.getUnitType();
            strategy.appendField(locator, this, "unitType", buffer, theUnitType, (this.unitType!= null));
        }
        {
            String thePrefixDirection;
            thePrefixDirection = this.getPrefixDirection();
            strategy.appendField(locator, this, "prefixDirection", buffer, thePrefixDirection, (this.prefixDirection!= null));
        }
        {
            String thePostfixDirection;
            thePostfixDirection = this.getPostfixDirection();
            strategy.appendField(locator, this, "postfixDirection", buffer, thePostfixDirection, (this.postfixDirection!= null));
        }
        {
            String theStreetName;
            theStreetName = this.getStreetName();
            strategy.appendField(locator, this, "streetName", buffer, theStreetName, (this.streetName!= null));
        }
        {
            String theStreetType;
            theStreetType = this.getStreetType();
            strategy.appendField(locator, this, "streetType", buffer, theStreetType, (this.streetType!= null));
        }
        {
            String theMduIndicator;
            theMduIndicator = this.getMduIndicator();
            strategy.appendField(locator, this, "mduIndicator", buffer, theMduIndicator, (this.mduIndicator!= null));
        }
        {
            String theLatitude;
            theLatitude = this.getLatitude();
            strategy.appendField(locator, this, "latitude", buffer, theLatitude, (this.latitude!= null));
        }
        {
            String theLongitude;
            theLongitude = this.getLongitude();
            strategy.appendField(locator, this, "longitude", buffer, theLongitude, (this.longitude!= null));
        }
        {
            String theCounty;
            theCounty = this.getCounty();
            strategy.appendField(locator, this, "county", buffer, theCounty, (this.county!= null));
        }
        return buffer;
    }

}
