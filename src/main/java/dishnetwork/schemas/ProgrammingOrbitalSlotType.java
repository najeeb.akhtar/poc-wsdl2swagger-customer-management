
package dishnetwork.schemas;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

import org.jvnet.jaxb2_commons.lang.Equals2;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy2;
import org.jvnet.jaxb2_commons.lang.HashCode2;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy2;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString2;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy2;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * Contains the Signal OrbitalInfo.
 *
 * <p>Java class for ProgrammingOrbitalSlotType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="ProgrammingOrbitalSlotType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="programmingType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="classification" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="serviceCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="signalOrbitalInfoList" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="SignalOrbitalInfo" type="{http://www.dishnetwork.com/schema/CustomerManagement/SignalOrbitalInfo/2013_01_17}SignalOrbitalInfoType" maxOccurs="unbounded"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProgrammingOrbitalSlotType", propOrder = {
    "programmingType",
    "classification",
    "serviceCode",
    "signalOrbitalInfoList"
})
@Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
public class ProgrammingOrbitalSlotType implements Equals2, HashCode2, ToString2
{

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String programmingType;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String classification;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String serviceCode;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected SignalOrbitalInfoList signalOrbitalInfoList;

    /**
     * Default no-arg constructor
     *
     */
    public ProgrammingOrbitalSlotType() {
        super();
    }

    /**
     * Fully-initialising value constructor
     *
     */
    public ProgrammingOrbitalSlotType(final String programmingType, final String classification, final String serviceCode, final SignalOrbitalInfoList signalOrbitalInfoList) {
        this.programmingType = programmingType;
        this.classification = classification;
        this.serviceCode = serviceCode;
        this.signalOrbitalInfoList = signalOrbitalInfoList;
    }

    /**
     * Gets the value of the programmingType property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getProgrammingType() {
        return programmingType;
    }

    /**
     * Sets the value of the programmingType property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setProgrammingType(String value) {
        this.programmingType = value;
    }

    /**
     * Gets the value of the classification property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getClassification() {
        return classification;
    }

    /**
     * Sets the value of the classification property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setClassification(String value) {
        this.classification = value;
    }

    /**
     * Gets the value of the serviceCode property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getServiceCode() {
        return serviceCode;
    }

    /**
     * Sets the value of the serviceCode property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setServiceCode(String value) {
        this.serviceCode = value;
    }

    /**
     * Gets the value of the signalOrbitalInfoList property.
     *
     * @return
     *     possible object is
     *     {@link SignalOrbitalInfoList }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public SignalOrbitalInfoList getSignalOrbitalInfoList() {
        return signalOrbitalInfoList;
    }

    /**
     * Sets the value of the signalOrbitalInfoList property.
     *
     * @param value
     *     allowed object is
     *     {@link SignalOrbitalInfoList }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setSignalOrbitalInfoList(SignalOrbitalInfoList value) {
        this.signalOrbitalInfoList = value;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy2 strategy) {
        if ((object == null)||(this.getClass()!= object.getClass())) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final ProgrammingOrbitalSlotType that = ((ProgrammingOrbitalSlotType) object);
        {
            String lhsProgrammingType;
            lhsProgrammingType = this.getProgrammingType();
            String rhsProgrammingType;
            rhsProgrammingType = that.getProgrammingType();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "programmingType", lhsProgrammingType), LocatorUtils.property(thatLocator, "programmingType", rhsProgrammingType), lhsProgrammingType, rhsProgrammingType, (this.programmingType!= null), (that.programmingType!= null))) {
                return false;
            }
        }
        {
            String lhsClassification;
            lhsClassification = this.getClassification();
            String rhsClassification;
            rhsClassification = that.getClassification();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "classification", lhsClassification), LocatorUtils.property(thatLocator, "classification", rhsClassification), lhsClassification, rhsClassification, (this.classification!= null), (that.classification!= null))) {
                return false;
            }
        }
        {
            String lhsServiceCode;
            lhsServiceCode = this.getServiceCode();
            String rhsServiceCode;
            rhsServiceCode = that.getServiceCode();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "serviceCode", lhsServiceCode), LocatorUtils.property(thatLocator, "serviceCode", rhsServiceCode), lhsServiceCode, rhsServiceCode, (this.serviceCode!= null), (that.serviceCode!= null))) {
                return false;
            }
        }
        {
            SignalOrbitalInfoList lhsSignalOrbitalInfoList;
            lhsSignalOrbitalInfoList = this.getSignalOrbitalInfoList();
            SignalOrbitalInfoList rhsSignalOrbitalInfoList;
            rhsSignalOrbitalInfoList = that.getSignalOrbitalInfoList();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "signalOrbitalInfoList", lhsSignalOrbitalInfoList), LocatorUtils.property(thatLocator, "signalOrbitalInfoList", rhsSignalOrbitalInfoList), lhsSignalOrbitalInfoList, rhsSignalOrbitalInfoList, (this.signalOrbitalInfoList!= null), (that.signalOrbitalInfoList!= null))) {
                return false;
            }
        }
        return true;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(Object object) {
        final EqualsStrategy2 strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode(ObjectLocator locator, HashCodeStrategy2 strategy) {
        int currentHashCode = 1;
        {
            String theProgrammingType;
            theProgrammingType = this.getProgrammingType();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "programmingType", theProgrammingType), currentHashCode, theProgrammingType, (this.programmingType!= null));
        }
        {
            String theClassification;
            theClassification = this.getClassification();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "classification", theClassification), currentHashCode, theClassification, (this.classification!= null));
        }
        {
            String theServiceCode;
            theServiceCode = this.getServiceCode();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "serviceCode", theServiceCode), currentHashCode, theServiceCode, (this.serviceCode!= null));
        }
        {
            SignalOrbitalInfoList theSignalOrbitalInfoList;
            theSignalOrbitalInfoList = this.getSignalOrbitalInfoList();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "signalOrbitalInfoList", theSignalOrbitalInfoList), currentHashCode, theSignalOrbitalInfoList, (this.signalOrbitalInfoList!= null));
        }
        return currentHashCode;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode() {
        final HashCodeStrategy2 strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String toString() {
        final ToStringStrategy2 strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        {
            String theProgrammingType;
            theProgrammingType = this.getProgrammingType();
            strategy.appendField(locator, this, "programmingType", buffer, theProgrammingType, (this.programmingType!= null));
        }
        {
            String theClassification;
            theClassification = this.getClassification();
            strategy.appendField(locator, this, "classification", buffer, theClassification, (this.classification!= null));
        }
        {
            String theServiceCode;
            theServiceCode = this.getServiceCode();
            strategy.appendField(locator, this, "serviceCode", buffer, theServiceCode, (this.serviceCode!= null));
        }
        {
            SignalOrbitalInfoList theSignalOrbitalInfoList;
            theSignalOrbitalInfoList = this.getSignalOrbitalInfoList();
            strategy.appendField(locator, this, "signalOrbitalInfoList", buffer, theSignalOrbitalInfoList, (this.signalOrbitalInfoList!= null));
        }
        return buffer;
    }


}
