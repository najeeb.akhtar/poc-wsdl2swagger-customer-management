
package dishnetwork.schemas;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.jvnet.jaxb2_commons.lang.Equals2;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy2;
import org.jvnet.jaxb2_commons.lang.HashCode2;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy2;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString2;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy2;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * contains the order related information for order creation.
 *
 * <p>Java class for AccountOrderType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="AccountOrderType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="orderInfo" type="{http://www.dishnetwork.com/schema/Customer/Order/2015_01_15}OrderType"/&gt;
 *         &lt;element name="installInfo"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="componentIdList" type="{http://www.dishnetwork.com/schema/common/ComponentId/2015_01_15}ComponentIdListType"/&gt;
 *                   &lt;element name="easternArcFlag" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *                   &lt;element name="spotBeamInformation" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
 *                   &lt;element name="programmingOrbitalSlot" type="{http://www.dishnetwork.com/schema/CustomerManagement/ProgrammingOrbitalSlot/2013_01_17}ProgrammingOrbitalSlotType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *                   &lt;element name="receiverItemList" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="receiverItem" type="{http://www.dishnetwork.com/schema/CustomerManagement/Item/2013_01_17}ItemType" maxOccurs="unbounded"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="antennaItemList" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="antennaItem" type="{http://www.dishnetwork.com/schema/CustomerManagement/Item/2013_01_17}ItemType" maxOccurs="unbounded"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="antennaTagCodeList" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="antennaTagCode" type="{http://www.dishnetwork.com/schema/CustomerManagement/Item/2013_01_17}ItemType" maxOccurs="unbounded"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="receiverConfigInfo" type="{http://www.dishnetwork.com/schema/common/ReceiverConfiguration/2016_01_14}ReceiverConfigurationType" minOccurs="0"/&gt;
 *                   &lt;element name="expectedEquipmentList"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="expectedEquipment" type="{http://www.dishnetwork.com/schema/Equipment/ExpectedEquipmentInfoList/2014_06_13}EquipmentType" maxOccurs="unbounded"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="broadbandItemList" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="broadbandItem" type="{http://www.dishnetwork.com/schema/CustomerManagement/Item/2013_01_17}ItemType" maxOccurs="unbounded"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="installData" type="{http://www.dishnetwork.com/schema/common/CustomFieldList/2011_04_01}CustomFieldListType" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="salesForce" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="orderAttributes" type="{http://www.dishnetwork.com/schema/common/AttributeList/2013_09_19}AttributeListType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AccountOrderTypeSubmitOrder", propOrder = {
    "orderInfo",
    "installInfo",
    "salesForce",
    "orderAttributes"
})
@Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
public class AccountOrderTypeSubmitOrder implements Equals2, HashCode2, ToString2
{

    //@XmlElement(required = true)
    @JsonProperty(required = true)
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected OrderType orderInfo;
    //@XmlElement(required = true)
    @JsonProperty(required = true)
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected InstallInfo installInfo;
    //@XmlElement(nillable = true)
    //@JsonProperty(nillable = true)
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String salesForce;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected AttributeListType orderAttributes;

    /**
     * Default no-arg constructor
     *
     */
    public AccountOrderTypeSubmitOrder() {
        super();
    }

    /**
     * Fully-initialising value constructor
     *
     */
    public AccountOrderTypeSubmitOrder(final OrderType orderInfo, final InstallInfo installInfo, final String salesForce, final AttributeListType orderAttributes) {
        this.orderInfo = orderInfo;
        this.installInfo = installInfo;
        this.salesForce = salesForce;
        this.orderAttributes = orderAttributes;
    }

    /**
     * Gets the value of the orderInfo property.
     *
     * @return
     *     possible object is
     *     {@link OrderType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public OrderType getOrderInfo() {
        return orderInfo;
    }

    /**
     * Sets the value of the orderInfo property.
     *
     * @param value
     *     allowed object is
     *     {@link OrderType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setOrderInfo(OrderType value) {
        this.orderInfo = value;
    }

    /**
     * Gets the value of the installInfo property.
     *
     * @return
     *     possible object is
     *     {@link InstallInfo }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public InstallInfo getInstallInfo() {
        return installInfo;
    }

    /**
     * Sets the value of the installInfo property.
     *
     * @param value
     *     allowed object is
     *     {@link InstallInfo }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setInstallInfo(InstallInfo value) {
        this.installInfo = value;
    }

    /**
     * Gets the value of the salesForce property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getSalesForce() {
        return salesForce;
    }

    /**
     * Sets the value of the salesForce property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setSalesForce(String value) {
        this.salesForce = value;
    }

    /**
     * Gets the value of the orderAttributes property.
     *
     * @return
     *     possible object is
     *     {@link AttributeListType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public AttributeListType getOrderAttributes() {
        return orderAttributes;
    }

    /**
     * Sets the value of the orderAttributes property.
     *
     * @param value
     *     allowed object is
     *     {@link AttributeListType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setOrderAttributes(AttributeListType value) {
        this.orderAttributes = value;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy2 strategy) {
        if ((object == null)||(this.getClass()!= object.getClass())) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final AccountOrderTypeSubmitOrder that = ((AccountOrderTypeSubmitOrder) object);
        {
            OrderType lhsOrderInfo;
            lhsOrderInfo = this.getOrderInfo();
            OrderType rhsOrderInfo;
            rhsOrderInfo = that.getOrderInfo();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "orderInfo", lhsOrderInfo), LocatorUtils.property(thatLocator, "orderInfo", rhsOrderInfo), lhsOrderInfo, rhsOrderInfo, (this.orderInfo!= null), (that.orderInfo!= null))) {
                return false;
            }
        }
        {
            InstallInfo lhsInstallInfo;
            lhsInstallInfo = this.getInstallInfo();
            InstallInfo rhsInstallInfo;
            rhsInstallInfo = that.getInstallInfo();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "installInfo", lhsInstallInfo), LocatorUtils.property(thatLocator, "installInfo", rhsInstallInfo), lhsInstallInfo, rhsInstallInfo, (this.installInfo!= null), (that.installInfo!= null))) {
                return false;
            }
        }
        {
            String lhsSalesForce;
            lhsSalesForce = this.getSalesForce();
            String rhsSalesForce;
            rhsSalesForce = that.getSalesForce();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "salesForce", lhsSalesForce), LocatorUtils.property(thatLocator, "salesForce", rhsSalesForce), lhsSalesForce, rhsSalesForce, (this.salesForce!= null), (that.salesForce!= null))) {
                return false;
            }
        }
        {
            AttributeListType lhsOrderAttributes;
            lhsOrderAttributes = this.getOrderAttributes();
            AttributeListType rhsOrderAttributes;
            rhsOrderAttributes = that.getOrderAttributes();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "orderAttributes", lhsOrderAttributes), LocatorUtils.property(thatLocator, "orderAttributes", rhsOrderAttributes), lhsOrderAttributes, rhsOrderAttributes, (this.orderAttributes!= null), (that.orderAttributes!= null))) {
                return false;
            }
        }
        return true;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(Object object) {
        final EqualsStrategy2 strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode(ObjectLocator locator, HashCodeStrategy2 strategy) {
        int currentHashCode = 1;
        {
            OrderType theOrderInfo;
            theOrderInfo = this.getOrderInfo();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "orderInfo", theOrderInfo), currentHashCode, theOrderInfo, (this.orderInfo!= null));
        }
        {
            InstallInfo theInstallInfo;
            theInstallInfo = this.getInstallInfo();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "installInfo", theInstallInfo), currentHashCode, theInstallInfo, (this.installInfo!= null));
        }
        {
            String theSalesForce;
            theSalesForce = this.getSalesForce();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "salesForce", theSalesForce), currentHashCode, theSalesForce, (this.salesForce!= null));
        }
        {
            AttributeListType theOrderAttributes;
            theOrderAttributes = this.getOrderAttributes();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "orderAttributes", theOrderAttributes), currentHashCode, theOrderAttributes, (this.orderAttributes!= null));
        }
        return currentHashCode;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode() {
        final HashCodeStrategy2 strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String toString() {
        final ToStringStrategy2 strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        {
            OrderType theOrderInfo;
            theOrderInfo = this.getOrderInfo();
            strategy.appendField(locator, this, "orderInfo", buffer, theOrderInfo, (this.orderInfo!= null));
        }
        {
            InstallInfo theInstallInfo;
            theInstallInfo = this.getInstallInfo();
            strategy.appendField(locator, this, "installInfo", buffer, theInstallInfo, (this.installInfo!= null));
        }
        {
            String theSalesForce;
            theSalesForce = this.getSalesForce();
            strategy.appendField(locator, this, "salesForce", buffer, theSalesForce, (this.salesForce!= null));
        }
        {
            AttributeListType theOrderAttributes;
            theOrderAttributes = this.getOrderAttributes();
            strategy.appendField(locator, this, "orderAttributes", buffer, theOrderAttributes, (this.orderAttributes!= null));
        }
        return buffer;
    }


}
