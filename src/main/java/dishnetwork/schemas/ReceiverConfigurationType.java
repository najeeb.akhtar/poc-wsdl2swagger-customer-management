
package dishnetwork.schemas;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;

import io.swagger.annotations.ApiModelProperty;
import org.jvnet.jaxb2_commons.lang.Equals2;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy2;
import org.jvnet.jaxb2_commons.lang.HashCode2;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy2;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString2;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy2;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * Equipment configurator info.
 *
 * <p>Java class for ReceiverConfigurationType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="ReceiverConfigurationType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="configurationDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="ruleSet" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="equipmentSolution" type="{http://www.dishnetwork.com/xsd/common/EquipmentSolution/2014_06_13}EquipmentSolutionType" minOccurs="0"/&gt;
 *         &lt;element name="receiverLocationList" type="{http://www.dishnetwork.com/xsd/Equipment/ReceiverLocationList/2016_01_14}ReceiverLocationListType" minOccurs="0"/&gt;
 *         &lt;element name="receiverSolutionList" type="{http://www.dishnetwork.com/xsd/Equipment/ReceiverSolutionList/2011_04_01}ReceiverSolutionListType" minOccurs="0"/&gt;
 *         &lt;element name="additionalReceiverList" type="{http://www.dishnetwork.com/schema/WorkOrderMod/AddedReceiverList/2013_09_19}AddedReceiverListType" minOccurs="0"/&gt;
 *         &lt;element name="optionList" type="{http://www.dishnetwork.com/schema/common/CustomFieldList/2011_04_01}CustomFieldListType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReceiverConfigurationType", propOrder = {
    "configurationDate",
    "ruleSet",
    "equipmentSolution",
    "receiverLocationList",
    "receiverSolutionList",
    "additionalReceiverList",
    "optionList"
})
@Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
public class ReceiverConfigurationType implements Equals2, HashCode2, ToString2
{

    @XmlSchemaType(name = "date")
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    @ApiModelProperty(example = "2022-07-12T21:32:55.842Z")
    protected String configurationDate;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String ruleSet;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected EquipmentSolutionType equipmentSolution;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected ReceiverLocationListType receiverLocationList;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected ReceiverSolutionListType receiverSolutionList;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected AddedReceiverListType additionalReceiverList;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected CustomFieldListType optionList;

    /**
     * Default no-arg constructor
     *
     */
    public ReceiverConfigurationType() {
        super();
    }

    /**
     * Fully-initialising value constructor
     *
     */
    public ReceiverConfigurationType(final String configurationDate, final String ruleSet, final EquipmentSolutionType equipmentSolution, final ReceiverLocationListType receiverLocationList, final ReceiverSolutionListType receiverSolutionList, final AddedReceiverListType additionalReceiverList, final CustomFieldListType optionList) {
        this.configurationDate = configurationDate;
        this.ruleSet = ruleSet;
        this.equipmentSolution = equipmentSolution;
        this.receiverLocationList = receiverLocationList;
        this.receiverSolutionList = receiverSolutionList;
        this.additionalReceiverList = additionalReceiverList;
        this.optionList = optionList;
    }

    /**
     * Gets the value of the configurationDate property.
     *
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getConfigurationDate() {
        return configurationDate;
    }

    /**
     * Sets the value of the configurationDate property.
     *
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setConfigurationDate(String value) {
        this.configurationDate = value;
    }

    /**
     * Gets the value of the ruleSet property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getRuleSet() {
        return ruleSet;
    }

    /**
     * Sets the value of the ruleSet property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setRuleSet(String value) {
        this.ruleSet = value;
    }

    /**
     * Gets the value of the equipmentSolution property.
     *
     * @return
     *     possible object is
     *     {@link EquipmentSolutionType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public EquipmentSolutionType getEquipmentSolution() {
        return equipmentSolution;
    }

    /**
     * Sets the value of the equipmentSolution property.
     *
     * @param value
     *     allowed object is
     *     {@link EquipmentSolutionType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setEquipmentSolution(EquipmentSolutionType value) {
        this.equipmentSolution = value;
    }

    /**
     * Gets the value of the receiverLocationList property.
     *
     * @return
     *     possible object is
     *     {@link ReceiverLocationListType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public ReceiverLocationListType getReceiverLocationList() {
        return receiverLocationList;
    }

    /**
     * Sets the value of the receiverLocationList property.
     *
     * @param value
     *     allowed object is
     *     {@link ReceiverLocationListType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setReceiverLocationList(ReceiverLocationListType value) {
        this.receiverLocationList = value;
    }

    /**
     * Gets the value of the receiverSolutionList property.
     *
     * @return
     *     possible object is
     *     {@link ReceiverSolutionListType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public ReceiverSolutionListType getReceiverSolutionList() {
        return receiverSolutionList;
    }

    /**
     * Sets the value of the receiverSolutionList property.
     *
     * @param value
     *     allowed object is
     *     {@link ReceiverSolutionListType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setReceiverSolutionList(ReceiverSolutionListType value) {
        this.receiverSolutionList = value;
    }

    /**
     * Gets the value of the additionalReceiverList property.
     *
     * @return
     *     possible object is
     *     {@link AddedReceiverListType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public AddedReceiverListType getAdditionalReceiverList() {
        return additionalReceiverList;
    }

    /**
     * Sets the value of the additionalReceiverList property.
     *
     * @param value
     *     allowed object is
     *     {@link AddedReceiverListType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setAdditionalReceiverList(AddedReceiverListType value) {
        this.additionalReceiverList = value;
    }

    /**
     * Gets the value of the optionList property.
     *
     * @return
     *     possible object is
     *     {@link CustomFieldListType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public CustomFieldListType getOptionList() {
        return optionList;
    }

    /**
     * Sets the value of the optionList property.
     *
     * @param value
     *     allowed object is
     *     {@link CustomFieldListType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setOptionList(CustomFieldListType value) {
        this.optionList = value;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy2 strategy) {
        if ((object == null)||(this.getClass()!= object.getClass())) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final ReceiverConfigurationType that = ((ReceiverConfigurationType) object);
        {
            String lhsConfigurationDate;
            lhsConfigurationDate = this.getConfigurationDate();
            String rhsConfigurationDate;
            rhsConfigurationDate = that.getConfigurationDate();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "configurationDate", lhsConfigurationDate), LocatorUtils.property(thatLocator, "configurationDate", rhsConfigurationDate), lhsConfigurationDate, rhsConfigurationDate, (this.configurationDate!= null), (that.configurationDate!= null))) {
                return false;
            }
        }
        {
            String lhsRuleSet;
            lhsRuleSet = this.getRuleSet();
            String rhsRuleSet;
            rhsRuleSet = that.getRuleSet();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "ruleSet", lhsRuleSet), LocatorUtils.property(thatLocator, "ruleSet", rhsRuleSet), lhsRuleSet, rhsRuleSet, (this.ruleSet!= null), (that.ruleSet!= null))) {
                return false;
            }
        }
        {
            EquipmentSolutionType lhsEquipmentSolution;
            lhsEquipmentSolution = this.getEquipmentSolution();
            EquipmentSolutionType rhsEquipmentSolution;
            rhsEquipmentSolution = that.getEquipmentSolution();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "equipmentSolution", lhsEquipmentSolution), LocatorUtils.property(thatLocator, "equipmentSolution", rhsEquipmentSolution), lhsEquipmentSolution, rhsEquipmentSolution, (this.equipmentSolution!= null), (that.equipmentSolution!= null))) {
                return false;
            }
        }
        {
            ReceiverLocationListType lhsReceiverLocationList;
            lhsReceiverLocationList = this.getReceiverLocationList();
            ReceiverLocationListType rhsReceiverLocationList;
            rhsReceiverLocationList = that.getReceiverLocationList();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "receiverLocationList", lhsReceiverLocationList), LocatorUtils.property(thatLocator, "receiverLocationList", rhsReceiverLocationList), lhsReceiverLocationList, rhsReceiverLocationList, (this.receiverLocationList!= null), (that.receiverLocationList!= null))) {
                return false;
            }
        }
        {
            ReceiverSolutionListType lhsReceiverSolutionList;
            lhsReceiverSolutionList = this.getReceiverSolutionList();
            ReceiverSolutionListType rhsReceiverSolutionList;
            rhsReceiverSolutionList = that.getReceiverSolutionList();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "receiverSolutionList", lhsReceiverSolutionList), LocatorUtils.property(thatLocator, "receiverSolutionList", rhsReceiverSolutionList), lhsReceiverSolutionList, rhsReceiverSolutionList, (this.receiverSolutionList!= null), (that.receiverSolutionList!= null))) {
                return false;
            }
        }
        {
            AddedReceiverListType lhsAdditionalReceiverList;
            lhsAdditionalReceiverList = this.getAdditionalReceiverList();
            AddedReceiverListType rhsAdditionalReceiverList;
            rhsAdditionalReceiverList = that.getAdditionalReceiverList();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "additionalReceiverList", lhsAdditionalReceiverList), LocatorUtils.property(thatLocator, "additionalReceiverList", rhsAdditionalReceiverList), lhsAdditionalReceiverList, rhsAdditionalReceiverList, (this.additionalReceiverList!= null), (that.additionalReceiverList!= null))) {
                return false;
            }
        }
        {
            CustomFieldListType lhsOptionList;
            lhsOptionList = this.getOptionList();
            CustomFieldListType rhsOptionList;
            rhsOptionList = that.getOptionList();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "optionList", lhsOptionList), LocatorUtils.property(thatLocator, "optionList", rhsOptionList), lhsOptionList, rhsOptionList, (this.optionList!= null), (that.optionList!= null))) {
                return false;
            }
        }
        return true;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(Object object) {
        final EqualsStrategy2 strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode(ObjectLocator locator, HashCodeStrategy2 strategy) {
        int currentHashCode = 1;
        {
            String theConfigurationDate;
            theConfigurationDate = this.getConfigurationDate();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "configurationDate", theConfigurationDate), currentHashCode, theConfigurationDate, (this.configurationDate!= null));
        }
        {
            String theRuleSet;
            theRuleSet = this.getRuleSet();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "ruleSet", theRuleSet), currentHashCode, theRuleSet, (this.ruleSet!= null));
        }
        {
            EquipmentSolutionType theEquipmentSolution;
            theEquipmentSolution = this.getEquipmentSolution();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "equipmentSolution", theEquipmentSolution), currentHashCode, theEquipmentSolution, (this.equipmentSolution!= null));
        }
        {
            ReceiverLocationListType theReceiverLocationList;
            theReceiverLocationList = this.getReceiverLocationList();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "receiverLocationList", theReceiverLocationList), currentHashCode, theReceiverLocationList, (this.receiverLocationList!= null));
        }
        {
            ReceiverSolutionListType theReceiverSolutionList;
            theReceiverSolutionList = this.getReceiverSolutionList();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "receiverSolutionList", theReceiverSolutionList), currentHashCode, theReceiverSolutionList, (this.receiverSolutionList!= null));
        }
        {
            AddedReceiverListType theAdditionalReceiverList;
            theAdditionalReceiverList = this.getAdditionalReceiverList();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "additionalReceiverList", theAdditionalReceiverList), currentHashCode, theAdditionalReceiverList, (this.additionalReceiverList!= null));
        }
        {
            CustomFieldListType theOptionList;
            theOptionList = this.getOptionList();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "optionList", theOptionList), currentHashCode, theOptionList, (this.optionList!= null));
        }
        return currentHashCode;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode() {
        final HashCodeStrategy2 strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String toString() {
        final ToStringStrategy2 strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        {
            String theConfigurationDate;
            theConfigurationDate = this.getConfigurationDate();
            strategy.appendField(locator, this, "configurationDate", buffer, theConfigurationDate, (this.configurationDate!= null));
        }
        {
            String theRuleSet;
            theRuleSet = this.getRuleSet();
            strategy.appendField(locator, this, "ruleSet", buffer, theRuleSet, (this.ruleSet!= null));
        }
        {
            EquipmentSolutionType theEquipmentSolution;
            theEquipmentSolution = this.getEquipmentSolution();
            strategy.appendField(locator, this, "equipmentSolution", buffer, theEquipmentSolution, (this.equipmentSolution!= null));
        }
        {
            ReceiverLocationListType theReceiverLocationList;
            theReceiverLocationList = this.getReceiverLocationList();
            strategy.appendField(locator, this, "receiverLocationList", buffer, theReceiverLocationList, (this.receiverLocationList!= null));
        }
        {
            ReceiverSolutionListType theReceiverSolutionList;
            theReceiverSolutionList = this.getReceiverSolutionList();
            strategy.appendField(locator, this, "receiverSolutionList", buffer, theReceiverSolutionList, (this.receiverSolutionList!= null));
        }
        {
            AddedReceiverListType theAdditionalReceiverList;
            theAdditionalReceiverList = this.getAdditionalReceiverList();
            strategy.appendField(locator, this, "additionalReceiverList", buffer, theAdditionalReceiverList, (this.additionalReceiverList!= null));
        }
        {
            CustomFieldListType theOptionList;
            theOptionList = this.getOptionList();
            strategy.appendField(locator, this, "optionList", buffer, theOptionList, (this.optionList!= null));
        }
        return buffer;
    }

}
