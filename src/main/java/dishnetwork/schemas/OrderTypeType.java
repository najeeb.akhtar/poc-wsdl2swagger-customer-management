
package dishnetwork.schemas;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OrderTypeType.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <pre>
 * &lt;simpleType name="OrderTypeType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="NEW_CONNECT_SERVICE"/&gt;
 *     &lt;enumeration value="RECONNECT_SERVICE"/&gt;
 *     &lt;enumeration value="RESTART_SERVICE"/&gt;
 *     &lt;enumeration value="CHANGE_OF_SERVICE"/&gt;
 *     &lt;enumeration value="MUST_CARRY"/&gt;
 *     &lt;enumeration value="TROUBLE_CALL"/&gt;
 *     &lt;enumeration value="SERVICE_CALL"/&gt;
 *     &lt;enumeration value="VOLUNTARY_DISCONNECT"/&gt;
 *     &lt;enumeration value="NON_PAY_DISCONNECT"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 *
 */
@XmlType(name = "OrderTypeType", namespace = "http://www.dishnetwork.com/schema/AccountOrder/OrderType/2011_04_01")
@XmlEnum
@Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
public enum OrderTypeType {

    NEW_CONNECT_SERVICE,
    RECONNECT_SERVICE,
    RESTART_SERVICE,
    CHANGE_OF_SERVICE,
    MUST_CARRY,
    TROUBLE_CALL,
    SERVICE_CALL,
    VOLUNTARY_DISCONNECT,
    NON_PAY_DISCONNECT;

    public String value() {
        return name();
    }

    public static OrderTypeType fromValue(String v) {
        return valueOf(v);
    }

}
