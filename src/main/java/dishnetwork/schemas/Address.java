package dishnetwork.schemas;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Address {
    @JsonProperty(required = true)
    protected String type;
    @JsonProperty(required = true)
    protected String lineOne;
    protected String lineTwo;
    @JsonProperty(required = true)
    protected String city;
    protected String county;
    protected String state;
    protected String zip;
    protected String zipPlus4;
    protected String pushAddressPossibleFlag;
}
