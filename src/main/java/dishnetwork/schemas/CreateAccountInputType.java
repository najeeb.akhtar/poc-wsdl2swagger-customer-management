
package dishnetwork.schemas;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.jvnet.jaxb2_commons.lang.Equals2;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy2;
import org.jvnet.jaxb2_commons.lang.HashCode2;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy2;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString2;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy2;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * Create Account Object
 *
 * <p>Java class for CreateAccountInputType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="CreateAccountInputType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="overrideMultipleAccountsFlag" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="accountInfo" type="{http://www.dishnetwork.com/schema/CustomerManagement/CreateAccountAccount/2016_03_24}AccountType"/&gt;
 *         &lt;element name="serviceProviderInfo" type="{http://www.dishnetwork.com/schema/common/ServiceProviderInfo/2012_08_01}ServiceProviderInfoType" minOccurs="0"/&gt;
 *         &lt;element name="billingGroup" type="{http://www.dishnetwork.com/schema/Billing/BillingGroup/2011_04_01}BillingGroupType"/&gt;
 *         &lt;element name="customer" type="{http://www.dishnetwork.com/schema/CustomerManagement/Customer/2014_10_22}CustomerType"/&gt;
 *         &lt;element name="customerQualification" type="{http://www.dishnetwork.com/schema/CustomerManagement/CustomerQualification/2015_01_15}CustomerQualificationType" minOccurs="0"/&gt;
 *         &lt;element name="planList" type="{http://www.dishnetwork.com/schema/CustomerManagement/PlanList/2015_01_15}PlanListType" minOccurs="0"/&gt;
 *         &lt;element name="accountOrder" type="{http://www.dishnetwork.com/schema/CustomerManagement/AccountOrder/2015_01_15}AccountOrderType"/&gt;
 *         &lt;element name="paymentInfo" type="{http://www.dishnetwork.com/schema/CustomerManagement/AccountPaymentInfo/2022_05_12}AccountPaymentInfoType"/&gt;
 *         &lt;element name="receiverConfigInfo" type="{http://www.dishnetwork.com/schema/common/ReceiverConfiguration/2016_01_14}ReceiverConfigurationType" minOccurs="0"/&gt;
 *         &lt;element name="expectedEquipmentList" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="expectedEquipment" type="{http://www.dishnetwork.com/schema/Equipment/ExpectedEquipmentInfoList/2014_06_13}EquipmentType" maxOccurs="unbounded"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="location" type="{http://www.dishnetwork.com/schema/CustomerManagement/CreateAccountLocation/2016_03_24}LocationType" minOccurs="0"/&gt;
 *         &lt;element name="securityInfo" type="{http://www.dishnetwork.com/schema/CustomerManagement/AccountPrivacyInfo/2011_04_01}AccountPrivacyInfoType" minOccurs="0"/&gt;
 *         &lt;element name="offerCode" type="{http://www.dishnetwork.com/schema/Product/OfferCodeInfo/2014_06_13}OfferCodeInfoType" minOccurs="0"/&gt;
 *         &lt;element name="sellerInfo" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="sellerId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="sellerName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="agreementList" type="{http://www.dishnetwork.com/schema/CustomerManagement/AgreementList/2015_01_15}AgreementListType" minOccurs="0"/&gt;
 *         &lt;element name="certificate" type="{http://www.dishnetwork.com/schema/CustomerManagement/Certificate/2011_04_01}CertificateType" minOccurs="0"/&gt;
 *         &lt;element name="externalOrder" type="{http://www.dishnetwork.com/schema/common/ExternalOrder/2015_10_08}ExternalOrderInput" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreateAccountInputType", propOrder = {
    "overrideMultipleAccountsFlag",
    "accountInfo",
    "serviceProviderInfo",
    "billingGroup",
    "customer",
    "customerQualification",
    "planList",
    "accountOrder",
    "paymentInfo",
    "receiverConfigInfo",
    "expectedEquipmentList",
    "location",
    "securityInfo",
    "offerCode",
    "sellerInfo",
    "agreementList",
    "certificate",
    "externalOrder"
})
@Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
public class CreateAccountInputType implements Equals2, HashCode2, ToString2
{

    //@XmlElement(defaultValue = "false")
    @JsonProperty(defaultValue = "false")
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String overrideMultipleAccountsFlag;
    //@XmlElement(required = true)
    @JsonProperty(required = true)
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected AccountType accountInfo;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected ServiceProviderInfoType serviceProviderInfo;
    //@XmlElement(required = true)
    @JsonProperty(required = true)
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected BillingGroupType billingGroup;
    //@XmlElement(required = true)
    @JsonProperty(required = true)
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected CustomerType customer;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected CustomerQualificationType customerQualification;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected PlanListType planList;
    //@XmlElement(required = true)
    @JsonProperty(required = true)
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected AccountOrderType accountOrder;
    //@XmlElement(required = true)
    @JsonProperty(required = true)
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected AccountPaymentInfoType paymentInfo;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected ReceiverConfigurationType receiverConfigInfo;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected ExpectedEquipmentList expectedEquipmentList;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected LocationType location;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected AccountPrivacyInfoType securityInfo;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected OfferCodeInfoType offerCode;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected SellerInfo sellerInfo;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected AgreementListType agreementList;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected CertificateType certificate;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected ExternalOrderInput externalOrder;
    protected String serializedOfferCode;
    protected RaHeader raHeader;

    public String getSerializedOfferCode() {
        return serializedOfferCode;
    }

    public void setSerializedOfferCode(String serializedOfferCode) {
        this.serializedOfferCode = serializedOfferCode;
    }

    public RaHeader getRaHeader() {
        return raHeader;
    }

    public void setRaHeader(RaHeader raHeader) {
        this.raHeader = raHeader;
    }

    /**
     * Default no-arg constructor
     *
     */
    public CreateAccountInputType() {
        super();
    }

    /**
     * Fully-initialising value constructor
     *
     */
    public CreateAccountInputType(final String overrideMultipleAccountsFlag, final AccountType accountInfo, final ServiceProviderInfoType serviceProviderInfo, final BillingGroupType billingGroup, final CustomerType customer, final CustomerQualificationType customerQualification, final PlanListType planList, final AccountOrderType accountOrder, final AccountPaymentInfoType paymentInfo, final ReceiverConfigurationType receiverConfigInfo, final ExpectedEquipmentList expectedEquipmentList, final LocationType location, final AccountPrivacyInfoType securityInfo, final OfferCodeInfoType offerCode, final SellerInfo sellerInfo, final AgreementListType agreementList, final CertificateType certificate, final ExternalOrderInput externalOrder) {
        this.overrideMultipleAccountsFlag = overrideMultipleAccountsFlag;
        this.accountInfo = accountInfo;
        this.serviceProviderInfo = serviceProviderInfo;
        this.billingGroup = billingGroup;
        this.customer = customer;
        this.customerQualification = customerQualification;
        this.planList = planList;
        this.accountOrder = accountOrder;
        this.paymentInfo = paymentInfo;
        this.receiverConfigInfo = receiverConfigInfo;
        this.expectedEquipmentList = expectedEquipmentList;
        this.location = location;
        this.securityInfo = securityInfo;
        this.offerCode = offerCode;
        this.sellerInfo = sellerInfo;
        this.agreementList = agreementList;
        this.certificate = certificate;
        this.externalOrder = externalOrder;
    }

    /**
     * Gets the value of the overrideMultipleAccountsFlag property.
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getOverrideMultipleAccountsFlag() {
        return overrideMultipleAccountsFlag;
    }

    /**
     * Sets the value of the overrideMultipleAccountsFlag property.
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setOverrideMultipleAccountsFlag(String value) {
        this.overrideMultipleAccountsFlag = value;
    }

    /**
     * Gets the value of the accountInfo property.
     *
     * @return
     *     possible object is
     *     {@link AccountType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public AccountType getAccountInfo() {
        return accountInfo;
    }

    /**
     * Sets the value of the accountInfo property.
     *
     * @param value
     *     allowed object is
     *     {@link AccountType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setAccountInfo(AccountType value) {
        this.accountInfo = value;
    }

    /**
     * Gets the value of the serviceProviderInfo property.
     *
     * @return
     *     possible object is
     *     {@link ServiceProviderInfoType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public ServiceProviderInfoType getServiceProviderInfo() {
        return serviceProviderInfo;
    }

    /**
     * Sets the value of the serviceProviderInfo property.
     *
     * @param value
     *     allowed object is
     *     {@link ServiceProviderInfoType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setServiceProviderInfo(ServiceProviderInfoType value) {
        this.serviceProviderInfo = value;
    }

    /**
     * Gets the value of the billingGroup property.
     *
     * @return
     *     possible object is
     *     {@link BillingGroupType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public BillingGroupType getBillingGroup() {
        return billingGroup;
    }

    /**
     * Sets the value of the billingGroup property.
     *
     * @param value
     *     allowed object is
     *     {@link BillingGroupType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setBillingGroup(BillingGroupType value) {
        this.billingGroup = value;
    }

    /**
     * Gets the value of the customer property.
     *
     * @return
     *     possible object is
     *     {@link CustomerType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public CustomerType getCustomer() {
        return customer;
    }

    /**
     * Sets the value of the customer property.
     *
     * @param value
     *     allowed object is
     *     {@link CustomerType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setCustomer(CustomerType value) {
        this.customer = value;
    }

    /**
     * Gets the value of the customerQualification property.
     *
     * @return
     *     possible object is
     *     {@link CustomerQualificationType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public CustomerQualificationType getCustomerQualification() {
        return customerQualification;
    }

    /**
     * Sets the value of the customerQualification property.
     *
     * @param value
     *     allowed object is
     *     {@link CustomerQualificationType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setCustomerQualification(CustomerQualificationType value) {
        this.customerQualification = value;
    }

    /**
     * Gets the value of the planList property.
     *
     * @return
     *     possible object is
     *     {@link PlanListType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public PlanListType getPlanList() {
        return planList;
    }

    /**
     * Sets the value of the planList property.
     *
     * @param value
     *     allowed object is
     *     {@link PlanListType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setPlanList(PlanListType value) {
        this.planList = value;
    }

    /**
     * Gets the value of the accountOrder property.
     *
     * @return
     *     possible object is
     *     {@link AccountOrderType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public AccountOrderType getAccountOrder() {
        return accountOrder;
    }

    /**
     * Sets the value of the accountOrder property.
     *
     * @param value
     *     allowed object is
     *     {@link AccountOrderType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setAccountOrder(AccountOrderType value) {
        this.accountOrder = value;
    }

    /**
     * Gets the value of the paymentInfo property.
     *
     * @return
     *     possible object is
     *     {@link AccountPaymentInfoType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public AccountPaymentInfoType getPaymentInfo() {
        return paymentInfo;
    }

    /**
     * Sets the value of the paymentInfo property.
     *
     * @param value
     *     allowed object is
     *     {@link AccountPaymentInfoType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setPaymentInfo(AccountPaymentInfoType value) {
        this.paymentInfo = value;
    }

    /**
     * Gets the value of the receiverConfigInfo property.
     *
     * @return
     *     possible object is
     *     {@link ReceiverConfigurationType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public ReceiverConfigurationType getReceiverConfigInfo() {
        return receiverConfigInfo;
    }

    /**
     * Sets the value of the receiverConfigInfo property.
     *
     * @param value
     *     allowed object is
     *     {@link ReceiverConfigurationType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setReceiverConfigInfo(ReceiverConfigurationType value) {
        this.receiverConfigInfo = value;
    }

    /**
     * Gets the value of the expectedEquipmentList property.
     *
     * @return
     *     possible object is
     *     {@link ExpectedEquipmentList }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public ExpectedEquipmentList getExpectedEquipmentList() {
        return expectedEquipmentList;
    }

    /**
     * Sets the value of the expectedEquipmentList property.
     *
     * @param value
     *     allowed object is
     *     {@link ExpectedEquipmentList }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setExpectedEquipmentList(ExpectedEquipmentList value) {
        this.expectedEquipmentList = value;
    }

    /**
     * Gets the value of the location property.
     *
     * @return
     *     possible object is
     *     {@link LocationType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public LocationType getLocation() {
        return location;
    }

    /**
     * Sets the value of the location property.
     *
     * @param value
     *     allowed object is
     *     {@link LocationType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setLocation(LocationType value) {
        this.location = value;
    }

    /**
     * Gets the value of the securityInfo property.
     *
     * @return
     *     possible object is
     *     {@link AccountPrivacyInfoType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public AccountPrivacyInfoType getSecurityInfo() {
        return securityInfo;
    }

    /**
     * Sets the value of the securityInfo property.
     *
     * @param value
     *     allowed object is
     *     {@link AccountPrivacyInfoType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setSecurityInfo(AccountPrivacyInfoType value) {
        this.securityInfo = value;
    }

    /**
     * Gets the value of the offerCode property.
     *
     * @return
     *     possible object is
     *     {@link OfferCodeInfoType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public OfferCodeInfoType getOfferCode() {
        return offerCode;
    }

    /**
     * Sets the value of the offerCode property.
     *
     * @param value
     *     allowed object is
     *     {@link OfferCodeInfoType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setOfferCode(OfferCodeInfoType value) {
        this.offerCode = value;
    }

    /**
     * Gets the value of the sellerInfo property.
     *
     * @return
     *     possible object is
     *     {@link SellerInfo }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public SellerInfo getSellerInfo() {
        return sellerInfo;
    }

    /**
     * Sets the value of the sellerInfo property.
     *
     * @param value
     *     allowed object is
     *     {@link SellerInfo }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setSellerInfo(SellerInfo value) {
        this.sellerInfo = value;
    }

    /**
     * Gets the value of the agreementList property.
     *
     * @return
     *     possible object is
     *     {@link AgreementListType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public AgreementListType getAgreementList() {
        return agreementList;
    }

    /**
     * Sets the value of the agreementList property.
     *
     * @param value
     *     allowed object is
     *     {@link AgreementListType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setAgreementList(AgreementListType value) {
        this.agreementList = value;
    }

    /**
     * Gets the value of the certificate property.
     *
     * @return
     *     possible object is
     *     {@link CertificateType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public CertificateType getCertificate() {
        return certificate;
    }

    /**
     * Sets the value of the certificate property.
     *
     * @param value
     *     allowed object is
     *     {@link CertificateType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setCertificate(CertificateType value) {
        this.certificate = value;
    }

    /**
     * Gets the value of the externalOrder property.
     *
     * @return
     *     possible object is
     *     {@link ExternalOrderInput }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public ExternalOrderInput getExternalOrder() {
        return externalOrder;
    }

    /**
     * Sets the value of the externalOrder property.
     *
     * @param value
     *     allowed object is
     *     {@link ExternalOrderInput }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setExternalOrder(ExternalOrderInput value) {
        this.externalOrder = value;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy2 strategy) {
        if ((object == null)||(this.getClass()!= object.getClass())) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final CreateAccountInputType that = ((CreateAccountInputType) object);
        {
            String lhsOverrideMultipleAccountsFlag;
            lhsOverrideMultipleAccountsFlag = this.getOverrideMultipleAccountsFlag();
            String rhsOverrideMultipleAccountsFlag;
            rhsOverrideMultipleAccountsFlag = that.getOverrideMultipleAccountsFlag();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "overrideMultipleAccountsFlag", lhsOverrideMultipleAccountsFlag), LocatorUtils.property(thatLocator, "overrideMultipleAccountsFlag", rhsOverrideMultipleAccountsFlag), lhsOverrideMultipleAccountsFlag, rhsOverrideMultipleAccountsFlag, true, true)) {
                return false;
            }
        }
        {
            AccountType lhsAccountInfo;
            lhsAccountInfo = this.getAccountInfo();
            AccountType rhsAccountInfo;
            rhsAccountInfo = that.getAccountInfo();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "accountInfo", lhsAccountInfo), LocatorUtils.property(thatLocator, "accountInfo", rhsAccountInfo), lhsAccountInfo, rhsAccountInfo, (this.accountInfo!= null), (that.accountInfo!= null))) {
                return false;
            }
        }
        {
            ServiceProviderInfoType lhsServiceProviderInfo;
            lhsServiceProviderInfo = this.getServiceProviderInfo();
            ServiceProviderInfoType rhsServiceProviderInfo;
            rhsServiceProviderInfo = that.getServiceProviderInfo();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "serviceProviderInfo", lhsServiceProviderInfo), LocatorUtils.property(thatLocator, "serviceProviderInfo", rhsServiceProviderInfo), lhsServiceProviderInfo, rhsServiceProviderInfo, (this.serviceProviderInfo!= null), (that.serviceProviderInfo!= null))) {
                return false;
            }
        }
        {
            BillingGroupType lhsBillingGroup;
            lhsBillingGroup = this.getBillingGroup();
            BillingGroupType rhsBillingGroup;
            rhsBillingGroup = that.getBillingGroup();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "billingGroup", lhsBillingGroup), LocatorUtils.property(thatLocator, "billingGroup", rhsBillingGroup), lhsBillingGroup, rhsBillingGroup, (this.billingGroup!= null), (that.billingGroup!= null))) {
                return false;
            }
        }
        {
            CustomerType lhsCustomer;
            lhsCustomer = this.getCustomer();
            CustomerType rhsCustomer;
            rhsCustomer = that.getCustomer();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "customer", lhsCustomer), LocatorUtils.property(thatLocator, "customer", rhsCustomer), lhsCustomer, rhsCustomer, (this.customer!= null), (that.customer!= null))) {
                return false;
            }
        }
        {
            CustomerQualificationType lhsCustomerQualification;
            lhsCustomerQualification = this.getCustomerQualification();
            CustomerQualificationType rhsCustomerQualification;
            rhsCustomerQualification = that.getCustomerQualification();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "customerQualification", lhsCustomerQualification), LocatorUtils.property(thatLocator, "customerQualification", rhsCustomerQualification), lhsCustomerQualification, rhsCustomerQualification, (this.customerQualification!= null), (that.customerQualification!= null))) {
                return false;
            }
        }
        {
            PlanListType lhsPlanList;
            lhsPlanList = this.getPlanList();
            PlanListType rhsPlanList;
            rhsPlanList = that.getPlanList();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "planList", lhsPlanList), LocatorUtils.property(thatLocator, "planList", rhsPlanList), lhsPlanList, rhsPlanList, (this.planList!= null), (that.planList!= null))) {
                return false;
            }
        }
        {
            AccountOrderType lhsAccountOrder;
            lhsAccountOrder = this.getAccountOrder();
            AccountOrderType rhsAccountOrder;
            rhsAccountOrder = that.getAccountOrder();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "accountOrder", lhsAccountOrder), LocatorUtils.property(thatLocator, "accountOrder", rhsAccountOrder), lhsAccountOrder, rhsAccountOrder, (this.accountOrder!= null), (that.accountOrder!= null))) {
                return false;
            }
        }
        {
            AccountPaymentInfoType lhsPaymentInfo;
            lhsPaymentInfo = this.getPaymentInfo();
            AccountPaymentInfoType rhsPaymentInfo;
            rhsPaymentInfo = that.getPaymentInfo();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "paymentInfo", lhsPaymentInfo), LocatorUtils.property(thatLocator, "paymentInfo", rhsPaymentInfo), lhsPaymentInfo, rhsPaymentInfo, (this.paymentInfo!= null), (that.paymentInfo!= null))) {
                return false;
            }
        }
        {
            ReceiverConfigurationType lhsReceiverConfigInfo;
            lhsReceiverConfigInfo = this.getReceiverConfigInfo();
            ReceiverConfigurationType rhsReceiverConfigInfo;
            rhsReceiverConfigInfo = that.getReceiverConfigInfo();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "receiverConfigInfo", lhsReceiverConfigInfo), LocatorUtils.property(thatLocator, "receiverConfigInfo", rhsReceiverConfigInfo), lhsReceiverConfigInfo, rhsReceiverConfigInfo, (this.receiverConfigInfo!= null), (that.receiverConfigInfo!= null))) {
                return false;
            }
        }
        {
            ExpectedEquipmentList lhsExpectedEquipmentList;
            lhsExpectedEquipmentList = this.getExpectedEquipmentList();
            ExpectedEquipmentList rhsExpectedEquipmentList;
            rhsExpectedEquipmentList = that.getExpectedEquipmentList();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "expectedEquipmentList", lhsExpectedEquipmentList), LocatorUtils.property(thatLocator, "expectedEquipmentList", rhsExpectedEquipmentList), lhsExpectedEquipmentList, rhsExpectedEquipmentList, (this.expectedEquipmentList!= null), (that.expectedEquipmentList!= null))) {
                return false;
            }
        }
        {
            LocationType lhsLocation;
            lhsLocation = this.getLocation();
            LocationType rhsLocation;
            rhsLocation = that.getLocation();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "location", lhsLocation), LocatorUtils.property(thatLocator, "location", rhsLocation), lhsLocation, rhsLocation, (this.location!= null), (that.location!= null))) {
                return false;
            }
        }
        {
            AccountPrivacyInfoType lhsSecurityInfo;
            lhsSecurityInfo = this.getSecurityInfo();
            AccountPrivacyInfoType rhsSecurityInfo;
            rhsSecurityInfo = that.getSecurityInfo();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "securityInfo", lhsSecurityInfo), LocatorUtils.property(thatLocator, "securityInfo", rhsSecurityInfo), lhsSecurityInfo, rhsSecurityInfo, (this.securityInfo!= null), (that.securityInfo!= null))) {
                return false;
            }
        }
        {
            OfferCodeInfoType lhsOfferCode;
            lhsOfferCode = this.getOfferCode();
            OfferCodeInfoType rhsOfferCode;
            rhsOfferCode = that.getOfferCode();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "offerCode", lhsOfferCode), LocatorUtils.property(thatLocator, "offerCode", rhsOfferCode), lhsOfferCode, rhsOfferCode, (this.offerCode!= null), (that.offerCode!= null))) {
                return false;
            }
        }
        {
            SellerInfo lhsSellerInfo;
            lhsSellerInfo = this.getSellerInfo();
            SellerInfo rhsSellerInfo;
            rhsSellerInfo = that.getSellerInfo();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "sellerInfo", lhsSellerInfo), LocatorUtils.property(thatLocator, "sellerInfo", rhsSellerInfo), lhsSellerInfo, rhsSellerInfo, (this.sellerInfo!= null), (that.sellerInfo!= null))) {
                return false;
            }
        }
        {
            AgreementListType lhsAgreementList;
            lhsAgreementList = this.getAgreementList();
            AgreementListType rhsAgreementList;
            rhsAgreementList = that.getAgreementList();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "agreementList", lhsAgreementList), LocatorUtils.property(thatLocator, "agreementList", rhsAgreementList), lhsAgreementList, rhsAgreementList, (this.agreementList!= null), (that.agreementList!= null))) {
                return false;
            }
        }
        {
            CertificateType lhsCertificate;
            lhsCertificate = this.getCertificate();
            CertificateType rhsCertificate;
            rhsCertificate = that.getCertificate();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "certificate", lhsCertificate), LocatorUtils.property(thatLocator, "certificate", rhsCertificate), lhsCertificate, rhsCertificate, (this.certificate!= null), (that.certificate!= null))) {
                return false;
            }
        }
        {
            ExternalOrderInput lhsExternalOrder;
            lhsExternalOrder = this.getExternalOrder();
            ExternalOrderInput rhsExternalOrder;
            rhsExternalOrder = that.getExternalOrder();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "externalOrder", lhsExternalOrder), LocatorUtils.property(thatLocator, "externalOrder", rhsExternalOrder), lhsExternalOrder, rhsExternalOrder, (this.externalOrder!= null), (that.externalOrder!= null))) {
                return false;
            }
        }
        return true;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(Object object) {
        final EqualsStrategy2 strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode(ObjectLocator locator, HashCodeStrategy2 strategy) {
        int currentHashCode = 1;
        {
            String theOverrideMultipleAccountsFlag;
            theOverrideMultipleAccountsFlag = this.getOverrideMultipleAccountsFlag();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "overrideMultipleAccountsFlag", theOverrideMultipleAccountsFlag), currentHashCode, theOverrideMultipleAccountsFlag, true);
        }
        {
            AccountType theAccountInfo;
            theAccountInfo = this.getAccountInfo();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "accountInfo", theAccountInfo), currentHashCode, theAccountInfo, (this.accountInfo!= null));
        }
        {
            ServiceProviderInfoType theServiceProviderInfo;
            theServiceProviderInfo = this.getServiceProviderInfo();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "serviceProviderInfo", theServiceProviderInfo), currentHashCode, theServiceProviderInfo, (this.serviceProviderInfo!= null));
        }
        {
            BillingGroupType theBillingGroup;
            theBillingGroup = this.getBillingGroup();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "billingGroup", theBillingGroup), currentHashCode, theBillingGroup, (this.billingGroup!= null));
        }
        {
            CustomerType theCustomer;
            theCustomer = this.getCustomer();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "customer", theCustomer), currentHashCode, theCustomer, (this.customer!= null));
        }
        {
            CustomerQualificationType theCustomerQualification;
            theCustomerQualification = this.getCustomerQualification();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "customerQualification", theCustomerQualification), currentHashCode, theCustomerQualification, (this.customerQualification!= null));
        }
        {
            PlanListType thePlanList;
            thePlanList = this.getPlanList();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "planList", thePlanList), currentHashCode, thePlanList, (this.planList!= null));
        }
        {
            AccountOrderType theAccountOrder;
            theAccountOrder = this.getAccountOrder();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "accountOrder", theAccountOrder), currentHashCode, theAccountOrder, (this.accountOrder!= null));
        }
        {
            AccountPaymentInfoType thePaymentInfo;
            thePaymentInfo = this.getPaymentInfo();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "paymentInfo", thePaymentInfo), currentHashCode, thePaymentInfo, (this.paymentInfo!= null));
        }
        {
            ReceiverConfigurationType theReceiverConfigInfo;
            theReceiverConfigInfo = this.getReceiverConfigInfo();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "receiverConfigInfo", theReceiverConfigInfo), currentHashCode, theReceiverConfigInfo, (this.receiverConfigInfo!= null));
        }
        {
            ExpectedEquipmentList theExpectedEquipmentList;
            theExpectedEquipmentList = this.getExpectedEquipmentList();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "expectedEquipmentList", theExpectedEquipmentList), currentHashCode, theExpectedEquipmentList, (this.expectedEquipmentList!= null));
        }
        {
            LocationType theLocation;
            theLocation = this.getLocation();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "location", theLocation), currentHashCode, theLocation, (this.location!= null));
        }
        {
            AccountPrivacyInfoType theSecurityInfo;
            theSecurityInfo = this.getSecurityInfo();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "securityInfo", theSecurityInfo), currentHashCode, theSecurityInfo, (this.securityInfo!= null));
        }
        {
            OfferCodeInfoType theOfferCode;
            theOfferCode = this.getOfferCode();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "offerCode", theOfferCode), currentHashCode, theOfferCode, (this.offerCode!= null));
        }
        {
            SellerInfo theSellerInfo;
            theSellerInfo = this.getSellerInfo();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "sellerInfo", theSellerInfo), currentHashCode, theSellerInfo, (this.sellerInfo!= null));
        }
        {
            AgreementListType theAgreementList;
            theAgreementList = this.getAgreementList();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "agreementList", theAgreementList), currentHashCode, theAgreementList, (this.agreementList!= null));
        }
        {
            CertificateType theCertificate;
            theCertificate = this.getCertificate();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "certificate", theCertificate), currentHashCode, theCertificate, (this.certificate!= null));
        }
        {
            ExternalOrderInput theExternalOrder;
            theExternalOrder = this.getExternalOrder();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "externalOrder", theExternalOrder), currentHashCode, theExternalOrder, (this.externalOrder!= null));
        }
        return currentHashCode;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode() {
        final HashCodeStrategy2 strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String toString() {
        final ToStringStrategy2 strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        {
            String theOverrideMultipleAccountsFlag;
            theOverrideMultipleAccountsFlag = this.getOverrideMultipleAccountsFlag();
            strategy.appendField(locator, this, "overrideMultipleAccountsFlag", buffer, theOverrideMultipleAccountsFlag, true);
        }
        {
            AccountType theAccountInfo;
            theAccountInfo = this.getAccountInfo();
            strategy.appendField(locator, this, "accountInfo", buffer, theAccountInfo, (this.accountInfo!= null));
        }
        {
            ServiceProviderInfoType theServiceProviderInfo;
            theServiceProviderInfo = this.getServiceProviderInfo();
            strategy.appendField(locator, this, "serviceProviderInfo", buffer, theServiceProviderInfo, (this.serviceProviderInfo!= null));
        }
        {
            BillingGroupType theBillingGroup;
            theBillingGroup = this.getBillingGroup();
            strategy.appendField(locator, this, "billingGroup", buffer, theBillingGroup, (this.billingGroup!= null));
        }
        {
            CustomerType theCustomer;
            theCustomer = this.getCustomer();
            strategy.appendField(locator, this, "customer", buffer, theCustomer, (this.customer!= null));
        }
        {
            CustomerQualificationType theCustomerQualification;
            theCustomerQualification = this.getCustomerQualification();
            strategy.appendField(locator, this, "customerQualification", buffer, theCustomerQualification, (this.customerQualification!= null));
        }
        {
            PlanListType thePlanList;
            thePlanList = this.getPlanList();
            strategy.appendField(locator, this, "planList", buffer, thePlanList, (this.planList!= null));
        }
        {
            AccountOrderType theAccountOrder;
            theAccountOrder = this.getAccountOrder();
            strategy.appendField(locator, this, "accountOrder", buffer, theAccountOrder, (this.accountOrder!= null));
        }
        {
            AccountPaymentInfoType thePaymentInfo;
            thePaymentInfo = this.getPaymentInfo();
            strategy.appendField(locator, this, "paymentInfo", buffer, thePaymentInfo, (this.paymentInfo!= null));
        }
        {
            ReceiverConfigurationType theReceiverConfigInfo;
            theReceiverConfigInfo = this.getReceiverConfigInfo();
            strategy.appendField(locator, this, "receiverConfigInfo", buffer, theReceiverConfigInfo, (this.receiverConfigInfo!= null));
        }
        {
            ExpectedEquipmentList theExpectedEquipmentList;
            theExpectedEquipmentList = this.getExpectedEquipmentList();
            strategy.appendField(locator, this, "expectedEquipmentList", buffer, theExpectedEquipmentList, (this.expectedEquipmentList!= null));
        }
        {
            LocationType theLocation;
            theLocation = this.getLocation();
            strategy.appendField(locator, this, "location", buffer, theLocation, (this.location!= null));
        }
        {
            AccountPrivacyInfoType theSecurityInfo;
            theSecurityInfo = this.getSecurityInfo();
            strategy.appendField(locator, this, "securityInfo", buffer, theSecurityInfo, (this.securityInfo!= null));
        }
        {
            OfferCodeInfoType theOfferCode;
            theOfferCode = this.getOfferCode();
            strategy.appendField(locator, this, "offerCode", buffer, theOfferCode, (this.offerCode!= null));
        }
        {
            SellerInfo theSellerInfo;
            theSellerInfo = this.getSellerInfo();
            strategy.appendField(locator, this, "sellerInfo", buffer, theSellerInfo, (this.sellerInfo!= null));
        }
        {
            AgreementListType theAgreementList;
            theAgreementList = this.getAgreementList();
            strategy.appendField(locator, this, "agreementList", buffer, theAgreementList, (this.agreementList!= null));
        }
        {
            CertificateType theCertificate;
            theCertificate = this.getCertificate();
            strategy.appendField(locator, this, "certificate", buffer, theCertificate, (this.certificate!= null));
        }
        {
            ExternalOrderInput theExternalOrder;
            theExternalOrder = this.getExternalOrder();
            strategy.appendField(locator, this, "externalOrder", buffer, theExternalOrder, (this.externalOrder!= null));
        }
        return buffer;
    }


}
