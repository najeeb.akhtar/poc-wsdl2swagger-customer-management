
package dishnetwork.schemas;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.Equals2;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy2;
import org.jvnet.jaxb2_commons.lang.HashCode2;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy2;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString2;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy2;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for CommAcctInfoType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="CommAcctInfoType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="miscellaneous" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="groupId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="hierarchyId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="password" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="billingArrangement" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="fullfillmentChannel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="mduServiceType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="signalDistributionType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="visibleOrbitalsInfo" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="orbitals" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CommAcctInfoTypeSubmitOrder", propOrder = {
    "miscellaneous",
    "groupId",
    "hierarchyId",
    "password",
    "billingArrangement",
    "fullfillmentChannel",
    "mduServiceType",
    "signalDistributionType",
    "visibleOrbitalsInfo"
})
@Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
public class CommAcctInfoTypeSubmitOrder implements Equals2, HashCode2, ToString2
{

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String miscellaneous;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String groupId;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String hierarchyId;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String password;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String billingArrangement;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String fullfillmentChannel;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String mduServiceType;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String signalDistributionType;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected VisibleOrbitalsInfo visibleOrbitalsInfo;

    /**
     * Default no-arg constructor
     *
     */
    public CommAcctInfoTypeSubmitOrder() {
        super();
    }

    /**
     * Fully-initialising value constructor
     *
     */
    public CommAcctInfoTypeSubmitOrder(final String miscellaneous, final String groupId, final String hierarchyId, final String password, final String billingArrangement, final String fullfillmentChannel, final String mduServiceType, final String signalDistributionType, final VisibleOrbitalsInfo visibleOrbitalsInfo) {
        this.miscellaneous = miscellaneous;
        this.groupId = groupId;
        this.hierarchyId = hierarchyId;
        this.password = password;
        this.billingArrangement = billingArrangement;
        this.fullfillmentChannel = fullfillmentChannel;
        this.mduServiceType = mduServiceType;
        this.signalDistributionType = signalDistributionType;
        this.visibleOrbitalsInfo = visibleOrbitalsInfo;
    }

    /**
     * Gets the value of the miscellaneous property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getMiscellaneous() {
        return miscellaneous;
    }

    /**
     * Sets the value of the miscellaneous property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setMiscellaneous(String value) {
        this.miscellaneous = value;
    }

    /**
     * Gets the value of the groupId property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getGroupId() {
        return groupId;
    }

    /**
     * Sets the value of the groupId property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setGroupId(String value) {
        this.groupId = value;
    }

    /**
     * Gets the value of the hierarchyId property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getHierarchyId() {
        return hierarchyId;
    }

    /**
     * Sets the value of the hierarchyId property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setHierarchyId(String value) {
        this.hierarchyId = value;
    }

    /**
     * Gets the value of the password property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getPassword() {
        return password;
    }

    /**
     * Sets the value of the password property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setPassword(String value) {
        this.password = value;
    }

    /**
     * Gets the value of the billingArrangement property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getBillingArrangement() {
        return billingArrangement;
    }

    /**
     * Sets the value of the billingArrangement property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setBillingArrangement(String value) {
        this.billingArrangement = value;
    }

    /**
     * Gets the value of the fullfillmentChannel property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getFullfillmentChannel() {
        return fullfillmentChannel;
    }

    /**
     * Sets the value of the fullfillmentChannel property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setFullfillmentChannel(String value) {
        this.fullfillmentChannel = value;
    }

    /**
     * Gets the value of the mduServiceType property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getMduServiceType() {
        return mduServiceType;
    }

    /**
     * Sets the value of the mduServiceType property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setMduServiceType(String value) {
        this.mduServiceType = value;
    }

    /**
     * Gets the value of the signalDistributionType property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getSignalDistributionType() {
        return signalDistributionType;
    }

    /**
     * Sets the value of the signalDistributionType property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setSignalDistributionType(String value) {
        this.signalDistributionType = value;
    }

    /**
     * Gets the value of the visibleOrbitalsInfo property.
     *
     * @return
     *     possible object is
     *     {@link VisibleOrbitalsInfo }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public VisibleOrbitalsInfo getVisibleOrbitalsInfo() {
        return visibleOrbitalsInfo;
    }

    /**
     * Sets the value of the visibleOrbitalsInfo property.
     *
     * @param value
     *     allowed object is
     *     {@link VisibleOrbitalsInfo }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setVisibleOrbitalsInfo(VisibleOrbitalsInfo value) {
        this.visibleOrbitalsInfo = value;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy2 strategy) {
        if ((object == null)||(this.getClass()!= object.getClass())) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final CommAcctInfoTypeSubmitOrder that = ((CommAcctInfoTypeSubmitOrder) object);
        {
            String lhsMiscellaneous;
            lhsMiscellaneous = this.getMiscellaneous();
            String rhsMiscellaneous;
            rhsMiscellaneous = that.getMiscellaneous();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "miscellaneous", lhsMiscellaneous), LocatorUtils.property(thatLocator, "miscellaneous", rhsMiscellaneous), lhsMiscellaneous, rhsMiscellaneous, (this.miscellaneous!= null), (that.miscellaneous!= null))) {
                return false;
            }
        }
        {
            String lhsGroupId;
            lhsGroupId = this.getGroupId();
            String rhsGroupId;
            rhsGroupId = that.getGroupId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "groupId", lhsGroupId), LocatorUtils.property(thatLocator, "groupId", rhsGroupId), lhsGroupId, rhsGroupId, (this.groupId!= null), (that.groupId!= null))) {
                return false;
            }
        }
        {
            String lhsHierarchyId;
            lhsHierarchyId = this.getHierarchyId();
            String rhsHierarchyId;
            rhsHierarchyId = that.getHierarchyId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "hierarchyId", lhsHierarchyId), LocatorUtils.property(thatLocator, "hierarchyId", rhsHierarchyId), lhsHierarchyId, rhsHierarchyId, (this.hierarchyId!= null), (that.hierarchyId!= null))) {
                return false;
            }
        }
        {
            String lhsPassword;
            lhsPassword = this.getPassword();
            String rhsPassword;
            rhsPassword = that.getPassword();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "password", lhsPassword), LocatorUtils.property(thatLocator, "password", rhsPassword), lhsPassword, rhsPassword, (this.password!= null), (that.password!= null))) {
                return false;
            }
        }
        {
            String lhsBillingArrangement;
            lhsBillingArrangement = this.getBillingArrangement();
            String rhsBillingArrangement;
            rhsBillingArrangement = that.getBillingArrangement();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "billingArrangement", lhsBillingArrangement), LocatorUtils.property(thatLocator, "billingArrangement", rhsBillingArrangement), lhsBillingArrangement, rhsBillingArrangement, (this.billingArrangement!= null), (that.billingArrangement!= null))) {
                return false;
            }
        }
        {
            String lhsFullfillmentChannel;
            lhsFullfillmentChannel = this.getFullfillmentChannel();
            String rhsFullfillmentChannel;
            rhsFullfillmentChannel = that.getFullfillmentChannel();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "fullfillmentChannel", lhsFullfillmentChannel), LocatorUtils.property(thatLocator, "fullfillmentChannel", rhsFullfillmentChannel), lhsFullfillmentChannel, rhsFullfillmentChannel, (this.fullfillmentChannel!= null), (that.fullfillmentChannel!= null))) {
                return false;
            }
        }
        {
            String lhsMduServiceType;
            lhsMduServiceType = this.getMduServiceType();
            String rhsMduServiceType;
            rhsMduServiceType = that.getMduServiceType();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "mduServiceType", lhsMduServiceType), LocatorUtils.property(thatLocator, "mduServiceType", rhsMduServiceType), lhsMduServiceType, rhsMduServiceType, (this.mduServiceType!= null), (that.mduServiceType!= null))) {
                return false;
            }
        }
        {
            String lhsSignalDistributionType;
            lhsSignalDistributionType = this.getSignalDistributionType();
            String rhsSignalDistributionType;
            rhsSignalDistributionType = that.getSignalDistributionType();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "signalDistributionType", lhsSignalDistributionType), LocatorUtils.property(thatLocator, "signalDistributionType", rhsSignalDistributionType), lhsSignalDistributionType, rhsSignalDistributionType, (this.signalDistributionType!= null), (that.signalDistributionType!= null))) {
                return false;
            }
        }
        {
            VisibleOrbitalsInfo lhsVisibleOrbitalsInfo;
            lhsVisibleOrbitalsInfo = this.getVisibleOrbitalsInfo();
            VisibleOrbitalsInfo rhsVisibleOrbitalsInfo;
            rhsVisibleOrbitalsInfo = that.getVisibleOrbitalsInfo();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "visibleOrbitalsInfo", lhsVisibleOrbitalsInfo), LocatorUtils.property(thatLocator, "visibleOrbitalsInfo", rhsVisibleOrbitalsInfo), lhsVisibleOrbitalsInfo, rhsVisibleOrbitalsInfo, (this.visibleOrbitalsInfo!= null), (that.visibleOrbitalsInfo!= null))) {
                return false;
            }
        }
        return true;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(Object object) {
        final EqualsStrategy2 strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode(ObjectLocator locator, HashCodeStrategy2 strategy) {
        int currentHashCode = 1;
        {
            String theMiscellaneous;
            theMiscellaneous = this.getMiscellaneous();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "miscellaneous", theMiscellaneous), currentHashCode, theMiscellaneous, (this.miscellaneous!= null));
        }
        {
            String theGroupId;
            theGroupId = this.getGroupId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "groupId", theGroupId), currentHashCode, theGroupId, (this.groupId!= null));
        }
        {
            String theHierarchyId;
            theHierarchyId = this.getHierarchyId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "hierarchyId", theHierarchyId), currentHashCode, theHierarchyId, (this.hierarchyId!= null));
        }
        {
            String thePassword;
            thePassword = this.getPassword();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "password", thePassword), currentHashCode, thePassword, (this.password!= null));
        }
        {
            String theBillingArrangement;
            theBillingArrangement = this.getBillingArrangement();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "billingArrangement", theBillingArrangement), currentHashCode, theBillingArrangement, (this.billingArrangement!= null));
        }
        {
            String theFullfillmentChannel;
            theFullfillmentChannel = this.getFullfillmentChannel();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "fullfillmentChannel", theFullfillmentChannel), currentHashCode, theFullfillmentChannel, (this.fullfillmentChannel!= null));
        }
        {
            String theMduServiceType;
            theMduServiceType = this.getMduServiceType();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "mduServiceType", theMduServiceType), currentHashCode, theMduServiceType, (this.mduServiceType!= null));
        }
        {
            String theSignalDistributionType;
            theSignalDistributionType = this.getSignalDistributionType();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "signalDistributionType", theSignalDistributionType), currentHashCode, theSignalDistributionType, (this.signalDistributionType!= null));
        }
        {
            VisibleOrbitalsInfo theVisibleOrbitalsInfo;
            theVisibleOrbitalsInfo = this.getVisibleOrbitalsInfo();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "visibleOrbitalsInfo", theVisibleOrbitalsInfo), currentHashCode, theVisibleOrbitalsInfo, (this.visibleOrbitalsInfo!= null));
        }
        return currentHashCode;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode() {
        final HashCodeStrategy2 strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String toString() {
        final ToStringStrategy2 strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        {
            String theMiscellaneous;
            theMiscellaneous = this.getMiscellaneous();
            strategy.appendField(locator, this, "miscellaneous", buffer, theMiscellaneous, (this.miscellaneous!= null));
        }
        {
            String theGroupId;
            theGroupId = this.getGroupId();
            strategy.appendField(locator, this, "groupId", buffer, theGroupId, (this.groupId!= null));
        }
        {
            String theHierarchyId;
            theHierarchyId = this.getHierarchyId();
            strategy.appendField(locator, this, "hierarchyId", buffer, theHierarchyId, (this.hierarchyId!= null));
        }
        {
            String thePassword;
            thePassword = this.getPassword();
            strategy.appendField(locator, this, "password", buffer, thePassword, (this.password!= null));
        }
        {
            String theBillingArrangement;
            theBillingArrangement = this.getBillingArrangement();
            strategy.appendField(locator, this, "billingArrangement", buffer, theBillingArrangement, (this.billingArrangement!= null));
        }
        {
            String theFullfillmentChannel;
            theFullfillmentChannel = this.getFullfillmentChannel();
            strategy.appendField(locator, this, "fullfillmentChannel", buffer, theFullfillmentChannel, (this.fullfillmentChannel!= null));
        }
        {
            String theMduServiceType;
            theMduServiceType = this.getMduServiceType();
            strategy.appendField(locator, this, "mduServiceType", buffer, theMduServiceType, (this.mduServiceType!= null));
        }
        {
            String theSignalDistributionType;
            theSignalDistributionType = this.getSignalDistributionType();
            strategy.appendField(locator, this, "signalDistributionType", buffer, theSignalDistributionType, (this.signalDistributionType!= null));
        }
        {
            VisibleOrbitalsInfo theVisibleOrbitalsInfo;
            theVisibleOrbitalsInfo = this.getVisibleOrbitalsInfo();
            strategy.appendField(locator, this, "visibleOrbitalsInfo", buffer, theVisibleOrbitalsInfo, (this.visibleOrbitalsInfo!= null));
        }
        return buffer;
    }


}
