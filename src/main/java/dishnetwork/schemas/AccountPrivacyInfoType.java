
package dishnetwork.schemas;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

import org.jvnet.jaxb2_commons.lang.Equals2;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy2;
import org.jvnet.jaxb2_commons.lang.HashCode2;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy2;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString2;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy2;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 *  Cantains the account authorization information.
 *
 * <p>Java class for AccountPrivacyInfoType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="AccountPrivacyInfoType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="pin" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="pinHint" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="authorizedUserList" type="{http://www.dishnetwork.com/schema/CustomerManagement/AuthorizedUserList/2011_04_01}AuthorizedUserListType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AccountPrivacyInfoType", propOrder = {
    "pin",
    "pinHint",
    "authorizedUserList"
})
@Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
public class AccountPrivacyInfoType implements Equals2, HashCode2, ToString2
{

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String pin;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String pinHint;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected AuthorizedUserListType authorizedUserList;

    /**
     * Default no-arg constructor
     *
     */
    public AccountPrivacyInfoType() {
        super();
    }

    /**
     * Fully-initialising value constructor
     *
     */
    public AccountPrivacyInfoType(final String pin, final String pinHint, final AuthorizedUserListType authorizedUserList) {
        this.pin = pin;
        this.pinHint = pinHint;
        this.authorizedUserList = authorizedUserList;
    }

    /**
     * Gets the value of the pin property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getPin() {
        return pin;
    }

    /**
     * Sets the value of the pin property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setPin(String value) {
        this.pin = value;
    }

    /**
     * Gets the value of the pinHint property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getPinHint() {
        return pinHint;
    }

    /**
     * Sets the value of the pinHint property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setPinHint(String value) {
        this.pinHint = value;
    }

    /**
     * Gets the value of the authorizedUserList property.
     *
     * @return
     *     possible object is
     *     {@link AuthorizedUserListType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public AuthorizedUserListType getAuthorizedUserList() {
        return authorizedUserList;
    }

    /**
     * Sets the value of the authorizedUserList property.
     *
     * @param value
     *     allowed object is
     *     {@link AuthorizedUserListType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setAuthorizedUserList(AuthorizedUserListType value) {
        this.authorizedUserList = value;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy2 strategy) {
        if ((object == null)||(this.getClass()!= object.getClass())) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final AccountPrivacyInfoType that = ((AccountPrivacyInfoType) object);
        {
            String lhsPin;
            lhsPin = this.getPin();
            String rhsPin;
            rhsPin = that.getPin();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "pin", lhsPin), LocatorUtils.property(thatLocator, "pin", rhsPin), lhsPin, rhsPin, (this.pin!= null), (that.pin!= null))) {
                return false;
            }
        }
        {
            String lhsPinHint;
            lhsPinHint = this.getPinHint();
            String rhsPinHint;
            rhsPinHint = that.getPinHint();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "pinHint", lhsPinHint), LocatorUtils.property(thatLocator, "pinHint", rhsPinHint), lhsPinHint, rhsPinHint, (this.pinHint!= null), (that.pinHint!= null))) {
                return false;
            }
        }
        {
            AuthorizedUserListType lhsAuthorizedUserList;
            lhsAuthorizedUserList = this.getAuthorizedUserList();
            AuthorizedUserListType rhsAuthorizedUserList;
            rhsAuthorizedUserList = that.getAuthorizedUserList();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "authorizedUserList", lhsAuthorizedUserList), LocatorUtils.property(thatLocator, "authorizedUserList", rhsAuthorizedUserList), lhsAuthorizedUserList, rhsAuthorizedUserList, (this.authorizedUserList!= null), (that.authorizedUserList!= null))) {
                return false;
            }
        }
        return true;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(Object object) {
        final EqualsStrategy2 strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode(ObjectLocator locator, HashCodeStrategy2 strategy) {
        int currentHashCode = 1;
        {
            String thePin;
            thePin = this.getPin();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "pin", thePin), currentHashCode, thePin, (this.pin!= null));
        }
        {
            String thePinHint;
            thePinHint = this.getPinHint();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "pinHint", thePinHint), currentHashCode, thePinHint, (this.pinHint!= null));
        }
        {
            AuthorizedUserListType theAuthorizedUserList;
            theAuthorizedUserList = this.getAuthorizedUserList();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "authorizedUserList", theAuthorizedUserList), currentHashCode, theAuthorizedUserList, (this.authorizedUserList!= null));
        }
        return currentHashCode;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode() {
        final HashCodeStrategy2 strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String toString() {
        final ToStringStrategy2 strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        {
            String thePin;
            thePin = this.getPin();
            strategy.appendField(locator, this, "pin", buffer, thePin, (this.pin!= null));
        }
        {
            String thePinHint;
            thePinHint = this.getPinHint();
            strategy.appendField(locator, this, "pinHint", buffer, thePinHint, (this.pinHint!= null));
        }
        {
            AuthorizedUserListType theAuthorizedUserList;
            theAuthorizedUserList = this.getAuthorizedUserList();
            strategy.appendField(locator, this, "authorizedUserList", buffer, theAuthorizedUserList, (this.authorizedUserList!= null));
        }
        return buffer;
    }

}
