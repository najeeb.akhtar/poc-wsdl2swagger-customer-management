
package dishnetwork.schemas;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.jvnet.jaxb2_commons.lang.Equals2;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy2;
import org.jvnet.jaxb2_commons.lang.HashCode2;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy2;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString2;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy2;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * ServiceProviderInfo type
 *
 * <p>Java class for ServiceProviderInfoType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="ServiceProviderInfoType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="hybridFlag" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="providerList" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="provider" type="{http://www.dishnetwork.com/schema/common/Provider/2012_08_01}ProviderType" maxOccurs="unbounded"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceProviderInfoType", propOrder = {
    "hybridFlag",
    "providerList"
})
@Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
public class ServiceProviderInfoType implements Equals2, HashCode2, ToString2
{

    //@XmlElement(defaultValue = "false")
    @JsonProperty(defaultValue = "false")
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String hybridFlag;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected ProviderList providerList;

    /**
     * Default no-arg constructor
     *
     */
    public ServiceProviderInfoType() {
        super();
    }

    /**
     * Fully-initialising value constructor
     *
     */
    public ServiceProviderInfoType(final String hybridFlag, final ProviderList providerList) {
        this.hybridFlag = hybridFlag;
        this.providerList = providerList;
    }

    /**
     * Gets the value of the hybridFlag property.
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getHybridFlag() {
        return hybridFlag;
    }

    /**
     * Sets the value of the hybridFlag property.
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setHybridFlag(String value) {
        this.hybridFlag = value;
    }

    /**
     * Gets the value of the providerList property.
     *
     * @return
     *     possible object is
     *     {@link ProviderList }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public ProviderList getProviderList() {
        return providerList;
    }

    /**
     * Sets the value of the providerList property.
     *
     * @param value
     *     allowed object is
     *     {@link ProviderList }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setProviderList(ProviderList value) {
        this.providerList = value;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy2 strategy) {
        if ((object == null)||(this.getClass()!= object.getClass())) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final ServiceProviderInfoType that = ((ServiceProviderInfoType) object);
        {
            String lhsHybridFlag;
            lhsHybridFlag = this.getHybridFlag();
            String rhsHybridFlag;
            rhsHybridFlag = that.getHybridFlag();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "hybridFlag", lhsHybridFlag), LocatorUtils.property(thatLocator, "hybridFlag", rhsHybridFlag), lhsHybridFlag, rhsHybridFlag, true, true)) {
                return false;
            }
        }
        {
            ProviderList lhsProviderList;
            lhsProviderList = this.getProviderList();
            ProviderList rhsProviderList;
            rhsProviderList = that.getProviderList();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "providerList", lhsProviderList), LocatorUtils.property(thatLocator, "providerList", rhsProviderList), lhsProviderList, rhsProviderList, (this.providerList!= null), (that.providerList!= null))) {
                return false;
            }
        }
        return true;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(Object object) {
        final EqualsStrategy2 strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode(ObjectLocator locator, HashCodeStrategy2 strategy) {
        int currentHashCode = 1;
        {
            String theHybridFlag;
            theHybridFlag = this.getHybridFlag();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "hybridFlag", theHybridFlag), currentHashCode, theHybridFlag, true);
        }
        {
            ProviderList theProviderList;
            theProviderList = this.getProviderList();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "providerList", theProviderList), currentHashCode, theProviderList, (this.providerList!= null));
        }
        return currentHashCode;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode() {
        final HashCodeStrategy2 strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String toString() {
        final ToStringStrategy2 strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        {
            String theHybridFlag;
            theHybridFlag = this.getHybridFlag();
            strategy.appendField(locator, this, "hybridFlag", buffer, theHybridFlag, true);
        }
        {
            ProviderList theProviderList;
            theProviderList = this.getProviderList();
            strategy.appendField(locator, this, "providerList", buffer, theProviderList, (this.providerList!= null));
        }
        return buffer;
    }


}
