
package dishnetwork.schemas;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

import org.jvnet.jaxb2_commons.lang.Equals2;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy2;
import org.jvnet.jaxb2_commons.lang.HashCode2;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy2;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString2;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy2;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * Contains the Signal OrbitalInfo.
 *
 * <p>Java class for SignalOrbitalInfoType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="SignalOrbitalInfoType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="signalType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="orbitalInfoList" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="orbitalInfo" type="{http://www.dishnetwork.com/schema/CustomerManagement/OrbitalInfo/2013_01_17}OrbitalInfoType" maxOccurs="unbounded"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SignalOrbitalInfoType", propOrder = {
    "signalType",
    "orbitalInfoList"
})
@Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
public class SignalOrbitalInfoType implements Equals2, HashCode2, ToString2
{

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String signalType;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected OrbitalInfoList orbitalInfoList;

    /**
     * Default no-arg constructor
     *
     */
    public SignalOrbitalInfoType() {
        super();
    }

    /**
     * Fully-initialising value constructor
     *
     */
    public SignalOrbitalInfoType(final String signalType, final OrbitalInfoList orbitalInfoList) {
        this.signalType = signalType;
        this.orbitalInfoList = orbitalInfoList;
    }

    /**
     * Gets the value of the signalType property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getSignalType() {
        return signalType;
    }

    /**
     * Sets the value of the signalType property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setSignalType(String value) {
        this.signalType = value;
    }

    /**
     * Gets the value of the orbitalInfoList property.
     *
     * @return
     *     possible object is
     *     {@link OrbitalInfoList }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public OrbitalInfoList getOrbitalInfoList() {
        return orbitalInfoList;
    }

    /**
     * Sets the value of the orbitalInfoList property.
     *
     * @param value
     *     allowed object is
     *     {@link OrbitalInfoList }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setOrbitalInfoList(OrbitalInfoList value) {
        this.orbitalInfoList = value;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy2 strategy) {
        if ((object == null)||(this.getClass()!= object.getClass())) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final SignalOrbitalInfoType that = ((SignalOrbitalInfoType) object);
        {
            String lhsSignalType;
            lhsSignalType = this.getSignalType();
            String rhsSignalType;
            rhsSignalType = that.getSignalType();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "signalType", lhsSignalType), LocatorUtils.property(thatLocator, "signalType", rhsSignalType), lhsSignalType, rhsSignalType, (this.signalType!= null), (that.signalType!= null))) {
                return false;
            }
        }
        {
            OrbitalInfoList lhsOrbitalInfoList;
            lhsOrbitalInfoList = this.getOrbitalInfoList();
            OrbitalInfoList rhsOrbitalInfoList;
            rhsOrbitalInfoList = that.getOrbitalInfoList();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "orbitalInfoList", lhsOrbitalInfoList), LocatorUtils.property(thatLocator, "orbitalInfoList", rhsOrbitalInfoList), lhsOrbitalInfoList, rhsOrbitalInfoList, (this.orbitalInfoList!= null), (that.orbitalInfoList!= null))) {
                return false;
            }
        }
        return true;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(Object object) {
        final EqualsStrategy2 strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode(ObjectLocator locator, HashCodeStrategy2 strategy) {
        int currentHashCode = 1;
        {
            String theSignalType;
            theSignalType = this.getSignalType();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "signalType", theSignalType), currentHashCode, theSignalType, (this.signalType!= null));
        }
        {
            OrbitalInfoList theOrbitalInfoList;
            theOrbitalInfoList = this.getOrbitalInfoList();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "orbitalInfoList", theOrbitalInfoList), currentHashCode, theOrbitalInfoList, (this.orbitalInfoList!= null));
        }
        return currentHashCode;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode() {
        final HashCodeStrategy2 strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String toString() {
        final ToStringStrategy2 strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        {
            String theSignalType;
            theSignalType = this.getSignalType();
            strategy.appendField(locator, this, "signalType", buffer, theSignalType, (this.signalType!= null));
        }
        {
            OrbitalInfoList theOrbitalInfoList;
            theOrbitalInfoList = this.getOrbitalInfoList();
            strategy.appendField(locator, this, "orbitalInfoList", buffer, theOrbitalInfoList, (this.orbitalInfoList!= null));
        }
        return buffer;
    }


}
