package dishnetwork.schemas;

import org.jvnet.jaxb2_commons.lang.*;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;

import javax.annotation.Generated;
import javax.xml.bind.annotation.*;

/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="customerQualificationType" type="{http://www.dishnetwork.com/schema/CustomerManagement/CustomerQualification/2015_01_15}CustomerQualificationIdentifierType" minOccurs="0"/&gt;
 *         &lt;element name="customerQualificationID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "customerQualificationType",
    "customerQualificationID"
})
@Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
public class CustomerQualificationIdentifier implements Equals2, HashCode2, ToString2 {

    //@XmlElement(nillable = true)
    @XmlSchemaType(name = "string")
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected CustomerQualificationIdentifierType customerQualificationType;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String customerQualificationID;

    /**
     * Default no-arg constructor
     */
    public CustomerQualificationIdentifier() {
        super();
    }

    /**
     * Fully-initialising value constructor
     */
    public CustomerQualificationIdentifier(final CustomerQualificationIdentifierType customerQualificationType, final String customerQualificationID) {
        this.customerQualificationType = customerQualificationType;
        this.customerQualificationID = customerQualificationID;
    }

    /**
     * Gets the value of the customerQualificationType property.
     *
     * @return possible object is
     * {@link CustomerQualificationIdentifierType }
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public CustomerQualificationIdentifierType getCustomerQualificationType() {
        return customerQualificationType;
    }

    /**
     * Sets the value of the customerQualificationType property.
     *
     * @param value allowed object is
     *              {@link CustomerQualificationIdentifierType }
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setCustomerQualificationType(CustomerQualificationIdentifierType value) {
        this.customerQualificationType = value;
    }

    /**
     * Gets the value of the customerQualificationID property.
     *
     * @return possible object is
     * {@link String }
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getCustomerQualificationID() {
        return customerQualificationID;
    }

    /**
     * Sets the value of the customerQualificationID property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setCustomerQualificationID(String value) {
        this.customerQualificationID = value;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy2 strategy) {
        if ((object == null) || (this.getClass() != object.getClass())) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final CustomerQualificationIdentifier that = ((CustomerQualificationIdentifier) object);
        {
            CustomerQualificationIdentifierType lhsCustomerQualificationType;
            lhsCustomerQualificationType = this.getCustomerQualificationType();
            CustomerQualificationIdentifierType rhsCustomerQualificationType;
            rhsCustomerQualificationType = that.getCustomerQualificationType();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "customerQualificationType", lhsCustomerQualificationType), LocatorUtils.property(thatLocator, "customerQualificationType", rhsCustomerQualificationType), lhsCustomerQualificationType, rhsCustomerQualificationType, (this.customerQualificationType != null), (that.customerQualificationType != null))) {
                return false;
            }
        }
        {
            String lhsCustomerQualificationID;
            lhsCustomerQualificationID = this.getCustomerQualificationID();
            String rhsCustomerQualificationID;
            rhsCustomerQualificationID = that.getCustomerQualificationID();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "customerQualificationID", lhsCustomerQualificationID), LocatorUtils.property(thatLocator, "customerQualificationID", rhsCustomerQualificationID), lhsCustomerQualificationID, rhsCustomerQualificationID, (this.customerQualificationID != null), (that.customerQualificationID != null))) {
                return false;
            }
        }
        return true;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(Object object) {
        final EqualsStrategy2 strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode(ObjectLocator locator, HashCodeStrategy2 strategy) {
        int currentHashCode = 1;
        {
            CustomerQualificationIdentifierType theCustomerQualificationType;
            theCustomerQualificationType = this.getCustomerQualificationType();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "customerQualificationType", theCustomerQualificationType), currentHashCode, theCustomerQualificationType, (this.customerQualificationType != null));
        }
        {
            String theCustomerQualificationID;
            theCustomerQualificationID = this.getCustomerQualificationID();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "customerQualificationID", theCustomerQualificationID), currentHashCode, theCustomerQualificationID, (this.customerQualificationID != null));
        }
        return currentHashCode;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode() {
        final HashCodeStrategy2 strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String toString() {
        final ToStringStrategy2 strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        {
            CustomerQualificationIdentifierType theCustomerQualificationType;
            theCustomerQualificationType = this.getCustomerQualificationType();
            strategy.appendField(locator, this, "customerQualificationType", buffer, theCustomerQualificationType, (this.customerQualificationType != null));
        }
        {
            String theCustomerQualificationID;
            theCustomerQualificationID = this.getCustomerQualificationID();
            strategy.appendField(locator, this, "customerQualificationID", buffer, theCustomerQualificationID, (this.customerQualificationID != null));
        }
        return buffer;
    }

}
