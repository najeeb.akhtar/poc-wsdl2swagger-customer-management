
package dishnetwork.schemas;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;

import io.swagger.annotations.ApiModelProperty;
import org.jvnet.jaxb2_commons.lang.Equals2;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy2;
import org.jvnet.jaxb2_commons.lang.HashCode2;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy2;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString2;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy2;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 *  Contains the schedule information.
 *
 * <p>Java class for ScheduleType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="ScheduleType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="date" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="rescheduleReason" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="serviceType" type="{http://www.dishnetwork.com/schema/ServiceType/ServiceType/2015_01_15}ServiceTypeType" minOccurs="0"/&gt;
 *         &lt;element name="timeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="totalUnit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="category" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ScheduleType", propOrder = {
    "date",
    "rescheduleReason",
    "serviceType",
    "timeCode",
    "totalUnit",
    "category"
})
@Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
public class ScheduleType implements Equals2, HashCode2, ToString2
{

    @XmlSchemaType(name = "date")
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    @ApiModelProperty(example = "2022-07-12T21:32:55.842Z")
    protected String date;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String rescheduleReason;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected ServiceTypeType serviceType;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String timeCode;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String totalUnit;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String category;

    /**
     * Default no-arg constructor
     *
     */
    public ScheduleType() {
        super();
    }

    /**
     * Fully-initialising value constructor
     *
     */
    public ScheduleType(final String date, final String rescheduleReason, final ServiceTypeType serviceType, final String timeCode, final String totalUnit, final String category) {
        this.date = date;
        this.rescheduleReason = rescheduleReason;
        this.serviceType = serviceType;
        this.timeCode = timeCode;
        this.totalUnit = totalUnit;
        this.category = category;
    }

    /**
     * Gets the value of the date property.
     *
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getDate() {
        return date;
    }

    /**
     * Sets the value of the date property.
     *
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setDate(String value) {
        this.date = value;
    }

    /**
     * Gets the value of the rescheduleReason property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getRescheduleReason() {
        return rescheduleReason;
    }

    /**
     * Sets the value of the rescheduleReason property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setRescheduleReason(String value) {
        this.rescheduleReason = value;
    }

    /**
     * Gets the value of the serviceType property.
     *
     * @return
     *     possible object is
     *     {@link ServiceTypeType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public ServiceTypeType getServiceType() {
        return serviceType;
    }

    /**
     * Sets the value of the serviceType property.
     *
     * @param value
     *     allowed object is
     *     {@link ServiceTypeType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setServiceType(ServiceTypeType value) {
        this.serviceType = value;
    }

    /**
     * Gets the value of the timeCode property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getTimeCode() {
        return timeCode;
    }

    /**
     * Sets the value of the timeCode property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setTimeCode(String value) {
        this.timeCode = value;
    }

    /**
     * Gets the value of the totalUnit property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getTotalUnit() {
        return totalUnit;
    }

    /**
     * Sets the value of the totalUnit property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setTotalUnit(String value) {
        this.totalUnit = value;
    }

    /**
     * Gets the value of the category property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getCategory() {
        return category;
    }

    /**
     * Sets the value of the category property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setCategory(String value) {
        this.category = value;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy2 strategy) {
        if ((object == null)||(this.getClass()!= object.getClass())) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final ScheduleType that = ((ScheduleType) object);
        {
            String lhsDate;
            lhsDate = this.getDate();
            String rhsDate;
            rhsDate = that.getDate();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "date", lhsDate), LocatorUtils.property(thatLocator, "date", rhsDate), lhsDate, rhsDate, (this.date!= null), (that.date!= null))) {
                return false;
            }
        }
        {
            String lhsRescheduleReason;
            lhsRescheduleReason = this.getRescheduleReason();
            String rhsRescheduleReason;
            rhsRescheduleReason = that.getRescheduleReason();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "rescheduleReason", lhsRescheduleReason), LocatorUtils.property(thatLocator, "rescheduleReason", rhsRescheduleReason), lhsRescheduleReason, rhsRescheduleReason, (this.rescheduleReason!= null), (that.rescheduleReason!= null))) {
                return false;
            }
        }
        {
            ServiceTypeType lhsServiceType;
            lhsServiceType = this.getServiceType();
            ServiceTypeType rhsServiceType;
            rhsServiceType = that.getServiceType();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "serviceType", lhsServiceType), LocatorUtils.property(thatLocator, "serviceType", rhsServiceType), lhsServiceType, rhsServiceType, (this.serviceType!= null), (that.serviceType!= null))) {
                return false;
            }
        }
        {
            String lhsTimeCode;
            lhsTimeCode = this.getTimeCode();
            String rhsTimeCode;
            rhsTimeCode = that.getTimeCode();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "timeCode", lhsTimeCode), LocatorUtils.property(thatLocator, "timeCode", rhsTimeCode), lhsTimeCode, rhsTimeCode, (this.timeCode!= null), (that.timeCode!= null))) {
                return false;
            }
        }
        {
            String lhsTotalUnit;
            lhsTotalUnit = this.getTotalUnit();
            String rhsTotalUnit;
            rhsTotalUnit = that.getTotalUnit();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "totalUnit", lhsTotalUnit), LocatorUtils.property(thatLocator, "totalUnit", rhsTotalUnit), lhsTotalUnit, rhsTotalUnit, (this.totalUnit!= null), (that.totalUnit!= null))) {
                return false;
            }
        }
        {
            String lhsCategory;
            lhsCategory = this.getCategory();
            String rhsCategory;
            rhsCategory = that.getCategory();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "category", lhsCategory), LocatorUtils.property(thatLocator, "category", rhsCategory), lhsCategory, rhsCategory, (this.category!= null), (that.category!= null))) {
                return false;
            }
        }
        return true;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(Object object) {
        final EqualsStrategy2 strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode(ObjectLocator locator, HashCodeStrategy2 strategy) {
        int currentHashCode = 1;
        {
            String theDate;
            theDate = this.getDate();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "date", theDate), currentHashCode, theDate, (this.date!= null));
        }
        {
            String theRescheduleReason;
            theRescheduleReason = this.getRescheduleReason();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "rescheduleReason", theRescheduleReason), currentHashCode, theRescheduleReason, (this.rescheduleReason!= null));
        }
        {
            ServiceTypeType theServiceType;
            theServiceType = this.getServiceType();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "serviceType", theServiceType), currentHashCode, theServiceType, (this.serviceType!= null));
        }
        {
            String theTimeCode;
            theTimeCode = this.getTimeCode();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "timeCode", theTimeCode), currentHashCode, theTimeCode, (this.timeCode!= null));
        }
        {
            String theTotalUnit;
            theTotalUnit = this.getTotalUnit();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "totalUnit", theTotalUnit), currentHashCode, theTotalUnit, (this.totalUnit!= null));
        }
        {
            String theCategory;
            theCategory = this.getCategory();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "category", theCategory), currentHashCode, theCategory, (this.category!= null));
        }
        return currentHashCode;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode() {
        final HashCodeStrategy2 strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String toString() {
        final ToStringStrategy2 strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        {
            String theDate;
            theDate = this.getDate();
            strategy.appendField(locator, this, "date", buffer, theDate, (this.date!= null));
        }
        {
            String theRescheduleReason;
            theRescheduleReason = this.getRescheduleReason();
            strategy.appendField(locator, this, "rescheduleReason", buffer, theRescheduleReason, (this.rescheduleReason!= null));
        }
        {
            ServiceTypeType theServiceType;
            theServiceType = this.getServiceType();
            strategy.appendField(locator, this, "serviceType", buffer, theServiceType, (this.serviceType!= null));
        }
        {
            String theTimeCode;
            theTimeCode = this.getTimeCode();
            strategy.appendField(locator, this, "timeCode", buffer, theTimeCode, (this.timeCode!= null));
        }
        {
            String theTotalUnit;
            theTotalUnit = this.getTotalUnit();
            strategy.appendField(locator, this, "totalUnit", buffer, theTotalUnit, (this.totalUnit!= null));
        }
        {
            String theCategory;
            theCategory = this.getCategory();
            strategy.appendField(locator, this, "category", buffer, theCategory, (this.category!= null));
        }
        return buffer;
    }

}
