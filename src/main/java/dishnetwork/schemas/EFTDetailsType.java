
package dishnetwork.schemas;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.jvnet.jaxb2_commons.lang.Equals2;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy2;
import org.jvnet.jaxb2_commons.lang.HashCode2;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy2;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString2;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy2;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for EFTDetailsType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="EFTDetailsType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="nameOnAccount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="financialInstituteAccountId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="financialInstituteAccountRoutingNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="financialInstituteAccountType"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;enumeration value="CHECKING"/&gt;
 *               &lt;enumeration value="SAVINGS"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="financialInstituteCheckNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EFTDetailsType", propOrder = {
    "nameOnAccount",
    "financialInstituteAccountId",
    "financialInstituteAccountRoutingNumber",
    "financialInstituteAccountType",
    "financialInstituteCheckNumber"
})
@Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
public class EFTDetailsType implements Equals2, HashCode2, ToString2
{

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String nameOnAccount;
    //@XmlElement(required = true)
    @JsonProperty(required = true)
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String financialInstituteAccountId;
    //@XmlElement(required = true)
    @JsonProperty(required = true)
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String financialInstituteAccountRoutingNumber;
    //@XmlElement(required = true)
    @JsonProperty(required = true)
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String financialInstituteAccountType;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String financialInstituteCheckNumber;

    /**
     * Default no-arg constructor
     *
     */
    public EFTDetailsType() {
        super();
    }

    /**
     * Fully-initialising value constructor
     *
     */
    public EFTDetailsType(final String nameOnAccount, final String financialInstituteAccountId, final String financialInstituteAccountRoutingNumber, final String financialInstituteAccountType, final String financialInstituteCheckNumber) {
        this.nameOnAccount = nameOnAccount;
        this.financialInstituteAccountId = financialInstituteAccountId;
        this.financialInstituteAccountRoutingNumber = financialInstituteAccountRoutingNumber;
        this.financialInstituteAccountType = financialInstituteAccountType;
        this.financialInstituteCheckNumber = financialInstituteCheckNumber;
    }

    /**
     * Gets the value of the nameOnAccount property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getNameOnAccount() {
        return nameOnAccount;
    }

    /**
     * Sets the value of the nameOnAccount property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setNameOnAccount(String value) {
        this.nameOnAccount = value;
    }

    /**
     * Gets the value of the financialInstituteAccountId property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getFinancialInstituteAccountId() {
        return financialInstituteAccountId;
    }

    /**
     * Sets the value of the financialInstituteAccountId property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setFinancialInstituteAccountId(String value) {
        this.financialInstituteAccountId = value;
    }

    /**
     * Gets the value of the financialInstituteAccountRoutingNumber property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getFinancialInstituteAccountRoutingNumber() {
        return financialInstituteAccountRoutingNumber;
    }

    /**
     * Sets the value of the financialInstituteAccountRoutingNumber property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setFinancialInstituteAccountRoutingNumber(String value) {
        this.financialInstituteAccountRoutingNumber = value;
    }

    /**
     * Gets the value of the financialInstituteAccountType property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getFinancialInstituteAccountType() {
        return financialInstituteAccountType;
    }

    /**
     * Sets the value of the financialInstituteAccountType property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setFinancialInstituteAccountType(String value) {
        this.financialInstituteAccountType = value;
    }

    /**
     * Gets the value of the financialInstituteCheckNumber property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getFinancialInstituteCheckNumber() {
        return financialInstituteCheckNumber;
    }

    /**
     * Sets the value of the financialInstituteCheckNumber property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setFinancialInstituteCheckNumber(String value) {
        this.financialInstituteCheckNumber = value;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy2 strategy) {
        if ((object == null)||(this.getClass()!= object.getClass())) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final EFTDetailsType that = ((EFTDetailsType) object);
        {
            String lhsNameOnAccount;
            lhsNameOnAccount = this.getNameOnAccount();
            String rhsNameOnAccount;
            rhsNameOnAccount = that.getNameOnAccount();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "nameOnAccount", lhsNameOnAccount), LocatorUtils.property(thatLocator, "nameOnAccount", rhsNameOnAccount), lhsNameOnAccount, rhsNameOnAccount, (this.nameOnAccount!= null), (that.nameOnAccount!= null))) {
                return false;
            }
        }
        {
            String lhsFinancialInstituteAccountId;
            lhsFinancialInstituteAccountId = this.getFinancialInstituteAccountId();
            String rhsFinancialInstituteAccountId;
            rhsFinancialInstituteAccountId = that.getFinancialInstituteAccountId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "financialInstituteAccountId", lhsFinancialInstituteAccountId), LocatorUtils.property(thatLocator, "financialInstituteAccountId", rhsFinancialInstituteAccountId), lhsFinancialInstituteAccountId, rhsFinancialInstituteAccountId, (this.financialInstituteAccountId!= null), (that.financialInstituteAccountId!= null))) {
                return false;
            }
        }
        {
            String lhsFinancialInstituteAccountRoutingNumber;
            lhsFinancialInstituteAccountRoutingNumber = this.getFinancialInstituteAccountRoutingNumber();
            String rhsFinancialInstituteAccountRoutingNumber;
            rhsFinancialInstituteAccountRoutingNumber = that.getFinancialInstituteAccountRoutingNumber();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "financialInstituteAccountRoutingNumber", lhsFinancialInstituteAccountRoutingNumber), LocatorUtils.property(thatLocator, "financialInstituteAccountRoutingNumber", rhsFinancialInstituteAccountRoutingNumber), lhsFinancialInstituteAccountRoutingNumber, rhsFinancialInstituteAccountRoutingNumber, (this.financialInstituteAccountRoutingNumber!= null), (that.financialInstituteAccountRoutingNumber!= null))) {
                return false;
            }
        }
        {
            String lhsFinancialInstituteAccountType;
            lhsFinancialInstituteAccountType = this.getFinancialInstituteAccountType();
            String rhsFinancialInstituteAccountType;
            rhsFinancialInstituteAccountType = that.getFinancialInstituteAccountType();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "financialInstituteAccountType", lhsFinancialInstituteAccountType), LocatorUtils.property(thatLocator, "financialInstituteAccountType", rhsFinancialInstituteAccountType), lhsFinancialInstituteAccountType, rhsFinancialInstituteAccountType, (this.financialInstituteAccountType!= null), (that.financialInstituteAccountType!= null))) {
                return false;
            }
        }
        {
            String lhsFinancialInstituteCheckNumber;
            lhsFinancialInstituteCheckNumber = this.getFinancialInstituteCheckNumber();
            String rhsFinancialInstituteCheckNumber;
            rhsFinancialInstituteCheckNumber = that.getFinancialInstituteCheckNumber();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "financialInstituteCheckNumber", lhsFinancialInstituteCheckNumber), LocatorUtils.property(thatLocator, "financialInstituteCheckNumber", rhsFinancialInstituteCheckNumber), lhsFinancialInstituteCheckNumber, rhsFinancialInstituteCheckNumber, (this.financialInstituteCheckNumber!= null), (that.financialInstituteCheckNumber!= null))) {
                return false;
            }
        }
        return true;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(Object object) {
        final EqualsStrategy2 strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode(ObjectLocator locator, HashCodeStrategy2 strategy) {
        int currentHashCode = 1;
        {
            String theNameOnAccount;
            theNameOnAccount = this.getNameOnAccount();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "nameOnAccount", theNameOnAccount), currentHashCode, theNameOnAccount, (this.nameOnAccount!= null));
        }
        {
            String theFinancialInstituteAccountId;
            theFinancialInstituteAccountId = this.getFinancialInstituteAccountId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "financialInstituteAccountId", theFinancialInstituteAccountId), currentHashCode, theFinancialInstituteAccountId, (this.financialInstituteAccountId!= null));
        }
        {
            String theFinancialInstituteAccountRoutingNumber;
            theFinancialInstituteAccountRoutingNumber = this.getFinancialInstituteAccountRoutingNumber();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "financialInstituteAccountRoutingNumber", theFinancialInstituteAccountRoutingNumber), currentHashCode, theFinancialInstituteAccountRoutingNumber, (this.financialInstituteAccountRoutingNumber!= null));
        }
        {
            String theFinancialInstituteAccountType;
            theFinancialInstituteAccountType = this.getFinancialInstituteAccountType();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "financialInstituteAccountType", theFinancialInstituteAccountType), currentHashCode, theFinancialInstituteAccountType, (this.financialInstituteAccountType!= null));
        }
        {
            String theFinancialInstituteCheckNumber;
            theFinancialInstituteCheckNumber = this.getFinancialInstituteCheckNumber();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "financialInstituteCheckNumber", theFinancialInstituteCheckNumber), currentHashCode, theFinancialInstituteCheckNumber, (this.financialInstituteCheckNumber!= null));
        }
        return currentHashCode;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode() {
        final HashCodeStrategy2 strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String toString() {
        final ToStringStrategy2 strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        {
            String theNameOnAccount;
            theNameOnAccount = this.getNameOnAccount();
            strategy.appendField(locator, this, "nameOnAccount", buffer, theNameOnAccount, (this.nameOnAccount!= null));
        }
        {
            String theFinancialInstituteAccountId;
            theFinancialInstituteAccountId = this.getFinancialInstituteAccountId();
            strategy.appendField(locator, this, "financialInstituteAccountId", buffer, theFinancialInstituteAccountId, (this.financialInstituteAccountId!= null));
        }
        {
            String theFinancialInstituteAccountRoutingNumber;
            theFinancialInstituteAccountRoutingNumber = this.getFinancialInstituteAccountRoutingNumber();
            strategy.appendField(locator, this, "financialInstituteAccountRoutingNumber", buffer, theFinancialInstituteAccountRoutingNumber, (this.financialInstituteAccountRoutingNumber!= null));
        }
        {
            String theFinancialInstituteAccountType;
            theFinancialInstituteAccountType = this.getFinancialInstituteAccountType();
            strategy.appendField(locator, this, "financialInstituteAccountType", buffer, theFinancialInstituteAccountType, (this.financialInstituteAccountType!= null));
        }
        {
            String theFinancialInstituteCheckNumber;
            theFinancialInstituteCheckNumber = this.getFinancialInstituteCheckNumber();
            strategy.appendField(locator, this, "financialInstituteCheckNumber", buffer, theFinancialInstituteCheckNumber, (this.financialInstituteCheckNumber!= null));
        }
        return buffer;
    }

}
