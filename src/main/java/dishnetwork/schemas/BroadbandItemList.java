package dishnetwork.schemas;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.jvnet.jaxb2_commons.lang.*;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="broadbandItem" type="{http://www.dishnetwork.com/schema/CustomerManagement/Item/2013_01_17}ItemType" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "broadbandItem"
})
@Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
public class BroadbandItemList implements Equals2, HashCode2, ToString2 {

    //@XmlElement(required = true)
    @JsonProperty(required = true)
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected List<Item> broadbandItem;

    /**
     * Default no-arg constructor
     */
    public BroadbandItemList() {
        super();
    }

    /**
     * Fully-initialising value constructor
     */
    public BroadbandItemList(final List<Item> broadbandItem) {
        this.broadbandItem = broadbandItem;
    }

    /**
     * Gets the value of the broadbandItem property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the broadbandItem property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBroadbandItem().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Item }
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public List<Item> getBroadbandItem() {
        if (broadbandItem == null) {
            broadbandItem = new ArrayList<Item>();
        }
        return this.broadbandItem;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy2 strategy) {
        if ((object == null) || (this.getClass() != object.getClass())) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final BroadbandItemList that = ((BroadbandItemList) object);
        {
            List<Item> lhsBroadbandItem;
            lhsBroadbandItem = (((this.broadbandItem != null) && (!this.broadbandItem.isEmpty())) ? this.getBroadbandItem() : null);
            List<Item> rhsBroadbandItem;
            rhsBroadbandItem = (((that.broadbandItem != null) && (!that.broadbandItem.isEmpty())) ? that.getBroadbandItem() : null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "broadbandItem", lhsBroadbandItem), LocatorUtils.property(thatLocator, "broadbandItem", rhsBroadbandItem), lhsBroadbandItem, rhsBroadbandItem, ((this.broadbandItem != null) && (!this.broadbandItem.isEmpty())), ((that.broadbandItem != null) && (!that.broadbandItem.isEmpty())))) {
                return false;
            }
        }
        return true;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(Object object) {
        final EqualsStrategy2 strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode(ObjectLocator locator, HashCodeStrategy2 strategy) {
        int currentHashCode = 1;
        {
            List<Item> theBroadbandItem;
            theBroadbandItem = (((this.broadbandItem != null) && (!this.broadbandItem.isEmpty())) ? this.getBroadbandItem() : null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "broadbandItem", theBroadbandItem), currentHashCode, theBroadbandItem, ((this.broadbandItem != null) && (!this.broadbandItem.isEmpty())));
        }
        return currentHashCode;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode() {
        final HashCodeStrategy2 strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String toString() {
        final ToStringStrategy2 strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        {
            List<Item> theBroadbandItem;
            theBroadbandItem = (((this.broadbandItem != null) && (!this.broadbandItem.isEmpty())) ? this.getBroadbandItem() : null);
            strategy.appendField(locator, this, "broadbandItem", buffer, theBroadbandItem, ((this.broadbandItem != null) && (!this.broadbandItem.isEmpty())));
        }
        return buffer;
    }

}
