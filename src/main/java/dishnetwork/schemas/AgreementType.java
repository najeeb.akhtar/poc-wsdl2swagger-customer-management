
package dishnetwork.schemas;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.jvnet.jaxb2_commons.lang.Equals2;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy2;
import org.jvnet.jaxb2_commons.lang.HashCode2;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy2;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString2;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy2;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * consist the agreement related information.
 *
 * <p>Java class for AgreementType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="AgreementType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="externalSourceId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="externalSourceType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="multipleFlag" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="promoAppCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="plan" type="{http://www.dishnetwork.com/schema/CustomerManagement/PlanDetails/2015_01_15}PlanDetailsType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AgreementType", propOrder = {
    "externalSourceId",
    "externalSourceType",
    "multipleFlag",
    "promoAppCode",
    "plan"
})
@Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
public class AgreementType implements Equals2, HashCode2, ToString2
{

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String externalSourceId;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String externalSourceType;
    //@XmlElement(defaultValue = "false")
    @JsonProperty(defaultValue = "false")
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String multipleFlag;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String promoAppCode;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected PlanDetailsType plan;

    /**
     * Default no-arg constructor
     *
     */
    public AgreementType() {
        super();
    }

    /**
     * Fully-initialising value constructor
     *
     */
    public AgreementType(final String externalSourceId, final String externalSourceType, final String multipleFlag, final String promoAppCode, final PlanDetailsType plan) {
        this.externalSourceId = externalSourceId;
        this.externalSourceType = externalSourceType;
        this.multipleFlag = multipleFlag;
        this.promoAppCode = promoAppCode;
        this.plan = plan;
    }

    /**
     * Gets the value of the externalSourceId property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getExternalSourceId() {
        return externalSourceId;
    }

    /**
     * Sets the value of the externalSourceId property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setExternalSourceId(String value) {
        this.externalSourceId = value;
    }

    /**
     * Gets the value of the externalSourceType property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getExternalSourceType() {
        return externalSourceType;
    }

    /**
     * Sets the value of the externalSourceType property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setExternalSourceType(String value) {
        this.externalSourceType = value;
    }

    /**
     * Gets the value of the multipleFlag property.
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getMultipleFlag() {
        return multipleFlag;
    }

    /**
     * Sets the value of the multipleFlag property.
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setMultipleFlag(String value) {
        this.multipleFlag = value;
    }

    /**
     * Gets the value of the promoAppCode property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getPromoAppCode() {
        return promoAppCode;
    }

    /**
     * Sets the value of the promoAppCode property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setPromoAppCode(String value) {
        this.promoAppCode = value;
    }

    /**
     * Gets the value of the plan property.
     *
     * @return
     *     possible object is
     *     {@link PlanDetailsType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public PlanDetailsType getPlan() {
        return plan;
    }

    /**
     * Sets the value of the plan property.
     *
     * @param value
     *     allowed object is
     *     {@link PlanDetailsType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setPlan(PlanDetailsType value) {
        this.plan = value;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy2 strategy) {
        if ((object == null)||(this.getClass()!= object.getClass())) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final AgreementType that = ((AgreementType) object);
        {
            String lhsExternalSourceId;
            lhsExternalSourceId = this.getExternalSourceId();
            String rhsExternalSourceId;
            rhsExternalSourceId = that.getExternalSourceId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "externalSourceId", lhsExternalSourceId), LocatorUtils.property(thatLocator, "externalSourceId", rhsExternalSourceId), lhsExternalSourceId, rhsExternalSourceId, (this.externalSourceId!= null), (that.externalSourceId!= null))) {
                return false;
            }
        }
        {
            String lhsExternalSourceType;
            lhsExternalSourceType = this.getExternalSourceType();
            String rhsExternalSourceType;
            rhsExternalSourceType = that.getExternalSourceType();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "externalSourceType", lhsExternalSourceType), LocatorUtils.property(thatLocator, "externalSourceType", rhsExternalSourceType), lhsExternalSourceType, rhsExternalSourceType, (this.externalSourceType!= null), (that.externalSourceType!= null))) {
                return false;
            }
        }
        {
            String lhsMultipleFlag;
            lhsMultipleFlag = this.getMultipleFlag();
            String rhsMultipleFlag;
            rhsMultipleFlag = that.getMultipleFlag();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "multipleFlag", lhsMultipleFlag), LocatorUtils.property(thatLocator, "multipleFlag", rhsMultipleFlag), lhsMultipleFlag, rhsMultipleFlag, true, true)) {
                return false;
            }
        }
        {
            String lhsPromoAppCode;
            lhsPromoAppCode = this.getPromoAppCode();
            String rhsPromoAppCode;
            rhsPromoAppCode = that.getPromoAppCode();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "promoAppCode", lhsPromoAppCode), LocatorUtils.property(thatLocator, "promoAppCode", rhsPromoAppCode), lhsPromoAppCode, rhsPromoAppCode, (this.promoAppCode!= null), (that.promoAppCode!= null))) {
                return false;
            }
        }
        {
            PlanDetailsType lhsPlan;
            lhsPlan = this.getPlan();
            PlanDetailsType rhsPlan;
            rhsPlan = that.getPlan();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "plan", lhsPlan), LocatorUtils.property(thatLocator, "plan", rhsPlan), lhsPlan, rhsPlan, (this.plan!= null), (that.plan!= null))) {
                return false;
            }
        }
        return true;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(Object object) {
        final EqualsStrategy2 strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode(ObjectLocator locator, HashCodeStrategy2 strategy) {
        int currentHashCode = 1;
        {
            String theExternalSourceId;
            theExternalSourceId = this.getExternalSourceId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "externalSourceId", theExternalSourceId), currentHashCode, theExternalSourceId, (this.externalSourceId!= null));
        }
        {
            String theExternalSourceType;
            theExternalSourceType = this.getExternalSourceType();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "externalSourceType", theExternalSourceType), currentHashCode, theExternalSourceType, (this.externalSourceType!= null));
        }
        {
            String theMultipleFlag;
            theMultipleFlag = this.getMultipleFlag();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "multipleFlag", theMultipleFlag), currentHashCode, theMultipleFlag, true);
        }
        {
            String thePromoAppCode;
            thePromoAppCode = this.getPromoAppCode();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "promoAppCode", thePromoAppCode), currentHashCode, thePromoAppCode, (this.promoAppCode!= null));
        }
        {
            PlanDetailsType thePlan;
            thePlan = this.getPlan();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "plan", thePlan), currentHashCode, thePlan, (this.plan!= null));
        }
        return currentHashCode;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode() {
        final HashCodeStrategy2 strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String toString() {
        final ToStringStrategy2 strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        {
            String theExternalSourceId;
            theExternalSourceId = this.getExternalSourceId();
            strategy.appendField(locator, this, "externalSourceId", buffer, theExternalSourceId, (this.externalSourceId!= null));
        }
        {
            String theExternalSourceType;
            theExternalSourceType = this.getExternalSourceType();
            strategy.appendField(locator, this, "externalSourceType", buffer, theExternalSourceType, (this.externalSourceType!= null));
        }
        {
            String theMultipleFlag;
            theMultipleFlag = this.getMultipleFlag();
            strategy.appendField(locator, this, "multipleFlag", buffer, theMultipleFlag, true);
        }
        {
            String thePromoAppCode;
            thePromoAppCode = this.getPromoAppCode();
            strategy.appendField(locator, this, "promoAppCode", buffer, thePromoAppCode, (this.promoAppCode!= null));
        }
        {
            PlanDetailsType thePlan;
            thePlan = this.getPlan();
            strategy.appendField(locator, this, "plan", buffer, thePlan, (this.plan!= null));
        }
        return buffer;
    }

}
