
package dishnetwork.schemas;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.jvnet.jaxb2_commons.lang.Equals2;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy2;
import org.jvnet.jaxb2_commons.lang.HashCode2;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy2;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString2;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy2;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * Plan info fro submitOrder.
 *
 * <p>Java class for PlanInfoType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="PlanInfoType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="plan" type="{http://www.dishnetwork.com/schema/CustomerManagement/PlanDetails/2015_01_15}PlanDetailsType"/&gt;
 *         &lt;element name="componentIdList" type="{http://www.dishnetwork.com/schema/common/ComponentId/2015_01_15}ComponentIdListType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PlanInfoType", propOrder = {
    "plan",
    "componentIdList"
})
@XmlSeeAlso({
    PlanInfo.class
})
@Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
public class PlanInfoType implements Equals2, HashCode2, ToString2
{

    //@XmlElement(required = true)
    @JsonProperty(required = true)
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected PlanDetailsType plan;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected ComponentIdListType componentIdList;

    /**
     * Default no-arg constructor
     *
     */
    public PlanInfoType() {
        super();
    }

    /**
     * Fully-initialising value constructor
     *
     */
    public PlanInfoType(final PlanDetailsType plan, final ComponentIdListType componentIdList) {
        this.plan = plan;
        this.componentIdList = componentIdList;
    }

    /**
     * Gets the value of the plan property.
     *
     * @return
     *     possible object is
     *     {@link PlanDetailsType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public PlanDetailsType getPlan() {
        return plan;
    }

    /**
     * Sets the value of the plan property.
     *
     * @param value
     *     allowed object is
     *     {@link PlanDetailsType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setPlan(PlanDetailsType value) {
        this.plan = value;
    }

    /**
     * Gets the value of the componentIdList property.
     *
     * @return
     *     possible object is
     *     {@link ComponentIdListType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public ComponentIdListType getComponentIdList() {
        return componentIdList;
    }

    /**
     * Sets the value of the componentIdList property.
     *
     * @param value
     *     allowed object is
     *     {@link ComponentIdListType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setComponentIdList(ComponentIdListType value) {
        this.componentIdList = value;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy2 strategy) {
        if ((object == null)||(this.getClass()!= object.getClass())) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final PlanInfoType that = ((PlanInfoType) object);
        {
            PlanDetailsType lhsPlan;
            lhsPlan = this.getPlan();
            PlanDetailsType rhsPlan;
            rhsPlan = that.getPlan();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "plan", lhsPlan), LocatorUtils.property(thatLocator, "plan", rhsPlan), lhsPlan, rhsPlan, (this.plan!= null), (that.plan!= null))) {
                return false;
            }
        }
        {
            ComponentIdListType lhsComponentIdList;
            lhsComponentIdList = this.getComponentIdList();
            ComponentIdListType rhsComponentIdList;
            rhsComponentIdList = that.getComponentIdList();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "componentIdList", lhsComponentIdList), LocatorUtils.property(thatLocator, "componentIdList", rhsComponentIdList), lhsComponentIdList, rhsComponentIdList, (this.componentIdList!= null), (that.componentIdList!= null))) {
                return false;
            }
        }
        return true;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(Object object) {
        final EqualsStrategy2 strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode(ObjectLocator locator, HashCodeStrategy2 strategy) {
        int currentHashCode = 1;
        {
            PlanDetailsType thePlan;
            thePlan = this.getPlan();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "plan", thePlan), currentHashCode, thePlan, (this.plan!= null));
        }
        {
            ComponentIdListType theComponentIdList;
            theComponentIdList = this.getComponentIdList();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "componentIdList", theComponentIdList), currentHashCode, theComponentIdList, (this.componentIdList!= null));
        }
        return currentHashCode;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode() {
        final HashCodeStrategy2 strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String toString() {
        final ToStringStrategy2 strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        {
            PlanDetailsType thePlan;
            thePlan = this.getPlan();
            strategy.appendField(locator, this, "plan", buffer, thePlan, (this.plan!= null));
        }
        {
            ComponentIdListType theComponentIdList;
            theComponentIdList = this.getComponentIdList();
            strategy.appendField(locator, this, "componentIdList", buffer, theComponentIdList, (this.componentIdList!= null));
        }
        return buffer;
    }

}
