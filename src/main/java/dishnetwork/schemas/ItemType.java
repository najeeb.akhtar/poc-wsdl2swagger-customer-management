
package dishnetwork.schemas;

import java.math.BigInteger;
import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.jvnet.jaxb2_commons.lang.Equals2;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy2;
import org.jvnet.jaxb2_commons.lang.HashCode2;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy2;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString2;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy2;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for ItemType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="ItemType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="code" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="quantity" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="beforeQuantity" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *         &lt;element name="billing" type="{http://www.dishnetwork.com/schema/Billing/ItemBilling/2011_04_01}ItemBillingType" minOccurs="0"/&gt;
 *         &lt;element name="connectDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="serviceType" type="{http://www.dishnetwork.com/schema/ServiceType/ServiceType/2015_01_15}ServiceTypeType" minOccurs="0"/&gt;
 *         &lt;element name="campaign" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="charge" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="jobId" type="{http://www.dishnetwork.com/schema/AccountOrder/JobId/2011_04_01}JobIdType" minOccurs="0"/&gt;
 *         &lt;element name="packageFlag" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="pkgItemList" type="{http://www.dishnetwork.com/schema/AccountOrder/PackageItemList/2015_01_15}PackageItemListType" minOccurs="0"/&gt;
 *         &lt;element name="attributeList" type="{http://www.dishnetwork.com/schema/common/AttributeList/2013_09_19}AttributeListType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ItemType", propOrder = {
    "code",
    "description",
    "status",
    "quantity",
    "beforeQuantity",
    "billing",
    "connectDate",
    "serviceType",
    "campaign",
    "charge",
    "jobId",
    "packageFlag",
    "pkgItemList",
    "attributeList"
})
@Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
public class ItemType implements Equals2, HashCode2, ToString2
{

    //@XmlElement(required = true)
    @JsonProperty(required = true)
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String code;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String description;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String status;
    //@XmlElement(required = true)
    @JsonProperty(required = true)
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String quantity;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String beforeQuantity;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected ItemBillingType billing;
    @XmlSchemaType(name = "date")
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    @ApiModelProperty(example = "2022-07-12T21:32:55.842Z")
    protected String connectDate;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected ServiceTypeType serviceType;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String campaign;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String charge;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected JobIdType jobId;
    //@XmlElement(defaultValue = "false")
    @JsonProperty(defaultValue = "false")
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String packageFlag;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected PackageItemListType pkgItemList;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected AttributeListType attributeList;

    /**
     * Default no-arg constructor
     *
     */
    public ItemType() {
        super();
    }

    /**
     * Fully-initialising value constructor
     *
     */
    public ItemType(final String code, final String description, final String status, final String quantity, final String beforeQuantity, final ItemBillingType billing, final String connectDate, final ServiceTypeType serviceType, final String campaign, final String charge, final JobIdType jobId, final String packageFlag, final PackageItemListType pkgItemList, final AttributeListType attributeList) {
        this.code = code;
        this.description = description;
        this.status = status;
        this.quantity = quantity;
        this.beforeQuantity = beforeQuantity;
        this.billing = billing;
        this.connectDate = connectDate;
        this.serviceType = serviceType;
        this.campaign = campaign;
        this.charge = charge;
        this.jobId = jobId;
        this.packageFlag = packageFlag;
        this.pkgItemList = pkgItemList;
        this.attributeList = attributeList;
    }

    /**
     * Gets the value of the code property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getCode() {
        return code;
    }

    /**
     * Sets the value of the code property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setCode(String value) {
        this.code = value;
    }

    /**
     * Gets the value of the description property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the status property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the quantity property.
     *
     * @return
     *     possible object is
     *     {@link BigInteger }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getQuantity() {
        return quantity;
    }

    /**
     * Sets the value of the quantity property.
     *
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setQuantity(String value) {
        this.quantity = value;
    }

    /**
     * Gets the value of the beforeQuantity property.
     *
     * @return
     *     possible object is
     *     {@link BigInteger }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getBeforeQuantity() {
        return beforeQuantity;
    }

    /**
     * Sets the value of the beforeQuantity property.
     *
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setBeforeQuantity(String value) {
        this.beforeQuantity = value;
    }

    /**
     * Gets the value of the billing property.
     *
     * @return
     *     possible object is
     *     {@link ItemBillingType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public ItemBillingType getBilling() {
        return billing;
    }

    /**
     * Sets the value of the billing property.
     *
     * @param value
     *     allowed object is
     *     {@link ItemBillingType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setBilling(ItemBillingType value) {
        this.billing = value;
    }

    /**
     * Gets the value of the connectDate property.
     *
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getConnectDate() {
        return connectDate;
    }

    /**
     * Sets the value of the connectDate property.
     *
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setConnectDate(String value) {
        this.connectDate = value;
    }

    /**
     * Gets the value of the serviceType property.
     *
     * @return
     *     possible object is
     *     {@link ServiceTypeType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public ServiceTypeType getServiceType() {
        return serviceType;
    }

    /**
     * Sets the value of the serviceType property.
     *
     * @param value
     *     allowed object is
     *     {@link ServiceTypeType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setServiceType(ServiceTypeType value) {
        this.serviceType = value;
    }

    /**
     * Gets the value of the campaign property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getCampaign() {
        return campaign;
    }

    /**
     * Sets the value of the campaign property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setCampaign(String value) {
        this.campaign = value;
    }

    /**
     * Gets the value of the charge property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getCharge() {
        return charge;
    }

    /**
     * Sets the value of the charge property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setCharge(String value) {
        this.charge = value;
    }

    /**
     * Gets the value of the jobId property.
     *
     * @return
     *     possible object is
     *     {@link JobIdType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public JobIdType getJobId() {
        return jobId;
    }

    /**
     * Sets the value of the jobId property.
     *
     * @param value
     *     allowed object is
     *     {@link JobIdType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setJobId(JobIdType value) {
        this.jobId = value;
    }

    /**
     * Gets the value of the packageFlag property.
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getPackageFlag() {
        return packageFlag;
    }

    /**
     * Sets the value of the packageFlag property.
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setPackageFlag(String value) {
        this.packageFlag = value;
    }

    /**
     * Gets the value of the pkgItemList property.
     *
     * @return
     *     possible object is
     *     {@link PackageItemListType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public PackageItemListType getPkgItemList() {
        return pkgItemList;
    }

    /**
     * Sets the value of the pkgItemList property.
     *
     * @param value
     *     allowed object is
     *     {@link PackageItemListType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setPkgItemList(PackageItemListType value) {
        this.pkgItemList = value;
    }

    /**
     * Gets the value of the attributeList property.
     *
     * @return
     *     possible object is
     *     {@link AttributeListType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public AttributeListType getAttributeList() {
        return attributeList;
    }

    /**
     * Sets the value of the attributeList property.
     *
     * @param value
     *     allowed object is
     *     {@link AttributeListType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setAttributeList(AttributeListType value) {
        this.attributeList = value;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy2 strategy) {
        if ((object == null)||(this.getClass()!= object.getClass())) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final ItemType that = ((ItemType) object);
        {
            String lhsCode;
            lhsCode = this.getCode();
            String rhsCode;
            rhsCode = that.getCode();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "code", lhsCode), LocatorUtils.property(thatLocator, "code", rhsCode), lhsCode, rhsCode, (this.code!= null), (that.code!= null))) {
                return false;
            }
        }
        {
            String lhsDescription;
            lhsDescription = this.getDescription();
            String rhsDescription;
            rhsDescription = that.getDescription();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "description", lhsDescription), LocatorUtils.property(thatLocator, "description", rhsDescription), lhsDescription, rhsDescription, (this.description!= null), (that.description!= null))) {
                return false;
            }
        }
        {
            String lhsStatus;
            lhsStatus = this.getStatus();
            String rhsStatus;
            rhsStatus = that.getStatus();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "status", lhsStatus), LocatorUtils.property(thatLocator, "status", rhsStatus), lhsStatus, rhsStatus, (this.status!= null), (that.status!= null))) {
                return false;
            }
        }
        {
            String lhsQuantity;
            lhsQuantity = this.getQuantity();
            String rhsQuantity;
            rhsQuantity = that.getQuantity();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "quantity", lhsQuantity), LocatorUtils.property(thatLocator, "quantity", rhsQuantity), lhsQuantity, rhsQuantity, (this.quantity!= null), (that.quantity!= null))) {
                return false;
            }
        }
        {
            String lhsBeforeQuantity;
            lhsBeforeQuantity = this.getBeforeQuantity();
            String rhsBeforeQuantity;
            rhsBeforeQuantity = that.getBeforeQuantity();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "beforeQuantity", lhsBeforeQuantity), LocatorUtils.property(thatLocator, "beforeQuantity", rhsBeforeQuantity), lhsBeforeQuantity, rhsBeforeQuantity, (this.beforeQuantity!= null), (that.beforeQuantity!= null))) {
                return false;
            }
        }
        {
            ItemBillingType lhsBilling;
            lhsBilling = this.getBilling();
            ItemBillingType rhsBilling;
            rhsBilling = that.getBilling();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "billing", lhsBilling), LocatorUtils.property(thatLocator, "billing", rhsBilling), lhsBilling, rhsBilling, (this.billing!= null), (that.billing!= null))) {
                return false;
            }
        }
        {
            String lhsConnectDate;
            lhsConnectDate = this.getConnectDate();
            String rhsConnectDate;
            rhsConnectDate = that.getConnectDate();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "connectDate", lhsConnectDate), LocatorUtils.property(thatLocator, "connectDate", rhsConnectDate), lhsConnectDate, rhsConnectDate, (this.connectDate!= null), (that.connectDate!= null))) {
                return false;
            }
        }
        {
            ServiceTypeType lhsServiceType;
            lhsServiceType = this.getServiceType();
            ServiceTypeType rhsServiceType;
            rhsServiceType = that.getServiceType();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "serviceType", lhsServiceType), LocatorUtils.property(thatLocator, "serviceType", rhsServiceType), lhsServiceType, rhsServiceType, (this.serviceType!= null), (that.serviceType!= null))) {
                return false;
            }
        }
        {
            String lhsCampaign;
            lhsCampaign = this.getCampaign();
            String rhsCampaign;
            rhsCampaign = that.getCampaign();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "campaign", lhsCampaign), LocatorUtils.property(thatLocator, "campaign", rhsCampaign), lhsCampaign, rhsCampaign, (this.campaign!= null), (that.campaign!= null))) {
                return false;
            }
        }
        {
            String lhsCharge;
            lhsCharge = this.getCharge();
            String rhsCharge;
            rhsCharge = that.getCharge();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "charge", lhsCharge), LocatorUtils.property(thatLocator, "charge", rhsCharge), lhsCharge, rhsCharge, (this.charge!= null), (that.charge!= null))) {
                return false;
            }
        }
        {
            JobIdType lhsJobId;
            lhsJobId = this.getJobId();
            JobIdType rhsJobId;
            rhsJobId = that.getJobId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "jobId", lhsJobId), LocatorUtils.property(thatLocator, "jobId", rhsJobId), lhsJobId, rhsJobId, (this.jobId!= null), (that.jobId!= null))) {
                return false;
            }
        }
        {
            String lhsPackageFlag;
            lhsPackageFlag = this.getPackageFlag();
            String rhsPackageFlag;
            rhsPackageFlag = that.getPackageFlag();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "packageFlag", lhsPackageFlag), LocatorUtils.property(thatLocator, "packageFlag", rhsPackageFlag), lhsPackageFlag, rhsPackageFlag, true, true)) {
                return false;
            }
        }
        {
            PackageItemListType lhsPkgItemList;
            lhsPkgItemList = this.getPkgItemList();
            PackageItemListType rhsPkgItemList;
            rhsPkgItemList = that.getPkgItemList();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "pkgItemList", lhsPkgItemList), LocatorUtils.property(thatLocator, "pkgItemList", rhsPkgItemList), lhsPkgItemList, rhsPkgItemList, (this.pkgItemList!= null), (that.pkgItemList!= null))) {
                return false;
            }
        }
        {
            AttributeListType lhsAttributeList;
            lhsAttributeList = this.getAttributeList();
            AttributeListType rhsAttributeList;
            rhsAttributeList = that.getAttributeList();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "attributeList", lhsAttributeList), LocatorUtils.property(thatLocator, "attributeList", rhsAttributeList), lhsAttributeList, rhsAttributeList, (this.attributeList!= null), (that.attributeList!= null))) {
                return false;
            }
        }
        return true;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(Object object) {
        final EqualsStrategy2 strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode(ObjectLocator locator, HashCodeStrategy2 strategy) {
        int currentHashCode = 1;
        {
            String theCode;
            theCode = this.getCode();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "code", theCode), currentHashCode, theCode, (this.code!= null));
        }
        {
            String theDescription;
            theDescription = this.getDescription();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "description", theDescription), currentHashCode, theDescription, (this.description!= null));
        }
        {
            String theStatus;
            theStatus = this.getStatus();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "status", theStatus), currentHashCode, theStatus, (this.status!= null));
        }
        {
            String theQuantity;
            theQuantity = this.getQuantity();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "quantity", theQuantity), currentHashCode, theQuantity, (this.quantity!= null));
        }
        {
            String theBeforeQuantity;
            theBeforeQuantity = this.getBeforeQuantity();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "beforeQuantity", theBeforeQuantity), currentHashCode, theBeforeQuantity, (this.beforeQuantity!= null));
        }
        {
            ItemBillingType theBilling;
            theBilling = this.getBilling();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "billing", theBilling), currentHashCode, theBilling, (this.billing!= null));
        }
        {
            String theConnectDate;
            theConnectDate = this.getConnectDate();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "connectDate", theConnectDate), currentHashCode, theConnectDate, (this.connectDate!= null));
        }
        {
            ServiceTypeType theServiceType;
            theServiceType = this.getServiceType();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "serviceType", theServiceType), currentHashCode, theServiceType, (this.serviceType!= null));
        }
        {
            String theCampaign;
            theCampaign = this.getCampaign();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "campaign", theCampaign), currentHashCode, theCampaign, (this.campaign!= null));
        }
        {
            String theCharge;
            theCharge = this.getCharge();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "charge", theCharge), currentHashCode, theCharge, (this.charge!= null));
        }
        {
            JobIdType theJobId;
            theJobId = this.getJobId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "jobId", theJobId), currentHashCode, theJobId, (this.jobId!= null));
        }
        {
            String thePackageFlag;
            thePackageFlag = this.getPackageFlag();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "packageFlag", thePackageFlag), currentHashCode, thePackageFlag, true);
        }
        {
            PackageItemListType thePkgItemList;
            thePkgItemList = this.getPkgItemList();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "pkgItemList", thePkgItemList), currentHashCode, thePkgItemList, (this.pkgItemList!= null));
        }
        {
            AttributeListType theAttributeList;
            theAttributeList = this.getAttributeList();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "attributeList", theAttributeList), currentHashCode, theAttributeList, (this.attributeList!= null));
        }
        return currentHashCode;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode() {
        final HashCodeStrategy2 strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String toString() {
        final ToStringStrategy2 strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        {
            String theCode;
            theCode = this.getCode();
            strategy.appendField(locator, this, "code", buffer, theCode, (this.code!= null));
        }
        {
            String theDescription;
            theDescription = this.getDescription();
            strategy.appendField(locator, this, "description", buffer, theDescription, (this.description!= null));
        }
        {
            String theStatus;
            theStatus = this.getStatus();
            strategy.appendField(locator, this, "status", buffer, theStatus, (this.status!= null));
        }
        {
            String theQuantity;
            theQuantity = this.getQuantity();
            strategy.appendField(locator, this, "quantity", buffer, theQuantity, (this.quantity!= null));
        }
        {
            String theBeforeQuantity;
            theBeforeQuantity = this.getBeforeQuantity();
            strategy.appendField(locator, this, "beforeQuantity", buffer, theBeforeQuantity, (this.beforeQuantity!= null));
        }
        {
            ItemBillingType theBilling;
            theBilling = this.getBilling();
            strategy.appendField(locator, this, "billing", buffer, theBilling, (this.billing!= null));
        }
        {
            String theConnectDate;
            theConnectDate = this.getConnectDate();
            strategy.appendField(locator, this, "connectDate", buffer, theConnectDate, (this.connectDate!= null));
        }
        {
            ServiceTypeType theServiceType;
            theServiceType = this.getServiceType();
            strategy.appendField(locator, this, "serviceType", buffer, theServiceType, (this.serviceType!= null));
        }
        {
            String theCampaign;
            theCampaign = this.getCampaign();
            strategy.appendField(locator, this, "campaign", buffer, theCampaign, (this.campaign!= null));
        }
        {
            String theCharge;
            theCharge = this.getCharge();
            strategy.appendField(locator, this, "charge", buffer, theCharge, (this.charge!= null));
        }
        {
            JobIdType theJobId;
            theJobId = this.getJobId();
            strategy.appendField(locator, this, "jobId", buffer, theJobId, (this.jobId!= null));
        }
        {
            String thePackageFlag;
            thePackageFlag = this.getPackageFlag();
            strategy.appendField(locator, this, "packageFlag", buffer, thePackageFlag, true);
        }
        {
            PackageItemListType thePkgItemList;
            thePkgItemList = this.getPkgItemList();
            strategy.appendField(locator, this, "pkgItemList", buffer, thePkgItemList, (this.pkgItemList!= null));
        }
        {
            AttributeListType theAttributeList;
            theAttributeList = this.getAttributeList();
            strategy.appendField(locator, this, "attributeList", buffer, theAttributeList, (this.attributeList!= null));
        }
        return buffer;
    }

}
