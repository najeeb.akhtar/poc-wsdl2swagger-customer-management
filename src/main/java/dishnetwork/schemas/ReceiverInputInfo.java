package dishnetwork.schemas;

import org.jvnet.jaxb2_commons.lang.*;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="accessNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="model" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ownership" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="attributeList" type="{http://www.dishnetwork.com/schema/common/AttributeList/2015_10_08}AttributeListTypeType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "accessNumber",
    "status",
    "model",
    "ownership",
    "attributeList"
})
@Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
public class ReceiverInputInfo implements Equals2, HashCode2, ToString2 {

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String accessNumber;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String status;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String model;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String ownership;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected AttributeListTypeType attributeList;

    /**
     * Default no-arg constructor
     */
    public ReceiverInputInfo() {
        super();
    }

    /**
     * Fully-initialising value constructor
     */
    public ReceiverInputInfo(final String accessNumber, final String status, final String model, final String ownership, final AttributeListTypeType attributeList) {
        this.accessNumber = accessNumber;
        this.status = status;
        this.model = model;
        this.ownership = ownership;
        this.attributeList = attributeList;
    }

    /**
     * Gets the value of the accessNumber property.
     *
     * @return possible object is
     * {@link String }
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getAccessNumber() {
        return accessNumber;
    }

    /**
     * Sets the value of the accessNumber property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setAccessNumber(String value) {
        this.accessNumber = value;
    }

    /**
     * Gets the value of the status property.
     *
     * @return possible object is
     * {@link String }
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the model property.
     *
     * @return possible object is
     * {@link String }
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getModel() {
        return model;
    }

    /**
     * Sets the value of the model property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setModel(String value) {
        this.model = value;
    }

    /**
     * Gets the value of the ownership property.
     *
     * @return possible object is
     * {@link String }
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getOwnership() {
        return ownership;
    }

    /**
     * Sets the value of the ownership property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setOwnership(String value) {
        this.ownership = value;
    }

    /**
     * Gets the value of the attributeList property.
     *
     * @return possible object is
     * {@link AttributeListTypeType }
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public AttributeListTypeType getAttributeList() {
        return attributeList;
    }

    /**
     * Sets the value of the attributeList property.
     *
     * @param value allowed object is
     *              {@link AttributeListTypeType }
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setAttributeList(AttributeListTypeType value) {
        this.attributeList = value;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy2 strategy) {
        if ((object == null) || (this.getClass() != object.getClass())) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final ReceiverInputInfo that = ((ReceiverInputInfo) object);
        {
            String lhsAccessNumber;
            lhsAccessNumber = this.getAccessNumber();
            String rhsAccessNumber;
            rhsAccessNumber = that.getAccessNumber();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "accessNumber", lhsAccessNumber), LocatorUtils.property(thatLocator, "accessNumber", rhsAccessNumber), lhsAccessNumber, rhsAccessNumber, (this.accessNumber != null), (that.accessNumber != null))) {
                return false;
            }
        }
        {
            String lhsStatus;
            lhsStatus = this.getStatus();
            String rhsStatus;
            rhsStatus = that.getStatus();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "status", lhsStatus), LocatorUtils.property(thatLocator, "status", rhsStatus), lhsStatus, rhsStatus, (this.status != null), (that.status != null))) {
                return false;
            }
        }
        {
            String lhsModel;
            lhsModel = this.getModel();
            String rhsModel;
            rhsModel = that.getModel();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "model", lhsModel), LocatorUtils.property(thatLocator, "model", rhsModel), lhsModel, rhsModel, (this.model != null), (that.model != null))) {
                return false;
            }
        }
        {
            String lhsOwnership;
            lhsOwnership = this.getOwnership();
            String rhsOwnership;
            rhsOwnership = that.getOwnership();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "ownership", lhsOwnership), LocatorUtils.property(thatLocator, "ownership", rhsOwnership), lhsOwnership, rhsOwnership, (this.ownership != null), (that.ownership != null))) {
                return false;
            }
        }
        {
            AttributeListTypeType lhsAttributeList;
            lhsAttributeList = this.getAttributeList();
            AttributeListTypeType rhsAttributeList;
            rhsAttributeList = that.getAttributeList();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "attributeList", lhsAttributeList), LocatorUtils.property(thatLocator, "attributeList", rhsAttributeList), lhsAttributeList, rhsAttributeList, (this.attributeList != null), (that.attributeList != null))) {
                return false;
            }
        }
        return true;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(Object object) {
        final EqualsStrategy2 strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode(ObjectLocator locator, HashCodeStrategy2 strategy) {
        int currentHashCode = 1;
        {
            String theAccessNumber;
            theAccessNumber = this.getAccessNumber();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "accessNumber", theAccessNumber), currentHashCode, theAccessNumber, (this.accessNumber != null));
        }
        {
            String theStatus;
            theStatus = this.getStatus();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "status", theStatus), currentHashCode, theStatus, (this.status != null));
        }
        {
            String theModel;
            theModel = this.getModel();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "model", theModel), currentHashCode, theModel, (this.model != null));
        }
        {
            String theOwnership;
            theOwnership = this.getOwnership();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "ownership", theOwnership), currentHashCode, theOwnership, (this.ownership != null));
        }
        {
            AttributeListTypeType theAttributeList;
            theAttributeList = this.getAttributeList();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "attributeList", theAttributeList), currentHashCode, theAttributeList, (this.attributeList != null));
        }
        return currentHashCode;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode() {
        final HashCodeStrategy2 strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String toString() {
        final ToStringStrategy2 strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        {
            String theAccessNumber;
            theAccessNumber = this.getAccessNumber();
            strategy.appendField(locator, this, "accessNumber", buffer, theAccessNumber, (this.accessNumber != null));
        }
        {
            String theStatus;
            theStatus = this.getStatus();
            strategy.appendField(locator, this, "status", buffer, theStatus, (this.status != null));
        }
        {
            String theModel;
            theModel = this.getModel();
            strategy.appendField(locator, this, "model", buffer, theModel, (this.model != null));
        }
        {
            String theOwnership;
            theOwnership = this.getOwnership();
            strategy.appendField(locator, this, "ownership", buffer, theOwnership, (this.ownership != null));
        }
        {
            AttributeListTypeType theAttributeList;
            theAttributeList = this.getAttributeList();
            strategy.appendField(locator, this, "attributeList", buffer, theAttributeList, (this.attributeList != null));
        }
        return buffer;
    }

}
