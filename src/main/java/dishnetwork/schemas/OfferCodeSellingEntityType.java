
package dishnetwork.schemas;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.jvnet.jaxb2_commons.lang.Equals2;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy2;
import org.jvnet.jaxb2_commons.lang.HashCode2;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy2;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString2;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy2;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for OfferCodeSellingEntityType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="OfferCodeSellingEntityType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="agentLoginId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="opId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="companyOe" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="companyName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="distributorCompanyOe" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="location" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="acquisitionChannel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OfferCodeSellingEntityType", propOrder = {
    "agentLoginId",
    "opId",
    "companyOe",
    "companyName",
    "distributorCompanyOe",
    "location",
    "acquisitionChannel"
})
@Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
public class OfferCodeSellingEntityType implements Equals2, HashCode2, ToString2
{

    //@XmlElement(nillable = true)
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String agentLoginId;
    //@XmlElement(nillable = true)
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String opId;
    //@XmlElement(required = true)
    @JsonProperty(required = true)
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String companyOe;
    //@XmlElement(nillable = true)
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String companyName;
    //@XmlElement(nillable = true)
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String distributorCompanyOe;
    //@XmlElement(nillable = true)
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String location;
    //@XmlElement(nillable = true)
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String acquisitionChannel;

    /**
     * Default no-arg constructor
     *
     */
    public OfferCodeSellingEntityType() {
        super();
    }

    /**
     * Fully-initialising value constructor
     *
     */
    public OfferCodeSellingEntityType(final String agentLoginId, final String opId, final String companyOe, final String companyName, final String distributorCompanyOe, final String location, final String acquisitionChannel) {
        this.agentLoginId = agentLoginId;
        this.opId = opId;
        this.companyOe = companyOe;
        this.companyName = companyName;
        this.distributorCompanyOe = distributorCompanyOe;
        this.location = location;
        this.acquisitionChannel = acquisitionChannel;
    }

    /**
     * Gets the value of the agentLoginId property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getAgentLoginId() {
        return agentLoginId;
    }

    /**
     * Sets the value of the agentLoginId property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setAgentLoginId(String value) {
        this.agentLoginId = value;
    }

    /**
     * Gets the value of the opId property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getOpId() {
        return opId;
    }

    /**
     * Sets the value of the opId property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setOpId(String value) {
        this.opId = value;
    }

    /**
     * Gets the value of the companyOe property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getCompanyOe() {
        return companyOe;
    }

    /**
     * Sets the value of the companyOe property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setCompanyOe(String value) {
        this.companyOe = value;
    }

    /**
     * Gets the value of the companyName property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getCompanyName() {
        return companyName;
    }

    /**
     * Sets the value of the companyName property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setCompanyName(String value) {
        this.companyName = value;
    }

    /**
     * Gets the value of the distributorCompanyOe property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getDistributorCompanyOe() {
        return distributorCompanyOe;
    }

    /**
     * Sets the value of the distributorCompanyOe property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setDistributorCompanyOe(String value) {
        this.distributorCompanyOe = value;
    }

    /**
     * Gets the value of the location property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getLocation() {
        return location;
    }

    /**
     * Sets the value of the location property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setLocation(String value) {
        this.location = value;
    }

    /**
     * Gets the value of the acquisitionChannel property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getAcquisitionChannel() {
        return acquisitionChannel;
    }

    /**
     * Sets the value of the acquisitionChannel property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setAcquisitionChannel(String value) {
        this.acquisitionChannel = value;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy2 strategy) {
        if ((object == null)||(this.getClass()!= object.getClass())) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final OfferCodeSellingEntityType that = ((OfferCodeSellingEntityType) object);
        {
            String lhsAgentLoginId;
            lhsAgentLoginId = this.getAgentLoginId();
            String rhsAgentLoginId;
            rhsAgentLoginId = that.getAgentLoginId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "agentLoginId", lhsAgentLoginId), LocatorUtils.property(thatLocator, "agentLoginId", rhsAgentLoginId), lhsAgentLoginId, rhsAgentLoginId, (this.agentLoginId!= null), (that.agentLoginId!= null))) {
                return false;
            }
        }
        {
            String lhsOpId;
            lhsOpId = this.getOpId();
            String rhsOpId;
            rhsOpId = that.getOpId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "opId", lhsOpId), LocatorUtils.property(thatLocator, "opId", rhsOpId), lhsOpId, rhsOpId, (this.opId!= null), (that.opId!= null))) {
                return false;
            }
        }
        {
            String lhsCompanyOe;
            lhsCompanyOe = this.getCompanyOe();
            String rhsCompanyOe;
            rhsCompanyOe = that.getCompanyOe();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "companyOe", lhsCompanyOe), LocatorUtils.property(thatLocator, "companyOe", rhsCompanyOe), lhsCompanyOe, rhsCompanyOe, (this.companyOe!= null), (that.companyOe!= null))) {
                return false;
            }
        }
        {
            String lhsCompanyName;
            lhsCompanyName = this.getCompanyName();
            String rhsCompanyName;
            rhsCompanyName = that.getCompanyName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "companyName", lhsCompanyName), LocatorUtils.property(thatLocator, "companyName", rhsCompanyName), lhsCompanyName, rhsCompanyName, (this.companyName!= null), (that.companyName!= null))) {
                return false;
            }
        }
        {
            String lhsDistributorCompanyOe;
            lhsDistributorCompanyOe = this.getDistributorCompanyOe();
            String rhsDistributorCompanyOe;
            rhsDistributorCompanyOe = that.getDistributorCompanyOe();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "distributorCompanyOe", lhsDistributorCompanyOe), LocatorUtils.property(thatLocator, "distributorCompanyOe", rhsDistributorCompanyOe), lhsDistributorCompanyOe, rhsDistributorCompanyOe, (this.distributorCompanyOe!= null), (that.distributorCompanyOe!= null))) {
                return false;
            }
        }
        {
            String lhsLocation;
            lhsLocation = this.getLocation();
            String rhsLocation;
            rhsLocation = that.getLocation();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "location", lhsLocation), LocatorUtils.property(thatLocator, "location", rhsLocation), lhsLocation, rhsLocation, (this.location!= null), (that.location!= null))) {
                return false;
            }
        }
        {
            String lhsAcquisitionChannel;
            lhsAcquisitionChannel = this.getAcquisitionChannel();
            String rhsAcquisitionChannel;
            rhsAcquisitionChannel = that.getAcquisitionChannel();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "acquisitionChannel", lhsAcquisitionChannel), LocatorUtils.property(thatLocator, "acquisitionChannel", rhsAcquisitionChannel), lhsAcquisitionChannel, rhsAcquisitionChannel, (this.acquisitionChannel!= null), (that.acquisitionChannel!= null))) {
                return false;
            }
        }
        return true;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(Object object) {
        final EqualsStrategy2 strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode(ObjectLocator locator, HashCodeStrategy2 strategy) {
        int currentHashCode = 1;
        {
            String theAgentLoginId;
            theAgentLoginId = this.getAgentLoginId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "agentLoginId", theAgentLoginId), currentHashCode, theAgentLoginId, (this.agentLoginId!= null));
        }
        {
            String theOpId;
            theOpId = this.getOpId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "opId", theOpId), currentHashCode, theOpId, (this.opId!= null));
        }
        {
            String theCompanyOe;
            theCompanyOe = this.getCompanyOe();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "companyOe", theCompanyOe), currentHashCode, theCompanyOe, (this.companyOe!= null));
        }
        {
            String theCompanyName;
            theCompanyName = this.getCompanyName();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "companyName", theCompanyName), currentHashCode, theCompanyName, (this.companyName!= null));
        }
        {
            String theDistributorCompanyOe;
            theDistributorCompanyOe = this.getDistributorCompanyOe();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "distributorCompanyOe", theDistributorCompanyOe), currentHashCode, theDistributorCompanyOe, (this.distributorCompanyOe!= null));
        }
        {
            String theLocation;
            theLocation = this.getLocation();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "location", theLocation), currentHashCode, theLocation, (this.location!= null));
        }
        {
            String theAcquisitionChannel;
            theAcquisitionChannel = this.getAcquisitionChannel();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "acquisitionChannel", theAcquisitionChannel), currentHashCode, theAcquisitionChannel, (this.acquisitionChannel!= null));
        }
        return currentHashCode;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode() {
        final HashCodeStrategy2 strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String toString() {
        final ToStringStrategy2 strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        {
            String theAgentLoginId;
            theAgentLoginId = this.getAgentLoginId();
            strategy.appendField(locator, this, "agentLoginId", buffer, theAgentLoginId, (this.agentLoginId!= null));
        }
        {
            String theOpId;
            theOpId = this.getOpId();
            strategy.appendField(locator, this, "opId", buffer, theOpId, (this.opId!= null));
        }
        {
            String theCompanyOe;
            theCompanyOe = this.getCompanyOe();
            strategy.appendField(locator, this, "companyOe", buffer, theCompanyOe, (this.companyOe!= null));
        }
        {
            String theCompanyName;
            theCompanyName = this.getCompanyName();
            strategy.appendField(locator, this, "companyName", buffer, theCompanyName, (this.companyName!= null));
        }
        {
            String theDistributorCompanyOe;
            theDistributorCompanyOe = this.getDistributorCompanyOe();
            strategy.appendField(locator, this, "distributorCompanyOe", buffer, theDistributorCompanyOe, (this.distributorCompanyOe!= null));
        }
        {
            String theLocation;
            theLocation = this.getLocation();
            strategy.appendField(locator, this, "location", buffer, theLocation, (this.location!= null));
        }
        {
            String theAcquisitionChannel;
            theAcquisitionChannel = this.getAcquisitionChannel();
            strategy.appendField(locator, this, "acquisitionChannel", buffer, theAcquisitionChannel, (this.acquisitionChannel!= null));
        }
        return buffer;
    }

}
