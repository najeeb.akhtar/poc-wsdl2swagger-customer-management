
package dishnetwork.schemas;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;

import io.swagger.annotations.ApiModelProperty;
import org.jvnet.jaxb2_commons.lang.Equals2;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy2;
import org.jvnet.jaxb2_commons.lang.HashCode2;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy2;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString2;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy2;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for ItemBillingType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="ItemBillingType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="discount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="beginDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="endDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="negotiatedCharge" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="negotiatedChargeOverride" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;pattern value="Y|N|y|n"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="hold" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ItemBillingType", propOrder = {
    "discount",
    "beginDate",
    "endDate",
    "negotiatedCharge",
    "negotiatedChargeOverride",
    "hold"
})
@Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
public class ItemBillingType implements Equals2, HashCode2, ToString2
{

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String discount;
    @XmlSchemaType(name = "date")
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    @ApiModelProperty(example = "2022-07-12T21:32:55.842Z")
    protected String beginDate;
    @XmlSchemaType(name = "date")
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    @ApiModelProperty(example = "2022-07-12T21:32:55.842Z")
    protected String endDate;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String negotiatedCharge;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String negotiatedChargeOverride;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String hold;

    /**
     * Default no-arg constructor
     *
     */
    public ItemBillingType() {
        super();
    }

    /**
     * Fully-initialising value constructor
     *
     */
    public ItemBillingType(final String discount, final String beginDate, final String endDate, final String negotiatedCharge, final String negotiatedChargeOverride, final String hold) {
        this.discount = discount;
        this.beginDate = beginDate;
        this.endDate = endDate;
        this.negotiatedCharge = negotiatedCharge;
        this.negotiatedChargeOverride = negotiatedChargeOverride;
        this.hold = hold;
    }

    /**
     * Gets the value of the discount property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getDiscount() {
        return discount;
    }

    /**
     * Sets the value of the discount property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setDiscount(String value) {
        this.discount = value;
    }

    /**
     * Gets the value of the beginDate property.
     *
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getBeginDate() {
        return beginDate;
    }

    /**
     * Sets the value of the beginDate property.
     *
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setBeginDate(String value) {
        this.beginDate = value;
    }

    /**
     * Gets the value of the endDate property.
     *
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getEndDate() {
        return endDate;
    }

    /**
     * Sets the value of the endDate property.
     *
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setEndDate(String value) {
        this.endDate = value;
    }

    /**
     * Gets the value of the negotiatedCharge property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getNegotiatedCharge() {
        return negotiatedCharge;
    }

    /**
     * Sets the value of the negotiatedCharge property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setNegotiatedCharge(String value) {
        this.negotiatedCharge = value;
    }

    /**
     * Gets the value of the negotiatedChargeOverride property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getNegotiatedChargeOverride() {
        return negotiatedChargeOverride;
    }

    /**
     * Sets the value of the negotiatedChargeOverride property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setNegotiatedChargeOverride(String value) {
        this.negotiatedChargeOverride = value;
    }

    /**
     * Gets the value of the hold property.
     *
     * @return
     *     possible object is
     *     {@link Boolean }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String isHold() {
        return hold;
    }

    /**
     * Sets the value of the hold property.
     *
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setHold(String value) {
        this.hold = value;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy2 strategy) {
        if ((object == null)||(this.getClass()!= object.getClass())) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final ItemBillingType that = ((ItemBillingType) object);
        {
            String lhsDiscount;
            lhsDiscount = this.getDiscount();
            String rhsDiscount;
            rhsDiscount = that.getDiscount();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "discount", lhsDiscount), LocatorUtils.property(thatLocator, "discount", rhsDiscount), lhsDiscount, rhsDiscount, (this.discount!= null), (that.discount!= null))) {
                return false;
            }
        }
        {
            String lhsBeginDate;
            lhsBeginDate = this.getBeginDate();
            String rhsBeginDate;
            rhsBeginDate = that.getBeginDate();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "beginDate", lhsBeginDate), LocatorUtils.property(thatLocator, "beginDate", rhsBeginDate), lhsBeginDate, rhsBeginDate, (this.beginDate!= null), (that.beginDate!= null))) {
                return false;
            }
        }
        {
            String lhsEndDate;
            lhsEndDate = this.getEndDate();
            String rhsEndDate;
            rhsEndDate = that.getEndDate();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "endDate", lhsEndDate), LocatorUtils.property(thatLocator, "endDate", rhsEndDate), lhsEndDate, rhsEndDate, (this.endDate!= null), (that.endDate!= null))) {
                return false;
            }
        }
        {
            String lhsNegotiatedCharge;
            lhsNegotiatedCharge = this.getNegotiatedCharge();
            String rhsNegotiatedCharge;
            rhsNegotiatedCharge = that.getNegotiatedCharge();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "negotiatedCharge", lhsNegotiatedCharge), LocatorUtils.property(thatLocator, "negotiatedCharge", rhsNegotiatedCharge), lhsNegotiatedCharge, rhsNegotiatedCharge, (this.negotiatedCharge!= null), (that.negotiatedCharge!= null))) {
                return false;
            }
        }
        {
            String lhsNegotiatedChargeOverride;
            lhsNegotiatedChargeOverride = this.getNegotiatedChargeOverride();
            String rhsNegotiatedChargeOverride;
            rhsNegotiatedChargeOverride = that.getNegotiatedChargeOverride();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "negotiatedChargeOverride", lhsNegotiatedChargeOverride), LocatorUtils.property(thatLocator, "negotiatedChargeOverride", rhsNegotiatedChargeOverride), lhsNegotiatedChargeOverride, rhsNegotiatedChargeOverride, (this.negotiatedChargeOverride!= null), (that.negotiatedChargeOverride!= null))) {
                return false;
            }
        }
        {
            String lhsHold;
            lhsHold = this.isHold();
            String rhsHold;
            rhsHold = that.isHold();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "hold", lhsHold), LocatorUtils.property(thatLocator, "hold", rhsHold), lhsHold, rhsHold, (this.hold!= null), (that.hold!= null))) {
                return false;
            }
        }
        return true;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(Object object) {
        final EqualsStrategy2 strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode(ObjectLocator locator, HashCodeStrategy2 strategy) {
        int currentHashCode = 1;
        {
            String theDiscount;
            theDiscount = this.getDiscount();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "discount", theDiscount), currentHashCode, theDiscount, (this.discount!= null));
        }
        {
            String theBeginDate;
            theBeginDate = this.getBeginDate();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "beginDate", theBeginDate), currentHashCode, theBeginDate, (this.beginDate!= null));
        }
        {
            String theEndDate;
            theEndDate = this.getEndDate();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "endDate", theEndDate), currentHashCode, theEndDate, (this.endDate!= null));
        }
        {
            String theNegotiatedCharge;
            theNegotiatedCharge = this.getNegotiatedCharge();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "negotiatedCharge", theNegotiatedCharge), currentHashCode, theNegotiatedCharge, (this.negotiatedCharge!= null));
        }
        {
            String theNegotiatedChargeOverride;
            theNegotiatedChargeOverride = this.getNegotiatedChargeOverride();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "negotiatedChargeOverride", theNegotiatedChargeOverride), currentHashCode, theNegotiatedChargeOverride, (this.negotiatedChargeOverride!= null));
        }
        {
            String theHold;
            theHold = this.isHold();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "hold", theHold), currentHashCode, theHold, (this.hold!= null));
        }
        return currentHashCode;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode() {
        final HashCodeStrategy2 strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String toString() {
        final ToStringStrategy2 strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        {
            String theDiscount;
            theDiscount = this.getDiscount();
            strategy.appendField(locator, this, "discount", buffer, theDiscount, (this.discount!= null));
        }
        {
            String theBeginDate;
            theBeginDate = this.getBeginDate();
            strategy.appendField(locator, this, "beginDate", buffer, theBeginDate, (this.beginDate!= null));
        }
        {
            String theEndDate;
            theEndDate = this.getEndDate();
            strategy.appendField(locator, this, "endDate", buffer, theEndDate, (this.endDate!= null));
        }
        {
            String theNegotiatedCharge;
            theNegotiatedCharge = this.getNegotiatedCharge();
            strategy.appendField(locator, this, "negotiatedCharge", buffer, theNegotiatedCharge, (this.negotiatedCharge!= null));
        }
        {
            String theNegotiatedChargeOverride;
            theNegotiatedChargeOverride = this.getNegotiatedChargeOverride();
            strategy.appendField(locator, this, "negotiatedChargeOverride", buffer, theNegotiatedChargeOverride, (this.negotiatedChargeOverride!= null));
        }
        {
            String theHold;
            theHold = this.isHold();
            strategy.appendField(locator, this, "hold", buffer, theHold, (this.hold!= null));
        }
        return buffer;
    }

}
