package dishnetwork.schemas;

import org.jvnet.jaxb2_commons.lang.*;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="questionId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="answerId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="attributeList" type="{http://www.dishnetwork.com/schema/common/AttributeList/2015_10_08}AttributeListTypeType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "questionId",
    "answerId",
    "attributeList"
})
@Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
public class QuestionAnswer implements Equals2, HashCode2, ToString2 {

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String questionId;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String answerId;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected AttributeListTypeType attributeList;

    /**
     * Default no-arg constructor
     */
    public QuestionAnswer() {
        super();
    }

    /**
     * Fully-initialising value constructor
     */
    public QuestionAnswer(final String questionId, final String answerId, final AttributeListTypeType attributeList) {
        this.questionId = questionId;
        this.answerId = answerId;
        this.attributeList = attributeList;
    }

    /**
     * Gets the value of the questionId property.
     *
     * @return possible object is
     * {@link String }
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getQuestionId() {
        return questionId;
    }

    /**
     * Sets the value of the questionId property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setQuestionId(String value) {
        this.questionId = value;
    }

    /**
     * Gets the value of the answerId property.
     *
     * @return possible object is
     * {@link String }
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getAnswerId() {
        return answerId;
    }

    /**
     * Sets the value of the answerId property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setAnswerId(String value) {
        this.answerId = value;
    }

    /**
     * Gets the value of the attributeList property.
     *
     * @return possible object is
     * {@link AttributeListTypeType }
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public AttributeListTypeType getAttributeList() {
        return attributeList;
    }

    /**
     * Sets the value of the attributeList property.
     *
     * @param value allowed object is
     *              {@link AttributeListTypeType }
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setAttributeList(AttributeListTypeType value) {
        this.attributeList = value;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy2 strategy) {
        if ((object == null) || (this.getClass() != object.getClass())) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final QuestionAnswer that = ((QuestionAnswer) object);
        {
            String lhsQuestionId;
            lhsQuestionId = this.getQuestionId();
            String rhsQuestionId;
            rhsQuestionId = that.getQuestionId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "questionId", lhsQuestionId), LocatorUtils.property(thatLocator, "questionId", rhsQuestionId), lhsQuestionId, rhsQuestionId, (this.questionId != null), (that.questionId != null))) {
                return false;
            }
        }
        {
            String lhsAnswerId;
            lhsAnswerId = this.getAnswerId();
            String rhsAnswerId;
            rhsAnswerId = that.getAnswerId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "answerId", lhsAnswerId), LocatorUtils.property(thatLocator, "answerId", rhsAnswerId), lhsAnswerId, rhsAnswerId, (this.answerId != null), (that.answerId != null))) {
                return false;
            }
        }
        {
            AttributeListTypeType lhsAttributeList;
            lhsAttributeList = this.getAttributeList();
            AttributeListTypeType rhsAttributeList;
            rhsAttributeList = that.getAttributeList();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "attributeList", lhsAttributeList), LocatorUtils.property(thatLocator, "attributeList", rhsAttributeList), lhsAttributeList, rhsAttributeList, (this.attributeList != null), (that.attributeList != null))) {
                return false;
            }
        }
        return true;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(Object object) {
        final EqualsStrategy2 strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode(ObjectLocator locator, HashCodeStrategy2 strategy) {
        int currentHashCode = 1;
        {
            String theQuestionId;
            theQuestionId = this.getQuestionId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "questionId", theQuestionId), currentHashCode, theQuestionId, (this.questionId != null));
        }
        {
            String theAnswerId;
            theAnswerId = this.getAnswerId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "answerId", theAnswerId), currentHashCode, theAnswerId, (this.answerId != null));
        }
        {
            AttributeListTypeType theAttributeList;
            theAttributeList = this.getAttributeList();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "attributeList", theAttributeList), currentHashCode, theAttributeList, (this.attributeList != null));
        }
        return currentHashCode;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode() {
        final HashCodeStrategy2 strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String toString() {
        final ToStringStrategy2 strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        {
            String theQuestionId;
            theQuestionId = this.getQuestionId();
            strategy.appendField(locator, this, "questionId", buffer, theQuestionId, (this.questionId != null));
        }
        {
            String theAnswerId;
            theAnswerId = this.getAnswerId();
            strategy.appendField(locator, this, "answerId", buffer, theAnswerId, (this.answerId != null));
        }
        {
            AttributeListTypeType theAttributeList;
            theAttributeList = this.getAttributeList();
            strategy.appendField(locator, this, "attributeList", buffer, theAttributeList, (this.attributeList != null));
        }
        return buffer;
    }

}
