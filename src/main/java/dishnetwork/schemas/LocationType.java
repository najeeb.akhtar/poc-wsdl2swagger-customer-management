
package dishnetwork.schemas;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.jvnet.jaxb2_commons.lang.Equals2;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy2;
import org.jvnet.jaxb2_commons.lang.HashCode2;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy2;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString2;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy2;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * Describe the Location details
 *
 * <p>Java class for LocationType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="LocationType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="locationId" type="{http://www.dishnetwork.com/schema/Customer/LocationId/2011_04_01}LocationIdType" minOccurs="0"/&gt;
 *         &lt;element name="dwellingType" type="{http://www.dishnetwork.com/schema/Location/DwellingType/2009_08_25}DwellingTypeType"/&gt;
 *         &lt;element name="address" type="{http://www.dishnetwork.com/schema/AccountOrder/Address/2016_03_24}AddressType"/&gt;
 *         &lt;element name="hookupType" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="geoCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="dma" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="miscellaneous" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="customField" type="{http://www.dishnetwork.com/schema/common/CustomField/2011_04_01}CustomFieldType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LocationType", propOrder = {
    "locationId",
    "dwellingType",
    "address",
    "hookupType",
    "geoCode",
    "dma",
    "miscellaneous",
    "customField"
})
@Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
public class LocationType implements Equals2, HashCode2, ToString2
{

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected LocationIdType locationId;
    //@XmlElement(required = true)
    @JsonProperty(required = true)
    @XmlSchemaType(name = "string")
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected DwellingTypeType dwellingType;
    //@XmlElement(required = true)
    @JsonProperty(required = true)
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected AddressTypeScrubAddressDetail address;
    //@XmlElement(required = true)
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String geoCode;
    @JsonProperty(required = true)
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String hookupType;
    //@XmlElement(required = true)
    @JsonProperty(required = true)
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String dma;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String miscellaneous;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected List<CustomFieldType> customField;

    /**
     * Default no-arg constructor
     *
     */
    public LocationType() {
        super();
    }

    /**
     * Fully-initialising value constructor
     *
     */
    public LocationType(final LocationIdType locationId, final DwellingTypeType dwellingType, final AddressTypeScrubAddressDetail address, final String hookupType, final String geoCode, final String dma, final String miscellaneous, final List<CustomFieldType> customField) {
        this.locationId = locationId;
        this.dwellingType = dwellingType;
        this.address = address;
        this.hookupType = hookupType;
        this.geoCode = geoCode;
        this.dma = dma;
        this.miscellaneous = miscellaneous;
        this.customField = customField;
    }

    /**
     * Gets the value of the locationId property.
     *
     * @return
     *     possible object is
     *     {@link LocationIdType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public LocationIdType getLocationId() {
        return locationId;
    }

    /**
     * Sets the value of the locationId property.
     *
     * @param value
     *     allowed object is
     *     {@link LocationIdType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setLocationId(LocationIdType value) {
        this.locationId = value;
    }

    /**
     * Gets the value of the dwellingType property.
     *
     * @return
     *     possible object is
     *     {@link DwellingTypeType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public DwellingTypeType getDwellingType() {
        return dwellingType;
    }

    /**
     * Sets the value of the dwellingType property.
     *
     * @param value
     *     allowed object is
     *     {@link DwellingTypeType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setDwellingType(DwellingTypeType value) {
        this.dwellingType = value;
    }

    /**
     * Gets the value of the address property.
     *
     * @return
     *     possible object is
     *     {@link AddressTypeScrubAddressDetail }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public AddressTypeScrubAddressDetail getAddress() {
        return address;
    }

    /**
     * Sets the value of the address property.
     *
     * @param value
     *     allowed object is
     *     {@link AddressTypeScrubAddressDetail }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setAddress(AddressTypeScrubAddressDetail value) {
        this.address = value;
    }

    /**
     * Gets the value of the hookupType property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getHookupType() {
        return hookupType;
    }

    /**
     * Sets the value of the hookupType property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setHookupType(String value) {
        this.hookupType = value;
    }

    /**
     * Gets the value of the geoCode property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getGeoCode() {
        return geoCode;
    }

    /**
     * Sets the value of the geoCode property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setGeoCode(String value) {
        this.geoCode = value;
    }

    /**
     * Gets the value of the dma property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getDma() {
        return dma;
    }

    /**
     * Sets the value of the dma property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setDma(String value) {
        this.dma = value;
    }

    /**
     * Gets the value of the miscellaneous property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getMiscellaneous() {
        return miscellaneous;
    }

    /**
     * Sets the value of the miscellaneous property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setMiscellaneous(String value) {
        this.miscellaneous = value;
    }

    /**
     * Gets the value of the customField property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the customField property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCustomField().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CustomFieldType }
     *
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public List<CustomFieldType> getCustomField() {
        if (customField == null) {
            customField = new ArrayList<CustomFieldType>();
        }
        return this.customField;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy2 strategy) {
        if ((object == null)||(this.getClass()!= object.getClass())) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final LocationType that = ((LocationType) object);
        {
            LocationIdType lhsLocationId;
            lhsLocationId = this.getLocationId();
            LocationIdType rhsLocationId;
            rhsLocationId = that.getLocationId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "locationId", lhsLocationId), LocatorUtils.property(thatLocator, "locationId", rhsLocationId), lhsLocationId, rhsLocationId, (this.locationId!= null), (that.locationId!= null))) {
                return false;
            }
        }
        {
            DwellingTypeType lhsDwellingType;
            lhsDwellingType = this.getDwellingType();
            DwellingTypeType rhsDwellingType;
            rhsDwellingType = that.getDwellingType();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "dwellingType", lhsDwellingType), LocatorUtils.property(thatLocator, "dwellingType", rhsDwellingType), lhsDwellingType, rhsDwellingType, (this.dwellingType!= null), (that.dwellingType!= null))) {
                return false;
            }
        }
        {
            AddressTypeScrubAddressDetail lhsAddress;
            lhsAddress = this.getAddress();
            AddressTypeScrubAddressDetail rhsAddress;
            rhsAddress = that.getAddress();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "address", lhsAddress), LocatorUtils.property(thatLocator, "address", rhsAddress), lhsAddress, rhsAddress, (this.address!= null), (that.address!= null))) {
                return false;
            }
        }
        {
            String lhsHookupType;
            lhsHookupType = this.getHookupType();
            String rhsHookupType;
            rhsHookupType = that.getHookupType();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "hookupType", lhsHookupType), LocatorUtils.property(thatLocator, "hookupType", rhsHookupType), lhsHookupType, rhsHookupType, (this.hookupType!= null), (that.hookupType!= null))) {
                return false;
            }
        }
        {
            String lhsGeoCode;
            lhsGeoCode = this.getGeoCode();
            String rhsGeoCode;
            rhsGeoCode = that.getGeoCode();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "geoCode", lhsGeoCode), LocatorUtils.property(thatLocator, "geoCode", rhsGeoCode), lhsGeoCode, rhsGeoCode, (this.geoCode!= null), (that.geoCode!= null))) {
                return false;
            }
        }
        {
            String lhsDma;
            lhsDma = this.getDma();
            String rhsDma;
            rhsDma = that.getDma();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "dma", lhsDma), LocatorUtils.property(thatLocator, "dma", rhsDma), lhsDma, rhsDma, (this.dma!= null), (that.dma!= null))) {
                return false;
            }
        }
        {
            String lhsMiscellaneous;
            lhsMiscellaneous = this.getMiscellaneous();
            String rhsMiscellaneous;
            rhsMiscellaneous = that.getMiscellaneous();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "miscellaneous", lhsMiscellaneous), LocatorUtils.property(thatLocator, "miscellaneous", rhsMiscellaneous), lhsMiscellaneous, rhsMiscellaneous, (this.miscellaneous!= null), (that.miscellaneous!= null))) {
                return false;
            }
        }
        {
            List<CustomFieldType> lhsCustomField;
            lhsCustomField = (((this.customField!= null)&&(!this.customField.isEmpty()))?this.getCustomField():null);
            List<CustomFieldType> rhsCustomField;
            rhsCustomField = (((that.customField!= null)&&(!that.customField.isEmpty()))?that.getCustomField():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "customField", lhsCustomField), LocatorUtils.property(thatLocator, "customField", rhsCustomField), lhsCustomField, rhsCustomField, ((this.customField!= null)&&(!this.customField.isEmpty())), ((that.customField!= null)&&(!that.customField.isEmpty())))) {
                return false;
            }
        }
        return true;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(Object object) {
        final EqualsStrategy2 strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode(ObjectLocator locator, HashCodeStrategy2 strategy) {
        int currentHashCode = 1;
        {
            LocationIdType theLocationId;
            theLocationId = this.getLocationId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "locationId", theLocationId), currentHashCode, theLocationId, (this.locationId!= null));
        }
        {
            DwellingTypeType theDwellingType;
            theDwellingType = this.getDwellingType();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "dwellingType", theDwellingType), currentHashCode, theDwellingType, (this.dwellingType!= null));
        }
        {
            AddressTypeScrubAddressDetail theAddress;
            theAddress = this.getAddress();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "address", theAddress), currentHashCode, theAddress, (this.address!= null));
        }
        {
            String theHookupType;
            theHookupType = this.getHookupType();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "hookupType", theHookupType), currentHashCode, theHookupType, (this.hookupType!= null));
        }
        {
            String theGeoCode;
            theGeoCode = this.getGeoCode();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "geoCode", theGeoCode), currentHashCode, theGeoCode, (this.geoCode!= null));
        }
        {
            String theDma;
            theDma = this.getDma();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "dma", theDma), currentHashCode, theDma, (this.dma!= null));
        }
        {
            String theMiscellaneous;
            theMiscellaneous = this.getMiscellaneous();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "miscellaneous", theMiscellaneous), currentHashCode, theMiscellaneous, (this.miscellaneous!= null));
        }
        {
            List<CustomFieldType> theCustomField;
            theCustomField = (((this.customField!= null)&&(!this.customField.isEmpty()))?this.getCustomField():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "customField", theCustomField), currentHashCode, theCustomField, ((this.customField!= null)&&(!this.customField.isEmpty())));
        }
        return currentHashCode;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode() {
        final HashCodeStrategy2 strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String toString() {
        final ToStringStrategy2 strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        {
            LocationIdType theLocationId;
            theLocationId = this.getLocationId();
            strategy.appendField(locator, this, "locationId", buffer, theLocationId, (this.locationId!= null));
        }
        {
            DwellingTypeType theDwellingType;
            theDwellingType = this.getDwellingType();
            strategy.appendField(locator, this, "dwellingType", buffer, theDwellingType, (this.dwellingType!= null));
        }
        {
            AddressTypeScrubAddressDetail theAddress;
            theAddress = this.getAddress();
            strategy.appendField(locator, this, "address", buffer, theAddress, (this.address!= null));
        }
        {
            String theHookupType;
            theHookupType = this.getHookupType();
            strategy.appendField(locator, this, "hookupType", buffer, theHookupType, (this.hookupType!= null));
        }
        {
            String theGeoCode;
            theGeoCode = this.getGeoCode();
            strategy.appendField(locator, this, "geoCode", buffer, theGeoCode, (this.geoCode!= null));
        }
        {
            String theDma;
            theDma = this.getDma();
            strategy.appendField(locator, this, "dma", buffer, theDma, (this.dma!= null));
        }
        {
            String theMiscellaneous;
            theMiscellaneous = this.getMiscellaneous();
            strategy.appendField(locator, this, "miscellaneous", buffer, theMiscellaneous, (this.miscellaneous!= null));
        }
        {
            List<CustomFieldType> theCustomField;
            theCustomField = (((this.customField!= null)&&(!this.customField.isEmpty()))?this.getCustomField():null);
            strategy.appendField(locator, this, "customField", buffer, theCustomField, ((this.customField!= null)&&(!this.customField.isEmpty())));
        }
        return buffer;
    }

}
