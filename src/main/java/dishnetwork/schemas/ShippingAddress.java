package dishnetwork.schemas;

import org.jvnet.jaxb2_commons.lang.*;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="addressLineOne" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="addressLineTwo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="city" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="state" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="zip" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="zipPlus4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="attributeList" type="{http://www.dishnetwork.com/schema/common/AttributeList/2015_10_08}AttributeListTypeType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "addressLineOne",
    "addressLineTwo",
    "city",
    "state",
    "zip",
    "zipPlus4",
    "attributeList"
})
@Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
public class ShippingAddress implements Equals2, HashCode2, ToString2 {

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String addressLineOne;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String addressLineTwo;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String city;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String state;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String zip;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String zipPlus4;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected AttributeListTypeType attributeList;

    /**
     * Default no-arg constructor
     */
    public ShippingAddress() {
        super();
    }

    /**
     * Fully-initialising value constructor
     */
    public ShippingAddress(final String addressLineOne, final String addressLineTwo, final String city, final String state, final String zip, final String zipPlus4, final AttributeListTypeType attributeList) {
        this.addressLineOne = addressLineOne;
        this.addressLineTwo = addressLineTwo;
        this.city = city;
        this.state = state;
        this.zip = zip;
        this.zipPlus4 = zipPlus4;
        this.attributeList = attributeList;
    }

    /**
     * Gets the value of the addressLineOne property.
     *
     * @return possible object is
     * {@link String }
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getAddressLineOne() {
        return addressLineOne;
    }

    /**
     * Sets the value of the addressLineOne property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setAddressLineOne(String value) {
        this.addressLineOne = value;
    }

    /**
     * Gets the value of the addressLineTwo property.
     *
     * @return possible object is
     * {@link String }
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getAddressLineTwo() {
        return addressLineTwo;
    }

    /**
     * Sets the value of the addressLineTwo property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setAddressLineTwo(String value) {
        this.addressLineTwo = value;
    }

    /**
     * Gets the value of the city property.
     *
     * @return possible object is
     * {@link String }
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getCity() {
        return city;
    }

    /**
     * Sets the value of the city property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setCity(String value) {
        this.city = value;
    }

    /**
     * Gets the value of the state property.
     *
     * @return possible object is
     * {@link String }
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getState() {
        return state;
    }

    /**
     * Sets the value of the state property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setState(String value) {
        this.state = value;
    }

    /**
     * Gets the value of the zip property.
     *
     * @return possible object is
     * {@link String }
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getZip() {
        return zip;
    }

    /**
     * Sets the value of the zip property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setZip(String value) {
        this.zip = value;
    }

    /**
     * Gets the value of the zipPlus4 property.
     *
     * @return possible object is
     * {@link String }
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getZipPlus4() {
        return zipPlus4;
    }

    /**
     * Sets the value of the zipPlus4 property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setZipPlus4(String value) {
        this.zipPlus4 = value;
    }

    /**
     * Gets the value of the attributeList property.
     *
     * @return possible object is
     * {@link AttributeListTypeType }
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public AttributeListTypeType getAttributeList() {
        return attributeList;
    }

    /**
     * Sets the value of the attributeList property.
     *
     * @param value allowed object is
     *              {@link AttributeListTypeType }
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setAttributeList(AttributeListTypeType value) {
        this.attributeList = value;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy2 strategy) {
        if ((object == null) || (this.getClass() != object.getClass())) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final ShippingAddress that = ((ShippingAddress) object);
        {
            String lhsAddressLineOne;
            lhsAddressLineOne = this.getAddressLineOne();
            String rhsAddressLineOne;
            rhsAddressLineOne = that.getAddressLineOne();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "addressLineOne", lhsAddressLineOne), LocatorUtils.property(thatLocator, "addressLineOne", rhsAddressLineOne), lhsAddressLineOne, rhsAddressLineOne, (this.addressLineOne != null), (that.addressLineOne != null))) {
                return false;
            }
        }
        {
            String lhsAddressLineTwo;
            lhsAddressLineTwo = this.getAddressLineTwo();
            String rhsAddressLineTwo;
            rhsAddressLineTwo = that.getAddressLineTwo();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "addressLineTwo", lhsAddressLineTwo), LocatorUtils.property(thatLocator, "addressLineTwo", rhsAddressLineTwo), lhsAddressLineTwo, rhsAddressLineTwo, (this.addressLineTwo != null), (that.addressLineTwo != null))) {
                return false;
            }
        }
        {
            String lhsCity;
            lhsCity = this.getCity();
            String rhsCity;
            rhsCity = that.getCity();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "city", lhsCity), LocatorUtils.property(thatLocator, "city", rhsCity), lhsCity, rhsCity, (this.city != null), (that.city != null))) {
                return false;
            }
        }
        {
            String lhsState;
            lhsState = this.getState();
            String rhsState;
            rhsState = that.getState();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "state", lhsState), LocatorUtils.property(thatLocator, "state", rhsState), lhsState, rhsState, (this.state != null), (that.state != null))) {
                return false;
            }
        }
        {
            String lhsZip;
            lhsZip = this.getZip();
            String rhsZip;
            rhsZip = that.getZip();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "zip", lhsZip), LocatorUtils.property(thatLocator, "zip", rhsZip), lhsZip, rhsZip, (this.zip != null), (that.zip != null))) {
                return false;
            }
        }
        {
            String lhsZipPlus4;
            lhsZipPlus4 = this.getZipPlus4();
            String rhsZipPlus4;
            rhsZipPlus4 = that.getZipPlus4();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "zipPlus4", lhsZipPlus4), LocatorUtils.property(thatLocator, "zipPlus4", rhsZipPlus4), lhsZipPlus4, rhsZipPlus4, (this.zipPlus4 != null), (that.zipPlus4 != null))) {
                return false;
            }
        }
        {
            AttributeListTypeType lhsAttributeList;
            lhsAttributeList = this.getAttributeList();
            AttributeListTypeType rhsAttributeList;
            rhsAttributeList = that.getAttributeList();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "attributeList", lhsAttributeList), LocatorUtils.property(thatLocator, "attributeList", rhsAttributeList), lhsAttributeList, rhsAttributeList, (this.attributeList != null), (that.attributeList != null))) {
                return false;
            }
        }
        return true;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(Object object) {
        final EqualsStrategy2 strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode(ObjectLocator locator, HashCodeStrategy2 strategy) {
        int currentHashCode = 1;
        {
            String theAddressLineOne;
            theAddressLineOne = this.getAddressLineOne();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "addressLineOne", theAddressLineOne), currentHashCode, theAddressLineOne, (this.addressLineOne != null));
        }
        {
            String theAddressLineTwo;
            theAddressLineTwo = this.getAddressLineTwo();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "addressLineTwo", theAddressLineTwo), currentHashCode, theAddressLineTwo, (this.addressLineTwo != null));
        }
        {
            String theCity;
            theCity = this.getCity();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "city", theCity), currentHashCode, theCity, (this.city != null));
        }
        {
            String theState;
            theState = this.getState();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "state", theState), currentHashCode, theState, (this.state != null));
        }
        {
            String theZip;
            theZip = this.getZip();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "zip", theZip), currentHashCode, theZip, (this.zip != null));
        }
        {
            String theZipPlus4;
            theZipPlus4 = this.getZipPlus4();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "zipPlus4", theZipPlus4), currentHashCode, theZipPlus4, (this.zipPlus4 != null));
        }
        {
            AttributeListTypeType theAttributeList;
            theAttributeList = this.getAttributeList();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "attributeList", theAttributeList), currentHashCode, theAttributeList, (this.attributeList != null));
        }
        return currentHashCode;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode() {
        final HashCodeStrategy2 strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String toString() {
        final ToStringStrategy2 strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        {
            String theAddressLineOne;
            theAddressLineOne = this.getAddressLineOne();
            strategy.appendField(locator, this, "addressLineOne", buffer, theAddressLineOne, (this.addressLineOne != null));
        }
        {
            String theAddressLineTwo;
            theAddressLineTwo = this.getAddressLineTwo();
            strategy.appendField(locator, this, "addressLineTwo", buffer, theAddressLineTwo, (this.addressLineTwo != null));
        }
        {
            String theCity;
            theCity = this.getCity();
            strategy.appendField(locator, this, "city", buffer, theCity, (this.city != null));
        }
        {
            String theState;
            theState = this.getState();
            strategy.appendField(locator, this, "state", buffer, theState, (this.state != null));
        }
        {
            String theZip;
            theZip = this.getZip();
            strategy.appendField(locator, this, "zip", buffer, theZip, (this.zip != null));
        }
        {
            String theZipPlus4;
            theZipPlus4 = this.getZipPlus4();
            strategy.appendField(locator, this, "zipPlus4", buffer, theZipPlus4, (this.zipPlus4 != null));
        }
        {
            AttributeListTypeType theAttributeList;
            theAttributeList = this.getAttributeList();
            strategy.appendField(locator, this, "attributeList", buffer, theAttributeList, (this.attributeList != null));
        }
        return buffer;
    }

}
