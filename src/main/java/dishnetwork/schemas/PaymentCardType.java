
package dishnetwork.schemas;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.jvnet.jaxb2_commons.lang.Equals2;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy2;
import org.jvnet.jaxb2_commons.lang.HashCode2;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy2;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString2;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy2;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * Customer Payment Card Information
 *
 * <p>Java class for PaymentCardType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="PaymentCardType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="number"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;pattern value="\d{15,20}"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="expirationDate" type="{http://www.dishnetwork.com/schema/Payment/PaymentCard/2022_05_12}ExpirationDateType"/&gt;
 *         &lt;element name="securityCode" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;pattern value="\d{3,4}"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="securityCodeOverride" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="billingAddress" type="{http://www.dishnetwork.com/schema/Location/Address/2009_08_25}AddressType" minOccurs="0"/&gt;
 *         &lt;element name="nameOnCard" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="brandName" type="{http://www.dishnetwork.com/schema/Payment/PaymentCard/2022_05_12}PaymentCardBrandType" minOccurs="0"/&gt;
 *         &lt;element name="cardProcessingType" type="{http://www.dishnetwork.com/schema/Payment/PaymentCard/2022_05_12}ProcessingTypeType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaymentCardType", propOrder = {
    "number",
    "expirationDate",
    "securityCode",
    "securityCodeOverride",
    "billingAddress",
    "nameOnCard",
    "brandName",
    "cardProcessingType"
})
@Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
public class PaymentCardType implements Equals2, HashCode2, ToString2
{

    //@XmlElement(required = true)
    @JsonProperty(required = true)
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String number;
    //@XmlElement(required = true)
    @JsonProperty(required = true)
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected ExpirationDateType expirationDate;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String securityCode;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String securityCodeOverride;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected AddressType billingAddress;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String nameOnCard;
    @XmlSchemaType(name = "string")
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected PaymentCardBrandType brandName;
    @XmlSchemaType(name = "string")
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected ProcessingTypeType cardProcessingType;

    /**
     * Default no-arg constructor
     *
     */
    public PaymentCardType() {
        super();
    }

    /**
     * Fully-initialising value constructor
     *
     */
    public PaymentCardType(final String number, final ExpirationDateType expirationDate, final String securityCode, final String securityCodeOverride, final AddressType billingAddress, final String nameOnCard, final PaymentCardBrandType brandName, final ProcessingTypeType cardProcessingType) {
        this.number = number;
        this.expirationDate = expirationDate;
        this.securityCode = securityCode;
        this.securityCodeOverride = securityCodeOverride;
        this.billingAddress = billingAddress;
        this.nameOnCard = nameOnCard;
        this.brandName = brandName;
        this.cardProcessingType = cardProcessingType;
    }

    /**
     * Gets the value of the number property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getNumber() {
        return number;
    }

    /**
     * Sets the value of the number property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setNumber(String value) {
        this.number = value;
    }

    /**
     * Gets the value of the expirationDate property.
     *
     * @return
     *     possible object is
     *     {@link ExpirationDateType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public ExpirationDateType getExpirationDate() {
        return expirationDate;
    }

    /**
     * Sets the value of the expirationDate property.
     *
     * @param value
     *     allowed object is
     *     {@link ExpirationDateType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setExpirationDate(ExpirationDateType value) {
        this.expirationDate = value;
    }

    /**
     * Gets the value of the securityCode property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getSecurityCode() {
        return securityCode;
    }

    /**
     * Sets the value of the securityCode property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setSecurityCode(String value) {
        this.securityCode = value;
    }

    /**
     * Gets the value of the securityCodeOverride property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getSecurityCodeOverride() {
        return securityCodeOverride;
    }

    /**
     * Sets the value of the securityCodeOverride property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setSecurityCodeOverride(String value) {
        this.securityCodeOverride = value;
    }

    /**
     * Gets the value of the billingAddress property.
     *
     * @return
     *     possible object is
     *     {@link AddressType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public AddressType getBillingAddress() {
        return billingAddress;
    }

    /**
     * Sets the value of the billingAddress property.
     *
     * @param value
     *     allowed object is
     *     {@link AddressType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setBillingAddress(AddressType value) {
        this.billingAddress = value;
    }

    /**
     * Gets the value of the nameOnCard property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getNameOnCard() {
        return nameOnCard;
    }

    /**
     * Sets the value of the nameOnCard property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setNameOnCard(String value) {
        this.nameOnCard = value;
    }

    /**
     * Gets the value of the brandName property.
     *
     * @return
     *     possible object is
     *     {@link PaymentCardBrandType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public PaymentCardBrandType getBrandName() {
        return brandName;
    }

    /**
     * Sets the value of the brandName property.
     *
     * @param value
     *     allowed object is
     *     {@link PaymentCardBrandType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setBrandName(PaymentCardBrandType value) {
        this.brandName = value;
    }

    /**
     * Gets the value of the cardProcessingType property.
     *
     * @return
     *     possible object is
     *     {@link ProcessingTypeType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public ProcessingTypeType getCardProcessingType() {
        return cardProcessingType;
    }

    /**
     * Sets the value of the cardProcessingType property.
     *
     * @param value
     *     allowed object is
     *     {@link ProcessingTypeType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setCardProcessingType(ProcessingTypeType value) {
        this.cardProcessingType = value;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy2 strategy) {
        if ((object == null)||(this.getClass()!= object.getClass())) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final PaymentCardType that = ((PaymentCardType) object);
        {
            String lhsNumber;
            lhsNumber = this.getNumber();
            String rhsNumber;
            rhsNumber = that.getNumber();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "number", lhsNumber), LocatorUtils.property(thatLocator, "number", rhsNumber), lhsNumber, rhsNumber, (this.number!= null), (that.number!= null))) {
                return false;
            }
        }
        {
            ExpirationDateType lhsExpirationDate;
            lhsExpirationDate = this.getExpirationDate();
            ExpirationDateType rhsExpirationDate;
            rhsExpirationDate = that.getExpirationDate();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "expirationDate", lhsExpirationDate), LocatorUtils.property(thatLocator, "expirationDate", rhsExpirationDate), lhsExpirationDate, rhsExpirationDate, (this.expirationDate!= null), (that.expirationDate!= null))) {
                return false;
            }
        }
        {
            String lhsSecurityCode;
            lhsSecurityCode = this.getSecurityCode();
            String rhsSecurityCode;
            rhsSecurityCode = that.getSecurityCode();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "securityCode", lhsSecurityCode), LocatorUtils.property(thatLocator, "securityCode", rhsSecurityCode), lhsSecurityCode, rhsSecurityCode, (this.securityCode!= null), (that.securityCode!= null))) {
                return false;
            }
        }
        {
            String lhsSecurityCodeOverride;
            lhsSecurityCodeOverride = this.getSecurityCodeOverride();
            String rhsSecurityCodeOverride;
            rhsSecurityCodeOverride = that.getSecurityCodeOverride();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "securityCodeOverride", lhsSecurityCodeOverride), LocatorUtils.property(thatLocator, "securityCodeOverride", rhsSecurityCodeOverride), lhsSecurityCodeOverride, rhsSecurityCodeOverride, (this.securityCodeOverride!= null), (that.securityCodeOverride!= null))) {
                return false;
            }
        }
        {
            AddressType lhsBillingAddress;
            lhsBillingAddress = this.getBillingAddress();
            AddressType rhsBillingAddress;
            rhsBillingAddress = that.getBillingAddress();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "billingAddress", lhsBillingAddress), LocatorUtils.property(thatLocator, "billingAddress", rhsBillingAddress), lhsBillingAddress, rhsBillingAddress, (this.billingAddress!= null), (that.billingAddress!= null))) {
                return false;
            }
        }
        {
            String lhsNameOnCard;
            lhsNameOnCard = this.getNameOnCard();
            String rhsNameOnCard;
            rhsNameOnCard = that.getNameOnCard();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "nameOnCard", lhsNameOnCard), LocatorUtils.property(thatLocator, "nameOnCard", rhsNameOnCard), lhsNameOnCard, rhsNameOnCard, (this.nameOnCard!= null), (that.nameOnCard!= null))) {
                return false;
            }
        }
        {
            PaymentCardBrandType lhsBrandName;
            lhsBrandName = this.getBrandName();
            PaymentCardBrandType rhsBrandName;
            rhsBrandName = that.getBrandName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "brandName", lhsBrandName), LocatorUtils.property(thatLocator, "brandName", rhsBrandName), lhsBrandName, rhsBrandName, (this.brandName!= null), (that.brandName!= null))) {
                return false;
            }
        }
        {
            ProcessingTypeType lhsCardProcessingType;
            lhsCardProcessingType = this.getCardProcessingType();
            ProcessingTypeType rhsCardProcessingType;
            rhsCardProcessingType = that.getCardProcessingType();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "cardProcessingType", lhsCardProcessingType), LocatorUtils.property(thatLocator, "cardProcessingType", rhsCardProcessingType), lhsCardProcessingType, rhsCardProcessingType, (this.cardProcessingType!= null), (that.cardProcessingType!= null))) {
                return false;
            }
        }
        return true;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(Object object) {
        final EqualsStrategy2 strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode(ObjectLocator locator, HashCodeStrategy2 strategy) {
        int currentHashCode = 1;
        {
            String theNumber;
            theNumber = this.getNumber();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "number", theNumber), currentHashCode, theNumber, (this.number!= null));
        }
        {
            ExpirationDateType theExpirationDate;
            theExpirationDate = this.getExpirationDate();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "expirationDate", theExpirationDate), currentHashCode, theExpirationDate, (this.expirationDate!= null));
        }
        {
            String theSecurityCode;
            theSecurityCode = this.getSecurityCode();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "securityCode", theSecurityCode), currentHashCode, theSecurityCode, (this.securityCode!= null));
        }
        {
            String theSecurityCodeOverride;
            theSecurityCodeOverride = this.getSecurityCodeOverride();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "securityCodeOverride", theSecurityCodeOverride), currentHashCode, theSecurityCodeOverride, (this.securityCodeOverride!= null));
        }
        {
            AddressType theBillingAddress;
            theBillingAddress = this.getBillingAddress();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "billingAddress", theBillingAddress), currentHashCode, theBillingAddress, (this.billingAddress!= null));
        }
        {
            String theNameOnCard;
            theNameOnCard = this.getNameOnCard();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "nameOnCard", theNameOnCard), currentHashCode, theNameOnCard, (this.nameOnCard!= null));
        }
        {
            PaymentCardBrandType theBrandName;
            theBrandName = this.getBrandName();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "brandName", theBrandName), currentHashCode, theBrandName, (this.brandName!= null));
        }
        {
            ProcessingTypeType theCardProcessingType;
            theCardProcessingType = this.getCardProcessingType();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "cardProcessingType", theCardProcessingType), currentHashCode, theCardProcessingType, (this.cardProcessingType!= null));
        }
        return currentHashCode;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode() {
        final HashCodeStrategy2 strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String toString() {
        final ToStringStrategy2 strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        {
            String theNumber;
            theNumber = this.getNumber();
            strategy.appendField(locator, this, "number", buffer, theNumber, (this.number!= null));
        }
        {
            ExpirationDateType theExpirationDate;
            theExpirationDate = this.getExpirationDate();
            strategy.appendField(locator, this, "expirationDate", buffer, theExpirationDate, (this.expirationDate!= null));
        }
        {
            String theSecurityCode;
            theSecurityCode = this.getSecurityCode();
            strategy.appendField(locator, this, "securityCode", buffer, theSecurityCode, (this.securityCode!= null));
        }
        {
            String theSecurityCodeOverride;
            theSecurityCodeOverride = this.getSecurityCodeOverride();
            strategy.appendField(locator, this, "securityCodeOverride", buffer, theSecurityCodeOverride, (this.securityCodeOverride!= null));
        }
        {
            AddressType theBillingAddress;
            theBillingAddress = this.getBillingAddress();
            strategy.appendField(locator, this, "billingAddress", buffer, theBillingAddress, (this.billingAddress!= null));
        }
        {
            String theNameOnCard;
            theNameOnCard = this.getNameOnCard();
            strategy.appendField(locator, this, "nameOnCard", buffer, theNameOnCard, (this.nameOnCard!= null));
        }
        {
            PaymentCardBrandType theBrandName;
            theBrandName = this.getBrandName();
            strategy.appendField(locator, this, "brandName", buffer, theBrandName, (this.brandName!= null));
        }
        {
            ProcessingTypeType theCardProcessingType;
            theCardProcessingType = this.getCardProcessingType();
            strategy.appendField(locator, this, "cardProcessingType", buffer, theCardProcessingType, (this.cardProcessingType!= null));
        }
        return buffer;
    }

}
