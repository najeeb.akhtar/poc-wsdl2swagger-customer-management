
package dishnetwork.schemas;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.jvnet.jaxb2_commons.lang.Equals2;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy2;
import org.jvnet.jaxb2_commons.lang.HashCode2;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy2;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString2;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy2;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for OfferCodeInfoType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="OfferCodeInfoType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="promotionalClaimCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="offerCodeInfo" type="{http://www.dishnetwork.com/schema/OfferCode/OfferCode/2013_01_17}OfferCodeType" minOccurs="0"/&gt;
 *         &lt;element name="gifts" type="{http://www.dishnetwork.com/schema/common/GiftList/2014_06_13}GiftsListType" minOccurs="0"/&gt;
 *         &lt;element name="sellingEntity" type="{http://www.dishnetwork.com/schema/OfferCode/OfferCodeSellingEntity/2013_01_17}OfferCodeSellingEntityType"/&gt;
 *         &lt;element name="referringEntity" type="{http://www.dishnetwork.com/schema/OfferCode/OfferCodeReferringEntity/2013_01_17}OfferCodeReferringEntityType" minOccurs="0"/&gt;
 *         &lt;element name="orderEntity" type="{http://www.dishnetwork.com/schema/OfferCode/OfferCodeOrderEntity/2013_01_17}OfferCodeOrderEntityType" minOccurs="0"/&gt;
 *         &lt;element name="commisionableEntity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OfferCodeInfoType", propOrder = {
    "promotionalClaimCode",
    "offerCodeInfo",
    "gifts",
    "sellingEntity",
    "referringEntity",
    "orderEntity",
    "commisionableEntity"
})
@Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
public class OfferCodeInfoType implements Equals2, HashCode2, ToString2
{

    //@XmlElement(nillable = true)
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String promotionalClaimCode;
    //@XmlElement(nillable = true)
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected OfferCodeType offerCodeInfo;
    //@XmlElement(nillable = true)
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected GiftsListType gifts;
    //@XmlElement(required = true)
    @JsonProperty(required = true)
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected OfferCodeSellingEntityType sellingEntity;
    //@XmlElement(nillable = true)
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected OfferCodeReferringEntityType referringEntity;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected OfferCodeOrderEntityType orderEntity;
    //@XmlElement(nillable = true)
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String commisionableEntity;

    /**
     * Default no-arg constructor
     *
     */
    public OfferCodeInfoType() {
        super();
    }

    /**
     * Fully-initialising value constructor
     *
     */
    public OfferCodeInfoType(final String promotionalClaimCode, final OfferCodeType offerCodeInfo, final GiftsListType gifts, final OfferCodeSellingEntityType sellingEntity, final OfferCodeReferringEntityType referringEntity, final OfferCodeOrderEntityType orderEntity, final String commisionableEntity) {
        this.promotionalClaimCode = promotionalClaimCode;
        this.offerCodeInfo = offerCodeInfo;
        this.gifts = gifts;
        this.sellingEntity = sellingEntity;
        this.referringEntity = referringEntity;
        this.orderEntity = orderEntity;
        this.commisionableEntity = commisionableEntity;
    }

    /**
     * Gets the value of the promotionalClaimCode property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getPromotionalClaimCode() {
        return promotionalClaimCode;
    }

    /**
     * Sets the value of the promotionalClaimCode property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setPromotionalClaimCode(String value) {
        this.promotionalClaimCode = value;
    }

    /**
     * Gets the value of the offerCodeInfo property.
     *
     * @return
     *     possible object is
     *     {@link OfferCodeType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public OfferCodeType getOfferCodeInfo() {
        return offerCodeInfo;
    }

    /**
     * Sets the value of the offerCodeInfo property.
     *
     * @param value
     *     allowed object is
     *     {@link OfferCodeType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setOfferCodeInfo(OfferCodeType value) {
        this.offerCodeInfo = value;
    }

    /**
     * Gets the value of the gifts property.
     *
     * @return
     *     possible object is
     *     {@link GiftsListType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public GiftsListType getGifts() {
        return gifts;
    }

    /**
     * Sets the value of the gifts property.
     *
     * @param value
     *     allowed object is
     *     {@link GiftsListType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setGifts(GiftsListType value) {
        this.gifts = value;
    }

    /**
     * Gets the value of the sellingEntity property.
     *
     * @return
     *     possible object is
     *     {@link OfferCodeSellingEntityType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public OfferCodeSellingEntityType getSellingEntity() {
        return sellingEntity;
    }

    /**
     * Sets the value of the sellingEntity property.
     *
     * @param value
     *     allowed object is
     *     {@link OfferCodeSellingEntityType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setSellingEntity(OfferCodeSellingEntityType value) {
        this.sellingEntity = value;
    }

    /**
     * Gets the value of the referringEntity property.
     *
     * @return
     *     possible object is
     *     {@link OfferCodeReferringEntityType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public OfferCodeReferringEntityType getReferringEntity() {
        return referringEntity;
    }

    /**
     * Sets the value of the referringEntity property.
     *
     * @param value
     *     allowed object is
     *     {@link OfferCodeReferringEntityType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setReferringEntity(OfferCodeReferringEntityType value) {
        this.referringEntity = value;
    }

    /**
     * Gets the value of the orderEntity property.
     *
     * @return
     *     possible object is
     *     {@link OfferCodeOrderEntityType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public OfferCodeOrderEntityType getOrderEntity() {
        return orderEntity;
    }

    /**
     * Sets the value of the orderEntity property.
     *
     * @param value
     *     allowed object is
     *     {@link OfferCodeOrderEntityType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setOrderEntity(OfferCodeOrderEntityType value) {
        this.orderEntity = value;
    }

    /**
     * Gets the value of the commisionableEntity property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getCommisionableEntity() {
        return commisionableEntity;
    }

    /**
     * Sets the value of the commisionableEntity property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setCommisionableEntity(String value) {
        this.commisionableEntity = value;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy2 strategy) {
        if ((object == null)||(this.getClass()!= object.getClass())) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final OfferCodeInfoType that = ((OfferCodeInfoType) object);
        {
            String lhsPromotionalClaimCode;
            lhsPromotionalClaimCode = this.getPromotionalClaimCode();
            String rhsPromotionalClaimCode;
            rhsPromotionalClaimCode = that.getPromotionalClaimCode();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "promotionalClaimCode", lhsPromotionalClaimCode), LocatorUtils.property(thatLocator, "promotionalClaimCode", rhsPromotionalClaimCode), lhsPromotionalClaimCode, rhsPromotionalClaimCode, (this.promotionalClaimCode!= null), (that.promotionalClaimCode!= null))) {
                return false;
            }
        }
        {
            OfferCodeType lhsOfferCodeInfo;
            lhsOfferCodeInfo = this.getOfferCodeInfo();
            OfferCodeType rhsOfferCodeInfo;
            rhsOfferCodeInfo = that.getOfferCodeInfo();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "offerCodeInfo", lhsOfferCodeInfo), LocatorUtils.property(thatLocator, "offerCodeInfo", rhsOfferCodeInfo), lhsOfferCodeInfo, rhsOfferCodeInfo, (this.offerCodeInfo!= null), (that.offerCodeInfo!= null))) {
                return false;
            }
        }
        {
            GiftsListType lhsGifts;
            lhsGifts = this.getGifts();
            GiftsListType rhsGifts;
            rhsGifts = that.getGifts();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "gifts", lhsGifts), LocatorUtils.property(thatLocator, "gifts", rhsGifts), lhsGifts, rhsGifts, (this.gifts!= null), (that.gifts!= null))) {
                return false;
            }
        }
        {
            OfferCodeSellingEntityType lhsSellingEntity;
            lhsSellingEntity = this.getSellingEntity();
            OfferCodeSellingEntityType rhsSellingEntity;
            rhsSellingEntity = that.getSellingEntity();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "sellingEntity", lhsSellingEntity), LocatorUtils.property(thatLocator, "sellingEntity", rhsSellingEntity), lhsSellingEntity, rhsSellingEntity, (this.sellingEntity!= null), (that.sellingEntity!= null))) {
                return false;
            }
        }
        {
            OfferCodeReferringEntityType lhsReferringEntity;
            lhsReferringEntity = this.getReferringEntity();
            OfferCodeReferringEntityType rhsReferringEntity;
            rhsReferringEntity = that.getReferringEntity();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "referringEntity", lhsReferringEntity), LocatorUtils.property(thatLocator, "referringEntity", rhsReferringEntity), lhsReferringEntity, rhsReferringEntity, (this.referringEntity!= null), (that.referringEntity!= null))) {
                return false;
            }
        }
        {
            OfferCodeOrderEntityType lhsOrderEntity;
            lhsOrderEntity = this.getOrderEntity();
            OfferCodeOrderEntityType rhsOrderEntity;
            rhsOrderEntity = that.getOrderEntity();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "orderEntity", lhsOrderEntity), LocatorUtils.property(thatLocator, "orderEntity", rhsOrderEntity), lhsOrderEntity, rhsOrderEntity, (this.orderEntity!= null), (that.orderEntity!= null))) {
                return false;
            }
        }
        {
            String lhsCommisionableEntity;
            lhsCommisionableEntity = this.getCommisionableEntity();
            String rhsCommisionableEntity;
            rhsCommisionableEntity = that.getCommisionableEntity();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "commisionableEntity", lhsCommisionableEntity), LocatorUtils.property(thatLocator, "commisionableEntity", rhsCommisionableEntity), lhsCommisionableEntity, rhsCommisionableEntity, (this.commisionableEntity!= null), (that.commisionableEntity!= null))) {
                return false;
            }
        }
        return true;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(Object object) {
        final EqualsStrategy2 strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode(ObjectLocator locator, HashCodeStrategy2 strategy) {
        int currentHashCode = 1;
        {
            String thePromotionalClaimCode;
            thePromotionalClaimCode = this.getPromotionalClaimCode();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "promotionalClaimCode", thePromotionalClaimCode), currentHashCode, thePromotionalClaimCode, (this.promotionalClaimCode!= null));
        }
        {
            OfferCodeType theOfferCodeInfo;
            theOfferCodeInfo = this.getOfferCodeInfo();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "offerCodeInfo", theOfferCodeInfo), currentHashCode, theOfferCodeInfo, (this.offerCodeInfo!= null));
        }
        {
            GiftsListType theGifts;
            theGifts = this.getGifts();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "gifts", theGifts), currentHashCode, theGifts, (this.gifts!= null));
        }
        {
            OfferCodeSellingEntityType theSellingEntity;
            theSellingEntity = this.getSellingEntity();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "sellingEntity", theSellingEntity), currentHashCode, theSellingEntity, (this.sellingEntity!= null));
        }
        {
            OfferCodeReferringEntityType theReferringEntity;
            theReferringEntity = this.getReferringEntity();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "referringEntity", theReferringEntity), currentHashCode, theReferringEntity, (this.referringEntity!= null));
        }
        {
            OfferCodeOrderEntityType theOrderEntity;
            theOrderEntity = this.getOrderEntity();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "orderEntity", theOrderEntity), currentHashCode, theOrderEntity, (this.orderEntity!= null));
        }
        {
            String theCommisionableEntity;
            theCommisionableEntity = this.getCommisionableEntity();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "commisionableEntity", theCommisionableEntity), currentHashCode, theCommisionableEntity, (this.commisionableEntity!= null));
        }
        return currentHashCode;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode() {
        final HashCodeStrategy2 strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String toString() {
        final ToStringStrategy2 strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        {
            String thePromotionalClaimCode;
            thePromotionalClaimCode = this.getPromotionalClaimCode();
            strategy.appendField(locator, this, "promotionalClaimCode", buffer, thePromotionalClaimCode, (this.promotionalClaimCode!= null));
        }
        {
            OfferCodeType theOfferCodeInfo;
            theOfferCodeInfo = this.getOfferCodeInfo();
            strategy.appendField(locator, this, "offerCodeInfo", buffer, theOfferCodeInfo, (this.offerCodeInfo!= null));
        }
        {
            GiftsListType theGifts;
            theGifts = this.getGifts();
            strategy.appendField(locator, this, "gifts", buffer, theGifts, (this.gifts!= null));
        }
        {
            OfferCodeSellingEntityType theSellingEntity;
            theSellingEntity = this.getSellingEntity();
            strategy.appendField(locator, this, "sellingEntity", buffer, theSellingEntity, (this.sellingEntity!= null));
        }
        {
            OfferCodeReferringEntityType theReferringEntity;
            theReferringEntity = this.getReferringEntity();
            strategy.appendField(locator, this, "referringEntity", buffer, theReferringEntity, (this.referringEntity!= null));
        }
        {
            OfferCodeOrderEntityType theOrderEntity;
            theOrderEntity = this.getOrderEntity();
            strategy.appendField(locator, this, "orderEntity", buffer, theOrderEntity, (this.orderEntity!= null));
        }
        {
            String theCommisionableEntity;
            theCommisionableEntity = this.getCommisionableEntity();
            strategy.appendField(locator, this, "commisionableEntity", buffer, theCommisionableEntity, (this.commisionableEntity!= null));
        }
        return buffer;
    }

}
