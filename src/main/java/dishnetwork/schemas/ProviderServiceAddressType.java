
package dishnetwork.schemas;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

import org.jvnet.jaxb2_commons.lang.Equals2;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy2;
import org.jvnet.jaxb2_commons.lang.HashCode2;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy2;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString2;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy2;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for providerServiceAddressType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="providerServiceAddressType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="houseNumberPrefix" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="houseNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="houseNumberSuffix" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="streetPrefix" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="streetName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="streetType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="streetDirectionalSuffix" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="locationDesignator1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="locationDesignator1Value" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="locationDesignator2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="locationDesignator2Value" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="locationDesignator3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="locationDesignator3Value" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="city" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="state" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="zip" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="callingAddressLocationArea" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="commonLanguageLocationIdentifier" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="addressDetailList" type="{http://www.dishnetwork.com/schema/common/AdditionalAddressInfo/2015_10_08}AdditionalAddressInfoType" minOccurs="0"/&gt;
 *         &lt;element name="attributeList" type="{http://www.dishnetwork.com/schema/common/AttributeList/2015_10_08}AttributeListTypeType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "providerServiceAddressType", propOrder = {
    "houseNumberPrefix",
    "houseNumber",
    "houseNumberSuffix",
    "streetPrefix",
    "streetName",
    "streetType",
    "streetDirectionalSuffix",
    "locationDesignator1",
    "locationDesignator1Value",
    "locationDesignator2",
    "locationDesignator2Value",
    "locationDesignator3",
    "locationDesignator3Value",
    "city",
    "state",
    "zip",
    "callingAddressLocationArea",
    "commonLanguageLocationIdentifier",
    "addressDetailList",
    "attributeList"
})
@Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
public class ProviderServiceAddressType implements Equals2, HashCode2, ToString2
{

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String houseNumberPrefix;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String houseNumber;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String houseNumberSuffix;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String streetPrefix;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String streetName;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String streetType;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String streetDirectionalSuffix;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String locationDesignator1;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String locationDesignator1Value;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String locationDesignator2;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String locationDesignator2Value;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String locationDesignator3;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String locationDesignator3Value;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String city;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String state;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String zip;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String callingAddressLocationArea;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String commonLanguageLocationIdentifier;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected AdditionalAddressInfoType addressDetailList;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected AttributeListTypeType attributeList;

    /**
     * Default no-arg constructor
     *
     */
    public ProviderServiceAddressType() {
        super();
    }

    /**
     * Fully-initialising value constructor
     *
     */
    public ProviderServiceAddressType(final String houseNumberPrefix, final String houseNumber, final String houseNumberSuffix, final String streetPrefix, final String streetName, final String streetType, final String streetDirectionalSuffix, final String locationDesignator1, final String locationDesignator1Value, final String locationDesignator2, final String locationDesignator2Value, final String locationDesignator3, final String locationDesignator3Value, final String city, final String state, final String zip, final String callingAddressLocationArea, final String commonLanguageLocationIdentifier, final AdditionalAddressInfoType addressDetailList, final AttributeListTypeType attributeList) {
        this.houseNumberPrefix = houseNumberPrefix;
        this.houseNumber = houseNumber;
        this.houseNumberSuffix = houseNumberSuffix;
        this.streetPrefix = streetPrefix;
        this.streetName = streetName;
        this.streetType = streetType;
        this.streetDirectionalSuffix = streetDirectionalSuffix;
        this.locationDesignator1 = locationDesignator1;
        this.locationDesignator1Value = locationDesignator1Value;
        this.locationDesignator2 = locationDesignator2;
        this.locationDesignator2Value = locationDesignator2Value;
        this.locationDesignator3 = locationDesignator3;
        this.locationDesignator3Value = locationDesignator3Value;
        this.city = city;
        this.state = state;
        this.zip = zip;
        this.callingAddressLocationArea = callingAddressLocationArea;
        this.commonLanguageLocationIdentifier = commonLanguageLocationIdentifier;
        this.addressDetailList = addressDetailList;
        this.attributeList = attributeList;
    }

    /**
     * Gets the value of the houseNumberPrefix property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getHouseNumberPrefix() {
        return houseNumberPrefix;
    }

    /**
     * Sets the value of the houseNumberPrefix property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setHouseNumberPrefix(String value) {
        this.houseNumberPrefix = value;
    }

    /**
     * Gets the value of the houseNumber property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getHouseNumber() {
        return houseNumber;
    }

    /**
     * Sets the value of the houseNumber property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setHouseNumber(String value) {
        this.houseNumber = value;
    }

    /**
     * Gets the value of the houseNumberSuffix property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getHouseNumberSuffix() {
        return houseNumberSuffix;
    }

    /**
     * Sets the value of the houseNumberSuffix property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setHouseNumberSuffix(String value) {
        this.houseNumberSuffix = value;
    }

    /**
     * Gets the value of the streetPrefix property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getStreetPrefix() {
        return streetPrefix;
    }

    /**
     * Sets the value of the streetPrefix property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setStreetPrefix(String value) {
        this.streetPrefix = value;
    }

    /**
     * Gets the value of the streetName property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getStreetName() {
        return streetName;
    }

    /**
     * Sets the value of the streetName property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setStreetName(String value) {
        this.streetName = value;
    }

    /**
     * Gets the value of the streetType property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getStreetType() {
        return streetType;
    }

    /**
     * Sets the value of the streetType property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setStreetType(String value) {
        this.streetType = value;
    }

    /**
     * Gets the value of the streetDirectionalSuffix property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getStreetDirectionalSuffix() {
        return streetDirectionalSuffix;
    }

    /**
     * Sets the value of the streetDirectionalSuffix property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setStreetDirectionalSuffix(String value) {
        this.streetDirectionalSuffix = value;
    }

    /**
     * Gets the value of the locationDesignator1 property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getLocationDesignator1() {
        return locationDesignator1;
    }

    /**
     * Sets the value of the locationDesignator1 property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setLocationDesignator1(String value) {
        this.locationDesignator1 = value;
    }

    /**
     * Gets the value of the locationDesignator1Value property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getLocationDesignator1Value() {
        return locationDesignator1Value;
    }

    /**
     * Sets the value of the locationDesignator1Value property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setLocationDesignator1Value(String value) {
        this.locationDesignator1Value = value;
    }

    /**
     * Gets the value of the locationDesignator2 property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getLocationDesignator2() {
        return locationDesignator2;
    }

    /**
     * Sets the value of the locationDesignator2 property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setLocationDesignator2(String value) {
        this.locationDesignator2 = value;
    }

    /**
     * Gets the value of the locationDesignator2Value property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getLocationDesignator2Value() {
        return locationDesignator2Value;
    }

    /**
     * Sets the value of the locationDesignator2Value property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setLocationDesignator2Value(String value) {
        this.locationDesignator2Value = value;
    }

    /**
     * Gets the value of the locationDesignator3 property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getLocationDesignator3() {
        return locationDesignator3;
    }

    /**
     * Sets the value of the locationDesignator3 property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setLocationDesignator3(String value) {
        this.locationDesignator3 = value;
    }

    /**
     * Gets the value of the locationDesignator3Value property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getLocationDesignator3Value() {
        return locationDesignator3Value;
    }

    /**
     * Sets the value of the locationDesignator3Value property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setLocationDesignator3Value(String value) {
        this.locationDesignator3Value = value;
    }

    /**
     * Gets the value of the city property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getCity() {
        return city;
    }

    /**
     * Sets the value of the city property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setCity(String value) {
        this.city = value;
    }

    /**
     * Gets the value of the state property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getState() {
        return state;
    }

    /**
     * Sets the value of the state property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setState(String value) {
        this.state = value;
    }

    /**
     * Gets the value of the zip property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getZip() {
        return zip;
    }

    /**
     * Sets the value of the zip property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setZip(String value) {
        this.zip = value;
    }

    /**
     * Gets the value of the callingAddressLocationArea property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getCallingAddressLocationArea() {
        return callingAddressLocationArea;
    }

    /**
     * Sets the value of the callingAddressLocationArea property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setCallingAddressLocationArea(String value) {
        this.callingAddressLocationArea = value;
    }

    /**
     * Gets the value of the commonLanguageLocationIdentifier property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getCommonLanguageLocationIdentifier() {
        return commonLanguageLocationIdentifier;
    }

    /**
     * Sets the value of the commonLanguageLocationIdentifier property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setCommonLanguageLocationIdentifier(String value) {
        this.commonLanguageLocationIdentifier = value;
    }

    /**
     * Gets the value of the addressDetailList property.
     *
     * @return
     *     possible object is
     *     {@link AdditionalAddressInfoType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public AdditionalAddressInfoType getAddressDetailList() {
        return addressDetailList;
    }

    /**
     * Sets the value of the addressDetailList property.
     *
     * @param value
     *     allowed object is
     *     {@link AdditionalAddressInfoType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setAddressDetailList(AdditionalAddressInfoType value) {
        this.addressDetailList = value;
    }

    /**
     * Gets the value of the attributeList property.
     *
     * @return
     *     possible object is
     *     {@link AttributeListTypeType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public AttributeListTypeType getAttributeList() {
        return attributeList;
    }

    /**
     * Sets the value of the attributeList property.
     *
     * @param value
     *     allowed object is
     *     {@link AttributeListTypeType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setAttributeList(AttributeListTypeType value) {
        this.attributeList = value;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy2 strategy) {
        if ((object == null)||(this.getClass()!= object.getClass())) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final ProviderServiceAddressType that = ((ProviderServiceAddressType) object);
        {
            String lhsHouseNumberPrefix;
            lhsHouseNumberPrefix = this.getHouseNumberPrefix();
            String rhsHouseNumberPrefix;
            rhsHouseNumberPrefix = that.getHouseNumberPrefix();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "houseNumberPrefix", lhsHouseNumberPrefix), LocatorUtils.property(thatLocator, "houseNumberPrefix", rhsHouseNumberPrefix), lhsHouseNumberPrefix, rhsHouseNumberPrefix, (this.houseNumberPrefix!= null), (that.houseNumberPrefix!= null))) {
                return false;
            }
        }
        {
            String lhsHouseNumber;
            lhsHouseNumber = this.getHouseNumber();
            String rhsHouseNumber;
            rhsHouseNumber = that.getHouseNumber();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "houseNumber", lhsHouseNumber), LocatorUtils.property(thatLocator, "houseNumber", rhsHouseNumber), lhsHouseNumber, rhsHouseNumber, (this.houseNumber!= null), (that.houseNumber!= null))) {
                return false;
            }
        }
        {
            String lhsHouseNumberSuffix;
            lhsHouseNumberSuffix = this.getHouseNumberSuffix();
            String rhsHouseNumberSuffix;
            rhsHouseNumberSuffix = that.getHouseNumberSuffix();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "houseNumberSuffix", lhsHouseNumberSuffix), LocatorUtils.property(thatLocator, "houseNumberSuffix", rhsHouseNumberSuffix), lhsHouseNumberSuffix, rhsHouseNumberSuffix, (this.houseNumberSuffix!= null), (that.houseNumberSuffix!= null))) {
                return false;
            }
        }
        {
            String lhsStreetPrefix;
            lhsStreetPrefix = this.getStreetPrefix();
            String rhsStreetPrefix;
            rhsStreetPrefix = that.getStreetPrefix();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "streetPrefix", lhsStreetPrefix), LocatorUtils.property(thatLocator, "streetPrefix", rhsStreetPrefix), lhsStreetPrefix, rhsStreetPrefix, (this.streetPrefix!= null), (that.streetPrefix!= null))) {
                return false;
            }
        }
        {
            String lhsStreetName;
            lhsStreetName = this.getStreetName();
            String rhsStreetName;
            rhsStreetName = that.getStreetName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "streetName", lhsStreetName), LocatorUtils.property(thatLocator, "streetName", rhsStreetName), lhsStreetName, rhsStreetName, (this.streetName!= null), (that.streetName!= null))) {
                return false;
            }
        }
        {
            String lhsStreetType;
            lhsStreetType = this.getStreetType();
            String rhsStreetType;
            rhsStreetType = that.getStreetType();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "streetType", lhsStreetType), LocatorUtils.property(thatLocator, "streetType", rhsStreetType), lhsStreetType, rhsStreetType, (this.streetType!= null), (that.streetType!= null))) {
                return false;
            }
        }
        {
            String lhsStreetDirectionalSuffix;
            lhsStreetDirectionalSuffix = this.getStreetDirectionalSuffix();
            String rhsStreetDirectionalSuffix;
            rhsStreetDirectionalSuffix = that.getStreetDirectionalSuffix();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "streetDirectionalSuffix", lhsStreetDirectionalSuffix), LocatorUtils.property(thatLocator, "streetDirectionalSuffix", rhsStreetDirectionalSuffix), lhsStreetDirectionalSuffix, rhsStreetDirectionalSuffix, (this.streetDirectionalSuffix!= null), (that.streetDirectionalSuffix!= null))) {
                return false;
            }
        }
        {
            String lhsLocationDesignator1;
            lhsLocationDesignator1 = this.getLocationDesignator1();
            String rhsLocationDesignator1;
            rhsLocationDesignator1 = that.getLocationDesignator1();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "locationDesignator1", lhsLocationDesignator1), LocatorUtils.property(thatLocator, "locationDesignator1", rhsLocationDesignator1), lhsLocationDesignator1, rhsLocationDesignator1, (this.locationDesignator1 != null), (that.locationDesignator1 != null))) {
                return false;
            }
        }
        {
            String lhsLocationDesignator1Value;
            lhsLocationDesignator1Value = this.getLocationDesignator1Value();
            String rhsLocationDesignator1Value;
            rhsLocationDesignator1Value = that.getLocationDesignator1Value();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "locationDesignator1Value", lhsLocationDesignator1Value), LocatorUtils.property(thatLocator, "locationDesignator1Value", rhsLocationDesignator1Value), lhsLocationDesignator1Value, rhsLocationDesignator1Value, (this.locationDesignator1Value!= null), (that.locationDesignator1Value!= null))) {
                return false;
            }
        }
        {
            String lhsLocationDesignator2;
            lhsLocationDesignator2 = this.getLocationDesignator2();
            String rhsLocationDesignator2;
            rhsLocationDesignator2 = that.getLocationDesignator2();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "locationDesignator2", lhsLocationDesignator2), LocatorUtils.property(thatLocator, "locationDesignator2", rhsLocationDesignator2), lhsLocationDesignator2, rhsLocationDesignator2, (this.locationDesignator2 != null), (that.locationDesignator2 != null))) {
                return false;
            }
        }
        {
            String lhsLocationDesignator2Value;
            lhsLocationDesignator2Value = this.getLocationDesignator2Value();
            String rhsLocationDesignator2Value;
            rhsLocationDesignator2Value = that.getLocationDesignator2Value();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "locationDesignator2Value", lhsLocationDesignator2Value), LocatorUtils.property(thatLocator, "locationDesignator2Value", rhsLocationDesignator2Value), lhsLocationDesignator2Value, rhsLocationDesignator2Value, (this.locationDesignator2Value!= null), (that.locationDesignator2Value!= null))) {
                return false;
            }
        }
        {
            String lhsLocationDesignator3;
            lhsLocationDesignator3 = this.getLocationDesignator3();
            String rhsLocationDesignator3;
            rhsLocationDesignator3 = that.getLocationDesignator3();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "locationDesignator3", lhsLocationDesignator3), LocatorUtils.property(thatLocator, "locationDesignator3", rhsLocationDesignator3), lhsLocationDesignator3, rhsLocationDesignator3, (this.locationDesignator3 != null), (that.locationDesignator3 != null))) {
                return false;
            }
        }
        {
            String lhsLocationDesignator3Value;
            lhsLocationDesignator3Value = this.getLocationDesignator3Value();
            String rhsLocationDesignator3Value;
            rhsLocationDesignator3Value = that.getLocationDesignator3Value();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "locationDesignator3Value", lhsLocationDesignator3Value), LocatorUtils.property(thatLocator, "locationDesignator3Value", rhsLocationDesignator3Value), lhsLocationDesignator3Value, rhsLocationDesignator3Value, (this.locationDesignator3Value!= null), (that.locationDesignator3Value!= null))) {
                return false;
            }
        }
        {
            String lhsCity;
            lhsCity = this.getCity();
            String rhsCity;
            rhsCity = that.getCity();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "city", lhsCity), LocatorUtils.property(thatLocator, "city", rhsCity), lhsCity, rhsCity, (this.city!= null), (that.city!= null))) {
                return false;
            }
        }
        {
            String lhsState;
            lhsState = this.getState();
            String rhsState;
            rhsState = that.getState();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "state", lhsState), LocatorUtils.property(thatLocator, "state", rhsState), lhsState, rhsState, (this.state!= null), (that.state!= null))) {
                return false;
            }
        }
        {
            String lhsZip;
            lhsZip = this.getZip();
            String rhsZip;
            rhsZip = that.getZip();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "zip", lhsZip), LocatorUtils.property(thatLocator, "zip", rhsZip), lhsZip, rhsZip, (this.zip!= null), (that.zip!= null))) {
                return false;
            }
        }
        {
            String lhsCallingAddressLocationArea;
            lhsCallingAddressLocationArea = this.getCallingAddressLocationArea();
            String rhsCallingAddressLocationArea;
            rhsCallingAddressLocationArea = that.getCallingAddressLocationArea();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "callingAddressLocationArea", lhsCallingAddressLocationArea), LocatorUtils.property(thatLocator, "callingAddressLocationArea", rhsCallingAddressLocationArea), lhsCallingAddressLocationArea, rhsCallingAddressLocationArea, (this.callingAddressLocationArea!= null), (that.callingAddressLocationArea!= null))) {
                return false;
            }
        }
        {
            String lhsCommonLanguageLocationIdentifier;
            lhsCommonLanguageLocationIdentifier = this.getCommonLanguageLocationIdentifier();
            String rhsCommonLanguageLocationIdentifier;
            rhsCommonLanguageLocationIdentifier = that.getCommonLanguageLocationIdentifier();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "commonLanguageLocationIdentifier", lhsCommonLanguageLocationIdentifier), LocatorUtils.property(thatLocator, "commonLanguageLocationIdentifier", rhsCommonLanguageLocationIdentifier), lhsCommonLanguageLocationIdentifier, rhsCommonLanguageLocationIdentifier, (this.commonLanguageLocationIdentifier!= null), (that.commonLanguageLocationIdentifier!= null))) {
                return false;
            }
        }
        {
            AdditionalAddressInfoType lhsAddressDetailList;
            lhsAddressDetailList = this.getAddressDetailList();
            AdditionalAddressInfoType rhsAddressDetailList;
            rhsAddressDetailList = that.getAddressDetailList();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "addressDetailList", lhsAddressDetailList), LocatorUtils.property(thatLocator, "addressDetailList", rhsAddressDetailList), lhsAddressDetailList, rhsAddressDetailList, (this.addressDetailList!= null), (that.addressDetailList!= null))) {
                return false;
            }
        }
        {
            AttributeListTypeType lhsAttributeList;
            lhsAttributeList = this.getAttributeList();
            AttributeListTypeType rhsAttributeList;
            rhsAttributeList = that.getAttributeList();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "attributeList", lhsAttributeList), LocatorUtils.property(thatLocator, "attributeList", rhsAttributeList), lhsAttributeList, rhsAttributeList, (this.attributeList!= null), (that.attributeList!= null))) {
                return false;
            }
        }
        return true;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(Object object) {
        final EqualsStrategy2 strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode(ObjectLocator locator, HashCodeStrategy2 strategy) {
        int currentHashCode = 1;
        {
            String theHouseNumberPrefix;
            theHouseNumberPrefix = this.getHouseNumberPrefix();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "houseNumberPrefix", theHouseNumberPrefix), currentHashCode, theHouseNumberPrefix, (this.houseNumberPrefix!= null));
        }
        {
            String theHouseNumber;
            theHouseNumber = this.getHouseNumber();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "houseNumber", theHouseNumber), currentHashCode, theHouseNumber, (this.houseNumber!= null));
        }
        {
            String theHouseNumberSuffix;
            theHouseNumberSuffix = this.getHouseNumberSuffix();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "houseNumberSuffix", theHouseNumberSuffix), currentHashCode, theHouseNumberSuffix, (this.houseNumberSuffix!= null));
        }
        {
            String theStreetPrefix;
            theStreetPrefix = this.getStreetPrefix();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "streetPrefix", theStreetPrefix), currentHashCode, theStreetPrefix, (this.streetPrefix!= null));
        }
        {
            String theStreetName;
            theStreetName = this.getStreetName();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "streetName", theStreetName), currentHashCode, theStreetName, (this.streetName!= null));
        }
        {
            String theStreetType;
            theStreetType = this.getStreetType();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "streetType", theStreetType), currentHashCode, theStreetType, (this.streetType!= null));
        }
        {
            String theStreetDirectionalSuffix;
            theStreetDirectionalSuffix = this.getStreetDirectionalSuffix();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "streetDirectionalSuffix", theStreetDirectionalSuffix), currentHashCode, theStreetDirectionalSuffix, (this.streetDirectionalSuffix!= null));
        }
        {
            String theLocationDesignator1;
            theLocationDesignator1 = this.getLocationDesignator1();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "locationDesignator1", theLocationDesignator1), currentHashCode, theLocationDesignator1, (this.locationDesignator1 != null));
        }
        {
            String theLocationDesignator1Value;
            theLocationDesignator1Value = this.getLocationDesignator1Value();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "locationDesignator1Value", theLocationDesignator1Value), currentHashCode, theLocationDesignator1Value, (this.locationDesignator1Value!= null));
        }
        {
            String theLocationDesignator2;
            theLocationDesignator2 = this.getLocationDesignator2();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "locationDesignator2", theLocationDesignator2), currentHashCode, theLocationDesignator2, (this.locationDesignator2 != null));
        }
        {
            String theLocationDesignator2Value;
            theLocationDesignator2Value = this.getLocationDesignator2Value();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "locationDesignator2Value", theLocationDesignator2Value), currentHashCode, theLocationDesignator2Value, (this.locationDesignator2Value!= null));
        }
        {
            String theLocationDesignator3;
            theLocationDesignator3 = this.getLocationDesignator3();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "locationDesignator3", theLocationDesignator3), currentHashCode, theLocationDesignator3, (this.locationDesignator3 != null));
        }
        {
            String theLocationDesignator3Value;
            theLocationDesignator3Value = this.getLocationDesignator3Value();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "locationDesignator3Value", theLocationDesignator3Value), currentHashCode, theLocationDesignator3Value, (this.locationDesignator3Value!= null));
        }
        {
            String theCity;
            theCity = this.getCity();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "city", theCity), currentHashCode, theCity, (this.city!= null));
        }
        {
            String theState;
            theState = this.getState();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "state", theState), currentHashCode, theState, (this.state!= null));
        }
        {
            String theZip;
            theZip = this.getZip();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "zip", theZip), currentHashCode, theZip, (this.zip!= null));
        }
        {
            String theCallingAddressLocationArea;
            theCallingAddressLocationArea = this.getCallingAddressLocationArea();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "callingAddressLocationArea", theCallingAddressLocationArea), currentHashCode, theCallingAddressLocationArea, (this.callingAddressLocationArea!= null));
        }
        {
            String theCommonLanguageLocationIdentifier;
            theCommonLanguageLocationIdentifier = this.getCommonLanguageLocationIdentifier();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "commonLanguageLocationIdentifier", theCommonLanguageLocationIdentifier), currentHashCode, theCommonLanguageLocationIdentifier, (this.commonLanguageLocationIdentifier!= null));
        }
        {
            AdditionalAddressInfoType theAddressDetailList;
            theAddressDetailList = this.getAddressDetailList();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "addressDetailList", theAddressDetailList), currentHashCode, theAddressDetailList, (this.addressDetailList!= null));
        }
        {
            AttributeListTypeType theAttributeList;
            theAttributeList = this.getAttributeList();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "attributeList", theAttributeList), currentHashCode, theAttributeList, (this.attributeList!= null));
        }
        return currentHashCode;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode() {
        final HashCodeStrategy2 strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String toString() {
        final ToStringStrategy2 strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        {
            String theHouseNumberPrefix;
            theHouseNumberPrefix = this.getHouseNumberPrefix();
            strategy.appendField(locator, this, "houseNumberPrefix", buffer, theHouseNumberPrefix, (this.houseNumberPrefix!= null));
        }
        {
            String theHouseNumber;
            theHouseNumber = this.getHouseNumber();
            strategy.appendField(locator, this, "houseNumber", buffer, theHouseNumber, (this.houseNumber!= null));
        }
        {
            String theHouseNumberSuffix;
            theHouseNumberSuffix = this.getHouseNumberSuffix();
            strategy.appendField(locator, this, "houseNumberSuffix", buffer, theHouseNumberSuffix, (this.houseNumberSuffix!= null));
        }
        {
            String theStreetPrefix;
            theStreetPrefix = this.getStreetPrefix();
            strategy.appendField(locator, this, "streetPrefix", buffer, theStreetPrefix, (this.streetPrefix!= null));
        }
        {
            String theStreetName;
            theStreetName = this.getStreetName();
            strategy.appendField(locator, this, "streetName", buffer, theStreetName, (this.streetName!= null));
        }
        {
            String theStreetType;
            theStreetType = this.getStreetType();
            strategy.appendField(locator, this, "streetType", buffer, theStreetType, (this.streetType!= null));
        }
        {
            String theStreetDirectionalSuffix;
            theStreetDirectionalSuffix = this.getStreetDirectionalSuffix();
            strategy.appendField(locator, this, "streetDirectionalSuffix", buffer, theStreetDirectionalSuffix, (this.streetDirectionalSuffix!= null));
        }
        {
            String theLocationDesignator1;
            theLocationDesignator1 = this.getLocationDesignator1();
            strategy.appendField(locator, this, "locationDesignator1", buffer, theLocationDesignator1, (this.locationDesignator1 != null));
        }
        {
            String theLocationDesignator1Value;
            theLocationDesignator1Value = this.getLocationDesignator1Value();
            strategy.appendField(locator, this, "locationDesignator1Value", buffer, theLocationDesignator1Value, (this.locationDesignator1Value!= null));
        }
        {
            String theLocationDesignator2;
            theLocationDesignator2 = this.getLocationDesignator2();
            strategy.appendField(locator, this, "locationDesignator2", buffer, theLocationDesignator2, (this.locationDesignator2 != null));
        }
        {
            String theLocationDesignator2Value;
            theLocationDesignator2Value = this.getLocationDesignator2Value();
            strategy.appendField(locator, this, "locationDesignator2Value", buffer, theLocationDesignator2Value, (this.locationDesignator2Value!= null));
        }
        {
            String theLocationDesignator3;
            theLocationDesignator3 = this.getLocationDesignator3();
            strategy.appendField(locator, this, "locationDesignator3", buffer, theLocationDesignator3, (this.locationDesignator3 != null));
        }
        {
            String theLocationDesignator3Value;
            theLocationDesignator3Value = this.getLocationDesignator3Value();
            strategy.appendField(locator, this, "locationDesignator3Value", buffer, theLocationDesignator3Value, (this.locationDesignator3Value!= null));
        }
        {
            String theCity;
            theCity = this.getCity();
            strategy.appendField(locator, this, "city", buffer, theCity, (this.city!= null));
        }
        {
            String theState;
            theState = this.getState();
            strategy.appendField(locator, this, "state", buffer, theState, (this.state!= null));
        }
        {
            String theZip;
            theZip = this.getZip();
            strategy.appendField(locator, this, "zip", buffer, theZip, (this.zip!= null));
        }
        {
            String theCallingAddressLocationArea;
            theCallingAddressLocationArea = this.getCallingAddressLocationArea();
            strategy.appendField(locator, this, "callingAddressLocationArea", buffer, theCallingAddressLocationArea, (this.callingAddressLocationArea!= null));
        }
        {
            String theCommonLanguageLocationIdentifier;
            theCommonLanguageLocationIdentifier = this.getCommonLanguageLocationIdentifier();
            strategy.appendField(locator, this, "commonLanguageLocationIdentifier", buffer, theCommonLanguageLocationIdentifier, (this.commonLanguageLocationIdentifier!= null));
        }
        {
            AdditionalAddressInfoType theAddressDetailList;
            theAddressDetailList = this.getAddressDetailList();
            strategy.appendField(locator, this, "addressDetailList", buffer, theAddressDetailList, (this.addressDetailList!= null));
        }
        {
            AttributeListTypeType theAttributeList;
            theAttributeList = this.getAttributeList();
            strategy.appendField(locator, this, "attributeList", buffer, theAttributeList, (this.attributeList!= null));
        }
        return buffer;
    }

}
