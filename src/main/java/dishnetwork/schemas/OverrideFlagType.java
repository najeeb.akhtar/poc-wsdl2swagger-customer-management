
package dishnetwork.schemas;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.jvnet.jaxb2_commons.lang.Equals2;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy2;
import org.jvnet.jaxb2_commons.lang.HashCode2;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy2;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString2;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy2;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * contains the override flag value.
 *
 *
 * <p>Java class for OverrideFlagType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="OverrideFlagType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="creditCardQualOverrideFlag" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="creditScoreQualOverrideFlag" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="formerOverrideFlag" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="oeNumberOverrideFlag" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OverrideFlagType", propOrder = {
    "creditCardQualOverrideFlag",
    "creditScoreQualOverrideFlag",
    "formerOverrideFlag",
    "oeNumberOverrideFlag"
})
@Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
public class OverrideFlagType implements Equals2, HashCode2, ToString2
{

    //@XmlElement(defaultValue = "false")
    @JsonProperty(defaultValue = "false")
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String creditCardQualOverrideFlag;
    //@XmlElement(defaultValue = "false")
    @JsonProperty(defaultValue = "false")
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String creditScoreQualOverrideFlag;
    //@XmlElement(defaultValue = "false")
    @JsonProperty(defaultValue = "false")
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String formerOverrideFlag;
    //@XmlElement(defaultValue = "false")
    @JsonProperty(defaultValue = "false")
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String oeNumberOverrideFlag;

    /**
     * Default no-arg constructor
     *
     */
    public OverrideFlagType() {
        super();
    }

    /**
     * Fully-initialising value constructor
     *
     */
    public OverrideFlagType(final String creditCardQualOverrideFlag, final String creditScoreQualOverrideFlag, final String formerOverrideFlag, final String oeNumberOverrideFlag) {
        this.creditCardQualOverrideFlag = creditCardQualOverrideFlag;
        this.creditScoreQualOverrideFlag = creditScoreQualOverrideFlag;
        this.formerOverrideFlag = formerOverrideFlag;
        this.oeNumberOverrideFlag = oeNumberOverrideFlag;
    }

    /**
     * Gets the value of the creditCardQualOverrideFlag property.
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getCreditCardQualOverrideFlag() {
        return creditCardQualOverrideFlag;
    }

    /**
     * Sets the value of the creditCardQualOverrideFlag property.
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setCreditCardQualOverrideFlag(String value) {
        this.creditCardQualOverrideFlag = value;
    }

    /**
     * Gets the value of the creditScoreQualOverrideFlag property.
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getCreditScoreQualOverrideFlag() {
        return creditScoreQualOverrideFlag;
    }

    /**
     * Sets the value of the creditScoreQualOverrideFlag property.
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setCreditScoreQualOverrideFlag(String value) {
        this.creditScoreQualOverrideFlag = value;
    }

    /**
     * Gets the value of the formerOverrideFlag property.
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getFormerOverrideFlag() {
        return formerOverrideFlag;
    }

    /**
     * Sets the value of the formerOverrideFlag property.
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setFormerOverrideFlag(String value) {
        this.formerOverrideFlag = value;
    }

    /**
     * Gets the value of the oeNumberOverrideFlag property.
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getOeNumberOverrideFlag() {
        return oeNumberOverrideFlag;
    }

    /**
     * Sets the value of the oeNumberOverrideFlag property.
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setOeNumberOverrideFlag(String value) {
        this.oeNumberOverrideFlag = value;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy2 strategy) {
        if ((object == null)||(this.getClass()!= object.getClass())) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final OverrideFlagType that = ((OverrideFlagType) object);
        {
            String lhsCreditCardQualOverrideFlag;
            lhsCreditCardQualOverrideFlag = this.getCreditCardQualOverrideFlag();
            String rhsCreditCardQualOverrideFlag;
            rhsCreditCardQualOverrideFlag = that.getCreditCardQualOverrideFlag();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "creditCardQualOverrideFlag", lhsCreditCardQualOverrideFlag), LocatorUtils.property(thatLocator, "creditCardQualOverrideFlag", rhsCreditCardQualOverrideFlag), lhsCreditCardQualOverrideFlag, rhsCreditCardQualOverrideFlag, true, true)) {
                return false;
            }
        }
        {
            String lhsCreditScoreQualOverrideFlag;
            lhsCreditScoreQualOverrideFlag = this.getCreditScoreQualOverrideFlag();
            String rhsCreditScoreQualOverrideFlag;
            rhsCreditScoreQualOverrideFlag = that.getCreditScoreQualOverrideFlag();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "creditScoreQualOverrideFlag", lhsCreditScoreQualOverrideFlag), LocatorUtils.property(thatLocator, "creditScoreQualOverrideFlag", rhsCreditScoreQualOverrideFlag), lhsCreditScoreQualOverrideFlag, rhsCreditScoreQualOverrideFlag, true, true)) {
                return false;
            }
        }
        {
            String lhsFormerOverrideFlag;
            lhsFormerOverrideFlag = this.getFormerOverrideFlag();
            String rhsFormerOverrideFlag;
            rhsFormerOverrideFlag = that.getFormerOverrideFlag();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "formerOverrideFlag", lhsFormerOverrideFlag), LocatorUtils.property(thatLocator, "formerOverrideFlag", rhsFormerOverrideFlag), lhsFormerOverrideFlag, rhsFormerOverrideFlag, true, true)) {
                return false;
            }
        }
        {
            String lhsOeNumberOverrideFlag;
            lhsOeNumberOverrideFlag = this.getOeNumberOverrideFlag();
            String rhsOeNumberOverrideFlag;
            rhsOeNumberOverrideFlag = that.getOeNumberOverrideFlag();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "oeNumberOverrideFlag", lhsOeNumberOverrideFlag), LocatorUtils.property(thatLocator, "oeNumberOverrideFlag", rhsOeNumberOverrideFlag), lhsOeNumberOverrideFlag, rhsOeNumberOverrideFlag, true, true)) {
                return false;
            }
        }
        return true;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(Object object) {
        final EqualsStrategy2 strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode(ObjectLocator locator, HashCodeStrategy2 strategy) {
        int currentHashCode = 1;
        {
            String theCreditCardQualOverrideFlag;
            theCreditCardQualOverrideFlag = this.getCreditCardQualOverrideFlag();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "creditCardQualOverrideFlag", theCreditCardQualOverrideFlag), currentHashCode, theCreditCardQualOverrideFlag, true);
        }
        {
            String theCreditScoreQualOverrideFlag;
            theCreditScoreQualOverrideFlag = this.getCreditScoreQualOverrideFlag();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "creditScoreQualOverrideFlag", theCreditScoreQualOverrideFlag), currentHashCode, theCreditScoreQualOverrideFlag, true);
        }
        {
            String theFormerOverrideFlag;
            theFormerOverrideFlag = this.getFormerOverrideFlag();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "formerOverrideFlag", theFormerOverrideFlag), currentHashCode, theFormerOverrideFlag, true);
        }
        {
            String theOeNumberOverrideFlag;
            theOeNumberOverrideFlag = this.getOeNumberOverrideFlag();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "oeNumberOverrideFlag", theOeNumberOverrideFlag), currentHashCode, theOeNumberOverrideFlag, true);
        }
        return currentHashCode;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode() {
        final HashCodeStrategy2 strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String toString() {
        final ToStringStrategy2 strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        {
            String theCreditCardQualOverrideFlag;
            theCreditCardQualOverrideFlag = this.getCreditCardQualOverrideFlag();
            strategy.appendField(locator, this, "creditCardQualOverrideFlag", buffer, theCreditCardQualOverrideFlag, true);
        }
        {
            String theCreditScoreQualOverrideFlag;
            theCreditScoreQualOverrideFlag = this.getCreditScoreQualOverrideFlag();
            strategy.appendField(locator, this, "creditScoreQualOverrideFlag", buffer, theCreditScoreQualOverrideFlag, true);
        }
        {
            String theFormerOverrideFlag;
            theFormerOverrideFlag = this.getFormerOverrideFlag();
            strategy.appendField(locator, this, "formerOverrideFlag", buffer, theFormerOverrideFlag, true);
        }
        {
            String theOeNumberOverrideFlag;
            theOeNumberOverrideFlag = this.getOeNumberOverrideFlag();
            strategy.appendField(locator, this, "oeNumberOverrideFlag", buffer, theOeNumberOverrideFlag, true);
        }
        return buffer;
    }

}
