package dishnetwork.schemas;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class RaHeader {
    protected String accountId;
    protected String phoneNumber;
    protected String firstName;
    protected String lastName;
    protected String orderSource;
    protected String type;
    protected String createdBy;
    protected String orderId;
    protected String holdRA;
    protected AddressType address;
    protected AddressType shipAddress;
    protected List<RaLine> raLineList;
}
