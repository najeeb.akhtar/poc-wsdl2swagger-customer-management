package dishnetwork.schemas;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.jvnet.jaxb2_commons.lang.*;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="expectedEquipment" type="{http://www.dishnetwork.com/schema/Equipment/ExpectedEquipmentInfoList/2014_06_13}EquipmentType" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "expectedEquipment"
})
@Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
public class ExpectedEquipmentList implements Equals2, HashCode2, ToString2 {

    //@XmlElement(required = true)
    @JsonProperty(required = true)
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected List<EquipmentType> expectedEquipment;

    /**
     * Default no-arg constructor
     */
    public ExpectedEquipmentList() {
        super();
    }

    /**
     * Fully-initialising value constructor
     */
    public ExpectedEquipmentList(final List<EquipmentType> expectedEquipment) {
        this.expectedEquipment = expectedEquipment;
    }

    /**
     * Gets the value of the expectedEquipment property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the expectedEquipment property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getExpectedEquipment().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EquipmentType }
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public List<EquipmentType> getExpectedEquipment() {
        if (expectedEquipment == null) {
            expectedEquipment = new ArrayList<EquipmentType>();
        }
        return this.expectedEquipment;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy2 strategy) {
        if ((object == null) || (this.getClass() != object.getClass())) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final ExpectedEquipmentList that = ((ExpectedEquipmentList) object);
        {
            List<EquipmentType> lhsExpectedEquipment;
            lhsExpectedEquipment = (((this.expectedEquipment != null) && (!this.expectedEquipment.isEmpty())) ? this.getExpectedEquipment() : null);
            List<EquipmentType> rhsExpectedEquipment;
            rhsExpectedEquipment = (((that.expectedEquipment != null) && (!that.expectedEquipment.isEmpty())) ? that.getExpectedEquipment() : null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "expectedEquipment", lhsExpectedEquipment), LocatorUtils.property(thatLocator, "expectedEquipment", rhsExpectedEquipment), lhsExpectedEquipment, rhsExpectedEquipment, ((this.expectedEquipment != null) && (!this.expectedEquipment.isEmpty())), ((that.expectedEquipment != null) && (!that.expectedEquipment.isEmpty())))) {
                return false;
            }
        }
        return true;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(Object object) {
        final EqualsStrategy2 strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode(ObjectLocator locator, HashCodeStrategy2 strategy) {
        int currentHashCode = 1;
        {
            List<EquipmentType> theExpectedEquipment;
            theExpectedEquipment = (((this.expectedEquipment != null) && (!this.expectedEquipment.isEmpty())) ? this.getExpectedEquipment() : null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "expectedEquipment", theExpectedEquipment), currentHashCode, theExpectedEquipment, ((this.expectedEquipment != null) && (!this.expectedEquipment.isEmpty())));
        }
        return currentHashCode;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode() {
        final HashCodeStrategy2 strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String toString() {
        final ToStringStrategy2 strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        {
            List<EquipmentType> theExpectedEquipment;
            theExpectedEquipment = (((this.expectedEquipment != null) && (!this.expectedEquipment.isEmpty())) ? this.getExpectedEquipment() : null);
            strategy.appendField(locator, this, "expectedEquipment", buffer, theExpectedEquipment, ((this.expectedEquipment != null) && (!this.expectedEquipment.isEmpty())));
        }
        return buffer;
    }

}
