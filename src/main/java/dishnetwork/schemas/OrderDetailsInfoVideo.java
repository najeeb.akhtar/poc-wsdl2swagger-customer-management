package dishnetwork.schemas;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class OrderDetailsInfoVideo {
    protected String tvCount;
    protected EquipmentSolutionType equipmentSolution;
    protected List<TvInfo> tvInfo;
    protected List<CustomizedSolution> customizedSolution;
    protected List<Plan> plan;
    protected List<ServCmpt> servCmpt;
    protected List<OfferCmpt> offerCmpt;
    protected CustomFieldListType customFieldList;
}
