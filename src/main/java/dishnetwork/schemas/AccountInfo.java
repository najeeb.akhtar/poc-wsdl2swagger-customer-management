package dishnetwork.schemas;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class AccountInfo {
    protected AccountIdType accountId;
    protected BillingAccountNumberType billingAccountNumber;
    protected String hardCopyStatementFlag;
    protected String accountUpdatedFlag;
    protected String statementLanguage;
    protected String estViewOccupancy;
    protected String fireOccupancy;
    protected TelephoneNumberListType accountContactList;
    protected BillTo billTo;
    protected PartnerAccountInfoType partnerAccountInfo;
    protected String dunningGroup;
    protected String dunningGroupPriority;
    protected CommAcctInfo commercialAccountInfo;
    protected String mduPropertyId;
    protected String taxGroup;
    protected String frequency;
    protected String vipCode;
    protected String ppvCreditLimit;
    protected List<Note> noteList;
    protected CustomFieldListType customFieldList;
}
