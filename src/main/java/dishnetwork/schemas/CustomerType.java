
package dishnetwork.schemas;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.jvnet.jaxb2_commons.lang.Equals2;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy2;
import org.jvnet.jaxb2_commons.lang.HashCode2;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy2;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString2;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy2;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 *  Object to present Customer input
 *
 * <p>Java class for CustomerType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="CustomerType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="customerId" type="{http://www.dishnetwork.com/schema/Customer/CustomerId/2011_04_01}CustomerIdType" minOccurs="0"/&gt;
 *         &lt;element name="customerName" type="{http://www.dishnetwork.com/schema/Party/IndividualName/2010_12_12}IndividualNameType" minOccurs="0"/&gt;
 *         &lt;element name="type" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="emailContact" type="{http://www.dishnetwork.com/schema/CustomerManagement/EmailContact/2011_04_01}EmailContactType" minOccurs="0"/&gt;
 *         &lt;element name="customerContactList" type="{http://www.dishnetwork.com/schema/Contact/TelephoneNumberList/2014_10_22}TelephoneNumberListType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CustomerType", propOrder = {
    "customerId",
    "customerName",
    "type",
    "emailContact",
    "customerContactList"
})
@Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
public class CustomerType implements Equals2, HashCode2, ToString2
{

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected CustomerIdType customerId;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected IndividualNameType customerName;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String type;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected EmailContactType emailContact;
    //@XmlElement(required = true)
    @JsonProperty(required = true)
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected TelephoneNumberListType customerContactList;

    /**
     * Default no-arg constructor
     *
     */
    public CustomerType() {
        super();
    }

    /**
     * Fully-initialising value constructor
     *
     */
    public CustomerType(final CustomerIdType customerId, final IndividualNameType customerName, final String type, final EmailContactType emailContact, final TelephoneNumberListType customerContactList) {
        this.customerId = customerId;
        this.customerName = customerName;
        this.type = type;
        this.emailContact = emailContact;
        this.customerContactList = customerContactList;
    }

    /**
     * Gets the value of the customerId property.
     *
     * @return
     *     possible object is
     *     {@link CustomerIdType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public CustomerIdType getCustomerId() {
        return customerId;
    }

    /**
     * Sets the value of the customerId property.
     *
     * @param value
     *     allowed object is
     *     {@link CustomerIdType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setCustomerId(CustomerIdType value) {
        this.customerId = value;
    }

    /**
     * Gets the value of the customerName property.
     *
     * @return
     *     possible object is
     *     {@link IndividualNameType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public IndividualNameType getCustomerName() {
        return customerName;
    }

    /**
     * Sets the value of the customerName property.
     *
     * @param value
     *     allowed object is
     *     {@link IndividualNameType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setCustomerName(IndividualNameType value) {
        this.customerName = value;
    }

    /**
     * Gets the value of the type property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setType(String value) {
        this.type = value;
    }

    /**
     * Gets the value of the emailContact property.
     *
     * @return
     *     possible object is
     *     {@link EmailContactType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public EmailContactType getEmailContact() {
        return emailContact;
    }

    /**
     * Sets the value of the emailContact property.
     *
     * @param value
     *     allowed object is
     *     {@link EmailContactType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setEmailContact(EmailContactType value) {
        this.emailContact = value;
    }

    /**
     * Gets the value of the customerContactList property.
     *
     * @return
     *     possible object is
     *     {@link TelephoneNumberListType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public TelephoneNumberListType getCustomerContactList() {
        return customerContactList;
    }

    /**
     * Sets the value of the customerContactList property.
     *
     * @param value
     *     allowed object is
     *     {@link TelephoneNumberListType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setCustomerContactList(TelephoneNumberListType value) {
        this.customerContactList = value;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy2 strategy) {
        if ((object == null)||(this.getClass()!= object.getClass())) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final CustomerType that = ((CustomerType) object);
        {
            CustomerIdType lhsCustomerId;
            lhsCustomerId = this.getCustomerId();
            CustomerIdType rhsCustomerId;
            rhsCustomerId = that.getCustomerId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "customerId", lhsCustomerId), LocatorUtils.property(thatLocator, "customerId", rhsCustomerId), lhsCustomerId, rhsCustomerId, (this.customerId!= null), (that.customerId!= null))) {
                return false;
            }
        }
        {
            IndividualNameType lhsCustomerName;
            lhsCustomerName = this.getCustomerName();
            IndividualNameType rhsCustomerName;
            rhsCustomerName = that.getCustomerName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "customerName", lhsCustomerName), LocatorUtils.property(thatLocator, "customerName", rhsCustomerName), lhsCustomerName, rhsCustomerName, (this.customerName!= null), (that.customerName!= null))) {
                return false;
            }
        }
        {
            String lhsType;
            lhsType = this.getType();
            String rhsType;
            rhsType = that.getType();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "type", lhsType), LocatorUtils.property(thatLocator, "type", rhsType), lhsType, rhsType, (this.type!= null), (that.type!= null))) {
                return false;
            }
        }
        {
            EmailContactType lhsEmailContact;
            lhsEmailContact = this.getEmailContact();
            EmailContactType rhsEmailContact;
            rhsEmailContact = that.getEmailContact();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "emailContact", lhsEmailContact), LocatorUtils.property(thatLocator, "emailContact", rhsEmailContact), lhsEmailContact, rhsEmailContact, (this.emailContact!= null), (that.emailContact!= null))) {
                return false;
            }
        }
        {
            TelephoneNumberListType lhsCustomerContactList;
            lhsCustomerContactList = this.getCustomerContactList();
            TelephoneNumberListType rhsCustomerContactList;
            rhsCustomerContactList = that.getCustomerContactList();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "customerContactList", lhsCustomerContactList), LocatorUtils.property(thatLocator, "customerContactList", rhsCustomerContactList), lhsCustomerContactList, rhsCustomerContactList, (this.customerContactList!= null), (that.customerContactList!= null))) {
                return false;
            }
        }
        return true;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(Object object) {
        final EqualsStrategy2 strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode(ObjectLocator locator, HashCodeStrategy2 strategy) {
        int currentHashCode = 1;
        {
            CustomerIdType theCustomerId;
            theCustomerId = this.getCustomerId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "customerId", theCustomerId), currentHashCode, theCustomerId, (this.customerId!= null));
        }
        {
            IndividualNameType theCustomerName;
            theCustomerName = this.getCustomerName();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "customerName", theCustomerName), currentHashCode, theCustomerName, (this.customerName!= null));
        }
        {
            String theType;
            theType = this.getType();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "type", theType), currentHashCode, theType, (this.type!= null));
        }
        {
            EmailContactType theEmailContact;
            theEmailContact = this.getEmailContact();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "emailContact", theEmailContact), currentHashCode, theEmailContact, (this.emailContact!= null));
        }
        {
            TelephoneNumberListType theCustomerContactList;
            theCustomerContactList = this.getCustomerContactList();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "customerContactList", theCustomerContactList), currentHashCode, theCustomerContactList, (this.customerContactList!= null));
        }
        return currentHashCode;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode() {
        final HashCodeStrategy2 strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String toString() {
        final ToStringStrategy2 strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        {
            CustomerIdType theCustomerId;
            theCustomerId = this.getCustomerId();
            strategy.appendField(locator, this, "customerId", buffer, theCustomerId, (this.customerId!= null));
        }
        {
            IndividualNameType theCustomerName;
            theCustomerName = this.getCustomerName();
            strategy.appendField(locator, this, "customerName", buffer, theCustomerName, (this.customerName!= null));
        }
        {
            String theType;
            theType = this.getType();
            strategy.appendField(locator, this, "type", buffer, theType, (this.type!= null));
        }
        {
            EmailContactType theEmailContact;
            theEmailContact = this.getEmailContact();
            strategy.appendField(locator, this, "emailContact", buffer, theEmailContact, (this.emailContact!= null));
        }
        {
            TelephoneNumberListType theCustomerContactList;
            theCustomerContactList = this.getCustomerContactList();
            strategy.appendField(locator, this, "customerContactList", buffer, theCustomerContactList, (this.customerContactList!= null));
        }
        return buffer;
    }

}
