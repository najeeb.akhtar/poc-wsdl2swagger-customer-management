
package dishnetwork.schemas;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.jvnet.jaxb2_commons.lang.Equals2;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy2;
import org.jvnet.jaxb2_commons.lang.HashCode2;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy2;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString2;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy2;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * contains the receiver information.
 *
 * <p>Java class for ReceiverSolutionListType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="ReceiverSolutionListType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="receiverSolution" type="{http://www.dishnetwork.com/xsd/Equipment/ReceiverSolution/2011_04_01}ReceiverSolutionType" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReceiverSolutionListType", propOrder = {
    "receiverSolution"
})
@Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
public class ReceiverSolutionListType implements Equals2, HashCode2, ToString2
{

    //@XmlElement(required = true)
    @JsonProperty(required = true)
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected List<ReceiverSolutionType> receiverSolution;

    /**
     * Default no-arg constructor
     *
     */
    public ReceiverSolutionListType() {
        super();
    }

    /**
     * Fully-initialising value constructor
     *
     */
    public ReceiverSolutionListType(final List<ReceiverSolutionType> receiverSolution) {
        this.receiverSolution = receiverSolution;
    }

    /**
     * Gets the value of the receiverSolution property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the receiverSolution property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReceiverSolution().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReceiverSolutionType }
     *
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public List<ReceiverSolutionType> getReceiverSolution() {
        if (receiverSolution == null) {
            receiverSolution = new ArrayList<ReceiverSolutionType>();
        }
        return this.receiverSolution;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy2 strategy) {
        if ((object == null)||(this.getClass()!= object.getClass())) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final ReceiverSolutionListType that = ((ReceiverSolutionListType) object);
        {
            List<ReceiverSolutionType> lhsReceiverSolution;
            lhsReceiverSolution = (((this.receiverSolution!= null)&&(!this.receiverSolution.isEmpty()))?this.getReceiverSolution():null);
            List<ReceiverSolutionType> rhsReceiverSolution;
            rhsReceiverSolution = (((that.receiverSolution!= null)&&(!that.receiverSolution.isEmpty()))?that.getReceiverSolution():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "receiverSolution", lhsReceiverSolution), LocatorUtils.property(thatLocator, "receiverSolution", rhsReceiverSolution), lhsReceiverSolution, rhsReceiverSolution, ((this.receiverSolution!= null)&&(!this.receiverSolution.isEmpty())), ((that.receiverSolution!= null)&&(!that.receiverSolution.isEmpty())))) {
                return false;
            }
        }
        return true;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(Object object) {
        final EqualsStrategy2 strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode(ObjectLocator locator, HashCodeStrategy2 strategy) {
        int currentHashCode = 1;
        {
            List<ReceiverSolutionType> theReceiverSolution;
            theReceiverSolution = (((this.receiverSolution!= null)&&(!this.receiverSolution.isEmpty()))?this.getReceiverSolution():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "receiverSolution", theReceiverSolution), currentHashCode, theReceiverSolution, ((this.receiverSolution!= null)&&(!this.receiverSolution.isEmpty())));
        }
        return currentHashCode;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode() {
        final HashCodeStrategy2 strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String toString() {
        final ToStringStrategy2 strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        {
            List<ReceiverSolutionType> theReceiverSolution;
            theReceiverSolution = (((this.receiverSolution!= null)&&(!this.receiverSolution.isEmpty()))?this.getReceiverSolution():null);
            strategy.appendField(locator, this, "receiverSolution", buffer, theReceiverSolution, ((this.receiverSolution!= null)&&(!this.receiverSolution.isEmpty())));
        }
        return buffer;
    }

}
