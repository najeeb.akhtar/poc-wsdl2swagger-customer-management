
package dishnetwork.schemas;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

import org.jvnet.jaxb2_commons.lang.Equals2;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy2;
import org.jvnet.jaxb2_commons.lang.HashCode2;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy2;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString2;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy2;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * Contains the details of partner account.
 *
 * <p>Java class for PartnerAccountInfoType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="PartnerAccountInfoType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="partnerAccountId" type="{http://www.dishnetwork.com/schema/CustomerManagement/PartnerAccountId/2011_04_01}PartnerAccountIdType" minOccurs="0"/&gt;
 *         &lt;element name="cycleDay" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PartnerAccountInfoType", propOrder = {
    "partnerAccountId",
    "cycleDay"
})
@Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
public class PartnerAccountInfoType implements Equals2, HashCode2, ToString2
{

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected PartnerAccountIdType partnerAccountId;
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    protected String cycleDay;

    /**
     * Default no-arg constructor
     *
     */
    public PartnerAccountInfoType() {
        super();
    }

    /**
     * Fully-initialising value constructor
     *
     */
    public PartnerAccountInfoType(final PartnerAccountIdType partnerAccountId, final String cycleDay) {
        this.partnerAccountId = partnerAccountId;
        this.cycleDay = cycleDay;
    }

    /**
     * Gets the value of the partnerAccountId property.
     *
     * @return
     *     possible object is
     *     {@link PartnerAccountIdType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public PartnerAccountIdType getPartnerAccountId() {
        return partnerAccountId;
    }

    /**
     * Sets the value of the partnerAccountId property.
     *
     * @param value
     *     allowed object is
     *     {@link PartnerAccountIdType }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setPartnerAccountId(PartnerAccountIdType value) {
        this.partnerAccountId = value;
    }

    /**
     * Gets the value of the cycleDay property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String getCycleDay() {
        return cycleDay;
    }

    /**
     * Sets the value of the cycleDay property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public void setCycleDay(String value) {
        this.cycleDay = value;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy2 strategy) {
        if ((object == null)||(this.getClass()!= object.getClass())) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final PartnerAccountInfoType that = ((PartnerAccountInfoType) object);
        {
            PartnerAccountIdType lhsPartnerAccountId;
            lhsPartnerAccountId = this.getPartnerAccountId();
            PartnerAccountIdType rhsPartnerAccountId;
            rhsPartnerAccountId = that.getPartnerAccountId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "partnerAccountId", lhsPartnerAccountId), LocatorUtils.property(thatLocator, "partnerAccountId", rhsPartnerAccountId), lhsPartnerAccountId, rhsPartnerAccountId, (this.partnerAccountId!= null), (that.partnerAccountId!= null))) {
                return false;
            }
        }
        {
            String lhsCycleDay;
            lhsCycleDay = this.getCycleDay();
            String rhsCycleDay;
            rhsCycleDay = that.getCycleDay();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "cycleDay", lhsCycleDay), LocatorUtils.property(thatLocator, "cycleDay", rhsCycleDay), lhsCycleDay, rhsCycleDay, (this.cycleDay!= null), (that.cycleDay!= null))) {
                return false;
            }
        }
        return true;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public boolean equals(Object object) {
        final EqualsStrategy2 strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode(ObjectLocator locator, HashCodeStrategy2 strategy) {
        int currentHashCode = 1;
        {
            PartnerAccountIdType thePartnerAccountId;
            thePartnerAccountId = this.getPartnerAccountId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "partnerAccountId", thePartnerAccountId), currentHashCode, thePartnerAccountId, (this.partnerAccountId!= null));
        }
        {
            String theCycleDay;
            theCycleDay = this.getCycleDay();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "cycleDay", theCycleDay), currentHashCode, theCycleDay, (this.cycleDay!= null));
        }
        return currentHashCode;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public int hashCode() {
        final HashCodeStrategy2 strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public String toString() {
        final ToStringStrategy2 strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    @Generated(value = "com.sun.tools.xjc.Driver", comments = "JAXB RI v2.3.5", date = "2022-07-06T08:08:55-06:00")
    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy2 strategy) {
        {
            PartnerAccountIdType thePartnerAccountId;
            thePartnerAccountId = this.getPartnerAccountId();
            strategy.appendField(locator, this, "partnerAccountId", buffer, thePartnerAccountId, (this.partnerAccountId!= null));
        }
        {
            String theCycleDay;
            theCycleDay = this.getCycleDay();
            strategy.appendField(locator, this, "cycleDay", buffer, theCycleDay, (this.cycleDay!= null));
        }
        return buffer;
    }

}
