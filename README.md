# poc-wsdl2swagger-customer-management

## Steps:

### 1. Configure wsdl to generate java classes

* Update build.gradle, task name is wsdl2java.
* Download your wsdl into 'src/main/resources/wsdl' location.

### 2. Generate java classes

Run wsdl2java or rebuild your project.

### 3. Make sure All model class name should be unique

In order to make sure unique class name, move all models class definition into one package: dishnetwork.schemas.

So that it will not allow two class with same name.

### 4. Replace @XmlElement
```
After generating the java classes:
replace all @XmlElement(name = "OrderEntry-Orders")
with @JsonProperty("OrderEntry-Orders")
or @JsonProperty(value = "OrderEntry-Orders")
```

### 4.a Change Datatype
Migrate below types as String.

All primitive data type(int, byte, short, long, float, double, boolean, and char)
java.math.BigDecimal
java.lang.Boolean
javax.xml.datatype.XMLGregorianCalendar((example = "2021-08-04T21:32:55.842Z")) - @ApiModelProperty(example = "2021-08-04T21:32:55.842Z")
java.lang.Integer
java.math.BigInteger
etc.

### 5. Create Controller

Create controller class under folder 'src/main/java/com/dish/es/wsUtil/wsdl2swagger/controller'

### 6. Run your application

Run as boot application

### 7. open swagger-ui

http://localhost:8080/swagger-ui.html

### 8. Verify and update your interface

Verify your swagger-ui and update if found any discrepancy

### 9. Open api-docs and save api-docs.json

http://localhost:8080/v2/api-docs?group=wsdl2swaggerGroup

You can see this link in your swagger-ui.

It will give you a json, save it.

### 10. Load into swagger editor

* Open: https://editor.swagger.io/
* Go to menu: 'File > Import file', 
* Brows your 'api-docs.json'.

### 11. Update your swagger discrepancy and review swagger-ui

#### 11.1. Update basePath tag

replace

```yaml
basePath: /
```

with

```yaml
basePath: "/customer-management"
```

#### 11.2. Add schemes tag

Add schemes tag below basePath tag:

```yaml
schemes:
  - "http"
```

#### 11.3. Remove xml tags from schema definitions

It looks like:

```yaml
    xml:
      name: BusinessShipping
      attribute: false
      wrapped: false
```

#### 11.4. Remove duplicate fields automatically generated

Remove duplicate fields from definition(due to bug in java swagger api):

If any field's name start with more than one consecutive capital letters

like:

```json
{
  "RMPEmailAddress": "string",
  "CESOENumber": "string"
}
```

So swagger yml definitions will have duplicate fields(name will be in small case letter).

like:

```yaml
definitions:
  Account:
    type: object
    properties:
      CESEmailAddress:
        type: string
      CESOENumber:
        type: string
      cesemailAddress:
        type: string
      cesoenumber:
        type: string
    title: Account
```

### 12. Save swagger-ui as yaml and upload into svn

* Go to menu: 'File > Save as Yaml'
* Rename file as 'ServiceContract_CustomerManagement_v1.yaml'
* Upload 'ServiceContract_CustomerManagement_v1.yaml' file in svn at location: http://it-soa.dish.com/svn/soa/final/CustomerManagement/REST/
